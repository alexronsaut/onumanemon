---
title: États du Monde. Lignes
date: 2024-08-18 14:47
sous_titre: ""
date_document: Septembre 2016
tags:
    - document
    - texte
    - Cosmologie Onuma Nemon
fichiers:
    - "/etats-du-monde-lignes.pdf"
---

 Après le premier gros volume d’un millier de pages de la Cosmologie paru chez Verticales en 2004, classé par _Quartiers_ et comprenant ses _Étoilements_ graphiques, celui-ci, aussi conséquent, a été publié par Mettray en septembre 2016. Il est classé par _Lignes_ de personnages, et comprend également ses _Étoilements_ graphiques sous forme de vignettes à coller, comme dans les anciens albums de chocolat. L’idée venait de Tristram, pour la publication de OR, en 2000, qui n’a jamais eu lieu.
