---
title: "Idéogramme X, Y"
date: 2011-12-26 18:53
fichiers: 
  - Ideogramme_X_Y._Maison_Lulu.jpg
sous_titre: "Maison Lulu. Idéogrammes dans la Neige" 
date_document: "1974" 
tags:
  - document
  - OGR
  - photographie
  - edition
---

_Maison Lulu_ est ce recueil de 1974 construit autour de l’Asile de Maître Jean (où traînent entre autres Tatie Marguerite, Fernande du Phœnyx et l’ignoble infirmière Ovarine…), et du Cimetière de la Chartreuse, entrée discrète du Pays des Morts. Le sergent Newton et le lieutenant Copernic sont là avec leur Thunderbird orangée, qui mènent l’enquête à propos des idéogrammes fourchus buissonnant à travers une grande partie du monde. Le premier chapitre en a été jadis publié dans la revue [Le Grand Os n°2](http://legrandos.blogspot.com/p/catalogue-edition.html).

NDLR