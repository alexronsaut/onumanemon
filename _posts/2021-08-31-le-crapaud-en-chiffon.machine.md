---
date: 2021-08-31 2:21
title: Le Crapaud en chiffon. (Machine)
sous_titre: ''
date_document: "1984"
fichiers:
- "/le-crapaud-en-chiffon-en-volume-2.jpg"
tags:
- document
- sculpture
- OGR

---
Crapaud mort, colle, cire à cacheter, chiffon, plaque de gravure en zinc, planche xylographique en sapin. 30 x 20 x 30cm.