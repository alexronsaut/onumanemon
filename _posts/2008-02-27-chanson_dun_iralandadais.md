---
title: "Chanson d’un Iralandadais"
date: 2008-02-27 22:39
fichiers: 
  - Chanson_dun_Iralandadais.pdf
sous_titre: "Les Grands Ancêtres. Ligne de Mac Carthy. Printemps" 
date_document: "1978" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_**Chanson d’un Iralandadais pinté dans les rues de Limer’ Hic !**_  
Qu’est-ce que le Daily Mail disait du Home Rul ?  
Accordé à l’Irlande ou non  
Et de la Grêce, ce tas de ruines  
Avec Aristote  
 ce loubard comme serviteur d’hôtel  
En gilet crasseux qui nous réveille pour partir à Olympie.