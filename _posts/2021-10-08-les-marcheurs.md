---
date: 2021-10-08 3:00
title: Les Marcheurs
sous_titre: Prologue de Histoire Deux
date_document: "1978"
fichiers:
- "/les-marcheurs.pdf"
tags:
- texte
- OGR
- document

---
LES MARCHEURS

À l’Omphalos les Anges viennent sans maquillage  
Et se dissolvent au bord des quais  
Avec des fleurs dans les cheveux,  
Vers la Montagne du Temple, toit de mains réunies ;  
Le Temple en carton peint,  
Le Palais-Gallien,  
Le cortex de cristal, le total de colonnes.

Les Séraphins de Delphes tournent la mie de pain entre leurs doigts  
Et la luplissent de lumière.  
L’Archange Saint-Michel, cuivre qui résonne, hanche souple,  
Vient avec son Bronica  
Pour prendre la première photo de la phemme du Dieu de Delphes

Et du python qu’elle y a tué :  
“Hatu Berato Niktu !”  
De bon commencement je n’en connais guère  
(“C’était la guerre.”).

Présent : hypotyposes et orichalque !  
Aux Chérubins inflammatoires l’éblouissement  
De l’addition des couleurs à travers leur corps prismatique ;  
Puis cette somme illunescente de bruits et de rubis fond...  
Et la trace en buée à son tour se dissout  
Avec les derniers bateaux glissant sur les artères.

“Voici les Trois Grands : le Cœur, le Poumon, le Rein !”  
Jésus devant concombres et tomates  
Sous les arbres.  
« Zénon, est-ce moi qui t’ai fait tomber ? »

On traîne d’une allée marchande à l’autre  
Au milieu des enseignes numineuses  
(Grenadine des garages et menthe des pharmacies).  
“Est-ce le Seigneur ou un Ange, je ne sais pas.”

Des jougs, des âges et des charrues ;  
Et ce qui vaut pour le village de Lavoux  
N’est pas sans intérêt pour la France.

Tout le groupe est parti dans cette idée à Dijon  
Sur les traces d’Aloysius à travers les rues  
Le mercredi, et le chien venait de mourir ;  
On a rapporté les paroles de Jésus en ville :  
“J’ai essayé, on peut.”, ou  
“Mon fils sera violoniste.”

**(lire la suite...)**

  
_Les Marcheurs_ sont le prologue poétique du recueil de Nouvelles & Petits récits nommé _Histoire Deux._

Ce recueil (dont des extraits figurent dans _Quartiers de ON !_), daté de 1984, présente plusieurs tableaux de l’Antiquité à la Renaissance dans une version rejouée, célébration parodique ou reprise de cauchemar.