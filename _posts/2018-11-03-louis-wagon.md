---
title: Louis Wagon
date: 2018-11-03 00:00:00 +0000
sous_titre: Marges des États du Monde
date_document: 1971
fichiers:
- "/louis-wagon.1971.pdf"
tags:
- texte
- Cosmologie Onuma Nemon
- document

---
LOUIS WAGON

Louis Mac Carthy s’est engagé dans la marine ; il n’est là qu’en permission mais il veut travailler aux **Wagons-Lits** quand il sera démobilisé ; il nous raconte qu’ils ont installé des pupitres électriques avec des télécommandes dans les _Premières_ : plusieurs boutons de couleur aboutissent au compartiment d’un policier assistant le contrôleur.

En bandeau au fronton de chaque wagon, désormais :	

**Votre anonymat sera respecté**

_(lire la suite…)_