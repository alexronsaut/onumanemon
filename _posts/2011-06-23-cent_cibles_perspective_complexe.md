---
title: "Cent Cibles : Perspective Complexe"
date: 2011-06-23 11:01
fichiers: 
  - Perspective_Complexe.jpg
sous_titre: "Acrylique blanc sur Cible 21 x 21cm pour carabine 50m" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - peinture
---

Cette série de Cent Cibles, semblable (selon l’auteur) à une suite de “Calendriers des Postes”, s'apparente à d’autres travaux de gravure au fusil des années 80 et à des gravures sur arbre vivant (émergence des figures de divinités à travers l’écorce). 

_NDLR_   
 _Celle-ci est à Dominique Abensour._