---
title: "Notre-Dame en brouillon"
date: 2009-06-28 18:45
fichiers: 
  - Notre-Dame_en_Brouillon.pdf
sous_titre: "Livre Poétique de Nycéphore 1964-1984. Futur Antérieur" 
date_document: "1964" 
tags:
  - document
  - OGR
  - texte
---

Texte déjà publié en 1966, puis en 1991 dans le cadre d’un ouvrage consacré à J.-N.-A. Rimbaud.  
_I. Revay_  


 **14. Notre Dame en Brouillon**  
  
Ciel pâle où je descends, je passe, plus de fleurs,  
Bas, au-delà de la Place Ducale, en boue  
Je crois qu’on reste fort par les yeux, pas de joue ;  
Ombre d’église d’or et d’eau crue, ses rumeurs  
  
Vertes dans le barrage à l’embarras gastrique ;  
Mais je reste debout, moi fumeur de sapin,  
Pauvre hêtre aux durées croustillantes de trique,  
Qui ne fait que mâcher sa tête dans son pain !  
  
Voir ! Plutôt les constats. La voie ferrée. Dioscures,  
Laissez-moi reposer _doth dépend_ à l’esprit,  
Malgré toi ! Son dos vient à plat que ces fumures  
Dans une obscurité orangée de crédit !