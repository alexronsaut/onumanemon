---
title: "Monde Dic, Duc, Fac, Fer & Memo"
date: 2008-05-09 16:11
fichiers: 
  - DicDucFacFerMemo.pdf
sous_titre: "Ligne du Chaos. Printemps" 
date_document: "1992" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

“Cher Monsieur Luc, je vous remercie de votre courrier à propos de la cathédrale de Cadiz, mais contrairement à Baltrusaïtis, je ne suis pas un spécialiste de l’Herméneutique en la matière, et je ne pense pas pouvoir vous aider avant votre départ dans votre Projet Nomade Souverain, que ce soit en Andalousie ou aux Amériques.  
Si j’ai écrit des petites choses à propos de la cathédrale d’Auch et de l’Hospice de la Charité à Séville, c’est au hasard de lignes biographiques et d’itinéraires du Jour des Morts. Je procède par _termes aimants_ qui attirent à eux et arrachent ci et là des bribes de parcours, des morceaux de puzzle du Temps.  
Duco.  
_P. S. N’oubliez pas que je ne suis qu’un chien_.”  


**\***  


Duco suit en reniflant la ligne de fuite des _Cathédrales_ et de la _Toussaint_, à Auch. Il sait que la ligne est un cheveu, qu’il ne faut _rien imaginer_, partir de la _Vérité_. Il regarde cette carte reçue de Aube, envoyée du Moulin du Mas pour annoncer son mariage : un gant blanc de main gauche, dont la couture, ourlet rouge du bord du poignet se scinde au milieu du dos de la main pour monter en arborescences veineuses jusqu’aux extrémités des doigts ; il songe à la semaine passée par La Fée Numida et Ulittle Nemo à Styx, à relever des empreintes des lignes de leurs quatre mains, à l’aide d’encre de taille-douce.  
Il fait froid à Auch, pour le jour des Morts, en traversant le pont de Lagarrasic, puis suivant la double allée des platanes par l’autre rive et remontant les escaliers d’Artagnan le long des torchis de l’ancienne ville, passant sous les immenses magnolias au moment de la volée des cloches jusqu’à atteindre la cathédrale de 18 heures 30.  
Arrêt au sommet. À peine décalé de l’axe du grand escalier, le sapin se dresse, spontanément ; accrochés de part et d’autre sur ses branches : tous les feux de la ville en dessous.  
À gauche la grande avenue lance une guirlande illuminée vers Toulouse.  
C’est le _Noël des Morts_, qui ne coïncide pas avec le nôtre.  
Dans les chapelles retirés, l’étau splendide des verrières d’Arnaud de Moles, le cœur splendide de Jésus, rubis de la fournaise. Au centre, les stalles de chêne du grand chœur que Duco connaît si bien : musiciens, bouffons et rondes joyeuses se tressant avec les démons, les serpents et les monstres à travers les veines du bois.  
Là comme à Reims, les ogives sont lancées tellement haut qu’elles disparaissent dans les lointains jusqu’à atteindre des profondeurs séculaires, se prolongeant en grottes verruqueuses suintantes et démesurées de plusieurs milliers de mètres de haut ; caverne de Reims, cathédrale creusée jusqu’au ciel paléolithique où tournoient les abîmes des rosaces tourbillonnantes.  
On sera tous transparents, dans l’illimité, jetés dans l’océan de fleurs sur l’autel, des fleurs plus brillantes même que les veilleuses et que les cierges !