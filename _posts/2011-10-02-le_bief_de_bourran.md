---
title: "Le Bief de Bourran"
date: 2011-10-02 17:27
fichiers: 
  - Bief_de_Bourran.pdf
sous_titre: "Ligne des Gras" 
date_document: "1989 & 1996" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

Et voilà ce qu’en dit Nycéphore :  
“Quatre heures du matin, l’été, nuages dans la rivière. Profiter au maximum des espèces qu’on a : l’ombre, les jalousies, le Katalpa… l’eau enfin distribuée si amoureusement ; défaire l’engoncement dramatique, avant Midi !  
L’Enfance côté du trottoir dans ses villes, nouvelles antiennes : ses objets successifs devant soi. Rue Verte, sous le marronnier rose, assis sur le banc des douze ans, et de là multitudes de scènes : dans chacune je m’assois et je suis. Merles, fracas des sensations ! Chèvrefeuille, abîme insondable ; si nous ne pouvons rien savoir de l’énigme, _disons-la, simplement_, stagnons, auprès des essences. Poésie : retenue, celui qui ment tire l’odeur des roses vers la prose, vous savez ?  
Moïse de bois doré sans être furieux, formidables senteurs : arums au printemps, proches du fenouil, mimosa des morts à Saint-Augustin, avec l’Idiote dans l’Église. Bonheur incompréhensible absolu (compression atroce des vitraux ; puis vitraux de nouveau dispersés au ciel, aux champs, aux temps), liseron sans odeur de la Préservation : _les orgues de Dieu canonnent quand les moissons_ !  
Ils canonnent les rues du Cancéra, du Pas-Saint-Georges, près de chez Nénette et Norbert Perez, devant les tissus Bordenave, chez Maïté (de Manolo), la rue Maucoudinat (suivante à gauche, son puits de Bahutiers, sa Truye qui file), rue Buhan, rue des Boucheries où bouchers, tripiers et crabiers bombardent le bar-tabac rouge de Saint-James de bestes mortes, trippes, laveures, bouillons puants et chairs filantes en contrebas de l’éblouissement du soleil et du courant d’air frais conjugués sous la Grosse Cloche de Saint-Éloi où Siona chante _La Juive_.  
  
_lire la suite…_  
  
_Ce texte fut jadis dédié à Bernard Manciet avec lequel il y eut une brêve correspondance, et publié dans une feuille locale de Bordeaux.  
I. Revay_