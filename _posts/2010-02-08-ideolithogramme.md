---
title: "Idéolithogramme"
date: 2010-02-08 22:18
fichiers: 
  - IdeolithogrammeCDDidier.jpg
sous_titre: "Encre de Chine, collage sur Arches format raisin" 
date_document: "Avant 1984" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - dessin et gravure
---

Photographie : Didier Morin.