---
title: "La Musique"
date: 2008-02-22 20:33
fichiers: 
  - Orphelins.ExtensionsLigne_de_Jean.pdf
sous_titre: "Les Orphelins Colporteurs. Extensions de la Ligne Jean" 
date_document: "1982" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_**La musique.**_  
À la Révolution, le citoyen-commissaire retourne les foins ; perte du bonheur disjoint ; tambours ; cocarde. La Bretagne résiste partout : Le Gallic et autres, Finis Terrae, Pleumeur, Côte Sauvage, partout…  
Puis voici des Écoliers dans la cour de l’École Primaire pour la célébration de la Musique. Pas encore de névrites, ni de paquets de vers sous le scrotum. De jeunes princes volatiles, dignes armoriés de Bretagne, et que le retour ne préoccupe pas.  
Leur corps est subtil. Certains sont adossés au mur pierreux du préau. Quelques-uns sourient comme on le ferait du progrès en Art ; mais tous se trouvent sans propos, à attendre…  
« Quand sera-t-on vraiment soi-même ; quand d’autres viendront-ils en nous ? »