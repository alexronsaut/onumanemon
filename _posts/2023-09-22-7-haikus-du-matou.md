---
title: "7 haïkus du matou"
sous_titre: "Typhaine Garnier"
date: "2023-09-22 16:19"
date_document: ""
fichiers:
    - /7-haikus-du-matou.pdf
tags:
    - document
    - texte
    - DAO
---

Ces textes d’apparence japonisante – façon Hergé : c’est du français en costume japonais, mais exagéré, caricatural et (involontairement ?) bouffon – avaient pour ambition de tenter _une autre transposition du haïku_. Plutôt que la version occidentale habituelle (l’insipide sandwich de 5/7/5 syllabes), je voulais trouver une forme qui restitue au moyen de l’alphabet latin quelque chose de la dimension graphique et de la polysémie (et polyphonie), c’est-à-dire de la _densité_ du haïku, associant l’idéogramme et l’écriture syllabaire.

Le résultat est un hybride, une sorte de chimère qu’on ne sait par quel bout ni dans quel sens prendre, ni s’il faut l’articuler ou seulement voir. Les signes font parfois rébus. Une syllabe s’étoile dans plusieurs directions. Des lignes sémantiques horizontales apparaissent en travers des verticales, mais furtives, hasardeuses. Le tout a un caractère enfantin, car c’est bien un jeu, un exercice d’assouplissement, où tous les coups (aïe) sont permis (alors que le haïku traditionnel obéit à des règles formelles strictes). D’où le sujet, sans doute : le chat Zoneille, agile et malicieux comme tous les jeunes de son âge, comme le chat à l’encre de Chine d’Onuma Nemon qui s’est glissé dans ces pages. À moins que ce ne soit l’influence de Maurice Roche, lu assidument à ce moment-là.
