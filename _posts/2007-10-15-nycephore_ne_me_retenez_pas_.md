---
title: "Nycéphore : ne me retenez pas !"
date: 2007-10-15 11:55
fichiers: 
  - 296.mp3
sous_titre: "Les Escholiers Primaires" 
date_document: "1989" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - son
---

Pages sonores inédites. Disque 2. Piste 36.  
Réalisation Philippe Prévot dans les studios de LIMCA à Auch.