---
title: Chauve-Souris
sous_titre: ""
date: 2024-08-18 14:46
date_document: "Après 1984"
tags:
    - document
    - peinture
    - Cosmologie Onuma Nemon
fichiers:
    - "/Chauve-Souris.Peinture.Acrylique-sur-Cible-20x20cm.jpg"
---

Peinture. Acrylique sur Cible 20x20cm

Don à la Bibliothèque Kandinsky de Beaubourg.
