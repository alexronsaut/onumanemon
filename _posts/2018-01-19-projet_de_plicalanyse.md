---
title: "Projet de Plicalanyse"
date: 2018-01-19 12:19
tags:
  - billet
---

Névrose, psychose et perversion ; valse à trois temps, variantes du négatif : déni, désaveu, clivage, dénégation, refoulement…  
La place du discours par rapport à l’objet, la place du réel entre les blocs de discours, l’occupation de cette place plus ou moins publique par les Enfants et les Morts. Le réel au lieu des frisettes de langage, trou de Kim Carson ; langage obus meurtrier pour le canon lui-même et la cible, comme ces saloperies de “sous-munitions”. Puis la place du langage comme transformation des choses ; la rhétorique de nouveau cristallise en pensée. La place n’est pas un vide, malgré toute cette vulgate du yin et du yang, etc. C’est un _plein_ ! _Pietro Bodhisva_