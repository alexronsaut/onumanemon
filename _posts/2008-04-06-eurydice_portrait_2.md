---
title: "Eurydice Portrait 2"
date: 2008-04-06 17:17
fichiers: 
  - Eurydice.Portrait2.jpg
sous_titre: "Les Grands Ancêtres. Mac Carthy. Saison de la Terre" 
date_document: "1982" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - dessin et gravure
---

Hommage à Anne Drucy