---
title: "Sac Milieu du Monde rehaussé à la plume"
date: 2007-05-08 18:22
fichiers: 
  - 148.jpg
sous_titre: "Linogravure, Encre, Plume. 15,5cm x 24cm." 
date_document: "2000" 
tags:
  - document
  - HSOR
  - dessin et gravure
---

Gravure reproduite pour la soirée au CIPM. Accompagnait la sortie de “Quartiers de ON !”  
Isabelle Revay.