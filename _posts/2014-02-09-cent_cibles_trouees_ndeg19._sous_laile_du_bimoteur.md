---
title: "Cent Cibles Trouées : N°19. Sous l’aile du bimoteur"
date: 2014-02-09 22:10
fichiers: 
  - 19.Sous_laile_du_BimoteurSapet.jpg
sous_titre: "Acrylique blanc sur Cible 17 x 17cm pour pistolet 10m" 
date_document: "Après 1984" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - peinture
---

Aux Sapet