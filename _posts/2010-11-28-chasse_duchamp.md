---
title: "Chasse Duchamp"
date: 2010-11-28 19:39
fichiers: 
  - ChasseDePaturageAMoutons.jpg
sous_titre: "Chasse de Pâturage à moutons" 
date_document: "2010" 
tags:
  - document
  - DAO
  - photographie
---

Dans un souci de scansion acupuncturale des campagnes, le collectif DAO a décidé à la suite des intempéries récentes de drainer champs, ruisseaux, canaux et sentiers de montagne. En réponse à l'urinoir urbain, on remarquera que la chasse, opérant une révolution complète, se retrouve parfaitement à l'endroit.