---
title: "Lambeaux de Roman dans les Andes"
date: 2008-05-19 18:45
fichiers: 
  - LambeauxRomanAndes.pdf
sous_titre: "Les Grands Ancêtres. Ligne de Don Qui. Automne" 
date_document: "1991" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

Texte inédit  
  
“Vive le _Roman de Brut_, le _Wace_ tendre !”  
« Qu’avez-vous fait aujourd’hui ? », demande le moine des Andes de Mendoza aux Jeunes Croisés. À travers les différents graphes du _passage du Col_, ils se sont rendus vers des énoncés fondateurs. Ils y ont rencontré _l’Homme Invisible_, avec son nez, si différent de Gogol, en carton-pâte, qui peut fondre si bien sous la pluie, la neige de Tamié, et qui est ce qui apparaît d’abord, rouge et luisant entre les pansements, qui dépasse. Sa bouche, quant à elle, dévore tout le bas de la figure. Ils lui ont demandé comment il avait pu réussir à rater l’expédition dans le Grand Magasin (celui des Marx) où l’on trouve tout, pour finir par se projeter dans des conditions pires, le degré au-dessous, dans l’antique magasin du vieillard. Pourquoi n’attend-il pas la nuit suivante ? C’est la même opposition qu’entre “Le Bonheur des Dames” et la petite boutique d’en face, ou le magasin du père Lalouette. Le bonheur, c’est comme la modernité : toute parodie en est immédiatement visible.