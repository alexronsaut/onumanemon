---
title: Parfois L’Hiver
date: 2011-04-06 19:49
fichiers:
- Parfois_LHiver.pdf
sous_titre: Texte de Joël Roussiez
date_document: "2010"
tags:
- document
- DAO
- texte

---
Nous buvions des chocolats dans des cafés aux plafonds bas, ornés de cuivre brillant et  
d’éclairages diffus, dans lesquels des gens pauvres jouaient aux courses sirotant des cafés bientôt froids et s’excusant d’être là au chaud tandis que dehors il faisait froid, très froid parfois si bien qu’il glaçait dans les rues mouillées et que ces pauvres hésitaient sur le pas de la porte avant de disparaître rapidement derrière les vitres embuées derrière lesquelles nous buvions.

_(lire la suite…)_