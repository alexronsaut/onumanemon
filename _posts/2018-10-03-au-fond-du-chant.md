---
title: Au fond du Chant
date: 2018-10-03 00:00:00 +0000
sous_titre: Os de Poésie
date_document: Avril 1968
fichiers:
- "/au-fond-du-Chant-os-poésie.pdf"
tags:
- texte
- OGR
- document

---
AU FOND DU CHANT

 

Au fond du chant, époques des maison chères et d'autres antres de magie, la nuit venue. Je plonge, au lieu de rentrer directement de l'École, par un détour aux jardins d'onyx et je m'assoie posément sur un banc pour fixer dans les vastes soirs de la Marne tout ce que l'enfant ne savait préciser.

On mâche une dernière croûte de pain alors que la troupe passe sous les platanes. Grande misère et tristesse de l'image télévisée au sein des hurlements chez les cousins Perez ; bonheur radiophonique de l'enfant seul enfin chez lui. Il l'a retenue par la manche ; elle lui a dit :   
 « Éteins !… » Elle s'est laissée déshabiller, embrasser, caresser… L'enfant nu, la femme défendue, le mystère…

 

_(lire la suite…)_