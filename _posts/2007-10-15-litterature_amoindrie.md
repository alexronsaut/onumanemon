---
title: "Littérature amoindrie"
date: 2007-10-15 13:39
fichiers: 
  - 269.mp3
sous_titre: "Les Adolescents. Bande à Jésus. Les Conjurés de la Tour Eiffel" 
date_document: "1989" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - son
---

Pages sonores inédites. Disque 1. Piste 9.  
Réalisation Philippe Prévot dans les studios de LIMCA à Auch.