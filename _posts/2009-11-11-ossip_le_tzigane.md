---
title: "Ossip le Tzigane"
date: 2009-11-11 19:40
fichiers: 
  - Ossip_Extrait_1-6.pdf
sous_titre: "Les Grands Ancêtres" 
date_document: "Après 1984" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

Pour ce qui est des Orpailleurs, les Garimpeiros étaient arrivés par la mer d’abord, du côté de Montpellier, et ils remontaient avec leurs retrouvailles vers l’acqueduc des Arceaux ; mais pour eux ni prostitution ni mecs armés ni violence. Curieusement il y avait toujours eu des orpailleurs dans les Cévennes et sur le Gardon. Et ça revint à la mode après 1968. En dehord du Gard beaucoup de chercheurs dans l’Ariège et dans la Dordogne, _recherche minière_ essentielle devenue linguistique et posturale comme on le verra plus loin.  
  
Bien avant cela Ossip qui mesurait 2m 06, né en 1820 participa à la ruée vers l’or dans le Far East en Mandchourie qui eut lieu de l’autre côté du détroit de Berring en même temps qu’au Far West, avec la mème exploitation féroce et intense.  
Ossip était fort comme un Auguste ; chaque nuit il faisait son enfant naturel, se réveillant seulement en sursaut avant l’aube comme devant un foyer en feu. À la fin du XIXème il participa à la création de la République formée de chercheurs d’or russes, chinois, finlandais et français. On dit qu’il fut un créateur de cette épopée chantée fantastique des Russes en territoire chinois.