---
date: 2021-11-13 10:40
title: La Longue Marche
sous_titre: Os de Poésie Nycéphore
date_document: 1984 & Après
fichiers:
- "/la-longue-marche.pdf"
tags:
- texte
- OGR
- document
---

La Longue Marche

_..._
Je n’en ai pas fini d’avoir dormi longtemps, frais et dispos d’emblée, arrêté dans un lieu pour boire du café, simplement, voir des gens, sourire à tous, admirer le visage des enfants, le jeu des animaux, l’air benêt, parfaitement sans ombre.  
On n’en a pas fini de la parole qui éteint tout sauf cette charge distribuée dans le ciel, de cette perpétuelle chute en avant de la course, la voix perdue quand on jouit dans la Neige jusqu’à pluspersonnevisible.  
On n’en a pas fini avec le cortège qu’on voit de loin à travers les bourrasques, les trombes de pluie, de se rendre au cimetière dans des cirés fluorescents en coupant dans la paille et la luzerne, dérangées…  
On n’en a pas fini comme personne privée de ramasser quantité de cadavres publics.  
On en finit pas avec l’arithmétique, le vélo au soleil et le chien souffrant ;
_..._

_Lire avant et après…_