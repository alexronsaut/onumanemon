---
title: "BAADER-MARS"
date: 2008-06-22 18:10
fichiers: 
  - Baader-Mars.jpg
  - Holger_MeinsMort_Mail.jpg
sous_titre: "Xylographie 1, 60m x 1, 20m" 
date_document: "Juin 2008" 
tags:
  - document
  - HSOR
  - dessin et gravure
  - edition
---

BAADER-MARS  
_Cette gravure sur bois a été réalisée grâce au soutien de l’URDLA, gravée et tirée dans leur atelier de Villeurbanne les 10 et 11 juin 2008. Elle est éditée en 14 exemplaires dont moitié du tirage réservé à l'auteur. Elle est en vente auprès de l’URDLA._   
La xylographie a pour double source historique l’impression des images religieuses et des cartes à jouer cosmologiques ancêtres du Tarot (peut-être d’origine gitane ?)  
Ici en dehors de l’évocation du Pendu (renversé) autant que d’un mort couché sur un plancher et du cadavre d’Holger Meins, il est question de Mars. Le dieu de la Guerre et la Planète Rouge convenaient autant que l’Or du dernier Empereur Mao pour une future activité de la Fraction Armée Rouge, dont le suicide collectif de quatre membres en deux jours n’a jamais bien été interprêté : en effet le Boeing détourné par quatre terroristes palestiniens, qui erra en traçant des V au-dessus de Chypre en hommage à Pynchon, et effectua un dessin savant au-dessus de Dubaï et Bahreïn, avant de se poser sur l’aéroport de Mogadiscio, en Somalie, n’avait pour but que de signaler aux détenus de la Bande à Baader une opportunité climatique et astrologique de fuir sur Mars dans la nuit du 17 au 18 octobre 1977. Les trous de balles que se firent volontairement dans le dos Andreas Baader et Jean-Carl Raspe à l’aide d’un pistolet ne servaient que de “prises d’air” et le cable de haut-parleur avec lequel Gudrun Ensslin se pendit permettait de maintenir la communication avec les amis Martiens tout le long de la téléportation. Irmgard Möller malheureusement ne put les accompagner, malgré les nombreux coups de couteau dont elle s'était perforée, faute d’avoir su inverser la carte des trous à faire qu’on lui avait transmise en droite-gauche. La Foudre, Initiale de Zeus et lettre du Dyable divise ici les deux Frères dans le même corps.  
Elle regrette sûrement depuis d’être restée vivante sur terre.  
Par contre Ingrid Shubert réussit bien à son tour la téléportation le 12 novembre 1977.  
On se souviendra de la photo de la mort d’Holger Meins en novembre 1974 parue dans Libération, et de ce corps devenu squelettique et difforme à force de grève de la faim : 42 kg pour 1,85 m, et de ce qu'il disait dans sa dernière lettre : “Un des côtés de Engels : transparent. \[…\] Ce n'est plus une question de matière mais de politique.” C’était un des premiers à avoir été téléporté et à avoir compris la transcription par la lumière, ce qui était aussi une façon de retrouver le corps des déportés pour ceux qui combattaient les anciens nazis, ou la méthode Kepax pour oublier les insupportables blessures.  
Gudrun Ensslin, fondatrice de la R.A.F. avait cet avantage d’être Lion ascendant Capricorne et Dragon de Métal en Astrologie Chinoise et de pouvoir ainsi guider le groupe dans sa fuite céleste.

Publication : [URDLA, Centre International de l’Estampe et du Livre](http://www.urdla.com/estampes/estampe.htm)