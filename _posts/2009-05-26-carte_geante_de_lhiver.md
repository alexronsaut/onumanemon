---
title: "Carte Géante de l'Hiver"
date: 2009-05-26 22:42
fichiers: 
  - Carte_de_lHiver.jpg
sous_titre: "Encre de Chine, Écolines, Crayon gras, Crayons de couleur" 
date_document: "2005" 
tags:
  - document
  - OR
  - dessin et gravure
  - extension
---

Carte de 3m x 3m ayant figuré à l'exposition du _Quartier_ en 2006