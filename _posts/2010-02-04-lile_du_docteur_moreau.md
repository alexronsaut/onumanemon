---
title: "L’île du Docteur Moreau"
date: 2010-02-04 18:39
fichiers: 
  - IleDrMoreau.Fleur-Visage.CDDidier.jpg
sous_titre: "Les Grands Ancêtres. Don Qui. Été" 
date_document: "1984" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - dessin et gravure
---

Le genre de fleur qu’on trouve sur l’Île de l’Animalité (archipel de Staphysagria).