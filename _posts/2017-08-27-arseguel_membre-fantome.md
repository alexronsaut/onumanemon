---
title: "Arseguel, Membre-Fantôme"
date: 2017-08-27 17:39
fichiers: 
  - arseguel_bras_gauche.pdf
  - arseguel.bras_gauche.jpg
sous_titre: "à propos d'Autobiographie du bras gauche" 
date_document: "15 août 2017" 
tags:
  - document
  - DAO
  - texte
---

_**(Autobiographie du bras gauche. Éditions Tarabuste. 2017.)**_  
Je pense ici bien sûr à Michaux qui à cause d’un problème de bras cassé, avait tout à coup découvert “Michaux côté gauche”. Beaucoup moins à Twombly et aux évidences qui ont surgi dans les années 70 de devoir écrire ou peindre mal comme un gaucher (qui plus est, dans un _bon milieu_). Le Twombly d’Arseguel, c’est Tapiès.  
Ce serait plutôt, dans le versant populaire, la plus grande rapidité dans le sport, comme le Fante de _1933 fut une mauvaise année_, avec son héros qui veut devenir un champion de base-ball, et masse sans cesse son bras gauche (auquel il parle pour l’encourager), avec l’Onguent de Sloan.“J’avais fière allume à l’époque, la souple démarche d’un tueur à gages, la décontraction typique du gaucher, l’épaule gauche légèrement tombante.” “Mais Le Bras me permettait d’aller de l’avant, ce cher bras gauche, le plus proche de mon cœur.”  
Je pense aussi à cette nouvelle de Maupassant où un pêcheur perd son bras arraché par le filet : comme l’expédition sera longue, il le conserve dans la saumure au milieu des poissons et le célèbre par un enterrement au retour.  
  
_(lire la suite…)_