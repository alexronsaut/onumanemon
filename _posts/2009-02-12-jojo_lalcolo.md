---
title: "Jojo l’Alcolo"
date: 2009-02-12 22:15
fichiers: 
  - Jojo_Pere_Porc.pdf
sous_titre: "Ligne des Escholiers Primaires. Le Singulier. Automne" 
date_document: "1983" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

_**L’Effondrement de la Carte**_  
“Mon Papa, il est bien malade, retourné, devenu lamade et baveur de bile, débile. Prenait Festale, avant de connaître les jumeaux Kay, les fils de Violet. À présent plus rien, c’est foutu.  
Les Kay vivaient sur le port, là où Charlie était au trimard ; leur père était broc. C’est à huit ans qu’ils se sont vus ; il y avait là José aussi, José Arès ; ils faisaient de la boxe avec le curé Bonnet , à La Flèche.  
La dernière fois, il a chu dans le fossé avec Jack “the Hat”, un bon irlandais, un pot’ de prison des Kay (le “Hat”, pour cacher qu’il est chauve, et peut-être _donneur_).  
On voit bien, à le suivre de dos, à l’importance de ses trapèzes pour un homme de la balle, tout le passé ardu de sa vie de cirque, les ours gris argent en hiver, la fréquentation de la neige (moins dangereuse que celle des Kay), l’arrachage et la plantation des piquets d’amarres.  
Il a pas été connu de tout temps. Ainsi, du moins. Ni connu les Kay. “Vice ignoble” disait Mamie. Salles de jeux. Leslie Hot et Lili Spot. Phrénésie ! Alcool de contrebande. Serments d’Hippocrites ! Dipsomane, qu’on lui crachait !  
Le docteur Dugoujon est venu, lui qui buvait que de l’eau, un jour de crise. On le chirurgica en ivrogne, comme un avare, à Saint-André. Jivago n’était pas là.  
Ensuite, plus les Kay débarquaient, plus ses blessures se compliquaient. Tout un tas d’autres apparitions lui tombèrent dessus à la suite, par “infiltrations”, insidieusement : Le Fisc, Le Cadavre de Terry Martin, La Dent Creuse de La Loi, Le Maître, Pete Bondurant…