---
date: 2019-09-14 3:38
title: Gérard, t'es là ?
sous_titre: ''
date_document: 2000 et après
fichiers:
- "/gerard-t-es-la.pdf"
tags:
- texte
- HSOR
- document

---
GéRARD, T’ES LA ?

Il n’y a d’inscription que d’agrément. L’art ne vient que par le cœur à l’esprit, et la peinture est comme une toile amicale qu’on dispose pour prendre un pique-nique en commun dans la campagne qu’on peindra demain, à mi-chemin vers la maison familiale d’été. À l’image de cette halte du Prince dans _Le Guépard_.

etc.

(_lire la suite…_)