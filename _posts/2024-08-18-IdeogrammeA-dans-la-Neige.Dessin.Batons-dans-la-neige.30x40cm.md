---
title: Idéogramme A dans la Neige
sous_titre: ""
date: 2024-08-18 14:46
date_document: "Après 1984"
tags:
    - document
    - dessin et gravure
    - Cosmologie Onuma Nemon
fichiers:
    - "/IdeogrammeA-dans-la-Neige.Dessin.Batons-dans-la-neige.30x40cm.jpg"
---

Dessin. Bâtons dans la neige. 30x40cm

Don à la Bibliothèque Kandinsky de Beaubourg.
