---
date: 2019-12-14 2:27
title: Journée Maussade
sous_titre: Petite prose de Joël Roussiez
date_document: "2011"
fichiers:
- "/journéemaussade.pdf"
tags: []

---
**Une journée maussade** où nous étions en rade dans le doux Ar Ménez, toute voile pendue ainsi que du linge mouillé, en attente du vent au milieu des coteaux, un jour de printemps où le clapot même était sans force; un jour donc où nous étions venus là pour aborder la côte en baie de Trez où se trouve une passe pour gagner la mer des Gascons en évitant le tour du Nez, gagnant ainsi du temps sur l'Amiral pour le rejoindre après un repos qu'il ne voulait accorder.

(_lire la suite…_)