---
title: "Alumbrados. La Rábida"
date: 2008-01-12 21:20
fichiers: 
  - La_Rabida.pdf
sous_titre: "Ligne de Don Qui Domingo" 
date_document: "1991" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_**Alumbrados**_  
Y’avait d’abord eu le clayon\* (\*éclaircissons clayon : c’est comme un layon dans la ville, la fraîcheur soudaine des forêts aux grands fûts, devenue sensible dans la profondeur des vieilles rues espagnoles ou bien à Bruges) poussiéreux de l’Aube, ce cliquetis de la matinée dans le crâne au sommet du chemin duquel le cycliste, de très loin, paraissant immobile, semblait, avec les rayures circulaires rouges et vertes de son maillot, un bouchon de liège de pêche oscillant dans les ondes d’air chaud.  
Elle leur dit, face aux légères effluves des marécages autour de la Rábida, pour le petit déjeuner si plantureux, silhouette des moines tressautant de parodie à travers les herbages (“qui s’avançaient trottinant et boitillant, sautillant et fôlatrant”), retroussant leur robe avec une consommation féminine (“pour jouer à saute-mouton, se cramponnant les uns aux autres, secoués d’une hilarité épaisse et fausse, se donnant des tapes sur le derrière, riant de leurs grossières malices, s’interpellant entre eux avec des surnoms familiers, chuchotant deux par deux, la main devant la bouche”), chacun d’entre eux traçant de sa voix séparée à travers le vent fort des algorithmes à partir de la théorie esthétique de saint Thomas, l’un tout à la vue de l’espace, l’autre tout à l’ouïe du temps, et cætera… ceci :