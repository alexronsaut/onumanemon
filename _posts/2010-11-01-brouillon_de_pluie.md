---
title: "Brouillon de pluie"
date: 2010-11-01 17:03
fichiers: 
  - Brouillon_de_pluie.pdf
sous_titre: "Récifs de Voyage" 
date_document: "1969" 
tags:
  - document
  - HSOR
  - texte
---

_**(Brouillon de pluie)**_  
  
On nommera comme il faut la pluie  
A barres creuses;  
La même courbe aponctue  
Ce germœuf à la carrée,  
Ces cours qu’on trisse  
Et l’allieu à propre le french-ship.  


  
_lire la suite…_