---
title: "Enfants Croisés"
date: 2011-05-21 16:59
fichiers: 
  - Jacques_Reeecrit.pdf
sous_titre: "Histoire Deux" 
date_document: "1984" 
tags:
  - document
  - OGR
  - texte
---

Quelques petits paragraphes autour des Enfants Croisés, semblables à ceux parus dans _Quartiers de ON !_ et faisant partie du volume _Histoire Deux_, à paraître.  
  
_**Jacques**_  
**Jacques** : « Robert le Diable s’est rendu aux nouveaux lieux, visitant la Grotte, la Colline et les Oliviers.  
Il faut bien voir que nous n’avons pas besoin de l’estimation des Aumôniers, car nos pentes eschatologiques sont pourvues de rats et de pauvres, que nous allons vers Jérusalem massacrant les juifs (comme “Les Carabiniers” !) que nous pillons, violons, que nous sommes d’indicibles cohues de misérables éclopés, que nous traînons avec nous des reliques, d’incomparables fétiches, des survivances, à chaque fois définitives et toujours à renouveler, que nous avons autant soif de pillage que désir d’inconnu. Nous préférons la Croix et les emblèmes sacrés, mais nous ne renonçons pas à l’épée ; la liberté de nos crimes vient de leur absence d’agressivité, laquelle n’appartient qu’aux adultes ; la puissance de la violence immature est en nous comme elle traversera Jeanne dans deux siècles.”  
**Blanche** : « Non, pas de Supérieur Général ! La Terre est devenue tératophile au moment de notre départ. L’Archevêque est venu avec tous ses crimes et ses curés patauger dans la mare, devant chez moi. Il a admiré les canards, sa robe couverte de merde. Il venait tous nous appeler avec mes frères, et nous consacrer en même temps : laboureurs, bergers et bergères, apprentie à la charpente comme moi ; il venait tous nous appeler à massacrer les Juifs, pour qu’on soit pas jaloux des Anglais avant d’être massacrés nous-mêmes. »  
 _lire la suite…_