---
title: "Carte Géante de la Terre"
date: 2009-05-26 23:15
fichiers: 
  - Carte_deTerre.jpg
sous_titre: "Encre de Chine, Pastel gras" 
date_document: "2005" 
tags:
  - document
  - OR
  - dessin et gravure
  - extension
---

Carte de 2m x 2m ayant figuré dans l’exposition du _Quartier_ en 2006