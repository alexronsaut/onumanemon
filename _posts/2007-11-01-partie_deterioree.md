---
title: "Partie détériorée"
date: 2007-11-01 20:20
fichiers: 
  - 266.mp3
date_document: "1989" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - son
---

Pages sonores inédites. Disque 1. Piste 3.  
Réalisation Philippe Prévot dans les studios de LIMCA à Auch.