---
title: "Bataillon de poules rousses"
date: 2011-05-20 12:58
tags:
  - billet
---

L’urgence c’est les foins, à cause du manque d’eau. Et ces problêmes de débroussailleuse : “Le girobroyeur à deux pales recourbées est dangereux pour ce genre de tête fragile”, m’a dit le mécanicien ; mais pour les taillis d’arbousiers, de genêts et de ronces grosses comme des avant-bras au-delà de la source, je préfère ça au “trois couteaux” qu’il a refilé ; à tel point qu’il est impossible de remettre la main dessus ! Pas de champignons : le sous-sol est sec comme fin août. Et des cerises “pleines d’os” à cause du soleil trop vif “qui les a tarées”, disent-ils dans la vallée.  
Arrivée pharamineuse d’un bataillon de poules rousses aux ébouriffés superfétatoires : pas de noires cette fois-ci. Dans l’humide petit matin, au-dessus de la vaste cloche de fraîcheur du marronnier dont la circonférence est désormais supérieure au volume de la maison, la plus vaste coupole des chants d’oiseaux répond aux psalmodies rigoureusement humaines des agneaux qu’on vient d’installer sur le coteau d’en face (la nuit ce sont les hurlements de femme qu’on égorge des blaireaux).  
Très longue course de côte avec A. après les 15 jours d’intervall-training, d’endurance et de vitesse alternés. Ensuite katas au sommet du plateau sur les foins coupés, dégagement magnifique au-dessus des trois vallées. Puis descente à la course par le Grand Hêtre.  
Du coup on a retrouvé les chiffres de Gilbert Descossy, le “Sculpteur buccal de chewing-gum” à ces moments où on devait faire des performances sportives ensemble (voir ici DAO). Il faisait de l’intervall-training par fractionnés de 100m en 15” : 2’ pour 800m. Il en tirait des conclusions sur son carré magique.  
C’est lui qui avait organisé nos expositions en commun avec Lucerné et quelques autres. J’ai encore en mémoire la gravure de son ami sur “La fondation de Buenos-Aires”.  
Il y avait aussi cette jeune artiste bavaroise qui se plaignait de n’avoir jamais joui de sa vie (“Le mot jouissance n’existe pas en allemand !”), mais qui mâchonnait du chewing-gum du matin au soir sans toutefois en tirer rien d’autre que des déformations de la machoire et de graves problèmes ligamentaires, ce qui lui valait d’être toujours fourrée chez son frère orthodontiste qui travaillait plusieurs fois par semaine là-dedans et reconstruisait ce qu’elle avait démoli. C’était une famille orale : le père ancien militant farouche des frontières de l’Est avait passé sa vie et l’avait perdue au mégaphone parce qu’un abruti apprenti électricien avait branché dessus le mauvais fil de la sono, et le mari qui avait repris le porte-voix ne souhaitait lui-même que d’exploser vociférant en pleine rue revendicatrice comme les cigales ou les cantaores flamencos de Lorca.  
O. N.  
  
[http://fr.wikipedia.org/wiki/Gilbert\_Descossy](http://fr.wikipedia.org/wiki/Gilbert_Descossy)

<http://www.anversauxabbesses.fr/artistes/user/gilbertdescossyart>