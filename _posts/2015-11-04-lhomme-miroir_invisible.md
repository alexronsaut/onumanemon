---
title: "L’homme-Miroir Invisible"
date: 2015-11-04 19:05
tags:
  - billet
---

Alexandre Bonnier, l’homme de la Mort Rose, avait rêvé d’un camouflage optique dans les années 70 ; il avait imaginé un vêtement fait de millier de miroirs qui permette à celui qui le porte de disparaître tout à fait dans le paysage. Curieusement il repensait à cela avant de disparaître lui-même, et il m’en reparla lors de l’enregistrement à la Maison de la Radio de Fraiseuse de Mots pour les éditions de La Petite École.

Et bien son rêve s’est réalisé en 2005 : la tenue existe, grâce à l’association de l’électronique et de la fibre optique. C’est le Département NCTRF (US Navy Clothing and Textile Research Facility) du centre américain de recherche Natick, qui a travaillé sur cette combinaison classée “défense” qui rend invisible l’homme qui la porte.

Il s’agit de milliers de mini-caméras cousues ensemble sur le vêtement qui filment tout autour du combattant, tandis que les fibres optiques qui composent sa tenue affichent l’image que verrait un observateur s’il n’y avait personne.

Le tissu se comporte comme un écran de téléviseur reproduisant en temps réel ce qui se passe derrière l’individu, mais également devant lui ou sur ses côtés. Quel que soit l’angle de vue où l’on se place, le combattant disparaît grâce à une illusion d’optique.

_Jean-Claude Vogel_