---
title: "beaubourg, joli petit village"
date: 2010-05-15 16:19
tags:
  - billet
---

Joli petit village où le nom importe peu des œuvres mais où la vigueur de certains pays est reconnaissable. Ainsi ce beau bouddha chinois de Huang Yong Ping (_Intestins de Bouddha 2006_), bois sculpté éventré, dont les tripes de soie grise déroulées dans la pièce sont en train d’être dévorées par des vautours naturalisés ; ce quadriréacteur immense d’osier, rotin et bambou tressés de Cai Guo-Qiang (_Bon Voyage 10 000 collectables from the airport 2004_) qui nous surplombe dont les réacteurs figurés par des ventilateurs font flotter des sortes de queues de cerf-volant, et où sont fichés dans la carlingue à claire-voie parmi quelques ampoules blafardes ciseaux, cutters et tous instruments tranchants multicolores confisqués à l’aéroport par crainte d’un attentat terroriste. Ou bien encore le considérable réservoir d’eau de de Louise Bourgeois comme on en trouve au sommet des immeubles à New-York et dans les grandes villes, bâti de bois devenu demeure magique à l’entrée des collections permanentes avec ses fioles et ses peaux transfigurantes à l’intérieur, et qui tient la place qu’occupait jadis une immense table ronde de bois brut faite par un artiste chinois célébrant les droits de l’homme et la concorde des pays.  


  
Le futurisme : ses jouissances de couleur jusque dans ses erreurs mêmes, quel enseignement d’utopie ! La Bataille de lumières de 1914 de Joseph Stella de la fête folle de Coney Island où l’on pourrait reconnaître les peintures de foire et de bord de cirque de l’autre Stella récent, aussi sculpteur que peintre ; Boccioni, splendeur simultanéiste, pénétration des rues et de ses cris dans la maison et du cosmos dans la peinture, Malévitch, exaltation suprématiste et cacophonie entre des paysans atrocement mal dessinés et des carrés parfaits dont le mélange est cependant d’un enseignement salutaire !  
  
À côté de cela la collection Daniel Cordier, les Réquichot, les Michaux mélangés à l’Art nègre et et pris dans les tissus océaniens, comme perdus pour deux rombières que cela exaspère de ne pouvoir les reconnaître et ne sachant plus quelle étiquette revient à qui. Plus loin deux jeunes chinoises qui photographient les zizis corrects de leurs compatriotes installés dans des caisses sur une immense photographie en couleur, puis s’arrêtent un long instant sidérées par le beau noir (et blanc bonnet sur la tête) de Mapplethorpe dont l’énorme queue pend entre les jambes comme une pompe à essence.  
  
Tout cela tellement drôle à côté des machines étiques de Duchamp, de l’austérité de sa machine à café ; ce pauvre Marcel, le Duchamp du singe. On en a connu des courtiers comme lui, aux Champs-Élysées et ailleurs, de noble famille, descendants impressionnants des Monet-Desynge par alliance, ici et là. Pop ! Ils papillonnaient puis disparaissaient, allant faire fortune dans un autre pays, abandonnant leurs engagements.  
Duchamp, sinistre incestueux n’a jamais fait que monnayer son insertion de force dans l’Histoire de l’Art comme un coin de vertu ou de torture, une feuille de vigne femelle.  
Quelqu’un l’a dit mieux que moi : celui qui a été frappé dans son corps pour arrêter de marcher, ce fut Rimbaud, et pas Duchamp, qui n’a fait semblant de disparaître que pour être _au mieux dans le marché_, un excellent courtier de ses œuvres et l’historien de ses dérobades.  
Roulements ridicules, même par rapport aux Picabia à côté, pauvres rouages qu’on fait sertir de force dans un mécano marchand, navrante machine célibataire : pourquoi tant de suivistes ! Cinq générations au moins de Duchampiens à vouloir une petite rondelle de cette pine si _triste_ (le seul mot psychologique utilisé de son vivant).  
  
Allons voir plus loin ; toujours autant d’exaltation devant Klein : l’éponge bleue, le moulage d’Armand, les anthropométries ; Beuys et sa vierge de bois primitif, archaïque : tout cela également salutaire ouverture.  
Dans ce petit village, la vastitude de la joie peinte prend toujours autant d’envergure, comme auparavant avec l’expo Giacometti la vie de la sculpture “en train de marcher”.  
Par contre il y a Basquiat, cette resucée du marché selon Wharrol dont on essaie de faire à tout prix un martyre avec des travaux merdeux de collage comme font tous les élèves de 2ème année aux Beaux-Arts, et surtout beaucoup de faux vendus par Templon.  
Il faut croire que dorénavant il y aura des écrivains comme Joyce(1) pour les universitaires et eux seuls, et des artistes uniquement pour les critiques, comme Duchamp. À côté heureusement la vie continuera.  
  
Oh la vie tragique, bien sûr, celle des coups et du courage physique, pas la vie mise en vitrine, fut-elle celle de l’infra-mince. La joyeuseté formidable de Pistoletto, Zorrio, Anselmo et tant d’autres… Ah ! bien sûr il y a ceux qui venaient uniquement voir Ortega, et de celui-ci l’œilleton du voyeur qui permet d’embrasser l’œil reconstitué à partir de pastilles de plastique pendues.  
En dehors des qualités d’Ortega, on peut dire que ce “voyeur” (comme quantité de gadgets techniques) on le “survoit” de façon incontournable dans les écoles d’Art à connotation conceptuelle depuis près de vingt ans sous tous les angles parmi le vrac des pulsions partielles, le registre névrotique obsessionnel, l’éloge des petites perversions (auxquelles on préfèrera les petites vertus), et le sacro-saint “protocole” de l’artiste dont la prétention à la maîtrise n’a d’égale que l’oubli de son inconscient…  
Contre cela la démesure, oui, baroque, volante, bridée, orientale, allemande, américaine, russe, en espérant qu’on en aura fini bientôt et à tout jamais des conceptuels, d’Art and Language, du minimalisme et de toute cette fascination convenue et puritaine de la logique et des mathématiques (fascination _ignare_, car Duchamp était tout sauf un mathématicien, ce n’était qu’un petit calculateur et un alchimiste du semblant, beaucoup moins alchimiste que Newton dans sa cave et Freud dans son cabinet, et beaucoup moins fou que Cantor) : minable métaphore du livre de mathématique pendue devant la chambre de la mariée !  
Car ce n’est pas l’objet que nous cherchons, c’est la connaissance. Mais ce n’est pas non plus cette fausse posture analytique qui voudrait nous faire croire qu’on en sait un bout sur un coin, toujours ce coin fiché dans un con (pour le boucher, le faire taire : et depuis, ces commentaires à l’infini !)  


**\***  


Plus tard, moi qui venais pour voir Freud j’ai subi Soulages.  
Noter le principe de l’accrochage des toiles de Soulages à Houston en 1966 par rapport à la série des peintures noires de Ad Reinhardt dès 1953 et jusqu’à la fin de sa vie, et voir surtout Number 11 de 1949. Lire en particulier les pages 37-38 du catalogue Reinhardt de son exposition à Paris en 1973.  
Immense “bois décoratif” et place du Tertre, que tout ce déballage de Pierrot (la seule différence c’est qu’il n’y a pas de poulbot), peinture au couteau bonne pour le poète Astringent, celui qui écrit en chiant. Sortes de plinthes et pas de drame. Rien de métaphysique. Rien de Malévitch ni de Reinhardt : c’est l’enduit qui est visible et les acryliques sont d’une platitude navrante. La brillance prédomine, les plans coupés, la graisse, un peu comme ces architectes des années 60 marqués par Le Corbusier qui trouvèrent de la plus grande plasticité de laisser des chiures de béton issues de la compression des coffrages sur les villas des photographes.  
Au milieu de ça des réflexions stupides sur la présence du _regardeur_ : encore un vocabulaire à la Duchamp pour du sous -Reinhardt.  
On pense aussi à Debré qui n’a jamais pu surnager de la Loire, jamais pu se décoller d’une référence particulièrement boueuse à la nature.  
Soulages aussi chiant que le design, qu’Erro avec ses “nègres” vietnamiens dont il parlait jadis dans une conférence à des publicistes, ses spécialistes d’affiches de cinéma qui réalisaient ses toiles là-bas au kilomètre, lui si fier d’avoir peint plus de mêtres carrés que Rubens, le fabricant de grosses cuisses.  
  
Celui que la couleur emporte et défigure comme la nudité elle-même ne peut pas avoir vu les grandes expositions des Grands Abstraits américains et s’intéresser encore à Soulages, qui reste un peintre du bord tranquille.  
Méconnaissance absolue de Newman et des autres grands abstraits depuis la fin des années 40 jusqu’au début des années 50. Hitler avait perdu la guerre mais gagné la culture, puisqu’on parlait à leur propos “d’art dégénéré”.  
Le post-moderne au sens d’anti-dogmatisme est né à New York dans ces années-là, contre les “ismes” de toute sorte.  
À 50 ans Barnett décida de devenir essayeur chez son tailleur pour ne plus être à la charge de sa compagne et surtout pour faire essayer un costume au président du MOMA et le retoucher directement lui-même à la craie.  
Reinhard est parti de lui ; Rothko est parti de son immensité ; tous ceux qui l’avaient abandonné sont partis de lui. Still est parti de lui.  
  
Enfin j’ai vu Freud ! Opposition du catalan qu’on pourrait dire d’un protestantisme revèche contre un anglo-saxon qu’on pourrait croire d’un catholicisme baroque (une fois n’est pas coutume !) Où l’on peut noyer les Cathares ascétiques dans un bain de chair débordante, en espérant qu’ils reviennent au cassoulet sans l’inonder de vinaigre.  
Mais c’est vrai que dans le fond Soulages ne se soulage pas tellement : cela m’a fait penser à sa femme de ménage qui racontait dans les années 70 à Montpellier qu’il l’obligeait à nettoyer la moindre aiguille de pin et la moindre trace de résine sur sa terrasse. Mais bien sûr, sans doute exagérait-elle (_c’est bien ça qu’il faut dire ?_)  


**\*** 

(1). Pensons à l’innommable _Stephen-des-fientes_ qui osa dire que Joyce était sans doute le plus grand écrivain de tous les temps ! Comme si les jouissances de toile cirée de ce minable archiviste miro équivalaient à une seule œuvre de Shakespeare ou au _Baladin du Monde Occidental_.  
  
Serge Victorien