---
title: "Apothéose des Classeurs"
date: 2010-03-23 18:06
fichiers: 
  - Des_Classeurs.pdf
sous_titre: "Livre Poétique de Nycéphore 1964-1984 Poème n°36" 
date_document: "Décembre 1965" 
tags:
  - document
  - OGR
  - texte
---

**36. Apothéose des Classeurs**  
  
Je vois les Classeurs d’ici faire ;  
Ce sont des guelfes, dont les palmes  
Sont bleues ; armés de leur calame,  
Ils sont bourreaux de Lucifer.  
  
Macrobe est parmi eux ; Saturne,  
Sénèque, et le vieillard Gaïus ;  
Ils ont des formes de théâtre ;  
Ils s’entretiennent tous les quatre,  
Et parfois se lèchent l’anus.\*  
  
Ils classent de petites choses,  
Des soucis, puis, de loin en loin  
Des fragments, des gueules de roses,  
Des nappes de sang, et du foin.  
  
L’un d’eux, venu de Forêt Noire,  
En Octobre, pour traverser  
Sur l’arête, puis par la foire,  
Vient ici pour tout renverser.  
  
Un Autrichien gauche du Rhin  
Exécute quelques calculs,  
Pris d’assaut par des échecs nuls,  
Reconnaissant qu’il n’est plus rien.  
  
Classant soixante-dix couronnes  
Du Un septembre Sept-Cents Quinze,  
Les cahiers écrits par les Faunes  
Sur leur méthode, et sur leur sinze.  
  
Certains, aux clartés de l’Époque  
Forment des spectres empoisonnés ;  
Claudius est là ; voilà sonner  
Les cloches, et pour Duncan les cloques !  
  
_etc._  
  
  
Ce poème a déjà été publié en 1966 dans _Saint-Michel & Saint-Augustin_, un fascicule littéraire bordelais, puis dans les “livraisons” du Livre Poétique par Tristram-Dao dans les années 80.