---
title: On se Croise !
date: 2018-10-03 00:00:00 +0000
sous_titre: Os de Poésie
date_document: Février 1982
fichiers:
- "/on-se-croise-os-de-poésie-février-1982.pdf"
tags:
- texte
- OGR
- document

---
ON SE CROISE !

Bassins comblés de houille, vous n’êtes plus de vastes jardins fleuris ; les enfants vont avec les pauvres, croisade Acéphale, oeillets splendides issus du sang d’Ajax ! 

Avant cela : famines ! Faute de blé, on mange des rhizomes, et on va !

_(lire la suite…)_