---
title: "Voyage Biographique. En passant par les Chiens"
date: 2008-02-19 23:20
fichiers: 
  - les-chiens-1PDF2.pdf
  - les-chiens-2.pdf
sous_titre: "Joël Roussiez" 
date_document: "Hiver 1987" 
tags:
  - document
  - DAO
  - texte
---

_Quoi qu’il en soit des déclarations d’intention toujours un peu excessives, voire présomptueuses, il faudrait considérer, ce texte, Voyage Biographique, comme une sorte de réponse à la vogue de l’autobiographie qui hanta, il y a plus de vingt ans, les auteurs du Nouveau Roman qui me semblèrent alors renoncer à l’ensemble de leur projet pour revenir au psychologisme ou pour le moins au sujet fondateur qu’ils avaient combattu._  
_C'est donc une matière autobiographique, la mienne, qui est traitée dans ce récit par l’intermédiaire de motifs (certaines peurs ou formes de sensualité par exemple) et au moyen d’une improvisation filtrée mais continue et régulière. Il s’agissait de donner aux motifs leur élan pour les dégager des limites, personnelles ou autres, et les faire vibrer en quelque sorte dans l’extérieur, le monde ou le cosmos, suivant ce qu’on acceptera…_  
_L’impression forte d’être traversé par des sensations, des impressions, des émotions neutres, c'est-à-dire sans véritable objet, ni sujet, d’être en quelque sorte absorbé par l’extérieur, sans intériorité donc, domine ce récit qui livre ainsi une sorte de denrée affective, proche de l’enfance (dont on ne livre, à titre d’exemple, que le tome 1 de l’ensemble qui en comporte 5)._   
_**Joël Roussiez**_

\*

Une sorte de chose tourne inlassablement, inlassablement comme si elle cherchait à se détendre et comme si elle souffrait. Elle tourne sur elle-même, sur son propre corps, elle se contorsionne dans une pièce sombre. Le monde dehors est à peine audible. On entend le souffle lent d'une chose qui halète ... Il fait chaud, très chaud, sous les couvertures. Je tourne inlassablement. Une main semble venir de loin et s'approcher de mon visage, une main qui tient un révolver et en appuie le canon sur ma tempe. Au loin, pas très loin, il y a mes pieds. Je ne m'en soucie pas. Ils sont l'un sur l'autre, croisés. Les jambes serrées nerveusement, suivent les pieds et maintiennent ainsi l'ensemble du corps en équilibre, en équilibre pas très libre, les jambes l'une sur l'autre pour maintenir le corps qui semble tourner sur lui-même. Plus haut, c'est à dire plus près de mes yeux, les cuisses s'aplatissent puis s'écartent, s'aplatissent puis s'écartent, inlassablement. Plus près encore, c'est le bas-ventre qui frémit légèrement. Au milieu, le sexe roule d'un côté, de l'autre, roule et gonfle un peu. Le ventre bat imperceptiblement sous la pulsion du sang qui passe, qui descend du cœur ou en remonte, qui suit un tuyau , un chemin tout tracé sous la peau; la peau blanche et veinée de violet bat aussi par endroit. En se rapprochant encore, c'est la poitrine qui monte, qui descend avec deux renflements, deux renflements de seins qui se froissent, se défroissent…Une palpitation à peine régulière soulève l'ensemble. Puis une main glisse, ce n'est pas vraiment une chose, elle monte le long du corps, caresse des formes, s'arrête parfois, attend, se pose sur un sexe d'homme. Elle monte, ensuite elle descend. Je ne suis ni bien, ni mal, je ne vois rien d'autre que le bout d'un nez lorsque j'essaie d'apercevoir tout un corps, un corps qui se cache sous les couvertures, qui semble vouloir tourner sur lui-même, une cuisse contre une autre cuisse pour se maintenir en équilibre. Alors, je ferme les yeux lourdement et quelque chose s'en va au loin, une chose qui tourne sans aucune raison en dehors d'un corps, en dehors de moi…

Publication : [“La Main de Singe” Louis Watt-Owen](http://lamaindesinge.blogspot.com/search/label/VOYAGE%20BIOGRAPHIQUE)