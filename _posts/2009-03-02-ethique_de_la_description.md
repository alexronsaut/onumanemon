---
title: "Éthique de la Description"
date: 2009-03-02 22:41
fichiers: 
  - ethiqudescription.pdf
sous_titre: "Joël Roussiez" 
date_document: "2009" 
tags:
  - document
  - DAO
  - texte
---

**Approche d'une éthique de la description.**  
Il faudrait dire qu'elle n'est pas représentation, ceci par exemple de Semprun(1) est à rejeter comme facticité « Il ne peuvent pas comprendre, pas vraiment, ces trois officiers. Il faudrait leur raconter la fumée : _dense parfois, d'un noir de suie dans le ciel variable. Ou bien légère et grise, presque vaporeuse, voguant au gré des vents sur les vivants rassemblés, comme un présage, comme un au revoir.  
Fumée pour un linceul aussi vaste que le ciel, dernière trace du passage, corps et âmes des copains_ ... » Le passage en italiques permet de bien comprendre ce qu'il ne faut pas faire pour rendre compte et offrir au lecteur une impression juste. Il faut être prés de la chose, or on s'en éloigne lorsque on veut « raconter la fumée », on ne raconte pas une chose, on la rend présente, c'est le rôle de la description.  
  
_etc._