---
title: "Petites Proses de Nicolaï"
date: 2016-10-12 13:25
fichiers: 
  - petites_proses_nicolai.pdf
date_document: "1965-1970" 
tags:
  - document
  - OGR
  - texte
---

LES PETITES PROSES comme la plupart des recueils du continent OGR sont écrites en vis à vis, d’un frère à l’autre. On a choisi entre plusieurs versions de chaque texte généralement la dernière. On trouvera ici quelques pages du recueil de Nicolaï qui comporte une cinquantaine de poèmes en prose et qui sera bientôt disponible en pdf sur le site.

L’ouvrage date pour la majeure partie du Lycée et de la Société Secrète des Cinq Doigts (en référence à Isidore Beautrelet), groupe constitué avec entre autres Nicolas le Hongrois, dont l’activité se poursuivra jusqu’en 1968, et dont Domnique Merlet était l’organiste et l’organisateur, du temps où il habitait près du Palais-Gallien.

Isabelle Revay