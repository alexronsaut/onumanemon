---
title: Maison en Travaux
sous_titre: ""
date: 2024-08-18 14:46
date_document: "Après 1984"
tags:
    - document
    - peinture
    - Cosmologie Onuma Nemon
fichiers:
    - "/Maison-en-Travaux.Peinture.Encres-et-autres-sur-Arches.Format-raisin.jpg"
---

Peinture. Encres et autres sur Arches. Format raisin

Don à la Bibliothèque Kandinsky de Beaubourg.
