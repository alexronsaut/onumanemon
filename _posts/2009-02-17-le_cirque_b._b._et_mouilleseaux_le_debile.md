---
title: "Le Cirque B. B. et Mouilleseaux le débile"
date: 2009-02-17 11:35
fichiers: 
  - Le_Cirque_B._B.__Mouilleseaux.pdf
sous_titre: "Ligne des Escholiers Primaires. Nycéphore. Été" 
date_document: "1970" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

_(Il s'agit d’un petit Extrait du Chapitre 6 de l'Été)_  
  
C’était bien de vouloir _partir_ ainsi. Mais qui se satisferait de ce simple redoublement de soi-même, de cette ombre portée en avant, idéale et nocturne, de cette infra-mince connaissance du monde ? Puis quel lecteur s’attardera sur cette Aventure matinée d’Argentine ?  
« On continue, on verra bien ! »  
\*  
Pendant ce temps de _préparation du mouvement_, entre le retour de Cádiz et la Grande Fugue aux Amériques, alors que je continuais à bien fournir le répertoire, à rencontrer d’autres comédiens, à choisir régisseur et techniciens et à améliorer ma connaissance de la photographie, mon frère Nicolaï travaillait comme magasinier à la Régie dans les champs du Bouscat.  


Comme il était stipulé dans le contrat qu’on ne pouvait recevoir   
d’argent sans déplacement (faiblesse théorique et a priori latin de l’oncle lointain, qui n’avait pas compris la vitesse immobile), et comme d’autre part la ville de départ ne pouvait être considérée comme une halte, nous subvenions ainsi chacun à notre façon à nos besoins.  
Magasinier ? Aucun grand écrivain ni artiste ne l’y autorisait. Il se fixait à peu près les mêmes objectifs possibles que Nany : expéditionnaire, bibliothécaire, etc. Il avait bien supporté pourtant de _teindre des chats_, tout le printemps de l’année précédente, sur la rive gauche de la Garonne, bien qu’il n’y ait jamais eu de “modèle exemplaire” dans ce sens-là non plus, travail plutôt repoussé par tous ceux qui les idolâtrèrent ! Mais magasinier ! La violence du sabotage était donc nécessaire pour résister au siphon de la connerie. Elle était poussée au centuple par Saïd. Et je vins les rejoindre à quelque temps de là.