---
title: "Le Vivant de Demain"
date: 2010-11-07 17:32
fichiers: 
  - Vivant_de_Demain.pdf
sous_titre: "Shijing" 
date_document: "1984" 
tags:
  - document
  - OGR
  - texte
---

Texte paru dans le recueil collectif des éditions Verticales “Qui est Vivant ?” paru en février 2007.  


  
  
_À Jean Schatz, Président de l’École Européénne d’Acupuncture, et qui persiste chez les Morts._  




  
Le génie du Cœur consiste à rester vivant.  
La respiration une fois reprise, alors que le soldat Vincentelli vient d’écrire à sa petite fille dans la fougère fraîche et les campanules, il faut repartir courant de nouveau, barda au dos. Seule l’ivresse des fougères dans les petites lunules du dernier soleil ; cette contradiction digne de la quadrature du cercle : comment le soleil peut-il produire son contraire en quantités vibrantes.  
À peine a-t’il fini qu’Oniès, Quiès et Boltès sont déja dans la pente, vive allure : toujours les mêmes à nous devancer que jadis, cuisiniers gorgés de la crême du veau blanc et rose, bondissant sur les rochers à pic, pami les merveilleux scandales du sperme d’automne.  
(_“Mon Papa, J’ai découvert le faux Corot de Dublin en même temps que Sennelier, les marchands de pigments d’ôcres purs et d’outremer luxueux, de belles toiles tendues pour le paysage et de beaux panneaux en tondo, Quai Malaquais, dans un désordre de craies sensibles.”_)  
Et Frantz Marc tué à Verdun le 4 mars 16 ! Avec lui meurent le petit cheval bleu et les trois chats. William Morgner meurt en Russie en août 17. Macke est tué sur le front de Champagne en septembre 1914. Avec lui disparaît Pierrot dans l’orage. Krichner tuberculeux et “dégénéré” se suicide en 1938.  
Il lui a écrit “Je crois qu’il y a une roche du désespoir, qui n’a rien à voir avec les conflits en cours. On peut la meuler sans s’en rendre compte ici ou là, selon comme le météorite tourne, mais la douleur de la carie réapparaît violemment la nuit !”  
Quand il est passé à Bruges, il lui envoya ce poème imprimé sur une carte de l’Hospice Saint-Jean :  
“Bandelettes d’elle je veux  
À l’avance  
Vous que voulez, du vivant ?  
_Aïno_,  
Le quai de Saint-Pierre.  
Je voulais d’elle, en débord.  
Ah ! J’arrivais, j’étais bon !  
Dors, dors, mon petit enfant,  
T’es mort ;  
Pourquoi ce bond dans mon corps ?”  
Ainsi il déjouait la ruse du cauchemar où la mère alerte et panique l’enfant en craignant les pourceaux de son avenir, montée des monstres qui se révèle à l’oppression soudaine du Poumon, aux sursauts cardiaques !  
  
  
_lire la suite…_