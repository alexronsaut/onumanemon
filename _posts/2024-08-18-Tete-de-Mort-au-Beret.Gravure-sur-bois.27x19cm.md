---
title: Tête de Mort au Béret
sous_titre: ""
date: 2024-08-18 14:46
date_document: "Après 1984"
tags:
    - document
    - dessin et gravure
    - Cosmologie Onuma Nemon
fichiers:
    - "/Tete-de-Mort-au-Beret.Gravure-sur-bois.27x19cm.jpg"
---

Gravure sur bois. 27x19cm

Don à la Bibliothèque Kandinsky de Beaubourg.
