---
title: "Le Coffre de Cuba"
date: 2007-10-09 12:57
fichiers:
    - le-coffre-de-cuba.pdf
date_document: "2007"
tags:
    - document
    - DAO
    - texte
---

Sphère Biographique. Inventaire Final.

_Contenu du Coffre de Cuba_

Contrairement à ce qu’avait cru voir dans un premier temps Isabelle Revay, le coffre de Cuba contenait un double fond dont Alcide Bali et moi avons fait l’inventaire.

En surface on trouve empilées à droite diverses sortes de paquets de cigarettes aux marques inconnues : “Everest”, “Attack”, “Old Toad” ou “Saint Georges” et “Saint Michel”. En réalité il s’agit de bâtonnets de chocolat au lait.

_(lire la suite…)_
