---
title: "Avec Obstination"
date: 2012-03-28 22:15
fichiers: 
  - avecobstination.pdf
sous_titre: "Texte de Joël Roussiez" 
date_document: "2012" 
tags:
  - document
  - DAO
  - texte
---

Ce portrait-là est rempli d’une description impeccable, d’une trame tellement recroisée dans ses traits de burin qu’elle échappe à elle-même ; et va se “_répandre une volupté comme d'un lit défait..._ ”.  
Elle est digne des portraits de femmes faits par Plossu, quelque chose de très attentif et de très rapide, une voyance en coin d’œil, un entrevu foudroyant, et d’autant plus précis que le regard n’insiste pas, n’écrase pas la personne considérée, _elle incipit_ : “_Elle commença son récit en baissant le visage sur son bol vide_”. Ce n’est pas non plus la parodie de _La vierge au bol_ en Thimotina Labinette : rien de caustique ni de cynique.  
La première phrase pourrait venir d’un Chant de Maldoror : “_Le rictus amer d'une femme étrusque au nez presque droit, les cheveux bouclés en masse le long de l'oreille cachée par des sortes de lauriers pour retenir la chevelure glissante ; un œil pour finir mais conquérant, voilà telle qu'elle m'apparut au lever du jour, passant devant ma maison, seule sur la route déserte et marchant._”  
Oh, oui certes, Roussiez est plein de vies et d’époques diverses, et plus ça va plus je crois (c’est-à-dire j’applaudis) à cette hantise des temps chez lui. On parle toujours dans les fictions et chez les parapsychologues de “vies antérieures”, et jamais des vies postérieures, or les époques apparaissant chez Roussiez sont des époques rabotées, devenues parfaites pour l’emboîtage et projetées en perspective ; Roussiez est un menuisier du temps. Son moyen-âge futur est un moyen-âge nettoyé. Peut-être que l’Éternel Retour c’est ça.  
Avec obstination et douceur, cette description, effectivement, ce commentaire. “_Je vous aime_”, voilà ce qu’il dit, je vous aime. Scorsese ne dit pas ça, mais Cassavettes le dit, et Bruno Dumont, et Bernard Plossu.  
On avance dans son texte qui ne sent pas le roussiez comme à travers les laies d’_Un balcon en Forêt_. Avec ces répétitions que le pseudo-pur styliste enlève mais que Gracq conserve en pierres de soutien latérales du chemin, car avec ça il fore, il avance, il troue la forêt dans ce grand vortex, cette spirale du temps.  
_O. N._  


  
  
**Avec obstination**

(_hommage à Krleza_)

Le rictus amer d'une femme étrusque au nez presque droit, les cheveux bouclés en masse le long de l'oreille cachée par des sortes de lauriers pour retenir la chevelure glissante ; un œil pour finir mais conquérant, voilà telle qu'elle m'apparut au lever du jour, passant devant ma maison, seule sur la route déserte et marchant. C'est son profil qui me frappa et c'est pourquoi je me levai rapidement pour faire sa connaissance. Il était tôt et il ne fallait pas l'effrayer, aussi laissais-je mon chien filer au devant d'elle pour l'accueillir. « Mais pourquoi se rictus amer » lui demandais-je plus tard lorsqu'elle eut accepté un bol de café. « J'ai quitté des lieux sombres où le temps ne passait pas, la pourriture chaude m'a éloignée et je marche pour me défaire d'une sorte de boue... Je vivais dans une ville aux lourdes colonnes et aux temples sobres ; la vie y était sereine, tranquille, pétrie d'habitudes et de calme. C'était une vie sans calcul qui se déroulait comme il convient sans malheur excessif, ni joie intempestive. On y disait les paroles qu'il fallait : va donner aux poules et aux lapins, ou bien : la vie n'est pas vaine qui s'accomplit chaque jour. Un beau jour on mourait et nous étions en deuil ; les cérémonies étaient courtes et sincères sans faste ni larmes abondantes... » Ses traits étaient doux et son regard puissant, les formes de son cou, de ses épaules et de ses bras, étaient rondes et agréables comme remplies d'une chair ferme et chaude qui venait sourdre de la peau et répandre une volupté comme d'un lit défait...

  
_lire la suite…_