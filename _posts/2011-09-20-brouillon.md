---
title: "Brouillon"
date: 2011-09-20 10:44
sous_titre: "Vrac Didier" 
date_document: "Aucune date" 
tags:
  - document
  - HSOR
  - texte
---

“Embryons humains : les nouveaux esclaves  
Produits en masse et conservés in vitro en attente d’être livrés  
Par Dieu le Père Papa Pantalon ;  
_Produits innommés_  
Voués à une non-naissance.  
 _Ena milo melomon_  
Bruit de fiacre du levantin  
(Elle attendait le train de Topeka),  
Four Courts 1922.  
Peut-être ici est-ce en sortant qui je suis ?  
  
Sur l’autre bord : suspension artificielle de la mort,  
Élimination des plus faibles.  
Au-delà : l’apoptose  
Et la reprogrammation du noyau :  
Est-ce un homme cette levure ?  
  
On n’est dans la nuit que la vomissure du ripailleur.  
Qui engendra quoi dans la Trinité :  
Plus d’odeur, d’auteur… comment ont-ils pu deviner ?  
  
Sed aureis furculis ;  
Noms de mon enclos sans trouée,  
Écriture de coins et recoins,  
Éclats de la douleur assourdis.  
Je suis la réalité fusible  
Et le vent du Sud casse les roses,  
Langue de mandarin dans une gueule poundissante.  
C’est est fini du Bouddha,  
On ira aux Enfers pour aider les autres  
Où l’on trouve autant de roses qu’on peut offrir,  
Où l’abuelo me mord la main par amour.  
  
Enfers de Dante, des Hindous, de l’Abbé Dubois ;  
Nagez dans l’étang rempli d’urine de chien et de morve !  
Inertie paisible de la stagnation,  
Jouissance de l’anéantissement dans le Tout :  
Ne jamais renaître !”  


**\***  




Oh ! Quelle horreur d’avoir Marie pour femme !  
Et le bulletin seul du premier trimestre  
Avec des notes autour de 11.  
  
_Ce texte fait partie dans les archives du vrac d’un dossier sur Didier. S’agit-il d’une esquisse de Pr’Ose reprise ailleurs ? Nous ne le savons pas._

_I. Revay_