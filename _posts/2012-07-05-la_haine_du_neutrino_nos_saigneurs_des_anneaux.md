---
title: "La Haine du Neutrino (Nos Saigneurs des Anneaux)"
date: 2012-07-05 14:47
tags:
  - billet
---

Nous saluons la naissance du boson de Higgs (que certains écrivent _X_, intersection de l’absolu, tellement ça paraissait _l’inconnue_ nécessaire), la particule-miracle disparue aussi vite qu’apparue, mais du moins _confirmée dans ce tremblement_. Bon. La communauté scientifique se réjouit. Nous aussi, qui cherchons toujours dans notre mémoire des chiffres en rouge et des choses insignifiantes.  
On se retrouve avec Léonard, Léonard et son “poids atomique nul” (bien au-delà de la pure adoration des nymphéas de Monet), ce que Madeleine Hours jadis dénichait : une absence absolue de touche, de trace, une sorte de non-matière dans la posée de la peinture.  
  
Il semblait pourtant voilà moins d’un an, fin septembre 2011, que la vitesse de déploiement d’un spermatozoïde lâché sur une moquette de luxe par un dreyfus d’opérette ou le choix du plus con parmi une liste d’éligibles, soit une grande préoccupation de la misère de _l’opinion_, plus grande en tout cas que la vitesse de la lumière, dans un monde livré à peu près partout à la guerre civile ; c’est du moins ce que clamait avec une certaine majesté le marchand de journaux d’un kiosque de campagne près de chez moi autour duquel il y avait foule, “ce monde qu’on a perdu où on n’y verra certainement plus clair que lorsqu’on aura étranglé le dernier banquier avec les tripes du dernier trader” ajoutait-il dans une belle envolée paraphrastique ou paraphrénétique.  
  
Mais j’ai eu alors la force de croire que la vitesse du neutrino dans les flancs du CERN relevait de la _passion_, qui plus est dans le cadre d’une opération surnommée OPERA, avec des ténors qui ont nom Dario Autiero et Antonio Ereditato.  
À preuve de la _passion_, cette centaine et plus d’articles parus à peine après quinze jours de l’annonce des résultats de l’expérience, et les débats chaleureux des scientifiques en radiophonie (les empoignades scientifiques prolongeant directement les courses d’électrons…)  
On sentait du reste une réticence, même chez de grands auteurs comme Levy-Leblond, notre spécialiste de l’idéologie de la science, à accepter avec cette utopie une remise en question de la théorie dans ce domaine quantique et extra (“Le quantique, c’est extra !”). Et chez plusieurs de ses collègues arc-boutés, la levée de boucliers donnait l’impression que la particule avait truqué son passage, qu’elle avait saoûlé les photons, histoire de les faire ricocher en désordre contre les murs ou qu’elle avait fait en sorte de les égarer dans le conduit des toilettes. Certains en avaient profité pour remettre en vitrine le chat de Schrödinger en expliquant que d’un côté le neutrino dépassait la vitesse de la lumière, et de l’autre non.  
Serait-ce donc aussi que l’idéologie y pleuve ? Car même si elle devait être à peu près infirmée dès le 22 février de cette année, l’expérience OPERA avait été réalisée dans des conditions attentives, et il n’est rien demandé d’autre à la communauté scientifique internationale que de refaire la démonstration dans un sens ou dans l’autre. Alice.  
  
Les Multivers, théorie au moins aussi éparpillante et aussi peu représentable, a fait son chemin depuis 2007 grâce entre autres à Aurélien Barrau, dont le ton ampoulé baroque “lance” lyriquement de belles planètes inconnues pour une soirée digne du Crazy Horse autant que de Chrétien de Troyes, sans pour autant révulser la communauté scientifique.  
Idem pour les théories cosmologiques révolutionnaires de Jean-Pierre Luminet, avec ses espaces sans bord, topologie non simplement connexe.  
Le neutrino, me direz-vous, c’est une curieuse appellation pour une particule qui part de Suisse ; c’est peut-être de là que venait toute la défiance ; mais n’oubliez pas qu’elle voltige vers le Grand Sasso qui a connu le bonheur de voir passer d’ilustres personnages dans une époque d’Or symbolique avant la catastrophes des usuriers.  
_“(……)with usura_  
_hath no man a painted paradise on his church wall (……)”_  
_(E. Pound. Canto LXV)_  


**\***  


Par contre, la théorie du _genre_, elle, passe bien : c’est tendance et camping. Et pourtant elle ne sert à rien de moins qu’à recouvrir _l’innommable_. À nous faire croire que l’altérité est une vieille manie et que le même est notre fondement, si j’ose dire.  
“Joyau, jouissance de la lumière”, disait le Papy à la houppelande, et aux cigares tordus à la Trinita par la distance critique.  
Sur les genres l’expérience scientifique enrichissante, c’est certainement celle sur les drosophiles (comme le Nobel français de cette année), où par mutation génétique on a créé des mouches sans phéromones qui curieusement deviennent “irrésistibles”, même pour les partenaires du même sexe. Et si on rajoute des phéromones à ces “irrésistibles”, plus personne n’en veut !  
Les Chinois sont encore plus marrants que la mayonnaise des genres ; ils font dans le mélange des arn : on devient plante, disent-ils, grâce à des micro-arn. Danger des plantes transgéniques mais aussi bien des haricots bouffés dans la chambrée. Les micro-arn rentrent comme information dans notre corps et modifient l’expression de nos gênes. C’est ainsi que l’homme fait encore plus partie de la Nature. Action sur les métabolismes des lipides et notamment les diabètes, etc.  
Numéro 1 des Chercheurs : l’Académie des Sciences Chinoises. Harvard n’est plus qu’au 4ème rang !  


\*  


Philipp K. Dick depuis longtemps à cristallisé les théories de Hugh Everett, tandis que nos artistes geigneurs des anneaux franciliens et pas même borroméens produisent des travaux topologiquement lamentables en regard de ça : Orlan à beau se faire charcuter sous toutes les coutures en hommage à Charcot, jamais elle n’atteindra ces dimensions ; elle a beau retourner sa couenne : aucun pli baroque. Rien que l’inévitable moi-peau, un concept purement universitaire aussi réversible que toutes les vestes de Sollers, et lavable comme une capote.  
Quant à Buren, notre installateur sur mesure, notre petit décorateur d’intérieur, sa platitude et son humour nous endorment à juste titre (“et toile à matelas !”). Dire qu’on a osé publier ses écrits complets ! Comme si ça pensait !  
Toujours, toujours l’opérette.  
Bien pire que ça.  
Car du moins Offenbach a nourri Rimbaud et Visconti a su y trouver des mélodies sublimes pour le suicide d’un grand Roi.  


  
_Jean-Claude Vogel._