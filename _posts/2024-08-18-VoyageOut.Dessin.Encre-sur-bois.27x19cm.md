---
title: Voyage Out
sous_titre: ""
date: 2024-08-18 14:46
date_document: "Après 1984"
tags:
    - document
    - dessin et gravure
    - Cosmologie Onuma Nemon
fichiers:
    - "/VoyageOut.Dessin.Encre-sur-bois.27x19cm.jpg"
---

Dessin. Encre sur bois. 27x19cm

Don à la Bibliothèque Kandinsky de Beaubourg.
