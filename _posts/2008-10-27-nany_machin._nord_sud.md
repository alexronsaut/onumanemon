---
title: "Nany Machin. Nord & Sud"
date: 2008-10-27 19:14
fichiers: 
  - Nany_Machin.Alumbrados.pdf
sous_titre: "Les Adolescents. Été" 
date_document: "1989" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

Nany Machin, c’est mon nom, je veux tout embrasser, tout emporter au fur à mesure, sans recours, sans aucun retour possible en arrière.  
Parce que sorti sans blessures de la fosse aux lions, je suis un fleuve plus impitoyable que le torrent de feu qui fit envoyer à Rimbaud un bas à varices le 27 mars 1891, que l’écriture _directe_ empruntant même sa graphie à l’immédiateté intensive n’est pas tout de moi, mais d_oit être dite_.  
C’est _Commode_, le premier, qui, loin d’une humeur facile, signalé plutôt par ses débauches et ses emportements, m’incita par son exemple à ce   
travail, craignant sans doute moi-même l’athlète qui viendrait m’inscrire historiquement dans mon bain de langue, étranglant les projets en cours, liés ensemble comme trois masses de biens jamais dépensés.  
Je fais en sorte de laisser un interligne suffisamment aéré (comme les Pyrénées), ce que la loi interdit dans les actes authentiques, de façon à pouvoir intercaler un fragment oublié tel qu’“_étranglant_”, quatre lignes au-dessus de celle-ci.  
C’est d’un _Vrac_ qu’il s’agit donc, apparemment irréductible, et cependant toujours possible à reprendre dans le mouvement biographique, on le verra.  
Il est bon de préciser aussi qu’il n’y a pas d’autre raison à ce récit que l’Aventure où nous fûmes lancés d’abord à quelques-uns changeants et mouvants, devenus des milliers malgré moi ou du moins bien au-delà de moi, par la transcendance d’un Grand-Oncle Gitan de Buenos Aires, enfoui dans la plupart de nos mémoires de famille, mais qui ressurgit sous la forme d’un cadavre baroque par l’intérmédiaire d’un Notaire,   
personnage important du Cours de Gourgue à Bordeaux, et, pour moi, proche de Mauriac et de tous les chais _un peu frais_ de la ville.