---
title: Le Grand Champ
date: 2024-08-18 14:17
sous_titre: ""
date_document: 1971
tags:
    - document
    - texte
    - HSOR
fichiers:
    - "/le-grand-champ.pdf"
---

_Ce poème est dans la ligne de Pr’Ose !_

Voilà mes ancêtres atrocement poussés sur le port, à Saint-Michel, préférant Sumer, Gilgamesh, la Renaissance et sa débâcle finale...

À l’heure certaine de l’Aube, je me réveillais ceint de bandelettes, corps momifié en érection, voiture en route vers Hadès, radicalement seul, ni vivant ni mort, sans heurt et sans histoire, force inutile de n’avoir aucun appui. Alors on est l’être-même, et l’on sait que ce désespoir durera jusqu’à la fin à moins que la Fée ne surgisse.

J’ai connu les flots flous énormes des jouissances hors des possibilités de transport habituelles, enchaînant d’un postérieur à l’autre comme on peut prendre le bus trois fois avec le même ticket, crachant ma monnaie partout sur le sol et les chaises.

J’ai vu à Manchester toute la suie, ensuite prié devant les ocres cheminées des Fées. “On nait trop petites, on pourra rien défaire, on a facilement retenu ce qui nous fait ne pas être, la Voie lactée jetée dans l’eau.” 

_(lire la suite…)_
