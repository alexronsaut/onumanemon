---
title: "Ligne des Adolescents."
date: 2007-11-15 20:56
fichiers: 
  - 211.pdf
sous_titre: "1967. Dits d'Elles" 
date_document: "Novembre 1968" 
tags:
  - document
  - OGR
  - texte
---

(Zinaïda) Alors il pleut beaucoup ; beaucoup de vent, un vent chaud. Le matin Nicolas me porte une jacynthe blanche. C’est Nicolas qui a eu l’idée d’une roulotte de théâtre à travers la France. Nany c’est plutôt le Moulin. Peut-être que nous fuirons dans le Nord comme Prosper ! C’est une idée d’Henri. L’anatomie du Parc Bordelais est très belle sous le soleil. Antón et Nicolaï arrachent des pancartes sur tout le trajet. Il dit qu’il ne sait pas même où il va ! Il pense sans cesse à son roman.  
(Aube) Samedi matin il pleut, je couds, je lis des poèmes. Le repas a été assez peu supportable ches les cousins Artaud, à cause de Jacquie, de leurs grands-parents, d’Annie et surtout de cet abruti de Bambi, un de leurs copains ! Ils ne savent toujours pas qui a tiré des coups de fusils dans la vitrine l’autre nuit ; les gendarmes ne leur ont rien dit. En modelage statuaire il me donne une jonquille. Il m’a parlé de Lulu, la tante de Nycéphore qui était voyante et qui guérissait les “envies”. Il m’a donné un poème sur elle de Nycéphore.  
(Lydou) Toujours tous les jardins à pieds, sans cesse, malgré sa fatigue. Ensuite nous allons tous deux chacun chez notre docteur ! (Il tousse encore beaucoup !) Le docteur nous parle d’un de ses collègues un peu fou, près de l’église Saint-Augustin, qui cherche à créer un cerveau infini : ça intrigue Jean. Sans cesse, allers et retours : Le Styx, les Abattoirs, la Cathédrale, le Jardin Public. Il parle de “L’Homme au crâne rasé”, de “La Source”, de “La Fin”.  
(Aube) On pose pour des croquis habillés les uns les autres dans la salle de gravure ; il me fait la tête, mais il finit par me graver. Il veut repartir voir Nicolas à Paris ; j’insiste pour qu’il reste. Nous restons un moment accodés ensemble à la fenêtre de notre classe de fusain de l’an dernier, notre première année ! Il allait me dessiner lorsqu’il a soudain vu que je ne le choisissais pas pour modèle !  
(Ramona) Il me souhaite ma fête avec un bouquet de violettes et un pot de sangria. Il boit énormément. Il complètement rompu avec sa famille ; il n’est pour ainsi dire jamais à Ste Monique. Nous n’allons pas au “Pétanque” ; il a su que je posais nue et ça l’a rendu furieux. Il fait beaucoup de vent le soir ; voilà deux jours il faisait beau sur les quais, _le jour où l’inspecteur est venu_.