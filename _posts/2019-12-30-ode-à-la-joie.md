---
date: 2019-12-30 1:16
title: Ode à la Joie
sous_titre: Joël Roussiez
date_document: "2011"
fichiers:
- "/Ode à la joie.pdf"
tags:
- texte
- DAO
- document

---
**Joie, joie, belle étincelle** (Schiller, Goethe, Ono No Komachi)

Qui chevauche sans bruit à l’aurore d’un jour, qui va joyeusement par un matin radieux, qui fait tinter les boucles du harnais, et qu’entends-on dans le feuillage des bois, c’est la rivière au bruit de gravier fin, nous y prendrons un bain, dépêche-toi, je veux sentir ta main... Le chemin est étroit ainsi cheminent-t-ils l’un derrière l’autre, la chaleur monte parmi les fougères et les herbes hautes qui penchent sous la rosée et mouillent leurs chausses.

_(lire la suite…)_