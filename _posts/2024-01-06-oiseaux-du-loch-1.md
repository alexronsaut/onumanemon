---
title: Oiseaux du Loch
date: 2024-01-06 8:47
sous_titre: à Typhaine
date_document: 2023
tags:
    - document
    - Cosmologie Onuma Nemon
    - dessin et gravure
fichiers:
    - "/oiseaux-du-loch-1.jpg"
---

Encre sur Japon. Format 30 x 45 cm
