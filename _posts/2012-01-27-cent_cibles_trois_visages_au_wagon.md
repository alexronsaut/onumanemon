---
title: "Cent Cibles : Trois visages au wagon"
date: 2012-01-27 21:52
fichiers: 
  - Trois_Visages_au_Wagon.jpg
sous_titre: "Acrylique blanc sur Cible 21 x 21cm pour carabine 50m" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - peinture
  - extension
---

Les Cent-Cibles font partie de l’exposition des États du Monde à l'URDLA du 4 février au 13 avril 2012. 

_NDLR_

Les Cent-Cibles sont liées à la discipline du tir (aussi bien tir traditionnel que Kyudo dans le cadre de la Cellule Sabaki), mais également au Pays des Morts ; de là cette blancheur de spectre des dessins. Sans oublier cette autre hantise de Cozens avec sa typologie aussi restreinte des paysages que celles des métaphores pour Borgès.  
Les dessins sont re-composés à partir de milliers de petits croquis (ceux-ci parfois combinés ensemble, juxtaposés : aboutissant à des perspectives et des points de fuite “hasardeux et fantaisistes”) pour s’approcher au mieux des visions de rêve du Pays des Morts (contrée féerique et communiquant une exaltation peu commune du côté de “la porte des _shen_”), dont on explore à chaque fois un nouvel endroit et dont il est possible d’établir une Carte assez précise : avec d’un côté de magnifiques gares baroques et des réseaux ferrés infinis à partir de points de vue redoutablement vertigineux (à l’image de Little Nemo), de l’autre une “schizocité” avec sa rue principale gorgée de cinémas, ailleurs des paysages montagneux, des lacs, etc. Et bien sûr différentes entrées et voies d’accès, malgré le fait qu’il y en ait une de priviliégiée.  
O. N.

[Exposition à l’Urdla des États du Monde](https://urdla.com/blog/etats-du-monde-cosmologie-onuma-nemon/)