---
title: "La Mesure du Tombeau"
date: 2012-02-24 23:22
fichiers: 
  - Bkup_lamesuredutombeau.pdf
sous_titre: "(Hommage à Verlaine)" 
date_document: "2012" 
tags:
  - document
  - DAO
  - texte
---

Ce texte aussi beau que du Gracq est du Travailleur Roussiez. 

_O. N._   
  
**La mesure des tombeaux**

(hommage à Verlaine)

 Nous sommes auprès du tombeau et scrutons les alentours où il n'y a rien, rien d'autre que nous ici et nous le savons bien, puisque nous sommes venus dans cet endroit seuls. Nous n'avons pas souhaité être accompagnés, il nous semblait que c'était à nous de faire la démarche, d'y aller nous-mêmes malgré la difficulté. Il a fallu traverser la plaine où guettaient des lanceurs et des piocheurs, il faut faire attention, nous a-t-on répété, lorsqu'ils te prennent, tu n'en a plus pour longtemps. Et nous avons répondu qu'on n'en avait de toute façon plus pour longtemps... Le tombeau est vide, il est fait en béton banché d'une seule pièce hors le couvercle qui est entreposé sur deux chevrons juste à côté. J'ai apporté mon mètre ruban, il faut mesurer le tombeau en longueur, largeur et profondeur, l'étude doit le spécifier. Pour mesurer la profondeur, il va me falloir descendre à l'intérieur... Je regarde les environs, il n'y a personne qu'une lande déserte de bruyères et d'ajoncs; un arbre court sur la gauche est si décharné qu'il ne peut rien dissimuler, un autre à trente mètres de même ; à côté se trouve une pierre assez grosse..., assez grosse pour cacher quelqu'un, je le pense et me le dis. Et puis je chante : des arbres et des moulins sont légers sur le vert tendre... Que faire, la crainte s'est insinuée en moi tout doucement alors que je chantais, elle est descendue et s'est emparée de quelques endroits, dans le ventre peut-être ou bien dans les épaules... Quelqu'un y guette quelque chose, c'est moi qu'il guette, je ne sais..., et s'il surgissait... Je tombe en rêve dans le tombeau que l'on bouche, personne ne m'entends plus, la lande est silencieuse... Descendre m'inquiète.   
 _lire la suite…_  


ﾠ

ﾠ