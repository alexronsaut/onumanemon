---
title: "Henri & les Skyboys"
date: 2010-05-30 18:28
fichiers: 
  - Henri__les_Skyboys.pdf
sous_titre: "Amères Loques, Livre de nouvelles de Nicolaï" 
date_document: "1979" 
tags:
  - document
  - OGR
  - texte
---

**Henri et les Skyboys**  


  
Louis été déjà allé plusieurs fois à New-York avant d’avoir idée d’y faire venir travailler Henri ; il en avait même ramené des cartes peintes de deux anciens buildings, cartes toutes en hauteur de près d’une trentaine de centimètres sur dix à peine de large représentant l’Équitable Building et le Metropolitan Life Insurance Building.  
Il était là au moment où les régiments de Noirs de Harlem combattants de l’Enfer s’engouffraient sous l’arc de triomphe près du Flat Iron, défilant de la 23ème à la 42ème en passant devant le spectaculaire rideau de pierres précieuses de Central Park tandis qu’un speaker fou dont la voix était diffusée par des haut-parleurs dans les rues vendait des appartements bucoliques de Queens à 150 000 dollars comme des petits pains. « Vendez-leur, disait-il, cet espoir des éléphants dans la ville qui renversent les bâtiments comme les acheteurs submergent les annonceurs ! » C’était avant que les Italiens chassent tous les noirs vers Harlem. Cole était là, et Georges aussi.  
« Mon sang dans votre bouche, votre sexe dans mes veines : quelle chance ! Mes gestes réinterprêtés par votre corps. »  


**\***  




  
Ces textes ne font pas partie du choix opéré pour la publication du volume _OGR_ chez Tristram en 1999. Ils ne sont pas placés ici dans l’ordre du volume définitif.  
_I. R._