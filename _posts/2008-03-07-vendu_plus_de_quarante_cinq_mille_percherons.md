---
title: "Vendu plus de quarante cinq mille percherons"
date: 2008-03-07 23:15
fichiers: 
  - vendu-plus-de-quarante-cinq-mille-percherons.pdf
sous_titre: "Extrait de Nous et Nos Troupeaux" 
tags:
  - document
  - DAO
  - texte
---

On se reportera à l’émission de Veinstein du Lundi 23 Février 2009 [sur France-Culture](https://www.franceculture.fr/emissions/du-jour-au-lendemain/joel-roussiez).

Publication : [Contre-Feux](https://web.archive.org/web/20081216013335/http://www.lekti-ecriture.com/contrefeux/) (archives)