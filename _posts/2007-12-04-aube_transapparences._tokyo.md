---
title: "Aube : Transapparences. Tokyo"
date: 2007-12-04 22:17
fichiers: 
  - 341.jpg
sous_titre: "Les Adolescents. Ligne d’Aube. Autres villes" 
date_document: "1978" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - peinture
---

“Contre un ordre misogyne (mot rare jusqu’au XIXème siècle), _j'exclose_ des petits morceaux de peau géographique trouée à l'emplacement des mers (ici aussi la terre tremble !). Je donne des “_Pieces of Life_”, morceaux de corps, de vie, de temps (et c’est une posture sexuelle, une posture de production, une PROSTITURE).   
Je désigne la fragilité de la matière (papier de soie), de la couleur (encres, or), l’éphémère buvant dans la couleur primaire la bulle d’air sortie du liquide glauque. Le papier se gonfle, se tend (en quelques heures le désert le plus aride peut se recouvrir d’une abondante végétation) ; j’opère sur la matière de la feuille un travail de chirurgien, replie au vaccinostyle des bourrelets de peau et de soi et de chair où mouille la partie lombaire de la côte du Pérou, m’abaisse au-dessous de l’Équateur : la colonne de Colombie me respire.  
Alors que le temps a passé, le papier se colle (à l’air) à l’eau de la vitre (le détacher doucement avec la pointe du vaccinostyle sans en déchirer la moindre membrane, comme quelque bijouterie précieuse - des yeux - ; je pense à mon père presqu’aveugle) ; ce sont _presque_ des transparences, des _Trans-Apparences_, et fantoches et grisées sur les murs ; les travers des apparences se font là, aujourd’hui, à travers ce gris.”   
Aube Lambrée. Printemps 1978.