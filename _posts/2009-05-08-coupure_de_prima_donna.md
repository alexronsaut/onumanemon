---
title: "Coupure de Prima Donna"
date: 2009-05-08 15:49
fichiers: 
  - CoupurePrimaDonna.pdf
sous_titre: "Orphelins.Groupe Folie-Méricourt.Automne.1" 
date_document: "1973" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

**Extrait de la version définitive des _États du Monde_ (en cours de réduction)**  
  
Autant ce regard sur le Roman de Marie Vetzera jeté vers l’arrière à partir du 7 octobre 1972 s’envole avec la force du négatif hallucinatoire par cette fin primaire du hercynien au-delà des portes rouges de la conduite intérieure, en arrière, jusque sur les hauteurs froides et silicieuses de la Forêt Noire, au-dessus de l’essence, du lin, de la culture industrielle du chanvre et du houblon, s’enroule dans les plis des drapeaux claquant un instant au-delà des stations, se mélange avec les slogans, se combine aux calicomanies gothiques, _autant vous voilà complètement projeté vers l’avant, ce goût que vous dites, d’un mouvement caoutchouteux et tendre ailleurs qu’en elle_, semblable au jeune pigeon duveteux que Prosper avait vu errer sur les décombres du Phœnyx, à travers les tamaisies sauvages du parc abandonné de la maison détruite, les repousses d’érables et les herbes folles, ricin et toutes sortes de raisin sauvage vert et violet ; oisillon égaré, cou plumé, tête chauve et œil de perle noire, pattes à rares plumetis.  
  
Chaque nuit Prosper avait une épreuve à franchir avant d’atteindre la Forêt Noire. Une fois c’étaient 8 chiens féroces qu’il lui fallait renouveler pour sa garde, devenus des sortes de boucliers, cauchemar qui se concluait par des brûlures stomacales et une puanteur pourrissante excrémentielle le réveillant en pleine nuit avec une terrible envie de vomir. Une autre fois c’étaient des serpents géants, des boas, qui s’étaient insidieusement glissés, déployés, déroulés menaçants _autour des pieds d’une petite fille qu’il n’avait pas_, le temps qu’il grave ses initiales sur un tronc. Il se précipitait, disant à celle-ci de ne pas bouger, tranchant les têtes de serpents à coups sourds de hachette. Une autre fois, malgré les travaux énormes entrepris dans le grand salon circulaire du Phœnyx, c’étaient des failles qui subsistent et des infiltrations au plafond de l’entrée. Chaque nuit avait sa progression négative, comme s’il ne cessait d’avancer vers une régression toujours plus terrible.  
Prosper advenait sans répétition ; il n’a jamais imité personne ; il se posait en soldat, avec sa logique du chaos et de la défaite, ses difficultés gastriques ; sortant de l’enfer abdominal de la guerre de 14 et d’une obscurité où tout se coagule et se confond pour débarquer sur le théâtre aux cent mouvements, ce froid de neige, ces vents gris ; torturé par ses chifres : 4444, 2 fois et 2 & 4, cherchant une forme désespérée mais absolue, invisible mais répandue partout au milieu de cette glaciation de la vue, tournant la tête de 3/4 arrière vers la droite, vers la Bavière et ses petits lacs, puis plus avant vers la Bohème.