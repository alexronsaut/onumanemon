---
title: "Asile (extraits)"
date: 2010-01-17 12:37
fichiers: 
  - Asile.pdf
sous_titre: "Livre Poétique de Nycéphore 1964-1984 Poème n°24" 
date_document: "1965" 
tags:
  - document
  - OGR
  - texte
---

_Ce poème fait pendant chez Nicolaï à_ Ave Maria d’un Domini_, de la même longueur._ I. R.  
  
**24. Asile**  
  
Haine du draconcule au fond des établis ;  
Puis en sortant devant les terrasses : gentianes,  
Surprises des pensées aux souffles affaiblis,  
Apodes hors des crasses à ramper. Ô campanes ! _\[………………………………………………\]_