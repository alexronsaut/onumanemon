---
title: "Colomb : Santoña"
date: 2008-01-28 22:20
fichiers: 
  - Santona.pdf
sous_titre: "Ligne de Don Qui Domingo" 
date_document: "1992" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

 _**Santoña**_   
« Cristoforonépatao !  
— Berlitz ? »  
Je ne comprenais pas ce que voulait me dire le prisonnier Chinois en train d’éxécuter ses Taos, le matin dans la cour. C’est que pour lui, Colomb n’était pas dans le Tao. Pas de fenêtre, pas d’autrui, pas de   
lointain, pour le Tao.  
« L’Été : le cœur, au matin ! Terre sera rendez-vous midi. Toi travail séries : corde, sac. Priorités ! Et longues suites poings et pieds (en ligne). Préparatoires ! Jamais extension absolue, au début ! Attention articulations ! Mouvements plus lents, d’abord ! Mais katas ou bunkaï : pas gym suédoise ! Hélas, Funakoshi lui-même ! Avant tout résistance ; tireur de substance parmi d’autres, et sans chef. Si vous réclamer concepts tout faits, moi pas donner ! Kisémé : sans sabre et sans poing. À gauche : centrale atomique ! Bunkaï difficile : formes intermédiaires perdues ! Musculation avec poids, si besoin, mais surtout : exercices forêt ! Semelles lestées, petits-haltères, charges légères chevilles et poignets. Ni abdos ni pompes, principe ! Tractions remplacées par escalades. Oui. Ça Japon ; mais pensée Chine ! Oui. »