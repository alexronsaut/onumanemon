---
title: "L’âme du visage"
date: 2011-06-17 22:55
fichiers: 
  - Lame_du_visage.pdf
sous_titre: "Texte de Joël Roussiez" 
date_document: "2011" 
tags:
  - document
  - DAO
  - texte
---

Suite de ces petits textes comme fragments d’Éternité, dont Roussiez a le secret. 

O. N.   
  
**L'âme du visage**

Nous passons l'éponge, effaçons ainsi ce qui s'est passé, du moins on veut le croire car sur nos visages se marquent imperceptiblement les émotions que nous vivons; sur ce tableau des impressions s'inscrivent donc les choses que nous avons vécues, non les choses matérielles mais en quelque sorte l'empreinte qu'elles laissent sur nous; et encore faudrait-il dire que passant par notre système émotif, elles lui donnent des impulsions qu'il transmet à notre peau ou grave, selon l'expression de Jean. Ceci expliquerait le vieillissement de ces dernières qui se rident tout doucement comme des parchemins... Pourquoi emprunter à l'extérieur l'état de nos cœurs, peut-être parce que nous ne sommes sûrs de rien, que nous ne souhaitons aussi rien exprimer de particulier, peut-être parce que nous ne sommes qu'une caisse qui résonne aux impulsions; en bref, nous déménageons ainsi sans cesse de nous-mêmes mais comme il est impossible de quitter nos corps, nous restons en surface, suspendus en quelque sorte entre deux lieux; c'est ainsi que la peau se fripe... L'explication des vieux était toujours amusante et le jeune Louis aimait bien les faire parler. C'est d'eux qu'il tenait ce qui semble avoir été un savoir profond « bien qu'au fond, disait-il, ce ne sont que des anecdotes »... Toute vie est transposée de sa caverne originelle vers un conteneur social, répétait le vieux Jean par exemple et Louis écoutait cela qui avait des résonances en lui mais lesquelles, on ne le sait pas. Pourquoi le vieux Jean portait-il le pantalon large des marins, ce qu'il n'avait jamais été, parce que, disait-il, le ventre du navire était sa patrie. « Je suis citoyen des pays de l'absence de gloire , je travaillais dans les soutes avec mes comparses mais ne va pas croire qu'on se sent protégé à l'intérieur de ces murailles et de ces forts, suivant l'expression qui désignait pour nous les machines et la salle de l'entreprise pour laquelle je travaillais qui fabriquait en acier des véhicules automoteur; ne va pas le croire ou t'imaginer qu'à l'heure du casse-croûte et dans l'agitation de la journée, une tranquillité s'installait dans nos cœurs comme celle du paysan qui travaille ses champs. Au contraire, Louis, il y avait dans notre fébrilité quelque chose de trop agité, on le sentait sans pourtant souhaiter suspendre l'activité de l'usine ou bien encore souhaiter partir de ce poste qui nous fermait cependant au dehors et au reste des entrepôts... » Le jeune Louis écoutait ces discours étranges qu'effaçaient les jours nouveaux sur le tableau des événements qu'on tenait à jour à la maison des informations.... C'était une bâtisse, dira-t-on, à trois volets de containers sur une longueur de cent cinquante trois mètres; elle était conçue pour être déplacée suivant les besoins, ainsi sa structure était faite de colonnes et traverses en carton pour être légère. Louis, pour parler aux gens des archives et des informations qui écoutaient avec plaisir ses enthousiasmes de jeune homme, s'y rendait plutôt vers la fin de la journée quand le travail ralentit...  
  
_lire la suite…_