---
title: "Isba Nicolas"
date: 2009-12-31 17:29
fichiers: 
  - Isba_Nicolas.pdf
sous_titre: "Livre Poétique de Nycéphore 1964-1984 Poème n°28" 
date_document: "8 Février 1965" 
tags:
  - document
  - OGR
  - texte
---

_Ce poème fait pendant chez Nicolaï au poème_ Ombres_, du même nombre de vers._ I. Revay  


 **28. Isba Nicolas**  
  
De mon veston, levée la poudre,  
Lançant mon couteau au soleil  
Sur le chemin qui nous voit sourdre  
Quand j’aurai beaucoup faim d’orseille.