---
title: "Le Mouvement !"
date: 2008-07-04 17:32
fichiers: 
  - Le_Mouvement.pdf
sous_titre: "Les Grands Ancêtres. Ligne de Don Qui. Automne" 
date_document: "1984" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_Où il est encore question de la prosodie, d’Aragon autant que du Maimonides Hospital, mais surtout du “duende” des fuyeurs des Tribus diverses de la Cosmologie, des Incarnations et de la façon dont l’Inscription transforme les corps, de la théorie des veilleuses et de la formidable invention réclusive d’Aloysius Bertrand ou du peintre Luncarné._

_I. Revay_

Ne nous trompons pas : l’Oncle ne confondait pas l’emportement lourd, sans nuances, la célérité violente et massive du fascisme, avec la légèreté de ses chers “_fuyeurs_”. Il savait que la plupart des mouvements avaient lieu dans une vrille sur place, comme dans l’escalier d’ébène mauve d’Eliseo.  
Disons de ce mouvement qu’il est simple, et qu’avec un vieux short ouvrier, un débardeur noir et des chaussures de fortune, en courant sur les plateaux aux ossatures crayeuses des Charentes, on peut y atteindre. Aux moments où tout paraît désespéré, dans la pire Guerre, le plus désastreux conflit, la course offrant un peu de sueur assez tôt, avant les tâches, et l’illumination revenait.  
On avait de ces bonheurs simples du Dimanche, avec le coq ; des   
bonheurs fermiers, des bonheurs contigus à la terre même ; rien de plus compliqué.  
Le Mouvement tenait à cela : retrouver dans les premières lueurs   
matinales toute l’archéologie de la Tribu, tout le mérite des Poussées Ouvrières.