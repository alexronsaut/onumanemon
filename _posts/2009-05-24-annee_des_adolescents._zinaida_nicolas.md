---
title: "Année des Adolescents. Zinaïda & Nicolas"
date: 2009-05-24 22:09
fichiers: 
  - AnneeDesAdosZinaida-Nicolas.pdf
sous_titre: "Les Adolescents. Été" 
date_document: "Avant 1984" 
tags:
  - document
  - OGR
  - OGR (épanchements)
  - texte
---

**Extrait de la version définitive de la Cosmologie (en dehors des _États du Monde_)**  
 **Le 6 janvier**  
Z. N. Zinaïda s’est endormie sur le livre de contes de l’Épiphanie où les noirs tellement aptes à ramasser les déjections dans les rues et à les jeter dans le fleuve où ils vont ensuite se laver, s’étaient déguisés en boeufs bouffons dans les arènes, aussitôt piétinés par les taureaux ; puis d’autres défilaient et dansaient en costumes carnavalesques avec des clochettes et le visage enduit de cirage noir sur noir, guidés par le chef, le chorizo, dit aussi “le boudin”, entrant dans les maisons pour réclamer les restes de tripes du cochon mort et s’enguirlandant avec, tout dégoulinants de graisse et de sang ; puis il vendent à la criée ce dont personne ne veut : la couenne, les poils, le groin, la queue, tandis que d’autres pour montrer la puissance de leur machoire d’âne soulèvent ce qu’il y a de pire autour d’eux : plots de béton, armatures, bureaux d’écoliers, sacs de guano de cent kilos. Quand ils passent les habitants crient “To ! To !”, qui est le cri du cochon, ou imitent la chèvre, ou leur crachent dessus, y compris les indiens qui ne travaillent pas dans les champs. Puis pour se détendre après tout ça ils vont blanchir les murs à la chaux. On dirait une histoire de sa mère. **Le 8 Février**  
Z. N. Demain Nicolas veut assister au récital de son ami Dominique Merlet au Grand-Théâtre. Il a rêvé d’un horrible jugement et que Zinaïda enceinte de Nycéphore était reçue chez lui, dans sa maison qu’il occupait avec une Zinaïda qui n’était plus Zinaïda ! Une maison du côté du Dorn avec ses quinze galciers autour du glacier géant du Gorner, immense reptile allongé. Le chien savant qui fait des bonds sur la route. Et les filles féministes qui viennent frapper à la porte ! Nicolas leur explique que _c’est une distraction du chien_.