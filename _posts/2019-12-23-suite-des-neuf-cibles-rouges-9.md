---
date: 2019-12-23 9:30
title: Suite des neuf cibles rouges
sous_titre: 'Cible n°9 : Mao de dos'
date_document: Après 1984
fichiers:
- "/Cible rouge.9.Mao de dos.jpg"
tags:
- dessin et gravure
- Cosmologie Onuma Nemon
- document

---
Pistolet 10m & dessin à la plume sur anciens cartons de cibles format 14 x 14cm en 5/10èmes