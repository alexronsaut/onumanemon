---
title: "Bruges en lambeaux"
date: 2007-11-20 16:47
fichiers: 
  - 305.pdf
sous_titre: "Brouillon de Poème" 
date_document: "1966 et Octobre 1969" 
tags:
  - document
  - HSOR
  - texte
---

Ce texte, qui n’est qu’un fragment d’un plus grand texte, s’intégrait dans un projet demandé en 1969 par le Service de la Recherche (situé alors Centre Pierre Bourdan) ; il fut utilisé en partie bien des années après dans une émission des Nuits Magnétiques consacrée à Bruges.   
D’autres morceaux allèrent nourrir le Livre Poétique de Nicolaï.  
Suivent une notation rapide et une lettre de Nany à Aube, de la même époque.  
Isabelle Revay