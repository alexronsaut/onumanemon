---
title: "Courbet sur un mur de New-York"
date: 2007-02-10 20:48
fichiers: 
  - 39.jpg
sous_titre: "Maison de la Danseuse à Greenwich Village. 18cms x 24cms" 
date_document: "1978" 
tags:
  - document
  - OGR
  - photographie
---

A paru dans le volume ON !