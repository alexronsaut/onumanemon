---
title: "Dimanche en Hiver (extraits)"
date: 2010-01-17 14:06
fichiers: 
  - Dimanche_En_Hiver.pdf
sous_titre: "Livre Poétique de Nycéphore 1964-1984 Poème n°27" 
date_document: "1965" 
tags:
  - document
  - OGR
  - texte
---

Ce poème fait pendant à Le Rêve de l’Œil Mort dans le Livre Poétique de Nicolaï. 

I. Revay   
  
**27. Dimanche en Hiver**  
  
Tranquille enthousiasme à falloir  
Qui colle mon œil mort pleurant.  
Chassons les ombres des couloirs  
Gouvernant des délicatesses  
Où les lettres grattées sont grises.  
  
Dans les dimanches d’hébétude  
Des abrutis y scient des planches,  
Bouffées pliées de galon vert ;  
On transporte des billes noires ;  
Buvards, lœthé, après-midis.  
  
_etc…_