---
date: 2019-12-30 12:49
title: Cible rouge n°4
sous_titre: Profil en fuite
date_document: Après 1984
fichiers:
- "/Cible rouge.4.Profil en fuite-1.jpg"
tags:
- dessin et gravure
- Cosmologie Onuma Nemon
- document

---
Pistolet 10m & dessin à la plume sur anciens cartons de cibles format 14 x 14cm en 5/10èmes