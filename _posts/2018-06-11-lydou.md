---
layout: post
title: Lydou
date: 2018-06-11 00:00:00 +0000
sous_titre: Ligne des Enfants
date_document: Après 1984
fichiers:
- "/Lydou.pdf"
tags:
- texte
- Cosmologie Onuma Nemon
- document

---
**COURRIER DE LYDOU** (_extrait_)

Lydou & Jean font partie du troisième tome des États du Monde, celui des Enfants, et non pas du quatrième des Adolescents. Ils sont restés dans l’Enfance, et dans ce volume ils ne font pas partie de la Ligne des Escholiers Primaires, mais de celle des Orphelins Colporteurs.  
Jean fait du cinéma, et Lydou l’aide.

(_lire la suite..._)