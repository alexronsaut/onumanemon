---
title: Louis sur l'Atlantique
date: 2018-11-03 00:00:00 +0000
sous_titre: Marges des États du Monde
date_document: 1971
fichiers:
- "/louis-atlantique1971.pdf"
tags:
- texte
- Cosmologie Onuma Nemon
- document

---
LOUIS SUR L'ATLANTIQUE  
(1939-1945)

Au-delà de la plage (cette fois-ci c’est l’Atlantique), d’un bout de l’horizon à l’autre sur la mer, dans le petit jour gris espérant l’aube aux doigts de rose, on voit toute une armada de silhouettes massives de cuirassés et de croiseurs, puis plus légères de destroyers.

Derrière eux d’énormes navires de commandement hérissés d’antennes, puis ceux de transport et les bateaux de débarquement.

_(lire la suite…)_