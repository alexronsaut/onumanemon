---
title: Diptyque Chinois Pays de Montagne
sous_titre: ""
date: 2024-08-18 14:46
date_document: "Après 1984"
tags:
    - document
    - dessin et gravure
    - Cosmologie Onuma Nemon
fichiers:
    - "/Diptyque-Chinois-Pays-de-Montagne.Dessin.Encre-sur-papier-a-la-cuve.30x40cm.jpg"
---

Dessin. Encre sur papier à la cuve. 30x40cm

Don à la Bibliothèque Kandinsky de Beaubourg.
