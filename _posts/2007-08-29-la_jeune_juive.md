---
title: "La Jeune Juive"
date: 2007-08-29 17:42
fichiers: 
  - 231.pdf
sous_titre: "Livre Poétique de Nycéphore 1968-1984. Futur Antérieur. Poème n°34" 
date_document: "Mars 1983" 
tags:
  - document
  - OGR
  - texte
---

**34. La Jeune Juive**  
  
L'Hiver, sucre lent qui fond, on erre, jusqu'à se taire,  
Se terrer. L'air qui languit et qui tourne, c'est celui de  
La jeune juive  
Au-dessus des sombres nations, au fond de la vallée de la Ruhr.  
  
« Oh ! Toute cette chute d'hosties vives dans la bouche, ce sont  
Les gateaux de mon père pour les Pâques,  
Les vitraux froids par endroits de l'Évangéliste  
Mais d'une telle grâce !