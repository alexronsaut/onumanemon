---
title: "Basile, Zinopino, Morisson"
date: 2008-07-25 21:01
fichiers: 
  - Basile_Zinopino_Morisson.pdf
sous_titre: "Les Escholiers Primaires. Ligne de Nicolaï. Été. Lycée" 
date_document: "1979" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_**Le voyage à Pau**_  
 Au Lycée, on adorait tous Basile, le conducteur de bus des voyages   
 scolaires en fin d’année. Le reste du temps il nous apprenait les passes de foot et officiellement il était factotum.  
 Il était du coin. Il adorait, surtout l’hiver, avant le jour, péter à la hauteur de l’usine à gaz après s’être gavé de champignons du Sud-Ouest, fureur mégalomaniaque lui donnant l’illusion d’empuantir tout le paysage et imprégnant tellement son siège qu’il en conservait tout du long une aura d’une infection persistante à couper au sabre !  
 Il nous tenait des discours sur tout pendant le voyage : la Bible qu’il connaissait par cœur, et surtout la Genèse et le problème de la Trinité et du semblant, version italienne. Il nous disait que pour eux, le semblant c’était pas du bidon, c’était même tout l’inverse. Il aurait adoré travailler dans un Hospice mais il avait pas les diplômes.  
 L’arbre tordu, la veste rouge, il trouvait ça beau, le désordre sur la voie et la route parallèles, par ce temps gris pluvieux couvert : et surtout ce jour-là un morceau déchiré de carton, un vieux chiffon, du poil avec un con, un fion, un tronc, des tétons, un étron…  
 Il s’agissait du fil même sur lequel les perles de sa folie étaient enfilées.  
 Par contre tous les lycéens haïssaient cette visite à Lacq, cette “partie utilitaire” du voyage de fin d’année, après Pau, la poule au pot, le roi Henri, Sully-les-Mamelles, l’entrée en pente du château. C’est pour cela qu’on tua cette conne de gallinacée de prof d’Histoire, qui croyait nous faire rire en retournant ses paupières, avec l’aide de Basile, et qu’on en jeta les morceaux par les fenêtres du bus, en désordre !