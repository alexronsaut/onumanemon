---
title: "École Toussaint"
date: 2007-03-30 10:12
fichiers: 
  - 104.pdf
sous_titre: "Livre Poétique de Nicolaï. 1968-1984. Poème n°5" 
date_document: "Novembre 1968" 
tags:
  - document
  - OGR
  - texte
---

Livre Poétique de Nicolaï  
  
Les poèmes des Livres Poétiques (1964-1968 et 1968-1984) sont indiqués ici par leur numéro dans chaque volume, chaque poème correspondant au poème qui porte le même numéro chez le frère “en face”.  
Isabelle Revay.