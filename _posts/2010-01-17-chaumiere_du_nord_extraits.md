---
title: "Chaumière du Nord (extraits)"
date: 2010-01-17 17:20
fichiers: 
  - Chaumiere_du_Nord.pdf
sous_titre: "Livre Poétique de Nycéphore 1964-1984 Poème n°31" 
date_document: "Septembre 1965" 
tags:
  - document
  - OGR
  - texte
---

_Ce poème de 132 vers répond à_ Anna Shern _dans le Livre Poétique de Nicolaï._  
I. Revay  
 **31. Chaumière du Nord**  
  
Chaumière, chevreuils et hunter  
“On chasse les rats !” dit Guillaume  
Dans les prairies des économes  
Alors qu’il attendait Gessler.  
  
On guettera notre volaille ;  
“Shooting is my plus grand pleasure !”  
Blotissement que hait Blücher,  
Vandale au présent de mitraille.  
  
Et au-delà, cristallisés,  
Récursoires des morts en crise,  
Les arbres, splendeurs irisées,  
Sucre et chocolat par endroits.  
  
_etc._