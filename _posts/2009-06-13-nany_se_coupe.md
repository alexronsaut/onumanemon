---
title: "Nany se coupe"
date: 2009-06-13 12:42
fichiers: 
  - Nany_se_coupe.pdf
sous_titre: "Les Adolescents. Été. 1" 
date_document: "1975" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

À chaque fois que je me rase, je songe à la coupure que je me suis infligée voilà peu en voulant me couper la gorge, comme on marche sur des couleuvres peintes qui tout à coup partent en sifflant et sont des vipères. Et lorsque je revisse le manche de mon rasoir, curieuse synesthésie, ce sont les bronzes du monument aux Girondins qui surgissent ! Ainsi on se vrille sur soi et on creuse, quelle que soit l’heure du jour, indépendamment de toute actualité, des circonstances matérielles, et au-delà de toute nécessité, bondissant hors de la pièce exiguë où l’on se trouve. Voilà deux ans à peine, je me postais ainsi à l’écart sur les rochers brûlants de la route de Jerez _pour ne pas en perdre les mérites_, constatant une fois de plus la platitude des images devant les spires de cette réalité poussiéreuse, tout en ignorant comment les absorber et les retenir à tout prix !