---
title: "Parcours du Parc n°3"
date: 2008-02-28 22:06
fichiers: 
  - Parcours_Parc_3.jpg
sous_titre: "Les Adolescents. Joyelle & Hill" 
date_document: "1978" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - photographie
---

Format 13 x 18 cms. Cette photographie faisait partie de l’exposition au “Quartier” en 2005