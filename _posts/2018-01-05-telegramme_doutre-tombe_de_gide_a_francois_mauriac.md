---
title: "Télégramme d'outre-tombe de Gide à François Mauriac"
date: 2018-01-05 21:07
tags:
  - billet
---

"L'enfer n'existe pas – STOP – Tu peux te dissiper – STOP – Préviens Claudel – STOP – Signé : André Gide"