---
title: "Au Verger des Anciens"
date: "2022-01-16 16:31"
sous_titre: "Récits de Joël Roussiez"
date_document: "2016. La Rumeur Libre"
fichiers:
  - /au-verger-des-anciens-femme-soldat.pdf
tags:
  - document
  - texte
  - DAO
---

**Au lecteur,**

S'il est bon de faire entendre des histoires, il faut préciser que les nôtres, Lecteur, ne sont pas à entendre au sens ancien qui veut dire comprendre mais au sens moderne qui passe par l'oreille car, dit Cicéron : « le discours doit chercher le plaisir de l'oreille », ce que rapporte Aulu-Gelle. C'est donc lorsque tu liras par les yeux que tu entendras par les oreilles ce que disent ces paroles dégelées, selon Rabelais. Ainsi sache, lecteur très cher, que ce que nous racontons, c'est pour la musique !  
Mais sache encore que nous ont guidé quelques maîtres anciens qui prirent soin de nous offrir quelques fruits et, dégelant nos doigts gourds, corrigèrent nos maladresses en ces temps divers, ou d'hiver, où rien n'est plus agréable à l'oreille que d'être frottée par d'autres…  
À bon entendeur donc, salut ! Et qu'il en soit pour toi comme du feu roi François dont Martin Du Bellay écrivit : « toutesfois jamais adversité qui luy peust advenir ne luy abaissa le cœur ».

(lire la suite…)