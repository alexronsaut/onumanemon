---
date: 2021-10-30 11:45
title: Petites proses inédites de Joël Roussiez
sous_titre: ""
date_document: "2020"
fichiers:
  - autres-textes-roussiez.pdf
  - fontaine.pdf
tags:
  - texte
  - DAO
  - document
---

**Derrière la vitre de la maison** (raga Durga, P. Istrati)

Le jour se lève enfin, sous les rafales de pluie, la lumière s’adoucit et doucement s’éclaircit alors que le vent se calme un peu en remuant les feuilles qui tombent et tournent loin au-dessus de la prairie, portées par les souffles intermittents qui passent ou s’enfuient… La fuite se propage au solitaire comme l’escapade à l’homme qui vieillit et le voyage entrevu comme une croisière mouvementée tente une jeune mère à la fenêtre de la maison. Elle regarde le remuement des branches et les feuilles qui s’envolent : je partirai un jour et que ce soit dans la tempête sera très amusant… Elle relève ses cheveux pour dégager son regard, elle observe dans le vague un petit buisson déraciné qui roule par à-coups dans l’herbe malmenée. Le vent pousse devant lui et l’âme le suit… Sur un lit de fleurs, je dormirai, écoutant les oiseaux de la prairie, les cailles, les perdrix…

_Derrière les Vitres de la maison_ est extrait de _Voyager Sans Partir_ (non publié),

_Invitation des fleurs_ de _Invitation Des Fleurs_ (non publié).

_Souvenir Anatolien_ ne fait pas partie d’un volume constitué.

  
(_lire la suite_...)