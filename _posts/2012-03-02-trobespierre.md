---
title: "Trobespierre"
date: 2012-03-02 22:00
tags:
  - billet
---

« En voici un qui me doit quatre allumettes ! » disait-on chez moi en Bretagne où l’on redoutait le poisson et ses épines tenant peu au ventre, le laissant pour ces bizarres de pêcheurs. C’est autre chose qu’un berger dans une valise, à savoir Léonard Cohen forcé de sortir du Monastère Zen de Los Angeles où il faisait retraite depuis 1994.  
En réalité, “Le Silencieux” était déjà parti de Mount Baldy au printemps 1999.  
C’est dans le premier numéro hebdomadaire des Inrocks que j’avais lu un article sur la retraite Zen de Cohen, avec sa photo en couverture.  
Dans _Old Ideas_, c’est la voix de l’extinction, d’une dépression qui n’en finira jamais (lui qui se dit dépressif chronique), car c’est celle du monde, de la lucidité Zen, qui donne une légère tristesse, disait Suzuki.  
Manager crapule, Kelley Lynch, pour détournement de fonds (5 millions USD), qu’on devrait _Lyncher_ ?  
Je pense à ce groupe qui se nommait E. P. S. : _Elimination Pure et Simple_. Je sais bien que ça n’avait rien à voir avec des personnes (c’était une thérapeutique égyptienne par la sueur) et que ça peut évoquer les horreurs nazies, mais je ne peux m’empêcher de penser à la nécessité d’éliminer les _intermédiaires_ avec le Z, la lettre qui coupe de Maurice Roche. Il faut ainsi penser à ses ennemis même en se raZant. Les impresarii, les éditeurs, les galeristes, toute la demi-flicaille lécheuse. Il y avait deux métiers que Maurice Roche avait interdits à son fils : journaliste et prostitué.  
Restons Zen, coupons-leur la tête ! (Le Lotus Bleu)  
Robespierre était très Zen, avec son orange et sa frugalité. Je crois que vous l’aimez bien.  
J’ai vu en 1989 les travaux d’un graveur Robespierriste qui avait réalisé courageusement une suite gravée en hommage à _Action Directe_.  
Saluons les amis de Robespierre.  
La France a toujours préféré les garçons bouchers pour toutes les besognes : Danton par exemple. Il est évident qu’un Danton mâche plus. Robespierre est du côté du sabre et Danton de la hache. Il y a des places Danton partout et pour ainsi dire aucune rue Saint-Just, Couthon, ni Robespierre. Pas de rue Robespierre à Paris, malgré les propositions du Parti Communiste en octobre 2009. Par contre un petit triangle à la pointe ouest de l'Ile-Saint-Louis s’appelle Aragon (Elle est dans l’Île ?)  
Est-ce un ministre inculte et chevalin à la cravate rouge, un secrétaire d’état ou un historien histrion et révisionniste, qui en raison de cette aversion instinctive pour le personnage, lors de la célébration de la Révolution en cette même année 89 très gaie (Jack Lang, Jean-Paul Gauthier, qui d’autre pour l’organisation de la fête ? J’ai oublié…) élimina purement et simplement le cachot de Robespierre à la Conciergerie au profit de celui de Louis XVI (qu’on agrandit alors !), pour la simple raison qu’il ignorait que Robespierre y avait séjourné. La très grande astuce que voilà !  
_Ronán Pleven_