---
title: "À propos du Roman"
date: 2010-08-15 23:17
fichiers: 
  - A_Propos_du_Roman.pdf
date_document: "2006" 
tags:
  - document
  - HSOR
  - texte
  - edition
---

**À Propos du Roman**

— La littérature peut-elle être encore pensée en terme d’évolution, de révolution ? Autrement dit, face aux impératifs commerciaux qui tendent, semblent-il, à la niveler en la réduisant, par exemple, à ne plus ressortir qu’au seul genre du roman, reste-t-elle cet espace (que l’on dit sacré) de liberté, ce lieu de tous les possibles ?

— Le Roman, je l’ai fréquenté en trois temps : de façon traditionnelle dans Roman en 1968 (c’était un pari fait avec un groupe d’anciens lycéens). De façon déja désintégrée avec Tuberculose du Roman en 1972, et enfin en 1984 avec Je suis le Roman Mort qui en signale l’éparpillement.

_etc._

Publication : [Enquête sur le Roman. Éditions le Grand Souffle](http://jdepetris.free.fr/load/enquete.html)