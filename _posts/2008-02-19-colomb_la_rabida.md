---
title: "Colomb : La Rábida"
date: 2008-02-19 22:35
fichiers: 
  - Don_Qui_Domingo_._La_Rabida.pdf
sous_titre: "Les Grands Ancêtres. Ligne de Don Qui" 
date_document: "1992" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_**La Rábida**_  
La cafetière de cuivre rouge est bien chaude, maintenue par O’Koffee qui s’émeut les yeux humides devant les Monts ou l’ourlet gris, tous moines gras à célébrer le rassemblement efficace des lueurs divines autour de la Table du Petit Jour, à vouloir l’anonymat absolu dans une langue étrangère, la peau poreuse et ne faisant plus contour, lieu de passage et surface de renvoi, moulin à prières parmi les vents, clochette entre d’autres rumeurs alpestres !  
« Tu peindras cet absolu, disent-il à l’Ogre Peintre, et ce sera plus magnifique encore que le Laocoon ! » Le matin laissait monter l’idéalité de la campagne, méditative, les différences de plans, germées... Lui pensait plutôt ailleurs bergère, accroc de chevalet dans la toile (“Je me lançais sur elle dans l’immense pièce par un mouvement projeté droit de la jambe, puis de santé le vent sur le ventre, en oblique, en descendant, sur la gauche.”)