---
title: "Osiris Usines"
date: 2009-01-29 17:59
fichiers: 
  - 6.Usines.Osiris.pdf
sous_titre: "Les Escholiers Primaires. Ligne de Nicolaï. Été. 6. Usines" 
date_document: "1970" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

_**L’Usine des Rêves**_  
C’est extraordinaire ! La terre s’est retournée mieux qu’avec une défonçeuse double. Il neige ! C’est l’Usine des Rêves.  
Voilà un plan d’exode massif qui passe, pour celui qui veut en finir avec l’Usine Seconde, sans se rendre compte qu’il tire sur son pousse-pousse un tas de tonneaux inutiles.  
La connerie de tous les abrutis du siège s’était feutrée ce matin-là mieux que de la passion des roses rouges. J’étais sorti du lit avec un rêve antédiluvien coincé dans mon cervelet, et une gêne de ce côté-là, reptile ou herbe, pris d’une desmopathie générale. Les Bouriates ne m’étaient encore qu’un horizon à perte de vue tandis que Saîd avait perdu depuis longtemps ses ancêtres kabyles ; j’en viendrais un jour par procéder de point à point sur une carte, et uniquement comme cela. Ai-je encore une fourrure de glouton ? Voyons.  
Du coup, je n’entendais plus les discussions, mais _des voix_, et celles-ci comme en hauteur, ou après avoir plongé profondément.  
(_Dans les hauteurs des monts kabyles en été, m’a dit Saîd, on entend sous les étoiles des voix dans les buissons, dans l’extrême fraîcheur de la Nuit : ce sont celles des jeunes recrues qui vont partir à l’armée, et qui chantent des mélodies d’amour en s’accompagnant à la harpe._)