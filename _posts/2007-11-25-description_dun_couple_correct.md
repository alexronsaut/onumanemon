---
title: "Description d’un couple correct"
date: 2007-11-25 20:30
fichiers: 
  - 325.pdf
sous_titre: "Livre Poétique de Nicolaï. 1964-68. Poème n°23" 
date_document: "1965" 
tags:
  - document
  - OGR
  - texte
---

**23. Description d’un couple correct**  
  
Jo ! Tout en paillons, le nez bleu, les pieds gelés,  
Où s’en va-t’il donc désodésarticulé  
Qui déploie son chocolat si beau de barrière ?  
Vois-le s’enflant, envahir partout du derrière !  
Qu’on regarde, violent et clair, surpris et dur,  
Le beau récit, extrait puant de béton pur  
Fourmillant vert, mousse d’esprit rêvée, d’idées  
Qu’il fait, se dégageant par blocs et non ridées !  
Enfin d’accroupissure, écarlate son Ouf !  
Et puis s’assoit dessus de prose, comme un pouf.