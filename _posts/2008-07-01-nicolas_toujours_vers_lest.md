---
title: "Nicolas, toujours vers l’Est"
date: 2008-07-01 22:33
fichiers: 
  - Nicolas_vers_lEst.pdf
sous_titre: "Les Adolescents. Ligne de Nicolas. Été" 
date_document: "2000" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_Il y est question de Rimbaud, de Pasternak et Jivago, d’Apollinaire, de Hugo et d’Aragon, de “la plus banale romance” et de la transmigration prosodique. Puis encore de_ la verrité _comme épaisseur du verre qui résiste à la vue, dont il est question ailleurs dans l'ouvrage._

_I.Revay_

À nous dans un multiple mouvement les grands pans de la pensée de l’irréel, les électrochocs de Fernande (que rappeleront si bien les spasmes de la petite chienne coker Pépita, gueule bavante dans les copeaux de bois), et ses faux bijoux du “Caillou du Rhône” ! _Je suis devenu Sergueï Kaltoukine_ ! Ni plus ni moins “un personnage” (“Jean, laissez-moi arriver aussi !” germentent les cupidités, vous avez vu ? Valeurs poncives de l’écrit. Tout ça pour en placer d’autres, gros éléments, fournis en quiproquo.), le jour où j’ai découvert que la majorité de mes poèmes préférés, jusqu’au rythme octosyllabique de leurs franchises hivernales (auxquelles je tenais tant !), avaient déjà été écrits en mieux par Boris Pasternak.  
Dans ce temps-là, Lola La Noire (grise sur la plage) fuyait la nuit dans les monts de Castille en chantant des idioties, folle, déliée :  
“Qui pue tant du cu et  
Perd ainsi son beau cerveau,  
Son sage servage en cerceaux ?”  
Je ne connaissais alors que l’art de la liste, et la compagnie de poètes russes alcooliques déambulant au petit jour, feulant de moins en moins jusqu’à _la puissance fabuleuse sans objet peint_, raclant le vers pour atteindre l’insignifiance sans cataplasmes ni connotations, plus ondulatoires que corpusculaires, la bouche embrassant avec angoisse forte la loi, la pressant de leurs bras sur les deux bords avec leur habitude cosaque de cavaliers de fer soumis à l’endurance, rapeux.