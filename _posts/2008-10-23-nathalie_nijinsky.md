---
title: "Nathalie & Nijinsky"
date: 2008-10-23 16:24
fichiers: 
  - Nathalie_Nijinsky.pdf
sous_titre: "Les Escholiers Primaires. Ligne de Nathalie. Été" 
date_document: "1986" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_À partir d’ici, en vidéo, dans le Studio Soudain où Nathalie vient se faire photographier en dansant, sur plusieurs écrans simultanés, le corps d’un arbre, vu par chacun des points d’articulation du corps : tête, épaules, hara, coudes, bite, chevilles.  
(Arbre vu par épaule droite puis coude gauche.)_  
**Nijinsky :** “Il y avait d’abord l’ours dansant, l’ours énorme surgi d’entre les branches, et Kyra riait de le voir attraper ce minuscule écureuil qui venait de s’enfuir de chez nous, par la fenêtre, d’où on regardait, sur cet étang, ce lac, ce bief. Mais l’ours ensuite se mit à vouloir dévorer notre chatte ! Il se mit à l’absorber une première fois, puis elle ressortit de son estomac, galopa devant lui. Il était de nouveau en train de l’engloutir, lorsque je décidai de sortir de la maison pour la sauver. Je montai sur un pic, et là, d’un plan plus général, j’aperçus soudain un énorme boa, enroulé dans les rochers du bord ; je le signalai à ma femme qui se trouvait un petit peu plus bas ; je lui jetai des pierres et des choses diverses pour l’exciter, et celui-ci fit vibrer sa langue au-dessus de l’eau ; c’est à cet endroit que je reconnus sa tête ; jusque là je ne voyais que la confusion des courbes, des nœuds, des sentiments et de leurs passages…  
Redescendu dans l’assemblée, je portais un col de clergyman qui m’allait véritablement à ravir, combiné avec ma veste chinoise à col mao, et un manteau du même genre, col officier. C’étaient trois successivement cols de même type. Mon père devait être un officier du passé simple.”  
_(Arbre vu par pied droit) (Arbre vu par Hara) (Arbre vu par bite)_