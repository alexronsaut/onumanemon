---
date: 2021-11-13 10:46
title: Bonheur Thoracique
sous_titre: Os de Poésie Nicolaï
date_document: Décembre 1977
fichiers:
- "/bonheur-thoracique.pdf"
tags:
- texte
- OGR
- document
---

BONHEUR THORACIQUE

Je fus dans le corps d'un aïeul : avec ses jambes qu'on ne maîtrise plus, le vent pelant la face hirsute de la mauvaise saison, burinant ses traînées entre poils et cheveux, emportant au loin dans l'Histoire des cauchemars polaires.  
La Vague circulait dans les tranchées et la lumière dans les creux, dans les versants de la vallée, à l'arrière des derniers talus d'arbustes ; les Irlandais sabotaient la campagne de Pâques jusqu'à l'insurrection. Le 16 avril 1917 flottait un énorme nuage noir pourri de neige, de fumier gelé, de gueulements au-delà des bosquets et des lacis barbelés jusqu'à la Crête des Dames d'où l'on domine les fonds humides de la vallée de l'Ailette, le Bois des Buttes, le Ruisseau de la Miette, la Ville au Bois et la Musette, et surtout les torches brûlantes et hurlantes jaillissant des chars incendiés.  
Si je suis d'un printemps, c'est de cette herbe encore trop pâle ou ma mère élève des plantes comme elle ferait pousser des enfants, car elle a les doigts verts de celle qui n'a pas su sauver son fils. Il n'y a pas de bras dressé de Diane pour la défendre ; mais il y a ses mains qui nous ont défendu des mâchoires du chien en rage quand nous étions enfant, d'abord aveuglé par la lampe en forme de globe de l'ophtalmo, enfin heureux de la fenêtre ouvrant sur le jardin du médecin suivant sauveur de notre œil.

_(lire la suite…)_