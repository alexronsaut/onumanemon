---
title: "La Clinique Béthanie"
date: 2017-11-06 11:26
fichiers: 
  - nicolai.la_clinique_bethanie.pdf
sous_titre: "Livre Poétique de Nicolaï" 
date_document: "1968" 
tags:
  - document
  - OGR
  - texte
---

**18. La Clinique Béthanie**  
  
Plus rien qu’un tournoi sur  
Une perspective abîmée ;  
Têtes coupées, matinées d’or.  
  
Cresson, canailles, confusion ;  
La guerre qui du moins nous sauve  
Ne fait plus aucun prisonnier.  
  
Salves de l’écho sur les monts,  
Esclandres, hoquets oniriques :  
Nous voilà soumis aux gloutons,  
  
Aux dévoreuses de bonbons.  
Sous de la corne, sur de la soie,  
L’Enfance est ressucitée grêle !  
  
_(lire la suite…)_

  
La Clinique Béthanie _et_ Le Pavillon Toussaint_, bien qu'écrits à un an de différence (1967 et 1968), se répondent d'un Livre Poétique à l'autre des deux frères._

_NDLR_