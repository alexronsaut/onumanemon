---
title: Fantôme Rouge & Noir
sous_titre: ""
date: 2024-08-18 14:46
date_document: "Après 1984"
tags:
    - document
    - dessin et gravure
    - Cosmologie Onuma Nemon
fichiers:
    - "/FantomeRouge-Noir.Dessin.Encres-sur-Arches.Format-raisin.jpg"
---

Dessin. Encres sur Arches. Format raisin

Don à la Bibliothèque Kandinsky de Beaubourg.
