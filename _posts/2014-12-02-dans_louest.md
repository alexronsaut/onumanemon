---
title: "Dans l'Ouest"
date: 2014-12-02 22:58
fichiers: 
  - dans_louest.comic_strip.1971.site_.jpg
sous_titre: "Comic.Strip" 
date_document: "1971" 
tags:
  - document
  - HSOR
  - dessin et gravure
---

Ce dessin faisait anciennement d'un cahier auquel il a été arraché. Il y a eu dans les années soixante-dix une série de onze cahiers complets inspirés de Comic-Strip des années 50 (Tex Tone, Buck John, Hoppalong Cassidy), et destinés pour certains à illustrer deux pièces de théâtre de Nycéphore du Continent OGR inspirées par les mêmes ouvrages. Ils composaient toutefois chacun un récit. Tous ont été détruits dans l'année 2000 sauf deux : l'un offert à Françoise Labat, l'autre à Pierre-Alain Lucerné.

Ce sont la plupart du temps des traces de crayons de couleur ou de pastels sur un fond de scène au graphite.

_NDLR_