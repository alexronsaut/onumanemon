---
title: "Nycéphore. La Défaillance"
date: 2008-11-12 21:31
fichiers: 
  - NycephoreAutomneDefaillance.pdf
sous_titre: "Les Escholiers Primaires. Ligne de Nycéphore. Automne" 
date_document: "1979" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

Texte Inédit. Travail de réduction en cours.  
  
Nycéphore sortit du cabinet d’Acupuncture de Jean Shatz rue de l’Université, une après-midi d’automne de 1979, et se rendit chez le libraire bibliophile dont ce dernier lui avait parlé près de Solférino. Il fit d’abord les cent pas dans les alentours, indécis, pris d’on ne sait quelle crainte d’une découverte, avant de se rendre devant la boutique, comme on se dilapide en marchant. Dehors, à même la rue, le libraire avait disposé sur un étal des rangées de livres de poche que les étudiants devaient lui piller régulièrement ; Nycéphore hésita encore à franchir les deux marches du seuil puis enfin il se présenta à l’homme à la face palpitante dont un tic relevait sans cesse la commissure droite des lèvres. Dans ce capharnaüm où il vivait exclusivement on trouvait Balzac, Hugo, mais surtout au-delà tout le romantisme européen et les décadents. Puis des livres à reliure verte scolaire, des livres de prix à tranche dorée, avec des étiquettes et des numéros : donations de bibliothèques, ainsi de suite. Manies, phobies du personnage chiffon à la main, crainte des taches de tous ordres, raréfaction des moindres gestes comme de se lever pour aller ouvrir le tirage du poèle, retourner un carnet sur la table.