---
title: "Front des Artistes Plasticiens 1972"
date: 2015-08-22 16:27
fichiers: 
  - f.a.p._1972.pdf
sous_titre: "Préparation de la contestation de l’Expo Pompidou" 
date_document: "Printemps 1972. Paris" 
tags:
  - document
  - DAO
  - texte
---

**Alain Sebag** (relit le projet de Statuts) : Nous soulignons notre indépendance de tout courant politique, mais nous ne somme spas a-politiques. Les problèmes moraux liés à notre pratique nous entraînent à un rapprochement avec les groupes ouvriers.

**Nave :** C’est un problème de fond, et non pas de forme. Est-ce qu’il faut mettre ou non le terme socialisme dans les statuts ; il faut définir notre engagement politique.

**Alain Sebag :** Ce texte est le cadre minimum du F.A.P. ; c’est celui qui a été adopté lors de la dernière assemblée générale.

**Nave :** Dans les statuts nous avons parlé “d’association pour la défense des intérêts moraux et sociaux des artistes, sans distinction raciale, religieuse ni idéologique… pour de nouveaux rapports sociaux et culturels dans une perspective socialiste.” C’est clair !

**Philippe :** Ce qui est important c’est la réfutation d’un “saut qualitatif” réformiste.

**Nave :** C’est mal exposé. Il faut marquer notre désaccord avec la “non-distinction idéologique” (à savoir le danger d’Ordre Nouveau, de l’U.D.R. et de tout le boy-scoutisme).

**X1 :** “Pour le socialisme”, c’est trop vague, et ça ne signifie rien si c’est uniquement dans les statuts : c’est du beurre.

**X2 :** Comment définir une action socialiste ? Comment dépister les fachos ? Est-ce que vous allez demander leur carte à ceux qui viennent ? Vous définissez un idéal, mais comment le suivra-t-on ?



_(lire la suite…)_