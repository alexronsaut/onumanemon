---
title: "Campagnes"
date: 2007-09-06 22:20
fichiers: 
  - 12.pdf
sous_titre: "Les Cinq Continents de Nycéphore. Deuxième Continent des Terres." 
date_document: "1966" 
tags:
  - document
  - LOGRES
  - texte
---

Les cinq continents sont la première division du monde de la Cosmologie. Ils sont écrits par Nycéphore et ont pour pendant dans l’Œuvre de Nicolaï “_Dédié, Journal de Bord_” destiné à Nycéphore et contenant également de longs poèmes.   
Ici l’on trouvera les trois premiers poèmes du Second Continent, celui des Terres.  
L’ensemble fait partie du Cahier des Études.  
Isabelle Revay.