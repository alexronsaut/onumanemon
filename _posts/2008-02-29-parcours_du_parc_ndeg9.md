---
title: "Parcours du Parc n°9"
date: 2008-02-29 22:29
fichiers: 
  - Parcours_du_Parc_9.jpg
sous_titre: "Les Adolescents. Joyelle & Hill" 
date_document: "1978" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - photographie
---

Format 13 x 18cms. Cette photographie faisait partie de l‘exposition au “Quartier” en 2005