---
title: "Troupeaux et Voyage"
date: 2009-03-07 21:37
fichiers: 
  - Joel_Roussiez.Troupeaux__Voyage.pdf
sous_titre: "À propos de Joël Roussiez" 
date_document: "2009" 
tags:
  - document
  - DAO
  - texte
  - edition
---

À chaque fois qu’on rencontre un Horrible Travailleur, il périme tout un pan de la vaine production d’alentour, la rend caduque, et on ne peut que s’en réjouir. Autant de débroussaillé ; nous sommes dans un territoire de l’Inscription et toute découverte dans un autre endroit est toujours bénéfique. Autant de temps gagné. La fraternité est de mise.  
Avec Joël Roussiez tout est mouvement comme dans la pensée chinoise où “les réalités que simulent les mots ne sont pas des choses arrêtées mais des mouvements”(1). Implosion, explosion et dispersion que la spirale du _Voyage Biographique_ emporte, ou mouvement des marcheurs cosmopolites à travers les méridiens du monde de _Nous et nos troupeaux_.

_1. Nous et nos troupeaux_  
“ « Cosmos terrien de vie » je dis.”  
“Avançons sans peur aucune, sans crainte des coups, ne cherchons rien  
Croisons des hommes qui ont cherché et s’en reviennent  
Des qui s’en revenaient, n’avaient rien vu…”

Les paysages des _Troupeaux_ sont comme ceux de Cozens, lui-même tellement chinois dans sa technique tachiste avec cet effet d’éloignement qui abolit la césure entre esquisse et dessin achevé, ce miracle ophtalmique permettant de faire disparaître autant les grossièretés de la tache que les finesses de l’exécution attentive. De loin le dessin devient une tache modulée et la tache un dessin vigoureux, tous deux pris dans le même ravissement de l’œil.  
Ce génie de l’esquisse est partout présent dans les traversées des paysages de Roussiez où des notations extrèmement précises sur les couleurs, les climats ou les coutumes de certaines populations (voire les “marques” mécaniques) alternent et glissent avec de vagues affairements : intrusions humaines laborieuses ou énigmatiques dans “une sorte de camp de matériaux variés”.  

[http://www.larumeurlibre.fr/](http://www.larumeurlibre.fr/)

Publication : [La Main de Singe](http://lamaindesinge.blogspot.com/search/label/VOYAGE%20BIOGRAPHIQUE)