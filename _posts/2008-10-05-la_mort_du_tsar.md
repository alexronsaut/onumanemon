---
title: "La Mort du Tsar"
date: 2008-10-05 14:51
fichiers: 
  - Theatre_Lyceen.Piece_TsarNicolas.pdf
sous_titre: "Les Adolescents. Été. Théâtre Lycéen" 
date_document: "1966" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_Voici l’exemple d’une des pièces que les Lycéens jouèrent à propos de la Révolution Russe en 1967 au “Théâtre du Styx” qui devait devenir la fameuse roulotte de Sainte-Croix._  
  
**Docteur Botkine.** Bien sûr, on peut toujours rêver le domaine secret et sain de l’Intelligence. Mais moi c’est pas de la mer Noire où règne Sérapis, dont je veux vous parler, c’est de bien avant Sosso, du temps du plâtre et de la mort du jeune Nicolas.  
**Domestique Troup.** Non, non ! Vous avez perdu ! Vous voyez : l’as de pique est tombé sur le valet de pique. (_Il rit_) Et donc c’est fini pour Sosso ! Son propre mal a été pire que lui. Cette sorte de variole bureaucratique sévit.  
**Docteur Botkine.** Ce Caton de basoche de Kerensky, où est-il, au fait ? Balayé par Lénine, c’est ça ?  
**Domestique Kharitonov.** Il est rendu à ses asperges vivaces et bleuies, bourré des idées saugrenues d’un généralissime de foire ! Tandis que vous, Docteur, vous voilà encore obligé de vous traîner avec vos maux de reins, et vos pieds gelés sur les fleuves !  
Je vais vous parler moi de la mort du père Empereur de l’Industrie, (_Macha au fond, en robe noire, s’avance_) toujours si digne avec ses pantalons à liseret qui lui affinaient la taille et allongeaient sa silhouette ! Et je vous parlerai aussi du plus petit Alexis Nicolaévitch, inclu comme une matriochka dans le Père Nicolas, cet Industriel Hongrois de Libourne. Ils lui en voulaient parce qu’il était soi-disant du côté des “Blancs”.  
**Docteur Botkine.** C’était pourtant un grand ami des Sales chez qui j’étais souvent, des Inventeurs. De grands Inventeurs et de dignes gens. De bons amis à moi aussi. Ils ne parlaient pas allemand, ils étaient orthodoxes. Le petit-fils fait du cinéma, aujourd’hui.   
**Macha.** Mettons que la vie soit un brouillon, et qu’on recommence. Mais les autres, les Demi-Deuils, les Millions de Marchés, les Installations de Machines à Tortures dans les Instants, les Représailles, non ! Jamais ! Je sais maintenant : il faut éviter tout ce qui glisse et qui caractérise, tout ce qui est crachat et ne se résout pas dans un cri uni. Sinon on se tait, on prend d’autres manières que les siennes, dans la soie ou dans le meurtre, on se résigne. Alors là, la vie serait propre, on recommencerait, on arrangerait une pièce, comme ici, avec des fleurs, une masse de lumière et des petites filles pleines d’espoir.  
(_Un silence_.)