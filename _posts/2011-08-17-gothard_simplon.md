---
title: "Gothard & Simplon"
date: 2011-08-17 22:06
tags:
  - billet
---

Débarqué à Fluelen, arrivé à Altorf, le manque de chevaux va me retenir une nuit au pied du Bannberg. \[…………\]  
Demain, du haut du Saint-Gothard, je saluerai de nouveau cette Italie que j’ai saluée du sommet du Simplon et du Mont-Cenis. Mais à quoi bon ce dernier regard jeté sur les régions du midi et de l’aurore ! Le pin des glaciers ne peut descendre parmi les orangers qu’il voit au-dessous de lui dans les vallées fleuries.  
\[…………\]  
D’Altorf ici, une vallée entre les montagnes rapprochées, comme on en voit partout ; la Reuss bruyante au milieu. À l’auberge du Cerf, un petit étudiant allemand qui vient des glaciers du Rhône et qui me dit : « Fous fenir l’Altorf ce madin ? allez fite ! » Il me croyait à pied comme lui ; puis apercevant mon char à bancs : « Oh ! tes chefals ! c’être autré chosse. »  
\[…………\]  
Le nouveau chemin du Saint-Gothard, en sortant d’Amsteg, va et vient en zigzag pendant deux lieues ; tantôt joignant la Reuss, tantôt en s’écartant quand la fissure du torrent s’élargit. Sur les reliefs perpendiculaires du paysage, des pentes rases ou bouquetées de cépées de hêtres, des pics dardant la nue, des dômes coiffés de glace, des sommets chauves ou conservant quelques rayons de neige comme des mêches de cheveux blancs ; dans la vallée, des ponts, des cabanes en planches noircies, des noyers et des arbres fruitiers qui gagnent en luxe de branches et de feuilles ce qu’ils perdent en succulence de fruits. La nature alpestre force ces arbres à redevenir sauvages ; la sève se fait jour malgré la greffe : un caractère énergique brise les liens de la civilisation.  
Chateaubriand 17 août 1832  
  
À Altdorf, à la pointe méridionale du lac des Quatre-Cantons qu’on a cotoyé en vapeur, commence la route du Gothard. À Amsteg, à une quinzaine de kilomètres d’Altdorf, la route commence à grimper et à tourner selon le caractère alpestre. Plus de vallées, on ne fait plus que dominer des précipices, par-dessus les bornes décamétriques de la route.  
Rimbaud 17 novembre 1878  
  
Soutenu en l’air par des murs le long des masses graniteuses, le chemin, torrent immobile, circule parallèle au torrent mobile de la Reuss. Çà et là, des voûtes en maçonnerie ménagent au voyageur un abri contre l’avalanche ; on vire encore quelques pas dans une espèce d’entonnoir tortueux et tout à coup, à l’une des volutes de la conque on se trouve face à face du pont du Diable.  
Ce pont coupe aujourd’hui l’arcade du nouveau pont plus élevé, bâti derrière et qui le domine ; le vieux pont ainsi altéré ne ressemble plus qu’à un court aqueduc à double étage. Le pont nouveau, lorsqu’on vient de la Suisse, masque la cascade en retraite.  
\[…………\]  
L’ancienne route du Saint-Gothard, par exemple, était tout autrement aventureuse que la route actuelle. Le pont du Diable méritait sa renommée, lorsqu’en l’abordant on apercevait au-dessus la cascade de la Reuss, et qu’il traçait un arc obscur, ou plutôt un étroit sentier à travers la vapeur brillante de la chute.  
Chateaubriand 17 août 1832  
  
Avant d’arriver à Andermatt, on passe un endroit d’une horreur remarquable, dit le Pont-du-Diable, - moins beau pourtant que la Via Mala du Splügen, que vous avez en gravure.  
Rimbaud 17 novembre 1878  
  
Au village de l’Hospital commence la seconde rampe, laquelle atteint le sommet du Saint-Gothard, qui est envahi par des masses de granit. Ces masses roulées, enflées, brisées, festonnées à leur cîme par quelques guirlandes de neige, ressemblent aux vagues fixes et écumeuses d’un _océan_ de pierre sur lequel l’homme a laissé les ondulations de son chemin.  
Chateaubriand 17 août 1832  
  
Puis commence la vraie montée, à Hospital, je crois : d’abord presque une escalade, par les traverses, puis des plateaux ou simplement la route des voitures. Car il faut bien se figurer que l’on ne peut suivre tout le temps celle-ci, qui ne monte qu’en zigs-zags ou terrasses fort douces, ce qui mettrait un temps infini, quand il n’y a à pic que 4 900 d’élévation, pour chaque face, et même mins de 4 900, vu l’élévation du voisinage. On ne monte plus à pic, on suit des montées habituelles, sinon frayées. Les gens non habitués au spectacle des montagnes apprennent aussi qu’une montagne peut avoir des pics, mais qu’un pic n’est pas la montagne. Le sommet du Gothard a donc plusieurs kilomètres de superficie.  
Rimbaud 17 novembre 1878  
  
Il y avait jadis, sur le Saint-Gothard, un hospice desservi par des capucins ; on n’en voit plus que les ruines ; il ne reste de la religion qu’une croix de bois vermoulu avec son Christ : Dieu demeure quand les hommes se retirent.  
Chateaubriand 17 août 1832  
  
Une ombre pâle derrière une tranchée : c’est l’hospice du Gothard, établissement civil et hospitalier, vilaine bâtisse de sapin et pierres ; un clocheton. À la sonnette, un jeune homme louche vous reçoit ; on monte dans une salle basse et malpropre où on vous régale de droit de pain et fromage, soupe et goutte. On voit les beaux gros chiens jaunes à l’histoire connue. Bientôt arrivent à moitié morts les retardataires de la montagne. Le soir on est une trentaine, qu’on distribue, après la soupe, sur des paillasses dures et sous des couvertures insuffisantes. La nuit on entend les hôtes exhaler en cantiques sacrés leur plaisir de voler un jour de plus les gouvernements qui subventionnent leur cahute.  
Rimbaud 17 novembre 1878  
  
Sur le plateau du Saint-Gothard, désert dans le ciel, finit un monde et commence un autre monde : les noms germaniques sont remplacés par des noms italiens. Je quitte ma compagne, la Reuss, qui m’avait amené, en la remontant du lac de Lucerne, pour descendre au lac de Lugano avec mon nouveau guide, le Tessin.  
\[…………\]  
J’ai passé de nuit Airolo, Bellinzona et la Val-Levantine : je n’ai point vu la terre, j’ai seulement entendu ses torrents. Dans le ciel, les étoiles se levaient parmi les coupoles et les aiguilles des montagnes.  
Chateaubriand 17 août 1832  
  
La route est en neige jusqu’à plus de trente kilomètres du Gothard. À trente k seulement, à Giornico, la vallée s’élargit un peu. Quelques berceaux de vignes et quelques bouts de prés qu’on fume soigneusement avec des feuilles et autres détritus de sapin qui ont dû servir de litière. Sur la route défilent chèvres, bœufs et vaches gris, cochons noirs. À Bellinzona, il y a un fort marché de ces bestiaux. À Lugano, à vingt lieues du Gothard, on prend le train et on va de l’agréable lac de Lugano à l’agréable lac de Como. Ensuite, trajet connu.  
Rimbaud 17 novembre 1878  
  
_On a ici une parfaite illustration de l’hallucination comme méthode chez Rimbaud : il est reçu dans un hospice déjà détruit du temps de Chateaubriand. À moins qu’il ne soit déjà dans un voyage au pays des morts.  
Me S.  
  
et quelque temps plus tard un digne Rimbaldien, assassin de cette baudruche de Wagner, sur cette ligne qui va du Gothard au Simplon (6 jours de marche) :_  
  
Voyage merveilleusement réussi. Je n’oublierai pas cette ascension jusqu’à la neige, au col du Simplon, sous le soleil d’août, ni cette arrivée dans une petite auberge du Jura, dans la nuit, en plein orage (les éclairs illuminaient les croix du cimetière), et, dans la petite mansarde boisée avec son fauteuil de velours rouge, la voix de M. me réveillant : « J’ai peur… — De quoi as-tu peur ? — De tout ! » Et cette halte entre Florence et Bologne, à la nuit tombante, le chant continu des grillons, une cloche suspendue dans la brume… Et la promenade dans les ruines d’Ostie, la villa San Michele, le moine du cloître de Fiesole !  
Jean-René Huguenin Jeudi 10 Août 1961, Paris.