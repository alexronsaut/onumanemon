---
title: "Chiens & Chats"
date: 2017-07-17 18:19
tags:
  - billet
---

Dans le numéro de Mettray consacré à _la lecture_, Marcellin Pleynet pense que la plupart de ses prétendus lecteurs lisent comme des chiens.  
Version pire, dans _1275 Âmes_ de Jim Thompson, tous les chiens de la terre réunis en congrés, sont condamnés à se sentir le cul jusqu’à l’éternité pour reconnaître le leur (se pencher, puis en déchiffrer les signes), depuis que la tempête a dispersé leurs trous de balle qu’ils avaient accrochés aux porte-manteaux par politesse pour qu’on ne sente pas autre chose que la fumée pendant les débats. (“Ce n’est qu’un débat, continuons le con bu !” Denis Roche)  
À l’inverse, on pense au _Portrait de l’artiste en jeune chien_ du fabuleux Dylan Thomas, à la lettre de Proust au chien de Reynaldo Hahn et à Proust disant qu’il ne pouvait écrire que quand il redevenait chien. Mais c’est d’un tout autre monde qu’il s’agit, spicilège des sensations décuplées, fraîches, ébouriffées, fruitives.  
Puis Maurice surgit, du temps de la rue Henri Barbusse, avec les amies de la Folie-Méricourt, après la mort de Titigre ou de Tidyable (je ne sais plus), un de ses nombreux chats, qui lorsque j’arrive chez lui me dit tout à trac : « Tout de même, Marcellin Pleynet, il exagère ! »   
C’était d’autant plus étonnant que Maurice aimait beaucoup Marcellin Pleynet et son écriture ; il faisait partie des très rares contre lesquels il ne pestait jamais. « Qu’est-ce qui se passe, Maurice ? — J’ai téléphoné à Pleynet pour lui demander ce qu’il faisait de ses chats morts… — Et alors ?   
— Il m’a dit : “Moi je les fous à la poubelle.” Tu te rends compte ? À la poubelle ! »  
_Un Témoin_