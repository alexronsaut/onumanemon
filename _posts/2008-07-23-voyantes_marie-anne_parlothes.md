---
title: "Voyantes : Marie-Anne Parlôthes"
date: 2008-07-23 08:57
fichiers: 
  - Voyantes.pdf
sous_titre: "Les Gras. Printemps. L’esplanade des Girondins" 
date_document: "1978" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

 **Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_Esplanade_  
 Dans ce Cabinet de Voyance où Camille s’est rendue peu avant avec Basta, pour connaître _soon-avenir_, chez Marie-Anne, il y avait des insécables et rugueux retours archaïques datant d’on ne sait quel ineffable. Déjà, tout enfant, chez les Vistandines, elle allait vers les visions comme on va vers le précipice, et aujourd’hui chez ceux qui la consultent, elle ne fait surgir vers le futur que ce qui était déjà là en eux, notamment avec son MUTUS-NOMEN-DEDIT-COCIS de cartomancienne à cadences paires.  


 Elle les jette sur le Minotaure au fond du Labyrinthe, mais pas de lamento d’Ariane, pas de pelotte, pas de moyen de se faire pelotter dans le tréfonds.  
 C’est ainsi qu’elle a vu pour le Tsar un siècle à l’avance : Tannenberg, les gourances de Raspoutine, les bolcheviques en dahuts, pattes gauches du côté de la pente… elle agrémente après-coup l’archaïque. Et c’est pour cela qu’à Bruges on l’a traitée de sorcière.  
 Une fois elle rêva pour Charlotte d’une paire d’yeux se mouvant seuls près de morceaux de corps aux diverses couleurs indépendants et de paquets de poils également autonomes, ainsi que de bouquets de feu et de lumière, mais dans la liaison formidable d’une phrase grandiosement organisée qui lui fit voir clairement l’efficacité du “génie” qui lui servait les phrases dans l’obscurité, car elle lui avait été dictée dans sa perfection d’un seul trait qu’elle lança en se levant.  
 C’était le portrait de Marat, qu’elle décrit comme un castrat qui chante dans son bain de sang.