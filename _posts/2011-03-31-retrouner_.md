---
title: "retrouner ?"
date: 2011-03-31 19:13
tags:
  - billet
---

Je me suis rendu compte que _LE CONDUCTEUR_ était là : il avait jamais quitté le navire, il filait son sillage. À dix ans de distance il avait suivi le même personnage, l’avait repris de deux points de vue différents : une fois à Paris, une fois à New-York, dans les mêmes ivresses de haine sur lui, parmi toutes les raclures à réduire, à achever dans l’étouffement, .  
Lui n’avait aucune importance ; il n’était que prétexte à écrire. Il était là sous deux noms différents mais c’était bien le même : le même manteau, le même faciès abruti. C’est comme si je lui avais tiré dessus par deux encoignures.  
Je suis toujours satisfait de ça comme d’_Épistaxis_, ce texte qui est apparu tout d’un coup de nulle part, et qui venait en définitive se loger dans la Cosmologie, lancer une dérivation, un chapitre qu’on avait pas écrit, resté en blanc, un endroit oublié, une friche.  
Ce _CONSTRUCTEUR_ qui travaille en nous est toujours fascinant. Le diamant avait sauté au-dessus ce sillon manquant ; et le voilà aujourd’hui bien en place dans son ensemble gravé, grave.  
La complétude : non. Il restera des tonnes de reprises par-tout : les misérables ont souvent des costumes rapiécés ; trop de carences, des caries ; idem en syntaxe, en apprentissage des beaux immeubles. Mais pas du manque. Ça on le laisse aux poètes astringents, à toutes les crevures de suivistes, aux bavards carrés qui ont plein du réel dans la gueule et qui disent qu’il lui faut des manques. Lacan aussi le disait et les archidiacres, tous les nantis que le manque fascine autant que les désastres et la pauvreté. Or le trou ne manque à personne ni de rien (dans l’Enfer de Coluche, les bouteilles ont des trous et les femmes n’en ont pas plus que le réel).  
Mais du moins on a rangé ce morceau de bois, ce débris tombé de la raboteuse. Je l’ai replacé sous le _Tas_, sous la verrière de l’Atelier, parmi les autres madriers. Et surtout les autres petits bouts de _ligots_ quelconques dont on faisait d’improbables sculptures, dans la sciure et le son.  
On se disait : “Où est-ce que cette hémorragie va bien pouvoir nous entraîner ?” Qu’est-ce encore que cette foutue digression, quand c’est donc qu’on va finir par marcher droit ? Encore une façon de rien finir, de tout remettre (_mais à qui_ ?)  
Au contraire. Tout s’emboîte. Parfaitement. Alors pourquoi on irait réclamer d’être l’auteur ? Responsable civil, oui, ça, certes. Mais dans les fariboles, les fanfrelas, les guipures, tout cet amusement ?   
Cette baudruche de Breton dans sa navrance qui voulait mettre son cachet sur l’inconscient, le légitimer. Ou Connard le Barbant qui voulait être roi du Port et qui passa (enfin !) un peu chez les fous, histoire de lui aplatir la bite qu’il avait déjà molle, aux neuroleptiques. Repris en maison de repros : là où on photocopie la dinguerie, gagné par sa diarrhée mentale.  
Un auteur : _l’autreu_. Et sinon l’autruche. Qui triche.  
_L’Autre_, avec un grand A, celui qu’on détestait le plus dans la Tribu (surnom populacier, rien à voir avec la psy), le salopard de l’étage au-dessous, qui nous préparait des embûches, précipitait le malheur. Il en a fait des petits morceaux, il a rancardé des misères, il en est responsable par bien des endroits.  
Tant qu’on atteint pas à cette démesure ça n’existe pas.  
Des figures aussi ont surgi dans l’envers de plusieurs dessins, les retournant, mais pas dans le sens historique de Kandinsky, plutôt dans celui de Tex Avery (_retrouner_ ?). Menant une main étrangère, et reconnaissable à sa sagacité, à son soin dans les zones intermédiaires, créant des ponts et des passages, dans le _ma_.  
O. N.  
  
_ndlr : pas de date_