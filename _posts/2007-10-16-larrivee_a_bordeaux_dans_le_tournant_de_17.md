---
title: "L’Arrivée à Bordeaux dans le Tournant de 17"
date: 2007-10-16 18:19
fichiers: 
  - 302.pdf
sous_titre: "Les Grands Ancêtres. Ligne de Vivien de Nérac" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

Une version totalement erronée de ce texte avait été donnée ici précédemment à la suite de disjonctions informatiques ayant effacé certains passages et interverti des paragraphes.  
Cette version est la seule définitive de l’auteur.  
Isabelle Revay