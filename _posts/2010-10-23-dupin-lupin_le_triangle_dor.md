---
title: "Dupin-Lupin : le Triangle d’OR"
date: 2010-10-23 19:13
fichiers: 
  - Triangle_dOr.jpg
sous_titre: "Les Grands Ancêtres. Lignes d’Ossip et de Don Qui Domingo" 
date_document: "1917" 
tags:
  - document
  - HSOR
  - photographie
---

Élément à propos de la recherche d’Ossip en Russie par Don Luis Perenna et du lien de ce dernier avec l’héritage de Colomb par Don Qui. 

_NDLR_