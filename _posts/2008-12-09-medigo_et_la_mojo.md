---
title: "Médigo et la Mojo"
date: 2008-12-09 20:50
fichiers: 
  - Medigo_et_La_Mojo_Etats_du_Monde.pdf
sous_titre: "Les Adolescents. Été. Cádiz" 
date_document: "2000" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

À l’occasion d’un voyage de Ian McCoy en Andalousie, Garcia Medigo a mis en place avec lui “La Mojo Nation”, piraterie internaute héritée de Flint, des aventuriers des mers du XVIIIe qui rêvaient de contrées libérées du joug de l’Administration, du partage _peer-to-peer_ et des marins de Pynchon. Répartissant les données sur les ordinateurs de trois millions d’internautes répartis sur toute la planète qui partagent leur disque dur, ils travaillent sur le projet Seti@home à la recherche de preuves d’une vie extraterrestre dans les signaux issus de radiotéléscopes. Chaque portion de signal interprétée multipliée par trois millions dépasse la puissance des meilleurs supercalculateurs.