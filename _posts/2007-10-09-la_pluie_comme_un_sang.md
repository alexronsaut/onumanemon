---
title: "La pluie comme un sang"
date: 2007-10-09 14:07
fichiers: 
  - 286.mp3
sous_titre: "Les Grands Ancêtres. Ligne de Vivien de Nérac" 
date_document: "1989" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - son
---

Pages sonores inédites. Disque 2. Piste 18.  
Réalisation de Philippe Prévot dans les studios de LIMCA à Auch.