---
title: "L'Abbé Vincent"
date: 2014-07-27 11:54
fichiers: 
  - Vincent.pdf
sous_titre: "Les Maigres Tendres. José & Marie. Printemps" 
date_document: "1971" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

Le choeur des petits enfants chantait déjà, alors qu’ils étaient encore près de la cloison qui rougeoie du confessionnal. L’Abbé Vincent dirigeait l’immense cortège de la retraite de l’Immaculée Conception sous tous les platanes de la place ; c’était intense : toute la foule quittait l’église après avoir salué Saint Michel de bois et de bronze noir. Les seuls endroits calmes, ça a toujours été les cimetières et les monastères, même si on a du mal à savoir où en est vraiment le foyer, sur ces grandes dalles. Les églises, ça bruisse.

ﾠ

_(lire la suite…)_