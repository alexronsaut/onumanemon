---
title: "Portrait de Nicolas"
date: 2008-07-25 19:54
fichiers: 
  - Portrait_de_Nicolas.pdf
sous_titre: "Les Adolescents. Ligne de Nicolas. Été. Lycée" 
date_document: "1979" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

**Daniel :** “Nicolas n’aime pas du tout Nicolaï. Tout juste s’il lui parle. Par contre il s’entend très bien avec Nycéphore, surtout pour notre projet du “Styx”. Mais Nycéphore et Nicolaï pensent tous les deux que leur frère Didier, s’il avait vécu, aurait pu être comme ça.  
  
De la même façon qu’en sculpture je reprenais des mises en scène précédentes de personnages en bas-reliefs, des maquettes que j’avais déjà disposées dans la salle de modelage ou des figures découpées et rapportées. Nicolas, lui, utilisait ce qu’il appelait ses “papiers bohémiens”, écrits en route et enfoncés dans les poches. Chacun de nous deux assemblait à sa façon des morceaux autonomes. On était déjà dans la double articulation sans sauce romantique. On se sentait frères de Mozart. C’était un baladin d’adoption qui fumait, buvait, et écrivait énormément. Un très bon poète incapable de la moindre réalisation matérielle. Il rêvait d’épouser une Gitane, c’était son but dans la vie, et pour cela fréquentait les roulottes de tous les campements rencontrés. Étant par excellence un _Sujet du Bord_, qui _zigzague en tzigane_, ce projet des “Enguirlandés” l’enthousiasmait.  
La première fois que Nicolas est venu de Libourne (son père avait une industrie là-bas), j’ignorais qu’il eût un frère mort. Il m’a simplement parlé de sa maison, d’une façon sommaire : de la porte sur la cour, puis de l’ouverture générale vers le village d’abord par le grand portail, ensuite par la route remontant au nord ; au sud et à l’arrière il y avait d’immenses champs, des prés herbeux, et au loin les faubourgs grisâtres et roses pâlis.  
C’est dans ces faubourgs qu’il a rencontré Claude qui vit près des décharges, “Fouailleur” minier qui perce avec le croc l’amas gelé des détritus à la recherche d’un élément rare, sépare le Ciel et la Terre à l’aide de ce trait et grâce à lui du Chaos se tire. Dans la famille de Claude, il ne leur restait plus que la roulotte, et la compagnie des gitans vivant des frites et de la vente des animaux ; autant partir ! Autant suivre cette ligne toute de contiguités à présent, cette ligne de chant du récit que heurtent les récifs, ces blocs primaires la trouant.