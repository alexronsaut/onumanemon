---
title: "Aïeul Colomb 3"
date: 2008-01-12 18:36
fichiers: 
  - Don_Qui.Aieul_Colomb_3.pdf
sous_titre: "Ligne de Don Qui Domingo" 
date_document: "1992" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

**_Christo Foro_**  
Déjà Christ lui-même, et cheminant pauvre avec son fils Diego, cassant la glace ou jetant les cendres dans le vide, construisant de mini-tactiques de guérillas contre la Dépression (sortir dans le froid, rencontrer son corps, rendre visite à un marin de Moguer pour retrouver sa voix), et tombant à genoux sur les marches du monastère et couvent de La Rábida, implorant de l’eau et du pain pour son fils et demandant à être reconnu dans ses folles aspirations.  
Et l’étant enfin grâce à Juan Perez le prêtre-ouvrier-mécano (vie ordinaire et limitrophe) et à ses courriers vers Isabelle. Et cette expansion de Noël reconquit Grenade. Mais si un écuyer ne l’avait pas rattrapé au Pont de Pinos, l’Amérique n’existerait pas.  
  
Marco Polo a regonflé le disque de Macrobius et les moines eux-mêmes arrondirent la Terre en dépit des Rois Catholiques.  
Christo a vu les photos qu’il a rapportées de Cipangu et les idéogrammes de Cambalu ; c’est depuis Sagres que les limites du monde ont reculé.  
Et toutes les flèches différentes volent pour se confondre dans le même horizon : Herman, Cortès, Francisco Pizarro, Vasco Nuñez Balboa, Alvarado : il s’agit de prendre l’Islam à revers avec l’aide du “Prêtre Jean”, à moins qu’il ne s’agisse déjà d’Arthur, en Abyssinie.