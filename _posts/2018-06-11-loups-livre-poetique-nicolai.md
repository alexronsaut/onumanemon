---
layout: post
title: Loups !
date: 2018-06-11 00:00:00 +0000
sous_titre: Livre Poétique de Nicolaï
date_document: 1979
fichiers:
- "/loups.pdf"
tags:
- texte
- OGR
- document

---
31\. Loups !  
\(_La laisse deleatur_)

Lézard et Serpent d’abord ;  
L’orage au ciel, la rage au sol  
Sur Marie Gay, défigurée  
Sous la lune, en prairie fraîchie de six heures,  
Et frise des bois glacés.

  
C’est du cirque où marche un ours  
Chocolant ventre d’un an, de Limoges,  
Que les montreurs de ménagerie  
Ont lâché une louve enragée, bavante.  
« Empeste au diable, ch’tit chien noir ! »

  
Neuf jours badés, neuf jours barrés,  
Neuf jours de chair, neuf nuits de sang,  
Loups renaissant parmi les failles,  
Trois mois sur l’herbe et trois sur vent,  
Ayant réduit enfant en bouillie pour croire,

  
Pour surgir. Cachés en ronces et faits de bords,  
Eux, enhardis d’impunités en leurs aziles,  
Dépècent dans l’Initiation d’autres viandes ;  
Et les fleurs blanches dont abonde  
Le ciel défait, vibrent à leurs dents d’acacias.

_(lire la suite…)_