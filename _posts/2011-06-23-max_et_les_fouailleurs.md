---
title: "Max et les Fouailleurs"
date: 2011-06-23 23:05
fichiers: 
  - Ca_Presse_ndeg49.Juin_2011.jpg
sous_titre: "À propos de l’exposition Max Schoendorff à l'URDLA" 
date_document: "Mars 2011" 
tags:
  - document
  - HSOR
  - texte
  - edition
---

_**Surréalisme**_.  
“Il y a quelque chose de mal digéré dans le surréalisme, disait Denis Roche et c’est un stade peu important de sa digestion.” C’est sans doute l’illusion de la profondeur : celle de l’inconscient comme boîte noire autant que celle de la plupart des représentations picturales de ceux qui se réclamaient de cette école.  
Au contraire chez Max on est à peine dans une épaisseur indurée de la peau, à l’endroit où se fichent les échardes ou les pinces à clamper. On est chez les fouailleurs, ceux qui crochètent les tas d’ordures à la recherche d’un aliment, les _gamines_ du Brésil ou d’ailleurs.  
Les fous sont ailleurs : ils crochètent dans le déchet en rêvant du Paradis alimentaire de Pinocchio.  
“Mais quoi qu’on fasse on ne ramènera aucun trésor au jour : on ne fait que retourner de l’ordure.” me dit un parano quelconque et lyonnais. Au contraire. Et ce que les _gamines_ cherchent, ce n’est précisèment pas de l’ordure mais un morceau de vie. Il n’y a pas à aller chercher loin : c’est ici et maintenant. C’est sans doute la leçon à tirer des révolutions arabes Facebook. À quand chez nous ? À Caen les vacances, disait Devos.  
 _**“Ça va muscler !” Le Vide de la Plaque.**_  
À Saint-Michel, lorsque “Nez-Rouge” l’air revêche au premier pastis du matin disait “Ça va muscler !” ça voulait dire que toute cette sorte de chaos qui s’était accumulé en lui depuis quelques jours allait trouver son expression sur un lit d’hôpital… _etc.  

Pour lire la suite, reportez-vous au [Ça Presse n°49 de juin 2011](http://www.urdla.com/urdla/actualites.htm)