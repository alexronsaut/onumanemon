---
title: "Nycéphore chez Soudain d’Arlac"
date: 2008-10-21 12:43
fichiers: 
  - 2.NycephoreSoudain-Arlac.pdf
sous_titre: "Les Escholiers Primaires. Ligne de Nycéphore. Été" 
date_document: "1976" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

Mon unique photo lauréate et prise dans les bois d’Arlac près de chez _Soudain_ (fils de _Houdin_ et de _Boudini_, originaires de colons du Soudan), cicerone pour la récolte des cicatrices, voilà où je m’enfonce, avec la sale humidité, les graminées coupantes, sans savoir encore si j’ai quelque chose de commun avec le narrateur précédent, gomme qui rit de sa beauté morale et de sa difformité physique. Je me souviens de mon enfance au milieu des essaims d’abeilles d’Abel.  
Avec le Rolleiflex, j’avais eu du moins la chance de découvrir “_la visée ventrale_”, l’appareil chaud tenu sur l’abdomen, comme en venant plus tôt le matin en vélo à Arlac, “l’énorme crabe rouge abstrait” sur un champ de blé de Dufy Dingo dans la revue “A la Page”, et la salle des pas perdus.  
Je me rendais là-bas avec ce vélo sur lequel j’avais fixé un guidon étroit, le vent violent plissant la chemise de nylon, poignets vers l’intérieur, position rentrée ; secouée.  
Puis au retour chez moi, toute l’eau avait envahi le débarras de l’ancien Couvent où nous logions, alors que je me sentais déjà tellement floué (quoiqu’il en soit, je boulonne !) de n’avoir obtenu, pour toute récompense de mon premier prix de photographie qu’un séjour de quinze jours de nettoyage et de restauration d’une ferme au Kansas où se promettait de m’accueillir un couple super sympa (“..._et si vous avez fini assez tôt, vous pourrez vous amuser avec nous à faire les courses le week-end dans le patelin avec toutes les sortes de gens et de commerces typiques, et, pourquoi pas, on pourra même vous offrir une bonne bière_ !”)  
Point d’aboutissement de multiples écoulements et de gouttières romanesques, le débarras, chambre noire, que l’eau inonde. L’eau, l’eau envahit tout comme la révolution des bourgeois charcutiers et marchands de grain l’église St-Pierre et la chapelle du Martyre, l’eau dont le niveau monte depuis les fossés du jardin désolé en contrebas. Heureusement, je trouve un tuyau de plomb qui curieusement surnage, et, le tirant, je retourne un lavabo qui me sert de coque de noix et me permet de pagayer jusqu’à regagner la chaussée.  
Chez les Sœurs, on s’affole aussi : tous les Saints flottent.