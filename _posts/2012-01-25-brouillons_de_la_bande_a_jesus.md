---
title: "Brouillons de La Bande à Jésus"
date: 2012-01-25 22:55
fichiers: 
  - Bande_a_Jesus_.pdf
tags:
  - document
  - HSOR
  - texte
---

Ce texte assimilable à _La Bande à Jésus_ n’a pas été repris dans les **États du Monde** ni dans _**Histoire Deux**_, du Continent **OGR** (à paraître dans le collectif _Courte—Line_**)**. Le personnage de _Issa_, cet ancêtre du Christ figure toutefois dans **_Crampes_**, le “recueil gitan” de **OGR**. 

_Isabelle Revay_   
  
  




(_Bande à Jésus ?_) 

Dans ce quartier plus de sabbat ni de boue : des oiseaux ! La pine à gauche, la jambe à droite, Aucune dignité de la chair, La langue noire, Et des passereaux vifs !  
Nouvelle traversée du jardin Capitan Et sa maison des oiseaux En compagnie de Joseph, Avec Husserl, le roi des claquettes, Arsène, qui trime comme graveur chez Leblanc. Et l’enfant qui le heurte dans les Arènes tombe mort.  
On paye pour la ménagerie : Fauves trempant dans le lac d’urine, Anges loukoum aux ailes de carton ; Mille menues attractions en éclipses Dont les pharisiens si terriblement bêtes.  
Là-bas la Fille de la Commune, nue jusqu’à la chair, Jambes coupées, le moyeu vide, Sur le matelas de sommeil des lettres En Grande Truanderie.  
Tandis que _Lui_, Exalté par la forme et le nom de la première lettre, Laisse le cartable au ruisseau, Dégorgeant de caricatures, De journaux illustrés, De résidus de Malakoff, Et lance à tous la date de leur mort.  
  


_Lire la suite…_