---
title: Appendice
date: 2023-12-19 8:05
sous_titre: ""
date_document: 2014
tags:
    - document
    - HSOR
    - texte
fichiers:
    - "/hsor-appendice.pdf"
---

La première et originale édition de cette “Cosmologie Portable” a été diffusée par les éditions Mettray accompagnée d’une grande eau-forte sur cuivre réalisée dans les ateliers de l’URDLA, constituant une des cartes de ladite Cosmologie.  
Malheureusement quantité de corrections et suppressions n’avaient pas été prises en compte avant l’impression à cause d’un transfert défectueux sur un autre logiciel.  
Nous donnons donc ici la version correcte prévue et définitive.  
Cet appendice n’est pas exhaustif et ce n’est pas non plus un plan originel qui aurait régi la constitution de l’œuvre. Complémentaire des Cartes successivement éditées, il a été lui aussi augmenté au fur et à mesure de l’avancée du travail.  
Les personnages, ou plutôt les _Figures_ ici décrites ne représentent qu’une infime partie de la Troupe Cosmologique ; de même, les indications de _construction_ ne sont jamais que _régionales_ : ici valides, ailleurs elles ne le sont plus.

_Quand bien même la Cosmologie n’existerait pas_, cet _Appendice_ doit être considéré lui-même comme un texte-projet utopique, au même titre que les recueils ici nommés _Absolus_.
