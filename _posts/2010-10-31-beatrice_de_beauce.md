---
title: "Béatrice de Beauce"
date: 2010-10-31 18:27
fichiers: 
  - Bea.Beauce.pdf
sous_titre: "Les Adolescents. Hill et Joyelle. Terre 4" 
date_document: "1984" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

Pour son projet sur les Enfants Croisés, Jean s’était largement inspiré des _Portes du Paradis_ d’Andrzejewski, livre que lui avait fait connaître Lydou en même temps que _Cendres et Diamant_ ; il avait vu également les deux films de Wajda qui en avaient été tirés. Il avait donc demandé en novembre 1975 à Tourangeau et Don Jujus de venir l’aider pour les repérages à Tours, bien qu’il ne les aimât guère, car ils étaient natifs de ce pays de marais et pourraient l’aider à se dépêtrer des miasmes.  
Tourangeau surtout possédait une aimable demeure tourangelle située sur les bords d’un affluent de l’Indre au centre d’un domaine de cinq cents hectares, dont une partie des granges pouvait être aménagée en studio, avec un pavillon des Quatre-Saisons vitré de rouge, de bleu, de jaune et de vert, pour abriter les accumulateurs, les dynamos, les projecteurs, tout le matériel électrique et toute la régie.  
Il y avait paraît-il des traces de passage des Enfants Croisés depuis Vendôme, du côté du Dolmen de La Roche aux Fées et de la Colonie Pénitentiaire de Mettray ; certains étaient passés par Château-la-Vallière et la route du Mans et d’autres étaient venus de Beauchêne, du Plessis-Puçay, du Thouard ou encore de Chastillon sur Indre.  
  
  
_lire la suite…_