---
title: "Jany-Janus"
date: 2008-07-23 09:44
fichiers: 
  - Jany-Janus_Etats_du_Monde.pdf
sous_titre: "Les Gras. Printemps. L’esplanade des Girondins" 
date_document: "1978" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

L’Esplanade rutilait de mille feux, miroirs des carroussels et des labyrinthes que venaient à peine embuer les fumées graisseuses qui flattaient le palais des nouveaux dieux.  
Jany-Janus tenait le Palais des Glaces.  
(Jany-Janus était une prostituée révolutionnaire anthropophage qui avait mangé son frère pour posséder les deux sexes à la fois ; à partir de là, hermaphrodite aux deux versants elle devint et s’offrit d’abord à Paris comme un travelo jeunot recto-verso, sur les Boulevards, porte Saint-Martin, Saint-Denis, et notamment à toutes les sorties des théâtres de ces mêmes boulevards avant de venir s’installer à Bordeaux.)