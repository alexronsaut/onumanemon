---
title: "Nycéphore : Secret des Ecoles"
date: 2007-10-09 13:57
fichiers: 
  - 293.mp3
sous_titre: "Les Escholiers Primaires" 
date_document: "1989" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - son
---

Pages sonores inédites. Disque 2. Piste 28.  
Réalisation Philippe Prévot dans les studios de LIMCA à Auch.