---
title: "Histoire Deux"
date: "2022-01-16 16:25"
sous_titre: "Nouvelles de Nycéphore"
date_document: "1976-1982"
fichiers:
  - /histoire-deux-1976-1982-premiers-extraits.pdf
tags:
  - document
  - texte
  - OGR
---

Tandis que le vent rabat la fumée de nos cheminées sur le ciel gris de neige et le fond des sapins, j’absorbe cette vue avec la même nécessité d’imprégnation que celle qui existe à partir d’un texte historique ou dans toute autre forme de récit. Seule la poésie offre la plus grande succession fragmentée de climats ; la force d’incantation visionnaire d’un poème comme Les Classeurs ou Terre de Grogne (1), par exemple, est pour moi colossale et n’a rien à voir avec un jeu littéraire ou une astuce verbale ; dans Les Chercheurs d’Or (2), une infime blessure ouvre un monde, comme l’agate de Roman. À toute communication préférons l’hermétisme du Chant ou de la danse guerrière.  
Ainsi vont les rêveries d’un enfant au fond d’un grenier à partir d’archives familiales, ou les constructions historiques qu’on se fait, les histoires deux, ces curieuses imprégnations à partir des récits entendus en classe et des livres d’Histoire lus.  
Grâce à ma maîtresse primaire (Mlle Angélique !) j’ai pu ainsi assister à la Saint-Bartélémy en la replaçant à Saint-Michel et la Sécession à Saint-Augustin. En d’autres quartiers de Bordeaux ou des villages connus, j’ai construit mes premiers récits de cape et d’épée et surtout j’ai participé à l’exaltation des Enfants Croisés qui sont venus me rendre visite la nuit, et qui agissaient essentiellement du côté du Maucaillou ; tout le Moyen-Âge était là et rayonnait tout autour de la maison du bourreau rue Saumenude jusqu’aux Halles des Capucins.

Il a suffi ensuite que Nicolas reçoive ses Commandements près du ruisseau du Peugue et devant l’entrepôt des Autobus, à Lescure, pour que ça tourne au carré, comme la Fortune !  
Il ne s’agit donc absolument pas de couleur locale ni de vérité historique mais de crever espaces et époques les unes par les autres aussi vrai que le Christ a suivi la route 66 et a subi l’horreur des urines de la place Canteloup.

(1) : Livre Poétique de Nycéphore  
(2) : Livre Poétique de Nicolaï

Hiver 1982
