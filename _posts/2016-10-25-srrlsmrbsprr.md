---
title: "SRRLSM/RBSPRR"
date: 2016-10-25 10:37
fichiers: 
  - srrsm.rbspr_.pdf
sous_titre: "Enquête à propos du Surréalisme" 
date_document: "21 Août 2015" 
tags:
  - document
  - HSOR
  - texte
---

_Ceci en réponse à une enquête organisée par **l'URDLA**, et qui a donné lieu à un ouvrage intitulé **Trois Chameaux rue de la Convention**_

Qu’avez-vous fait, vous, les gidiens,  
les cérébraux, les rilkéens,  
les mystériens, les faux sorciers  
existentiels, vous, les pavots  
surréalistes qui flambiez  
sur une tombe, les cadavres  
de la mode européisés,  
les blancs asticots du fromage  
capitaliste, qu’avez-vous fait  
devant le règne de l’angoisse,  
devant cet obscur être humain,  
cette présence piétinée,  
cette tête qu’on enfonçait  
dans le fumier, cette nature  
de rudes vies foulées aux pieds ?

Vous avez pris la poudre d’escampette  
pour vendre des morceaux d’ordure,  
pour chercher des chevaux célestes,  
la plante lâche, l’ongle ébréché,  
la « Beauté pure », le « sortilège »,  
des œuvres de pauvres capons  
pour que les yeux s’évadent, pour  
que les délicates pupilles  
s’embrouillent, pour survivre  
avec ce plat de rogatons  
que vous ont jeté les seigneurs,  
sans voir la pierre à l’agonie,  
sans protéger, sans conquérir,  
plus aveugles que les couronnes  
du cimetière, quand la pluie  
tombe sur les fleurs immobiles,  
les fleurs pourries des sépultures.  
_Pablo Neruda. Le chant Général._

Pourquoi répondre — Je réponds à ceci par intérêt pour votre travail d’artiste et votre travail d’enseignant et parce que très précisèment, voilà un peu plus d’un an, par une sorte de “hasard objectif”, au moment où j’étais en train de distribuer pour les amis de l’URDLA un de vos livres dans les confins les plus reculés de Paris, j’ai eu la tentation de chercher le siège du dernier Cercle Surréaliste, histoire de voir à quoi ça pouvait ressembler aujourd’hui. Et je me suis retrouvé dans un quartier sinistre de cités, un non-lieu, comme s’il s’agissait d’une adresse fantôme.

J’ai toujours été à la recherche d’une fraternité impossible, dans une sorte d’enthousiasme à venir. Dans ces temps de sinistrose il aurait été curieux de voir si des surréalistes attardés réussissaient enfin à travailler dans des conditions de laboratoire telles qu’ Artaud les voulait.

SRRLSM SRRLST — Voilà encore quelque chose qui me pousse à vous écrire, que ces consonnes imprononçables. J’ai écrit ainsi Robespierre RBSPRR dans Quartiers de ON ! À la fois le Tas de Pierres de Hugo et le tranchant consonantique de la guillotine.

 _(lire la suite…)_