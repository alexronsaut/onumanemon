---
title: "Exposition à l’URDLA, Centre International de l’Estampe et du Livre"
date: 2012-04-13 11:30
fichiers: []
sous_titre: ""
date_document: "Du 4 février au 13 Avril 2012"
tags:
  - extension
---

- [Télécharger le document](/files/D.Presse_URDLA_2011_Def.Crane_.pdf)
- [Photopgraphie](/files/1RouleauCreteCuivreTetes.jpg)