---
title: "Poor Arthur"
date: 2015-04-08 21:14
fichiers: 
  - poor_arthur.pdf
sous_titre: "Tribu des Gras" 
date_document: "1984 et Après" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

_**Poor Arthur**_

Il faut descendre Arthur, mais on sait pas par où le prendre, comment le saisir. À chaque fois qu’il revient, il est différent, teinté de Marseille, de Siddi-Bel-Abbès, de Colomb-Béchar et sa section de Discipline, en train de casser des cailloux à la masse, passé par le Kef ou de retour de l’île du Diable ; il a toujours vu les gardiens révolver au poing depuis sa naissance. Ça dépend avec qui il erre.

Arthur avait commis un crime sur un chantier en se faisant passer pour Louis, après avoir volé ses papiers d’identité, et Louis avait hérité de son casier judiciaire qui s’élevait à hauteur d’homme.

On serait bien allé le voir dans sa “concession à perpétuité”. Pour peu, Henri l’aurait crevé d’un coup de couteau, et il aurait commencé à l’embaumer par le ventre.

Y’avait un Amar, là-bas, à Cayenne, à la Case des Fous, prénommé Arthur comme lui, qui habitait rue Verte à Caudéran.

Lui en réalité c’est Jules-Arthur, mais il voulait pas entendre parler de Jules, il trouvait que ça faisait pot de chambre. Nous on trouvait que ça faisait pas assez excessif. “Arthur, ça crache sur le soleil ; Jules, c’est tout juste si ça pisse !”

  
  
_(lire la suite…)_