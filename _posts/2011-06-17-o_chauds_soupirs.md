---
title: "Ô chauds soupirs"
date: 2011-06-17 22:51
fichiers: 
  - O_chauds_soupirs_.pdf
sous_titre: "Texte de Joël Roussiez" 
date_document: "2011" 
tags:
  - document
  - DAO
  - texte
---

**Ô chauds soupirs! (Louise Labé)**

J'entends à l'intérieur une bête qui grogne et secoue mon cœur sur toute la longueur de ma poitrine et je médite alors sur le temps court de ma vie. On peut être d'un autre avis et me trouver un temps à vivre long mais il me semble que j'approche de la fin et cette bête qui grogne bat le temps qu'il me reste. Elle ne grogne en effet que par à-coup mais assez régulièrement comme si..., comme si quoi? Me dis-je en me prenant les mains et secouant la tête, cela porte à méditer et cependant méditer sur quoi, quoi donc dans mon cœur bat et qui ou quoi grogne ou ronge sous les os du thorax? Qu'on me ronge les organes est une chose curieuse car je ne sens que peu de douleur et il m'est donc difficile d'imaginer un animal vivant sous l'enveloppe cutanée. Pourtant je sens que mes forces déclinent doucement et que, dès le matin, j'ai des affaiblissements de jambe qui m'obligent à m'asseoir assez vite avant de reprendre le cours normal du réveil. Pourrait-on chasser une bête pareille avec des poisons et des drogues?  
  
_lire la suite…_