---
title: "Aube à Tokyo"
date: 2007-12-14 16:21
fichiers: 
  - Aube_a_Tokyo_0.pdf
sous_titre: "Les Adolescents. Ligne d’Aube. Terre" 
date_document: "1978" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

11 000 mètres d’altitude. 900km/heure. Passage le long du cercle polaire, soleil de minuit, nuit pincée entre la lumière du couchant et celle du levant. Intense humidité stagnante. Nuit. Shinjuku.  
Aube écrivait à Monique (qui aurait dû venir) que son expo à Tokyo le 26 août faisait partie de “Femmes et Histoire”. Mr Yamagishi lui offrait le luxe de montrer des morceaux de peau et de chair dans une galerie. “Les femmes ont créé 52% de toutes les formes de pensée humaine ; que les hommes assument au moins les 48% qui leur restent !” Elle avait envoyé un télégramme à l’ambassade de Russie Bd Lannes pour le soutien des femmes russes en lutte comme elle en enverrait quatre ans plus tard encore pour éviter le séjour en camp à Nathalia Lazareva. À Shigel elle avait dit : “You are not living in my body.” et “J’ai le droit de briser l’ordre des chapitres.”  
À Tokyo elle fit de l’Ikebana avec Yemoto et d’autres Senseï ; mais tous ces Senseï et ces dames très courtoises de la bourgeoisie l’agaçaient comme un rebrousse-poil électrique sur un pelage de chat. Elle avait amené de la poudre de cacao Van Houten et du sucre en poudre pour leur faire des crèpes comme ils aimaient quand ils logeaient chez elle rue de Lancry pour des stages à Paris, avec Sankai Juku. En sortant dans la rue elle avait parlé avec un jeune garçon qui faisait partie d’un groupe politique contre la bombe atomique. Dans la rue elle acheta tout un tas de coupes, de bols et de baguettes. “Les choses, quand elle sont juste perçues, ne sont pas encore cochées.” Plutôt le futur pour son texte de présentation : _“Les traversées des apparences se feront là, à travers ce gris.”_