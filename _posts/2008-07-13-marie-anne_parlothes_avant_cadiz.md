---
title: "Marie-Anne Parlôthes avant Cádiz"
date: 2008-07-13 15:56
fichiers: 
  - Marie-Anne_Parlothes.pdf
sous_titre: "Les Escholiers Primaires. Ligne de Nycéphore. Terre" 
date_document: "1978" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_**Les Enfermés (ils ne partiront pas !)**_  
_**Nycéphore**_  
J’ai rendez-vous avec Marie-Anne à trois heures à la Clinique. Je lui ai téléphoné hier à midi depuis le petit bureau de poste de Saint-Augustin alors qu’il allait fermer, et j’ai visiblement retardé l’enfilage de manteaux du couple de vieux qui doit être employé là depuis la fondation.  
Ô Femme, voix profonde ou suraiguë, bombement de son sexe si finement perçu à chaque modulation vocale ! Théodosius ! Marie-Anne avait en effet comme “doublure” grasseyante son délicieux accent austro-hongrois, grave. “C’est elle-même. Viens chez moi à six heures.” “Si c’est possible, j’aimerais mieux qu’on se voie ailleurs ; je ne tiens pas tellement à voir ton père.” (Son père s’était opposé à son départ avec nous et toute la troupe de théâtre de Cádiz.) “Bon, et bien, retrouvons-nous à l’Hôpital de Jour ; c’est boulevard Wilson, près de la Radio ; j’ai eu une dépression nerveuse ; on peut se voir demain à 15h, si tu préfères.”  
La dernière fois que j’avais rencontré Marie-Anne, c’était pour la fête chez Walter H. lors de la préparation de l’Opération Cádiz (entrecôtes et ceps en sous-sol), lors de la descente des flics ; elle était saoûle, et je ne sais plus qui l’avait embrassée ni comment elle était rentrée chez elle.  


**\***  


Je suis à l’Hôpital vers 15h 30. Là, d’autres pensionnaires aux yeux bouffis, comme ourlés, gonflés à l’hélium, gestes neuroptisés atrocement lents la cherchent sans la trouver. L’âme d’Elcé exaltée par le jeûne.  
Elle n’y est plus, et pourtant, elle attendait avec impatience sur le pas de la porte vers trois heures moins le quart, selon ce que tous me disent, héroïsme qui emplissait les fenêtres d’une crainte indéfinissable.  
On regarde au sous-sol, et de nouveau dans les étages, dans sa chambre.  
Les Infirmières :   
« Qui êtes-vous ? Vous êtes de la famille ? (L’une rougit violemment.)  
— Elle doit être rue Maître-Jean, dans l’autre clinique ; elle devait rendre visite à un ami. »