---
title: O (Orphée hermétiste)
sous_titre: ""
date: 2025-02-02 16:49
date_document: "1973-2000"
tags:
  - document
  - texte
  - O
fichiers:
  - "/o-orphee-hermetiste.pdf"
---

MÂLE TEMPS

Un homme est devenu en quelque sorte à lui-même son propre rébus. C’est cette histoire que nous allons vous raconter.

J’ai eu parfois quelques abattements à ne pas avoir écrit d’évangile. Je suis si bien, _“Deus lo vult”_, comme ceux-là qui criaient, affublés de fausses culottes, singes ornés des maux des autres, ou l’homme qui s’incruste dans le Temps et qui dans l’espoir de mener à bien sa seule œuvre, refait toute sa vie. La finir n’est pas pire que la commencer ; en tout cas c’est ce qu’on regrette tout du long.

On se doute que j’étais loin!

Ma jeunesse était une sombre galerie dans la ville d’Irbiz très fameuse et tout à fait magique, cependant où donnaient quantité de fenêtres : des pèlerins traversaient les ruelles pleines de serpents, mais parfois la réalité des serpents cède et certains sont obligés d’enjamber et de sauter au-dessus de la vérité d’un fils qui est là tout de suite (elle a un fils : Frantz ; le père mesure les parts de poisson, des morceaux de viande cuite aussi bien, mais il en rajoute un morceau pour ce fils très adorable) ; et d’autres portent des écrevisses avec eux sur leur dos, qui jouent avec des enfants à faire des roulades sous le ciel comme les croissants qu’on y trouve !...

Je reste muet. À quelle fin une opération si terrible en cet endroit sur un enfant tellement pâle ? Là j’ai atteint quelque chose d’irréversible, stupéfait.

Tu as de longues fleurs autour de tes jambes et de tout ton corps, très joliment, au Maroc, le soir. Il faut _chercher en creusant_ à travers les Nuits : c’est la clé de la jeunesse et surtout la nouvelle science des interprétations. J’ai laissé si longtemps se reposer les ouvrages, perdus en jachère, en lichens et en marcottages...

Le torse abonde de fraîcheur ; on sort en chemise. Il est question d’un homme (jaloux de nos amours), qu’on doit _semer_ dans notre jardin noir où de grands soleils se balancent, au fond de l’atelier, de façon totalement abstraite : ce n’est ni un corps, ni de la matière ; on doit distribuer ses caractéristiques dans le sol, ses coordonnées neurologiques, sous forme de texte ou musique : l’humus va réussir à reproduire l’intégralité scientifique de cet homme, me dis-tu. “Il sera d’une grande beauté.”

-

_(lire la suite…)_
