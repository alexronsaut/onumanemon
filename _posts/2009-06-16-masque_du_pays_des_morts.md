---
title: "Masque du Pays des Morts"
date: 2009-06-16 18:10
fichiers: 
  - MasquePaysMortsCD0.jpg
date_document: "1984" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - dessin et gravure
---

Ce travail figure dans le cadre de l’exposition Sapet 2009