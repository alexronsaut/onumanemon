---
title: "Le Groupe à Bilbao"
date: 2008-10-29 20:01
fichiers: 
  - Le_Groupe_a_Bilbao.pdf
sous_titre: "Les Adolescents. Été" 
date_document: "1986" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**
