---
title: "Intermède CH 2"
date: 2009-10-13 20:52
fichiers: 
  - Terraplane_CH_2.pdf
sous_titre: "Tuberculose du Roman" 
date_document: "1972" 
tags:
  - document
  - OGR
  - texte
---

_Ce texte a été repris dans la partie Schola de l’Ourcq des États du Monde (Ligne des Escholiers Primaires. Ligne de Didier. Saison de la Terre. )_  
  
« Et toi, qu’est-ce que tu crois qu’il fait là-dedans ? dit Smilet.  
— Est-cccccccccccccccce que j’chsais ! » dit CH.  
John continuait à marcher en avant-garde, et CH et Smilet étaient restés à l’arrière, à discuter.  
« Qu’est-ce que tu crois qu’c’est, cette volonté de couper à travers prés?  
— .... ....  
— C’t’une métaphysique du crime. On peut le suivre dans son mouvement, c’est tout. C’est pas à cause de John que tout est dans la colle. I_l est le seul à être vraiment dans le temps_ ! C’est une phrase courte qu’il scande dans sa marche sans le savoir. S’i s’arrêtait, j’suis même plus sûr qu’il existerait ! Gamin, il disparaissait dans le charbon des trains. I fait partie de la mécanique de nos bagnoles, tu vois ! À conduire sa Terraplane d’une main et à tirer de l’autre. Rien n’obligeait vraiment à ce qu’il soit là. Aucune Histoire des Etats traversés ne prédispose à ça ; il est passé _tangent_ à chacun. C’est pour çà, qu’i veut qu’on aille de plus en plus vite.  
— Ch’sais pas, moi, dit CH. Ch’suis plutôt du genre sensible !