---
title: Cosmologie (Plan)
sous_titre: ""
date: 2024-08-18 14:46
date_document: "Après 1984"
tags:
    - document
    - dessin et gravure
    - Cosmologie Onuma Nemon
fichiers:
    - "/Cosmologie-Plan-.Gravure.Eau-forte-sur-cuivre.30x50cm.jpg"
---

Gravure. Eau-forte sur cuivre. 30x50cm

Don à la Bibliothèque Kandinsky de Beaubourg.
