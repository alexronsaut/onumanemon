---
date: 2019-12-23 9:42
title: Cible rouge n°5
sous_titre: Dos hétéromorphe
date_document: Après 1984
fichiers:
- "/Cible rouge.5.Dos hétéromorphe.jpg"
tags:
- dessin et gravure
- Cosmologie Onuma Nemon
- document

---
Pistolet 10m & dessin à la plume sur anciens cartons de cibles format 14 x 14cm en 5/10èmes