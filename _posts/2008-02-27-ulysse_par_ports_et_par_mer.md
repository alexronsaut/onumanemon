---
title: "Ulysse par Ports et par Mer"
date: 2008-02-27 22:55
fichiers: 
  - Ulysse_par_Ports_et_par_Mer.pdf
sous_titre: "Les Grands Ancêtres. Ligne de Mac Carthy. Terre" 
date_document: "Décembre 1968, 1978 et 1980" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**_Ce texte fait partie de “Quartiers de ON !” paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3) et du Livre Poétique de Nicolaï dans OGR en cours de publication sur ce site._**

_**Ulysse par Ports et par Mer  
(Lunette Arrière de la Voiture)**_  
Bel Ulysse, aux traits distingués sous les arbres  
Avec difficulté,  
Aux suées sous les ifs, les aunes, les yeuses  
Par les rues emporté, pluvieuses, dans la voiture,  
Sur la banquette arrière, balloté,  
  
Vois comme le vide est bon, du quotidien pulpeux  
Jouant d’harmonies symboliques  
Dans l’éclairage phareux des quinquets.  
  
Le Monde est magnifique de la Nuit ;  
Ton vaisseau va, rapide parmi les toiles  
Claquantes du ciel, les lueurs oranges  
Dont les bougnats sont ailleurs.  
Et c’est un incendie de millions de figures  
Que ton visage sous les saccades de poulpes de lunes  
Soudaines, aussitôt changeantes,  
Aux tentacules électriques.