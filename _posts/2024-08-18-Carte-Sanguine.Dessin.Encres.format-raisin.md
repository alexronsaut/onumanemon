---
title: Carte Sanguine
sous_titre: ""
date: 2024-08-18 14:46
date_document: "Après 1984"
tags:
    - document
    - dessin et gravure
    - Cosmologie Onuma Nemon
fichiers:
    - "/Carte-Sanguine.Dessin.Encres.format-raisin.jpg"
---

Dessin. Encres. Format raisin

Don à la Bibliothèque Kandinsky de Beaubourg.
