---
title: "Lambo à Staphysagria"
date: 2009-01-31 19:35
fichiers: 
  - Lambo_a_Staphysagria.pdf
sous_titre: "Les Grands Ancêtres. Don Qui. Été" 
date_document: "1984" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

VUE DE LOIN, la citadelle grise gigantesque dans les nuages avec une irradiation blanche verte, des nuages gris-verts, et dessous le sommet de la colline. Au-delà la nacre d’huître des crêtes de vagues tout autour de l’île, les glauques ourlets. Un idiot dans la voiture à côté de la mienne qui roule sa langue, gratte l’arrière de sa tête de sa main droite, bras torsadé, et qui enfonce en tournoyant sans cesse son index gauche dans son oreille gauche. Rien ne dira le bien fourni de ces nuées pas plus que l’immense cavalcade d’énormes cumulus sur la gauche doublant les arbres de la vallée. Là pour le coup devant le film la langue est pauvre. J’arrive dans mon quartier.  
Au film par contre échappe l’ampleur de la lumière orageuse sur toute la vallée, cette pénétration de l’or à travers toutes les couches du vert, la façon dont le clocher d’église repose sur un autre terroir de verts profus ; cela, seuls la peinture et le dessin peuvent le déployer.  
Incroyables liserés blanc magique fuligineux des nuées grises, ces hauteurs de château, sans doute après l’équinoxe, cette vibration de craquelures à mesure qu’on avance dans la voie de la vallée, ces tressements, ces chevelures en zigzag, ce boisseau de foudres : non certes l’écriture ne peut en rendre compte. Voilà le bloc où se trouve mon bureau.