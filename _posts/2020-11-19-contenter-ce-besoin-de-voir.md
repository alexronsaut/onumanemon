---
date: 2020-11-19 18:36
title: Contenter ce besoin de voir
sous_titre: Joël Roussiez
date_document: "2019"
fichiers: 
  - "/contenter-ce-besoin-de-voir.pdf"
tags:
  - texte
  - DAO
  - document

---

_Contenter ce besoin de voir_ est extrait de _Sur la Barque des jours_, La Rumeur Libre, 2019.