---
title: "États du Monde"
date: 2017-03-29 18:43
fichiers: 
  - parution_etats_du_monde_v4.pdf
  - mettray_2016.pdf
  - soiree_poesie.png.pdf
date_document: "2016"
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

_**ÉTATS DU MONDE**_

La réalisation de cet ouvrage, publié par les éditions METTRAY, correspond parfaitement au projet initial de l’auteur. Il concerne le Pré, les Grands Ancêtres et les Gras.

Ce premier volume sera suivi de quatre autres volumes (Les Maigres, Les Enfants, Les Adolescents, La Bande à Jésus), qui seront publiés sous forme d’édition numérique sur ce site consacré à la Cosmologie Onuma Nemon, également créé par Mettray voilà une dizaine d’années.   
En dehors des œuvres plastiques reproduites dans le texte, il y a une soixantaine de vignettes en couleurs et en noir et blanc destinées à être collées sur les emplacements désignés tout au long du livre, et qui participent à _l’étoilement_ du propos.   

Les _États du Monde_ sont l’aboutissement le plus important de la Cosmologie, mais bien sûr ils sont à considérer dans le réseau d’ensemble des Voix et des territoires des autres ouvrages déjà publiés ou disponibles sur ce site. On trouvera dans le numéro de la revue METTRAY paru en Septembre 2016 une présentation du volume ainsi qu’un dossier de photographies réalisées par Bernard Plossu dans le Quartier Saint-Michel de Bordeaux qui revêt une grande importance dans cet ouvrage.  
_NDLR_

Publication : [Mettray Éditions](https://mettray.com/livres/index)
