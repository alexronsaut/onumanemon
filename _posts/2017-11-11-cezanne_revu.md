---
title: "Cézanne revu"
date: 2017-11-11 12:20
fichiers: 
  - cezanne_revu.pdf
sous_titre: "Texte de Anne Guelvilec" 
date_document: "2014" 
tags:
  - document
  - DAO
  - texte
---

_(Anne Guelvilec écrit de petits récits (non destinés à la publication) où souvent les voix et les personnages, bien que distincts, s’enchevêtrent.) Elle est née à Quimper. Après des études de peinture au Canada, elle vit à Paris. Elle a bien connu Thomasine Wallace à la fin de sa vie à New York._  
  
« Jeanne, ma sœur Jeanne, comment nous vois-tu devenir ?  
— Plusieurs voix me conseillent d’être gentille avec tous ces artistes.  
— C’est toi, Jeanne, on a voulu que ça soit toi qui serves de modèle. C’est bien de toi qu’il s’agit ! Monet adorait les nuances azurées avant de les perdre, vermillons et garances, les teintes pures d’un seul jet, des épiphanies, les multiples rapports des choses visibles avec le ciel et la terre, l’harmonie lumineuse d’un grand système de vérité cosmique. Et Cézanne, souviens-toi : “J’ai épousé la mansuétude.” C’est autre chose. “Il n’y aura pas de Sainte-Victoire définitive de la vérité, mais en tout cas elle est peinte.” Rends-toi compte ! C’est à tout ça que tu peux participer.  
  
  
_(lire la suite…)_