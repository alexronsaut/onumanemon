---
title: "Cuba au Cube (Vigo, Klein, Isnard)"
date: 2007-11-11 18:52
fichiers: 
  - 314.pdf
sous_titre: "Les Grands Ancêtres. Ligne de Don Qui" 
date_document: "1989" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_**Voix d’OR  
Été**_  
(Ailleurs Colomb Croix Rouge !  
Toujours Ailleurs les Enfants Croisés !)  
Bain de l’Été répandu pulvérisé sur les muscles.  
Tintin l’Aventurier Croix-Rose.  
En 1946, il prend le Ciel, le Grand Ciel pur des punctures, et laisse la Terre à Claude Pascal  
_Monoton Son_ Mysticisme absolu sans Objet  
_Judao ! Waana ?_  
Plus loin : voix de l’Arc. Au-delà : zébrures Zen.  
Dans ce monochrome YKB : fond Giotto.  
L’assaut de vernis retenti dripping, au bord des auréoles, des drappés nocturnes, dans un squash des veinules. Cœur frappé ? Ça sert à quoi ? Inutile incendie de soi, gaspillage… Yang en excès. Folies désordonnées. Danse hystérique dans le salon. Rien de commun dans le cerveau ; même pas le sens. Peut-être initier plus tôt au Tragique, à la Prosopoppée dans le petit théâtre de bois peint construit derrière, à la hâte, sur la butte, au lieu de tout ce temps perdu, tout ça… (pffhvtt !)