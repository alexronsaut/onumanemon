---
title: La Mort des Amis
date: 2018-10-02 00:00:00 +0000
sous_titre: ''
date_document: 22 Octobre 1916
fichiers:
- "/La Mort des Amis.pdf"
tags:
- HSOR
- texte
- document

---
**LA MORT DES AMIS**

 

On peut penser que certains, un peu voyants, nous énoncent leur mort sans le savoir : Didier Morin parle de la femme écrasée dans un terrain vague par une _Alfa Roméo_ dans _Accattone_ de Pasolini, et Genet de “Ah ! Que ma quille éclate !” du _Bateau Ivre_ à propos de l’amputation de la jambe de Rimbaud pour ostéosarcome qui précède sa mort de peu.

Au cours de son séminaire Barthes envisagea une fois la façon dont la maladie ou la mort pouvait toucher un homme dans la part de lui-même qui lui était la plus chère. Il prenait comme exemple Benveniste touché par l’aphasie et Mallarmé mort d’un spasme de la glotte. Tout cela _très linguistique_, tout de même : on était dans les années 70.

 

_(lire la suite…)_