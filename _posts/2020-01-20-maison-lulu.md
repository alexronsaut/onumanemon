---
date: 2020-01-20 12:26
title: Maison Lulu
sous_titre: Livre Poétique de Nycéphore
date_document: "1968"
fichiers:
- "/maison-lulu-1968.pdf"
tags:
- texte
- OGR
- document

---
**Maison Lulu**

« Il va falloir sortir. O Neige dont je dors !   
Gredins je vais courir ! Flocons insoupçonnables !   
C’était donc fors cela que Midi est un soir !  
Cette rumeur grinçante des mats et trapèzes,

_(lire la suite…)_