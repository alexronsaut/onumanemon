---
title: "Journal de Lydou"
date: 2008-08-03 16:52
fichiers: 
  - Journal_de_Lydou.pdf
sous_titre: "Les Adolescents. Lycée de Terraube" 
date_document: "1975" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_**À Terraube**_  
(Importance de “l’Europe plate et froide”.)

_**Journal de Lydou  
24 août**_   
La pluie était très forte, quand nous avons décidé (“Un… deux… trois !”) avec Aube de quitter notre abri pour rejoindre Papa sur le port. Un pauvre chien perdu courait en travers, et en nous voyant surgir, il aboya furieusement, appeuré ; son écho lamentable se répandait dans le lointain. Au loin, la tempête faisait rage, et un bâteau mal amarré alla donner du nez dans la jetée où il se brisa comme un œuf, avec un fracas terrible de craquements multipliés.  
Soudain, tout se calma ! Et dans ce cadre tourmenté ce fut comme si tout recommençait.  
_**Dimanche 30 décembre**_  
Claude et Loulou nous achètent des bonbons.  
Claude ne m’appelle que Mademoiselle.  
Le soir, j’écris à Monique R., Annette P., Marie-José M., Nadine C., Liliane C., Liliane D., Nady F., Marie-Thérèse G., Colette K., Jacqueline L., Lucile M., Monique N.  
Cet après-midi, avec Christiane D., nous avons recherché _les endroits I_ (c’était avant tout, avant même _les paroles I_ ). Nous rappellions les quoi ? Il faut dormir.  
**\***

_**L’année Suivante  
Mardi 1er janvier**_  
À trois heures, Christiane D. arrive et nous faisons un devoir d’anglais dans ma chambre par la fenêtre. On rit bien. Puis elle me fait un souvenir (celui qui est à la page précédente), quand Papa arrive et me demande “si il se fait, cet anglais ?” Alors vite Christiane D. tourne la page et fait semblant de faire de l’anglais. Là : “_walking back from the school_ ”. Ensuite on part.  
_**Jeudi 17 janvier**_  
Jean-Pierre Moustéou devait me porter les photos d’identité (il est collé), mais c’est Monique Dégans qui les a prises avant lui. L’après-midi, nous allons à Gauge et on discute avec Alain F. et Jean-Pierre Moustéou. Il y a aussi M. Olivier Larronde, qui est poète ; c’est un ami de Jean. Il est vieux ; il a 36 ans.  
Le matin, j’ai donné la lettre de C. et de Christiane D. à une externe.