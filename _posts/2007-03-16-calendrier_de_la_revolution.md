---
title: "Calendrier de la Révolution"
date: 2007-03-16 18:09
fichiers: 
  - 85.jpg
sous_titre: "Document des Gras. Stylo-bille sur papier teinté. 13cm x 18cm" 
date_document: "1962" 
tags:
  - document
  - OGR
  - dessin et gravure
---

Figure dans “Quartiers de ON!”