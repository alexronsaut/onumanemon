---
title: Ogr Maigre
date: 2024-08-18 14:24
sous_titre: ""
date_document: 
tags:
    - document
    - texte
    - OGR
fichiers:
    - "/ogr-maigre.pdf"
---

_Ce volume est paru aux éditions Tristram en 1999._

Il s’occupe de celles qu’il abandonne ; il leur trouve des maris, des situations ; il fait l’entremetteur. Il est généreux et tendre avec elles ; il est _de leur côté_ ; il est fémi- nin, c’est _un fennec_ ! Mais il est plus viril qu’aucun imbécile guerrier ; c’est un Héros et un Saint en même temps qu’un Poète.

Il rève de mariage blanc plus qu’elle, plus ; il voit des enfants dans le jardin ; il veut le bonheur parfait à chaque fois (mais il ne peut se marier !). Il fait écriture de sa Vie. Il a le noir du jais et la violence éjaculatoire.

Il racontera sa vie dans une œuvre considérable, écrivant sur les talus ; il est une partie d’un tas plus lointain, beaucoup plus obscur et nucléaire, dangereux. Il est dupé ; il parvient à la dépossession absolue, il s’éloigne à la fin sur la blancheur de la Neige, dans la Montagne, vers un Col (Tamié).

_États de la matière et de l’être, angoisse extatique et délicieuse en dehors de tout territoire, cette finalité d’un projet chargé d’explosifs !_

_(lire la suite…)_
