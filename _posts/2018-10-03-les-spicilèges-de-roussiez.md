---
title: Les Spicilèges de Roussiez
date: 2018-10-03 00:00:00 +0000
sous_titre: La lecture, une forme de conversation
date_document: Novembre 2017
fichiers:
- "/la lecture-conversation-novembre-2017.pdf"
tags:
- texte
- DAO
- document

---
**Les Spicilèges de Roussiez**

 

En complément du texte de Joël Roussiez paru dans le numéro de la revue _Mettray_ consacré à la lecture, nous vous offrons celui-ci à lire, d’une texture différente et totalement inédit.

Nous ne saurions trop vous inciter à lire ses ouvrages récents : _Au verger des Anciens_, récits parus à _la rumeur libre_, et le grand roman picaresque autour du pirate Farfali, aux _éditions de L’Arbre Vengeur_.

 

_NDLR_

 

 

**La lecture une forme de conversation,** 

Si l’on se propose de dire quelque chose sur la lecture, on ne peut faire l’économie de considérer qu’elle est devenue silencieuse ; et c’est à ce qui se passe dans ce silence qu’on va particulièrement s’intéresser. On interprète généralement cette situation comme celle d’un le lecteur qui reçoit passivement les signes émis; s’il est silencieux, c’est qu’il reçoit, tel est le raisonnement. 

_(lire la suite…)_