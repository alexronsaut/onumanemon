---
title: "Maurice à Buenos-Aires"
date: 2008-07-12 22:07
fichiers: 
  - Maurice_a_Buenos-Aires.pdf
sous_titre: "Les Grands Ancêtres. Ligne de Don Qui. Été" 
date_document: "1986" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

Avant de mourir, Maurice a eu envie de se rincer l’œil (plus d’un siècle qu’il n’a pas pris de bain !). Il est venu en tant que globe-trotter, trotter chez l’Oncle à Buenos Aires, histoire de ne pas se refroidir en mourant et de mourir au chaud (il refuse d’être cryogénisé : rien que l’idée le refroidit !).