---
title: "Les Thaïs Hollandais"
date: 2007-02-27 21:13
fichiers: 
  - 76.pdf
sous_titre: "Brilleman-Paturel" 
date_document: "2007" 
tags:
  - document
  - Sabaki
  - texte
---

Vous avez parlé l’autre jour sur votre site du gymnase de la Porte Pouchet à Paris, et bien j’y ai vu les premiers thaï-boxeurs qui curieusement étaient des Hollandais, champions dans le domaine sans se prétendre des kami. Ce n’étaient ni Akebono ni Takamiyama chez les Sumoka.  
Je connaissais Paschy que j’avais rencontré par l’intermédiaire de Didier entraîneur de l’équipe de France, lorsqu’il faisait ses premiers essais au cinéma.  
Il me semble que c’est en 1979 qu’il a organisé une première rencontre entre des hollandais formés à la boxe thaï et des français (qui n’y connaissaient pas grand’chose).  
Je me souviens surtout du combat de Brilleman (qui allait devenir, du reste, champion du monde de kick-boxing) avec cette capacité de ripostes foudroyantes.  
Les premiers français ont dégagé comme si de rien n’était.