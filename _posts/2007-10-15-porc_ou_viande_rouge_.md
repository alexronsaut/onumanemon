---
title: "Porc ou viande rouge ?"
date: 2007-10-15 13:22
fichiers: 
  - 273.mp3
sous_titre: "Les Grands Ancêtres. Ligne de Don Qui. Le Pollack Anar" 
date_document: "1989" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - son
---

Pages sonores inédites. Disque 1. Piste 16.  
Réalisation Philippe Prévot dans les studios de LIMCA à Auch.