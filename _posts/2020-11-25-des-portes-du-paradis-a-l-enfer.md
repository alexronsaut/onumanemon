---
date: 2020-11-25 18:39
title: "Des Portes du Paradis à l’Enfer"
sous_titre: ""
date_document: "1959-1970"
fichiers: 
  - "/paradis-1.jpg"
  - "/paradis-2.jpg"
tags:
  - texte
  - DAO

---

On est passé des _Portes du Paradis_, ce roman d’Andrzejewski écrit d’une seule phrase de plus de cent pages sans ponctuation, qui raconte la Croisade des Enfants, ouvrage paru en 1959 en Pologne, ce pays dont Guyotat est si proche par sa mère, à la longue phrase de _Eden, Eden , Eden_, paru en 1970.

Stanislaw Brzezinski. Cerisy.