---
title: "Celui qui part en Chine"
date: 2006-12-29 16:24
fichiers: 
  - 32.jpg
sous_titre: "“Je deviens fou” : aquarelle, dessin, collages, encre de chine." 
date_document: "Entre 1991 et 2000" 
tags:
  - document
  - OR
  - peinture
---

Travail en collaboration avec Alicia B. A fait partie de la présentation de la Cosmologie au "Quartier” de Quimper en 2006.