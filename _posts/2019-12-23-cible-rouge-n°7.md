---
date: 2019-12-23 9:38
title: Cible rouge n°7
sous_titre: Les deux marins à la foire
date_document: Après 1984
fichiers:
- "/Cible rouge.7.Les deux marins à la foire.jpg"
tags:
- dessin et gravure
- Cosmologie Onuma Nemon
- document

---
Pistolet 10m & dessin à la plume sur anciens cartons de cibles format 14 x 14cm en 5/10èmes