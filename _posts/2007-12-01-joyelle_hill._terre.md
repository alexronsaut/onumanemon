---
title: "Joyelle & Hill. Terre"
date: 2007-12-01 10:43
fichiers: 
  - 335.pdf
sous_titre: "Les Adolescents. Le Parc" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_**Hill et Joyelle  
(Hill)**_  
Patère, patère extrêmement creuse, patrimoine en soufflant ; la place du 14 juillet, pendant que les noirs mariés en blanc et frappés de stupeur se font filmer en couleurs devant les motifs du Parc ; le poudroiement du jet tournant, l’eau devenue vapeur, à la hauteur de ce phallus, de dos : un buste au-dessus du parterre massif de fleurs essentiellement rouges, roses et blanches. Tournoiement du jet d’eau, avec un frottement délicat continu, différant de ceux qui tournent par saccades ; frottement velouté flûté, près du flottement.  
_**(et)**_  
(_Paradis où l’orgasme dure plus que les mille siècles que met un cheval à traverser l’ombre des arbres gigantesques de ce parc, de ce jardin-là. Parcadis. Îles ou Montagnes des Bienheureux qui grognent, où ça sent la sueur et le roussi, où les Indiens veillent, où l’on craindrait en arrivant d’être pris pour l’un de ces producteurs de borborygmes ou métèques sans droits._)  
_**(Joyelle !)**_