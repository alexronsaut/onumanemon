---
title: "Les Postures."
date: 2007-04-10 19:48
fichiers: 
  - 118.pdf
sous_titre: "Par Maître Ho" 
date_document: "1980" 
tags:
  - document
  - Sabaki
  - texte
---

— J’ai très simplement tendance à considérer les postures comme une modulation plutôt que comme des moules fixes.  
Une série de variantes déployées dans un cercle qui va, disons, de zen-kutsu à kiba-dachi, chacune représentant un angle différent.  
S’il est facile de saisir pour un pratiquant le glissement dans un sens ou l’autre entre nekko-achi-dachi et ko-kutsu-dachi, cela peut être aisèment étendu (et pour ma part je l’ai fait) en gardant trois constantes : position verticale du tibia, position à demi avancée, le genou surplombant la moitié du pied ou position avancée genou au-dessus du gros orteil.