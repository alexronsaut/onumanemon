---
title: "Tête de Mort Gravée n°1"
date: 2008-05-10 21:19
fichiers: 
  - Tete_de_Mort_Gravee_1.jpg
sous_titre: "Ligne du Chaos" 
date_document: "1982" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - dessin et gravure
  - edition
---

Cette gravure sur bois qui figure dans la version définitive des “États du Monde” des éditions publie.net est éditée et vendue par l’URDLA

Publication : [Tête de Mort gravée n°2. Éditions de l'URDLA. 2007](http://www.urdla.com/estampes/estampe.htm)