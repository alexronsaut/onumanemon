---
date: 2019-12-30 12:58
title: Suite des neuf cibles rouges
sous_titre: Cible rouge n°1. Tête bandée
date_document: Après 1984
fichiers:
- "/Cible rouge.1.Tête bandée-1.jpg"
tags:
- dessin et gravure
- Cosmologie Onuma Nemon
- document

---
Pistolet 10m & dessin à la plume sur anciens cartons de cibles format 14 x 14cm en 5/10èmes