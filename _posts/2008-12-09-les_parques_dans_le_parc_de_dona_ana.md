---
title: "Les Parques dans le Parc de Doña Ana"
date: 2008-12-09 21:25
fichiers: 
  - Parc_de_Dona_Ana.pdf
sous_titre: "Les Adolescents. Été. Huelva" 
date_document: "1969" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

Les Parques filent dans le Parc  
L’air fluide de la lumière semblable  
(On l’a dit)  
Pour le ski nautique à  
Cette douceur violine :  
Antiennes anciennes puis