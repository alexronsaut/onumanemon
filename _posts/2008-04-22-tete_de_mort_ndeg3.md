---
title: "Tête de Mort n°3"
date: 2008-04-22 16:10
fichiers: 
  - MortChine3.jpg
sous_titre: "Ligne du Chaos. 3." 
date_document: "1982" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - dessin et gravure
  - edition
---

Encre de Chine sur Arches 20cm x 25cm. Figure dans “États du Monde”. Version définitive.

Publication : [publie.net](http://www.publie.net/tnc/spip.php?article85)