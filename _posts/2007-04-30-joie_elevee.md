---
title: "Joie Élevée"
date: 2007-04-30 21:53
fichiers: 
  - 129.pdf
sous_titre: "Livre Poétique de Nicolaï 1968-1984. Pressent. Poème n°3" 
date_document: "1968" 
tags:
  - document
  - OGR
  - texte
---

_A. Charente_  
  
Monsieur Voiture  
Fait son aventure  
Que Nicolas  
Dégueulass.