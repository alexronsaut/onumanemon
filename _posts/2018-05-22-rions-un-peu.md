---
layout: post
title: Rions un peu !
date: 2018-05-22 00:00:00 +0000
tags:
- billet
sous_titre: ''
date_document: ''
fichiers: []

---
L’homme d’aujourd’hui, lorsqu’il cherche à se représenter le Moyen Âge, croit généralement avoir à accomplir un énorme effort d’imagination. Le Moyen Âge lui paraît une époque sombre, reculée dans les ténèbres du temps, un moment du monde où il ne faisait jamais de soleil et où vivaient une humanité, de sociétés radicalement différentes de celles que nous connaissons. Or il nous suffit d’ouvrir les yeux sur notre univers, il nous suffit de lire chaque matin nos journaux : le Moyen Age est à notre porte ; il persiste à côté de nous et point seulement par quelques vestiges monumentaux ; il est de l’autre côté de la mer qui borde nos rivages, à quelques heures de vol ; il fait partie de ce qu’on appelle encore l’Empire français, et pose à nos hommes d’Etat du xxe siècle des questions qu’ils n’arrivent pas à résoudre.

Plusieurs pays musulmans d’Afrique du Nord et du Moyen-Orient, qui en sont très exactement au au xive siècle de leur ère, peuvent nous fournir, sous bien des aspects, l’image de ce que fut le monde médiéval européen. Mêmes villes aux masures tassées, aux rues étroites et grouillantes, enfermant quelques palais somptueux ; même opposition entre l’effroyable misère des classes pauvres et l’opulence des grands seigneurs ; mêmes conteurs aux coins des rues, propageant à la fois le rêve et les nouvelles ; même plèbe aux neuf dixièmes illettrée, subissant pendant de longues années l’oppression et puis soudain traversée de révoltes violentes, de paniques meurtrières ; même intrusion de la conscience religieuse dans les affaires publiques ; mêmes fanatismes, mêmes intrigues de la puissance, mêmes haines entre les factions, mêmes complots si étrangement ourdis qu’ils n’arrivent à se dénouer que dans le sang !… Les conclaves du Moyen Age devaient ressembler assez bien aux actuels collèges d’ulémas. Les drames dynastiques qui marquèrent la fin des Capétiens directs ont leur correspondance dans les drames dynastiques qui agitent de nos jours les pays arabes ; et l’on comprendra sans doute mieux la trame même de ce récit quand nous aurons dit qu’on pourrait la définir comme une lutte sans merci entre le pacha de Valois et le grand vizir Marigny. La seule différence, c’est que les pays européens du Moyen Age ne servaient pas de champ d’expansion aux intérêts de nations mieux équipées en moyens techniques et en armement. Depuis la chute de l’Empire romain, le colonialisme était mort, au moins en nos régions.

_Maurice Druon._ La Reine Étranglée. _1955._ (_de l’ensemble_ Les Rois Maudits). _Ancien résistant et combattant sur la Loire en 39-45. Auteur entre autres du_ Chant des Partisans _avec son oncle Joseph Kessel. Ministre de la Culture de 1972 à 1974._

_Abdelaziz Benihyia_