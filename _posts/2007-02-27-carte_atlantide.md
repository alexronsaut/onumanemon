---
title: "Carte Atlantide"
date: 2007-02-27 21:22
fichiers: 
  - 77.jpg
sous_titre: "Encre de Chine. 25 cms x 40cms" 
date_document: "Entre 1984 et 2000" 
tags:
  - document
  - OR
  - peinture
---

Cette Encre a été reproduite dans le Chant IX de “Quartiers de ON !”