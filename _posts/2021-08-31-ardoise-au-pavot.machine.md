---
date: 2021-08-31 2:09
title: Ardoise au pavot. (Machine)
sous_titre: ''
date_document: "1984"
fichiers:
- "/ardoise-gravee-au-pavot-2eme-etat.jpg"
tags:
- document
- sculpture
- OR

---
Ardoise gravée. Deuxième état. Fleur de pavot, cire. 25 x 35cm.