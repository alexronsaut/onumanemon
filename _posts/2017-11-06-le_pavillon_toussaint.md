---
title: "Le Pavillon Toussaint"
date: 2017-11-06 11:07
fichiers: 
  - nycephore.le_pavillon_toussaint.pdf
sous_titre: "Livre Poétique de Nycéphore" 
date_document: "1968" 
tags:
  - document
  - OGR
  - texte
---

 **18. Le Pavillon Toussaint**  
  
« Ces raseurs aux fifres, aux cuivres, ce dimanche  
Quels genoux d’acajou, musique pour chevaux  
De bois ! Alors que je trimais dans la source  
Au lavoir (simplement _ne plus rien voir_, par la fenêtre),  
Hébétude au-delà des pluies, des gouttes, des pendeloques ;  
Derrière un masque d’ours s’en viennent les chasseurs.  
  
La splendeur d’or que les sous-bois !  
Ces cavernes dans le feuillage  
Par endroits vineuses, un peu rousses à d’autrefois ;  
Rien du déchet dans une litanie.  
« Es-tu là, Fernande la Grosse ?  
— Je suis au fond de mon lit, Nany ! »  
  
_(lire la suite…)_