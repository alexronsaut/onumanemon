---
title: "Portrait du Marin Y"
date: 2011-07-02 11:44
fichiers: 
  - Portrait_du_Marin_Y.jpg
sous_titre: "Encre de Chine sur verso Papier de Riz pour tracé des idéogrammes 51,5 x 25cm" 
date_document: "1984" 
tags:
  - document
  - HSOR
  - peinture
---

Un des marins des aventures de Don Qui Domingo, au début de la Cosmologie, proche du Carpintero de la Niña.