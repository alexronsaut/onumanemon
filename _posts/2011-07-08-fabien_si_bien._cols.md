---
title: "Fabien Si Bien. Cols"
date: 2011-07-08 16:34
fichiers: 
  - Fabien_Si_Bien.Cols_.pdf
sous_titre: "Ligne des Enfants. Orphelins Colporteurs. Saison de la Terre" 
date_document: "1976" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

Fabien qui fait tout si bien s’était arrêté le matin au tout début de l’entraînement à cause d’une légère tendinite à la cheville droite, à peine une demie-heure passée. Il devait faire ce test sur les poumons et la gelée, pour vérifier au sommet du col si London avait vraiment raison, et si le tissu mort se détache ; ils étaient partis pour ça ; Élizabeth venait aussi d’une lignée de tuberculeux. Il ne voyait pas pourquoi il lui avait dit “Je t’aime” au col de V. : il faisait frisquet, surtout au cou, dans l’ombre du col ! cela était sorti de sa bouche comme un éjaculat, malgré le peu d’exaltation cardiaque de leur entraînement suspendu ; sa maigreur, le froid lui semblaient avoir augmenté à cette déclaration, alors qu’il avait l’impression d’être un charbonnier qui se jette un sac noirâtre sur les épaules comme une écharpe, en lâchant ça, à présent avec une charge inutile sur les pentes, en descendant. C’était aussi désagréable que quelque chose qu’on ne parvient pas à réparer ; il y avait dans les monts d’alentour une odeur de pommes pourries.  
 _lire la suite…_