---
title: "Enfance de Lugaid"
date: 2008-02-22 21:13
fichiers: 
  - Orphelins.ExtensionsLigneJean.Lugaid.pdf
sous_titre: "Les Orphelins Colporteurs. Extensions de la Ligne Jean" 
date_document: "1989" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_**Enfance de Lugaid. Bretagne**_  
(Ronces, fougères, immenses retombées de chèvrefeuilles :  
Élément torsadé par rapport aux autres parfums,  
Noisetiers)  
Très verts !  
  
On va là entre deux demeures,  
Mais maintenues à bonne distance  
Par ces murets comblés de terre où des arbres poussent  
Et d’où le propriétaire surplombe le passant.  
Rivières d’orties entre les maisons, de lierre, de houx extrêmes vernissés,  
De petits chênes, de marronniers ;  
Partout autour des maisons  
L’herbe toujours entretenue avec soin, tondue,  
Sans arrêt  
(Creuser la mer, tondre la terre…),  
Tandis que le ciel ne cesse d’arroser gentiment.