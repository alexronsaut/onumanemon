---
title: "Enfants Croisés"
date: 2008-03-25 22:26
fichiers: 
  - Histoire_Deux._Enfants_Croises.pdf
sous_titre: "Histoire Deux" 
date_document: "1975" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

**II. Enfants Croisés**  
  
_**1. Nicolas**_  
C’est à Cologne en Hiver, que Nicolas  
Au Printemps, qu’Étienne,  
Lançant les leurs, soulèvent, réunissent,  
Baptisent et passent à pied sec.  
  
Tout ce qui est émotion en Europe a fui !  
Cologne-Mayence-Spire-Colmar-Rhin gauche,  
Alpes, Italie du Nord :  
Pillage, attaques, défections…  
Gênes, Pise, Brindisi :  
Déflagration psychique !  
  
Anarchie des jeunes filles et des hommes mêlés  
Au Panier :  
On joue de la flûte ; on jette la flûte !  
Les drapeaux claquent au vent de la Mer !  
  
Les armateurs marchands ont baisé le cul du Diable ;  
Frédéric en rachète sept cents :  
Jouissance dans le Seigneur !