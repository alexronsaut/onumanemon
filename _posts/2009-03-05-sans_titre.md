---
title: "Sans Titre"
date: 2009-03-05 16:05
fichiers: 
  - Photo_22_Denis_Laget_Sans_titre_2006._Huile_sur_toile._27_X_35__cm.jpg
sous_titre: "Denis Laget. Huile sur toile. 27 x 35 cm" 
date_document: "2006" 
tags:
  - document
  - DAO
  - peinture
---

[Galerie Claude Bernard](http://www.claude-bernard.com/artiste.php?artiste_id=106)