---
title: "Une journée maussade"
date: 2011-04-06 19:48
fichiers: 
  - journeemaussade.pdf
sous_titre: "Texte de Joël Roussiez" 
date_document: "2011" 
tags:
  - document
  - DAO
  - texte
---

Une journée maussade où nous étions en rade dans le doux Ar Ménez, toute voile pendue ainsi que du linge mouillé, en attente du vent au milieu des coteaux, un jour de printemps où le clapot même était sans force; un jour donc où nous étions venus là pour aborder la côte en baie de Trez où se trouve une passe pour gagner la mer des Gascons en évitant le tour du Nez, gagnant ainsi du temps sur l'Amiral pour le rejoindre après un repos qu'il ne voulait accorder. Les hommes étaient dans un état lamentable, leurs corps se traînaient sur le pont; dix jours de navigation dans la tempête les avaient mollis...  


  
_lire la suite…_