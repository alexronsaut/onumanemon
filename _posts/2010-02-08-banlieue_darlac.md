---
title: "Banlieue d’Arlac"
date: 2010-02-08 18:51
fichiers: 
  - BanlieueDArlac.CDDidier.jpg
sous_titre: "Encre de Chine, lavis, aquarelle et divers sur Arches. Format raisin" 
date_document: "Avant 1984" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - dessin et gravure
---

Photographie de Didier Morin