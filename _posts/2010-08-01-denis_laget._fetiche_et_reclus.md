---
title: "Denis Laget. Fétiche et Reclus"
date: 2010-08-01 11:01
fichiers: 
  - Denis_Laget.Forclos.pdf
sous_titre: "Fortiche ou Forclos ?" 
date_document: "2010" 
tags:
  - document
  - HSOR
  - texte
  - edition
---

_**Reclus**_  
Je pressens la jouissance du Mort, du disparu, comme un joyau absolu, de façon à la fois fulgurante et labile, instable et aussitôt évanouie qu’approchée. Ces papillons de certitude n’ont rien de morbide et volètent dans l’air joyeux des Adolescents\* et de toutes les fêtes baroques de la Cosmologie\*, car c’est le propre de _l’Inscription_, cette catégorie à définir et ce territoire à défendre tout à fait en dehors de la culture et de l’art.  
Je ne suis pas un artiste car je n’ai jamais eu d’atelier ; je ne suis pas non plus un écrivain : rien qui me débecte autant que ce milieu-là (particulièrement en France aujourd’hui), ses truies, ses danseurs mondains, ses plagiaires, ses sociologismes et ses livres qui ne sont jamais que les cendres de la vie, au mieux un reportage correct.  
_lire la suite…_

Publication : [D’après modèle. Denis Laget et pratiques contemporaines](http://editionslienart.blogspot.com/)