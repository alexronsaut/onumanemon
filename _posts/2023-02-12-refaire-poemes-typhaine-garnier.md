---
title: "Refaire. Poèmes de Typhaine Garnier"
date: "2023-02-12 12:03"
date_document: "Février 2023"
fichiers:
    - /REFAIRE_vierge-couchee.pdf
    - /REFAIRE_extraits.pdf
tags:
    - document
    - texte
    - DAO
---

Ce texte est extrait d’un travail en cours intitulé « Refaire ». Même s’il y est question de choses vues, de paysages et de personnes aimées, _refaire_ n’est pas *revoir* : foin des nostalgies (sauf surjouées pour rire : « Ô… ») ! Il s’agit de faire du neuf, et si possible en forme, avec de l’informulé : fatras de souvenirs, pelote de sensations, magma d’émois, etc.

Pour ça, on a fabriqué un moule, censé aider à faire consister. Description du moule :

1. Le texte est disposé sur deux colonnes et apparaît donc lézardé d’un vide central ;
2. Les colonnes doivent pouvoir être lues séparément, mais une lecture globale (ligne à ligne) doit également être possible ;
3. Le texte s’ouvre par un verbe à infinitif (par exemple _faire_) et se termine par les lettres « re » qui forment avec le premier un second verbe (_refaire_), invitant à une nouvelle lecture.
4. Les lignes sont des vers grosso modo réguliers (le plus souvent octo ou décasyllabes).

Le but : que ça ne colle jamais. Ne pas adhérer, mais aérer. Que la droite contredise la gauche, la mine d’ironie, la glose de commentaires oiseux ou obscènes, en déforme symétriquement les phonèmes, bref, la malmène d’une manière ou d’une autre. La faille béante au milieu du texte n’étant pas frontière étanche, les deux côtés se contaminent aussi l’un l’autre. Et bien sûr, aucun n’est le « bon ». La vérité est au milieu : dans l’interstice.

_Typhaine Garnier_

Il faut lire également [Les Pages Perdues de OR](/posts/pages-perdues-de-or-1972-et-1989.html)
