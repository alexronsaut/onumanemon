---
title: La Pièce est enfumée, il est temps de sortir
date: 2023-12-03 15:44
sous_titre:
date_document: 2014
tags:
    - document
    - HSOR
    - texte
fichiers:
    - "/la-piece-est-enfumee.pdf"
---

Entretien de 2014, deux ans avant la parution des États du Monde, avec un ami Franc-Maçon.
