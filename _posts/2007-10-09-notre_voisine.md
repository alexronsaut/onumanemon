---
title: "Notre voisine"
date: 2007-10-09 14:11
fichiers: 
  - 284.mp3
sous_titre: "Les Escholiers Primaires. Ligne Nycéphore" 
date_document: "1989" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - son
---

Pages sonores inédites. Disque 2. Piste 16.  
Réalisation de Philippe Prévot dans les studios de LIMCA à Auch.