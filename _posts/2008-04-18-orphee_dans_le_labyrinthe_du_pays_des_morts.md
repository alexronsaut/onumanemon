---
title: "Orphée dans le Labyrinthe du Pays des Morts"
date: 2008-04-18 21:13
fichiers: 
  - OrpheeLabyrinthe.pdf
sous_titre: "Les Gras. Automne. Extensions de la Ligne Prosper" 
date_document: "1984" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_**Orphée dans le Labyrinthe du Pays des Morts**_  
(_“Che sera sera.”Doris Day. Chanson de “L’homme qui en savait trop”avec James Stewart_.)  
Plan de la course essoufflé au sommet du labyrinthe.  
Vent.  
Temps froid.  
À quelque chose. À une autre (époque dans la disparition) ! Mais je ne sais pas laquelle.  
Lac de la pensée, que c’est beau ! Déproprié.  
(Aurait souhaité atteindre ces passants d’un autre univers en parallèle auquel on ne peut avoir accès (mais dont ils désignent en même temps le bonheur possible impensable))