---
title: "Retour à la Rábida"
date: 2008-01-17 13:53
fichiers: 
  - Retour_a_la_Rabida.pdf
sous_titre: "Ligne de Don Qui Domingo" 
date_document: "1991" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_**Retour à La Rábida**_  
Et voilà le moine O’Koffee, gros lard irlandais moins autruche que les autrichiens de la Rábida, en train de sauter dans sa robe obscène, sur les tréteaux du théâtre de l’embarcadère de Palos de Moguer, tête de faune dans sa mousse blanche de rasage jouant le rôle moqué de C.C. avant son départ, (à moins qu’il ne se moque de Y.K.) :