---
title: "Pierre l’Hermitte"
date: 2010-11-07 18:32
fichiers: 
  - PierreLHermite.pdf
sous_titre: "Ligne des Orphelins Colporteurs. Hiver. CM1" 
date_document: "1980" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

Le texte qui suit ainsi que le pdf sont des états préliminaires et non définitifs. 

_NDLR. **Imprégnation dans l’ombre des tableaux.**_  


Tandis que le vent rabat la fumée de nos cheminées sur le ciel gris de neige et le fond des sapins, j’absorbe cette vue avec la même nécessité d’imprégnation que celle qui existe à partir d’un texte historique ou dans toute autre forme de récit. Seul le poème offre une plus grande succession fragmentée de climats ; la force d’incantation d’un poème comme “Les Chasseurs”, par exemple, est _colossale_ et n’a rien à voir avec un jeu littéraire ou une astuce verbale.  
Ainsi vont les rêveries d’un enfant au fond d’un grenier à partir d’archives familiales, ou les constructions historiques qu’on se fait, les _histoires deux_, ces curieuses imprégnations à partir des récits entendus en classe et des livres d’Histoire lus.  
Grâce à ma maîtresse, Mlle Angélique j’ai ainsi pu assister à la Saint-Bartélémy en la replaçant autant à Saint-Michel qu’à Saint-Augustin, j’ai construit mes premiers récits de cape et d’épée et surtout j’ai participé à l’exaltation des Enfants Croisés qui sont venus me rendre visite la nuit.  
Ils se situaient essentiellement du côté du Maucaillou ; tout le Moyen-Âge était là et rayonnait tout autour de la maison du bourreau jusqu’aux Halles des Capucins  


**\***  


_**Pierre L’Ermite & Robert Darbrisseau : les Vues**_  
Pierre l’Ermite prépare _la boucle métaphysique_, ne cherche plus ce qu’il en serait du mal métaphysique comme avant lui les bandes de pélerins en marche contre les juifs d’Allemagne, anéanties par les Hongrois, avec leurs rares survivants sur la rive asiatique du Bosphore, les Croisades ne seront plus des guerres saintes pour des conversions forcées des infidèles ; à présent il fouille toujours l’énigme du monde mais du moins ne cherche plus à la résoudre ; il vise, comme les enfants qui sont venus habiter dans la petite maison avec lui (dans les marais cernant l’Abbaye Sainte-Croix d’où ils touchaient la dîme jusqu’à Soulac et la pointe de Grave avec les joncs, les dunes et les prés salés), au Nirvana, à l’inexistence, au vide, au monde blanc de l’absence d’objets ; il aspire à être un zéro, à une néantisation de la parole et surtout de son écriture de copiste et d’enlumineur dans La Petite Louverie, et dans ce repos des Dieux la petite fille se souvient de la cachette qu’elle avait sous l’escalier tournant de bois, du grand salon ouvrant à droite de la porte avec sa cheminée géante et ses carreaux de céramique simple à motifs blancs comme dans l’entrée ; en face : de la toute petite cuisine et du rebord de l’étroite fenêtre où ils avaient l’habitude de voler le gâteau en train de refroidir depuis le champ derrière, peu vaste, et pourtant déjà si fatiguant à faucher pour l’Ermite ; d’une débauche de la Pensée en mauvaise énergie autour de la myriade de poires mortes ; ils se souviennent de la salle de bains bleuâtre dans un renfoncement très humide (couche de plomb jamais mise contre tout ce mur du Nord, laissant les champignons proliférer ; dans la cuisine aussi), avec ses carrés de liège autour du miroir ; de la cruche penchante et du lavabo où elle n’avait pas le droit de sauter, petite, par risque d’arracher celui-ci, fragilement fixé au mur ; de sa porte donnant sur le garage où se trouvait la machine typographique à platine Effel, la Chrysler rouge, le sac de frappe, toutes les casses de caractères Garamond et Plantin, garage ouvrant largement sur le grand pré bienheureux d’aujourd’hui en fleurs avant Pâques où des bouquets d’oiseaux chantent et piailleront jusque tard dans la nuit en écoutant les voix d’Emily Dickinson au coffre secret, de George Eliot, la fille du charpentier, d’Elizabeth Browning… Au début, elle avait pensé que l’Ermite Loutier souhaitait imprimer un “journal d’enfermement”, le narrateur devenant de plus en plus fou - et radical dans son énoncé - jusqu’au _crime_ de tuer le Destin et l’Indifférence, couple maudit, car il veut une constance, un monoton Kleinien ; il a toujours refusé que les siens meurent, il se ferait maffioso auprès de Dieu pour cela, il veut maîtriser le monde jusqu’aux moindres climats,  
or voici le Loutier qui fait des philtres avec les foies :  
article offert à chaque tête ! toutes les têtes  
sont sur l’immense étal, les offres liées dessus ;  
prédominance des rouges ; le serreux les range et les fait dormir  
alignés dans la paille du grenier d’où _le condamné à brûler_  
doit à tout prix se relever,  
mais tout est clos ;  
grosses perles à son front ;  
abattre le loup-garou d’une balle bénite,  
il le faudra ! soi-même vêtu de leurs peaux ;  
un certain foulage de sauvagine à la nuit tombée ;  
de ces façons funéraires…  
mais qu’est-ce cette toison qui flambe  
et ne s’arrache plus ? ! je courrai, je courrai  
me jetant à la gueule des chevaux gabelous  
excité par les faux sauniers, peut-être femme !  
  
le loup apprivoisé dévore le manteau du Maître  
sur sa charrette  
(quantité d’argent de l’ouverture des chiens)  
et le Maître en travers,  
au soleil,  
au jardin des Moines,  
en ombres sur les trous des prés, tandis  
que Messieurs les loups précités font concert : Madame  
des Taillis du Taillan (ils en ont tué six,  
mangé quatre et massacré dix) ; Monsieur  
à la Croix de Majou, et les voix des louveteaux en chœur ;  
  
_lire la suite…_