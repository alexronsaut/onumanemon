---
title: "Merci Sophie Marceau !"
date: 2009-02-04 18:02
tags:
  - billet
---

On aurait pu croire, à suivre la crétinisation des médias et à entendre Sophie Marceau dire après le tournage de _Guerre et Paix_ qu’elle avait pour ainsi dire traîné toute sa jeunesse l’ouvrage impérissable de Tolstoï dans son sac de classe, qu’il y avait un voisinage entre elle et les grands écrivains russes (en somme une nouvelle version côté Est de _La Nuit du Chasseur_ : un sein pour la guerre, un sein pour la paix : cf. Souchon), que Maritie et Gilbert Carpentier, c’était, à quelque chose près la même chose que l’Ezruversité, et que Carla Bruni était dans la même catégorie que La Callas : tout glissait en people !  
Mais non ! Déclaration de sauvegarde inespérée : elle nous apprend récemment qu’alors qu’elle ne savait pas trop pour qui voter la dernière fois, elle a voté pour Sarkozy et qu’elle ne regrette pas, Dieu merci ! Cette déclaration rétablit les choses et remet Van Gogh à sa place et on retrouve pour Sophie Marceau les qualificatifs dont usait Pialat à son endroit après le tournage de _Police_. Maritie et Gilbert Carpentier, Patrick Poivre d’Arvor, Patrick Sébastien, les Nuls de Canal Plus, Michel Drucker, Beigbeder, tout cela reste de la variété ; ce n’était pas Pasolini c’est Besson sur le plateau des Nuls ; l’Inceste de Christine Angot n’équivaut pas à l’Amant de Duras ni à la Maison d’Anaïs Nin ; l’art sociologique n’est pas de l’art mais de la culture plus de l’université ; les angoisses pédagogiques des fils de pub de la MGEN ne suffisent pas à composer un roman, et Sophie Marceau continue à flotter dans sa baignoire comme si de rien n’était. Merci Sophie ! À toutes choses malheurs sont bons.  
  
France Martin