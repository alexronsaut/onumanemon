---
title: "Nycéphore. Cádiz"
date: 2008-12-09 19:30
fichiers: 
  - Les_Adolescents.Espagne_du_Nord_.pdf
sous_titre: "Les Adolescents. Été" 
date_document: "1988" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

“Vivrai-je jusqu’en 2028 ? La quarantaine est le moment de ce navire bleuâtre, bien différent dans ce cas de celui où je me trouvais à Cádiz, prêt alors à refaire la traversée de Colomb, mais avant cela préparant tous les éléments techniques pour la venue de la troupe sur ce site.  
Hier Héraklès est descendu de sa coupe sur le quai, après avoir tué Orthros, le berger Eurytion et Géryon, le fils de Chrysaor. Il a débarqué avec lui tout le troupeau de Géryon et il va remonter par la terre jusqu’à la Grèce.  
Galère, drakkar, nef de Byzance, caravelle, vaisseau de premier rang puis brick de guerre, lougre, tartane, frégate et enfin cinq mâts… Prêt à pouvoir construire une naumachie intensive au-delà de toutes mesures des bougies décimales, en coupant au milieu du flux qu’il absorbe comme un buvard.”