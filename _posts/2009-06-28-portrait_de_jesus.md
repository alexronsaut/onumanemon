---
title: "Portrait de Jésus"
date: 2009-06-28 18:53
fichiers: 
  - Etats_du_Monde.pdf
sous_titre: "Les Adolescents. La Bande à Jésus. Hiver" 
date_document: "1974" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

Jésus tient son surnom d’une part de spasmes qui l’ébranlent issus de “la porte du Ciel”, d’autre part de la jouissance qu’il retire des clous.  
Pour ce qui est des spasmes, il commence par ressentir une douleur vague au sommet de la tête, à ce point précis qu’en acupuncture on appelle “porte du Ciel” et que les pratiquants de iaïdo mettent à profit pour se grandir. Il lui semble que cette partie du crâne est moins épaisse que le reste, d’une consistance à peine faiblement cartilagineuse comme la fontanelle du bébé. Cependant cette portion de la paroi crânienne exerce une pression sur son cerveau telle qu’il est obligé de saisir ses cheveux à cet endroit et de les tirer de toutes ses forces pour soulever cette région et faire cesser la compression en faisant sauter les sutures comme s’il voulait se soulever lui-même par là. La douleur y est très violente et c’est là que commencent les spasmes : sa cervelle entre en ébullition. De là la sensation descend par la nuque, suit la colonne vertébrale, se répand dans les bras et les jambes, semblable à la secousse électrique que reçoit un foudroyé ; en même temps sa gorge se serre, sa poitrine se contracte et d’après ce qu’on lui a dit son visage s’anime, ses regards s’allument d’un feu étrange et sa physionomie prend une expression de stupidité sensuelle ; il éprouve un frémissement intérieur dans la verge et par une légère pression de sa main sur son bas-ventre il augmente l’intensité du spasme et en prolonge la durée.