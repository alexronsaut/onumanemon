---
title: "Toujours plus au Nord !"
date: 2015-04-08 21:05
tags:
  - billet
---

“Marie-Thérèse avait offert à Lili toute la bibliothèque du vieux, tout ce qu'elle voulait !… la porte à côté de son salon… je voyais ce que Lili avait pris… tout Paul de Koch… tout Murger… pour nous deux La Vigue du sérieux !… la _Revue des Deux Mondes_ des soixante-quinze dernières années… la _Vie des Astres_ par Flammarion… elles s'y étaient mises, toutes les femmes, gitane avec, pour nous les descendre, nous les arranger bien en ordre, par numéros, dates, que notre rond de tour prenne un petit air meublé, convenable… le drôle c'est que nous avons eu le temps de tout lire !… romans, essais, critiques, discours… il m'est donc permis d'affirmer, preuves à l'appui, que les rois, députés, ministres, profèrent toujours, décade en décade, les mêmes sottises… à peine ci, là, quelque imprévu… plus con, moins con…que les romanciers écrivent tourjours les mêmes romans, plus ou moins cocus, plus ou moins faisandés, pédés, mélimélo, poison, browning… à tout bien voir, garniture lianes de fortes pensées… Tallement suffit, compact, vous met tout, pognon, les crimes, l'amour… en pas trois pages… vous pouvez bien vous rendre compte que les critiques, une revue l'autre, ont toujours les mêmes doigts dans l'œil, manquent pas de se gourer absolu, du bout d'un siècle l'autre… raffolent que de la merde, tant plus que c'est nul plus ils se poignent… fols ! jaculent, fervents, ahanent ! à genoux !… les revues de fonds de bibliothèques sont toujours joliment actuelles… toujours le canal de Suez… toujours les vingt guerres imminentes !… toujours l'humanité qu'augmente… vous arrivez à plus rien lire, plus vouloir, plus savoir, tellement vous êtes très sûr du reste…”

Louis-Ferdinand Céline. Nord