---
layout: post
title: Géographie
date: 2018-05-22 00:00:00 +0000
sous_titre: Livre Poétique de Nicolaï
date_document: 1979
fichiers:
- "/Géographie.pdf"
tags:
- OGR
- document

---
**32. Géographie**

“Ainsi s’unissent astres et planètes, Fogazarro,  
Non par le corps, mais par la lumière ; ainsi  
Les palmiers non par la racine mais le feuillage.  
Voici nos noms sortis des forêts, sentant le fauve.”

« Au milieu des lions te voilà ! C’est toi qui descends,  
T’assieds tranquille ! » disait le maître de Géographie,  
Tendant sa cannevelle immense sertissant la craie  
Vers la carte, à propos de la Louve.

Lutte avec l’Archange, chastes récits vivants !  
Plus qu’aucun autre, “çui des images”  
Passant à travers philtres le moindre zéphyr lu

\(Un élève du Sodoma : Ricciarelli, dit _Le Braguetteur_.)

\(_lire la suite…_)