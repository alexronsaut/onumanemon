---
title: "L'Automne de Paris"
date: 2007-04-08 19:44
fichiers: 
  - 116_1.pdf
sous_titre: "Livre Poétique de Nycéphore 1964-1968. Futur Antérieur. Texte n°7" 
date_document: "Automne 1964" 
tags:
  - document
  - OGR
  - texte
---

_Les poèmes des Livres Poétiques sont indiqués ici par leur numéro, chaque poème correspondant au poème qui porte le même numéro chez le frère “en face”.  
Isabelle Revay._ **7. L’Automne de Paris**  
  
Mon Dieu, ayez pitié de ce gouvernement  
Très sain ; faites fleurir l’ombre sur les cafés.  
J’aime les amoureux et les aucunement,  
Les promesses qu’on dit et le caca qu’on fait !  
  
Longtemps c’était le soir pendant cette saison ;  
On vidait des cageots sur le bord des trottoirs,  
On trottait tout le temps jusqu’à sentir le soir  
Envelopper les reins des femmes sans un son.