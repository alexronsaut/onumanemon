---
title: "Vision de Jean"
date: 2008-02-22 21:33
fichiers: 
  - Orphelins.LigneJean.Vision_de_Jean.pdf
sous_titre: "Les Orphelins Colporteurs. Ligne de Jean" 
date_document: "1989" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

 _**Vision de Jean en Bretagne.**_  
Champ de blé frissonnant doucement. D’un côté chaleur implacable, muraille de fougères de l’autre, et, par les trouées entre les chênes, les magnolias, marécages de l’estuaire ; noirs, immobiles.  
Et dessus des cormorans qui se posent, décollent, avec lenteur, sinon ne bougent.  
Exaltation du marcheur celte de part et d’autre : entre le noir et le blond doré, à la démesure de la passion, de la luxure, du viol divin, du désordre hagard si cher aux amis de Cuchulainn.  
De ce côté-ci, entre les murailles de fougères et les ramures énormes retombantes des chênes ensorcelés de lierre par les Femmes-Sorcières des Hautes-Terres, l’estuaire est profondément noir avec quelques endroits de luisance gris boueux-marrons et des ombres rapides qui passent dessus en même temps que les lents cercles des ailes grises et ailes noires poussant des sortes de longs hooo ! de gémissements.  
Puis les revoilà, ces chers oiseaux ratés par Cuchulainn, attendant immobiles sur la boue à fond jaune fixée comme au bord de la Boyne au centre de leurs petites ombres rondes.  
Du côté du chemin du bois, bruyères et ajoncs, la clairière sent le carambar.