---
title: "Saint-Michel"
date: 2009-03-22 18:11
fichiers: 
  - Saint-Michel.pdf
sous_titre: "Livre Poétique de Nycéphore 1964-1968. Texte n°5." 
date_document: "22 Mars 1964" 
tags:
  - document
  - OGR
  - texte
  - edition
---

**5. Saint-Michel**  
  
Jean, Norbert, Manolo,  
Le gourdin à la main  
Sali dès le matin,  
S’en vont à leur boulot.  
Et la robe des ânes  
Par la rue Traversane,  
Au-dessus des barquettes  
Couvre mal leur quiquette.  
Il auront un peu d’or  
Qui descend sur le port,  
Le gras-double qu’on trouve  
Sur la marché des Douves,  
Un reste de guano  
Et du churrizano,  
Le cassoulet de Jules,  
La saucisse d’Hercule,  
Place du Maucaillou,  
Du graillon barbaillou  
De Sœur Marie-Thérèse  
Dont ils baffrent les fraises,  
Un tonneau de piquette,  
Un lapin, la sanquette,  
Et des frères Moga  
Le pâté noir bien gras,  
Une soupe à l’oignon  
De chez Napoléon…

Publication : [Mettray n°7. Automne 2004](https://mettray.com/revue/mettray-s01-n07)