---
title: "Colomb : Dortoir de Santoña"
date: 2008-02-19 22:21
fichiers: 
  - Don_Qui._Dortoir_de_Santona.pdf
sous_titre: "Les Grands Ancêtres. Ligne de Don Qui" 
date_document: "1992" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_**Dortoir de Santoña**_  
Cette brume dans la vallée, toute cette rumeur dans le Sud manque de terrain vague pour se retrouver étranger, comme dans le Nord.  
À côté de soi, sur le lit, le Corps Inconscient, barbouillé de lettres, sombrant dans le sommeil, redevenu exceptionnellement prédateur, _renard_ ; il faut un immense effort pour surnager… à l’aide du parfum soudain de petites roses et de belles de jour.  
_(“Moi, j’aime bien les lombrics. Mon frère pas trop ; mais il m’aide pour les poulardes, les grosses poulardes bien grasses !   
C’est là que je donne le meilleur de mes petits bonds brillants dans les prés : je la poursuis féroce, et le frérot la guette calmement et lui saute dessus au passage. Y’a bien les pêches et d’autres fruits, mais ils sont peints moins romantiques qu’avant ; les raisins, surtout. Mon plaisir, c’est pas tellement de jouer au lancer de taupe, c’est plutôt pisser tous les cent mètres et de chier sur des pierres bien installées comme des stelles. J’ai fait ça tout le long du camp, en recouvrant les merdes des autres ; c’était amusant ! Mais y veulent plus !”)_