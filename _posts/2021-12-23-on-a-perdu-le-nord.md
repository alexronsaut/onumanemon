---
title: "On a perdu le Nord !"
date: "2021-12-23 13:37"
sous_titre: "Hommage à Bernard Plossu"
date_document: "2012"
fichiers:
  - /on-a-perdu-le-nord-dapres-couv.jpg
  - /on-a-perdu-le-nord-dapres-p13.jpg
  - /on-a-perdu-le-nord-d-apres-et-mettray-fabien-ribery.pdf
tags:
  - document
  - texte
  - HSOR
  - edition
---

Chronique de Fabien Ribéry à propos de _On a perdu le nord !_ paru [dans la collection _d'Après_ chez METTRAY éditions](https://mettray.com/d-apres/index)