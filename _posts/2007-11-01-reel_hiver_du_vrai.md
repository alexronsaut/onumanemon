---
title: "Réel Hiver du Vrai"
date: 2007-11-01 20:25
fichiers: 
  - 264.mp3
sous_titre: "Les Adolescents. Ligne d’Aube au Mas" 
date_document: "1989" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - son
---

Pages sonores inédites. Disque 1. Piste 1.  
Réalisation Philippe Prévot dans les studios de LIMCA à Auch.