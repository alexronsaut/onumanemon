---
date: 2020-03-02 12:12
title: La Longue Lettre
sous_titre: José Izquierdo
date_document: "1972"
fichiers:
- "/jose-izquierdo.pdf"
tags:
- texte
- DAO
- document

---
LE GROUPE DE LA FOLIE-MÉRICOURT

Le _groupe de la Folie-Méricourt_ (nommé ainsi parce qu’il s’était constitué autour de Monique Charvet, qui habitait alors avec Ermanno Krumm rue de la Folie-Méricourt), était constitué uniquement de filles : Monique, Ariane, Mina, Laurence, Rio, Marie, Françoise, Ophélie, Anna, et de Frédéric/_que_, à la fois androgyne et albinos, élevé toute sa vie comme une fille par ses parents, et depuis fort indécis. Il faut préciser que tout ce groupe-là passait tout de même l’essentiel de son temps à Sainte-Anne, pavillon Magnan ou ailleurs.

Ermanno et les autres garçons, comme José Izquierdo (patient chronique lui aussi de Sainte-Anne), ne pouvaient suivre que des lignes tangentes par rapport au polygone du groupe, devenu parfois hexagonal et producteur d’_Hexagones Monions_, qui provoquaient des crises particulièrement violentes chez Monique en l’envahissant.

À son arrivée à Paris en 1970, Ermanno Krumm, bien que familier des Novissimi et passionné par Denis Roche avait composé de magnifiques longs poèmes lyriques à la fois épiques et élégiaques : _Endymion_, _Atala_, _Dédale_... sans la moindre ombre formaliste, dans une sorte d’heureuse joie antique préservée. Ce sont ces poèmes-là, découpés en strophes qu’il offrait aux terrasses de café avec Monique en faisant la manche.

Monique de son côté a écrit beaucoup de textes et composé plusieurs recueils, mais malgré plusieurs tentatives il n’y a jamais eu de possibilité de publications aux _Éditions des Femmes_ ni de véritable rapprochement avec le groupe_Psychépo_. Elle traduisit à un moment Verdiglione à la demande de François Wahl et de Tel Quel.

C’est peu après avoir composé _D’Hors_ en particulier, qu’elle s’est suicidée sur le bord de la mer en 1974.

Le groupe de la _Folie_ augmenté de quelques électrons libres a beaucoup créé également de _Romans à Usage Interne_, qui mettaient en scène une sorte de situation catastrophique entre des membres du groupe, où le dialogue avec les Morts avait une grande part.

Ces ouvrages, peu volumineux, étaient destinés en principe uniquement aux protagonistes, mais peu à peu le cercle de la diffusion s’était élargi à d’autres amis.

Quant à la _Grande Lettre_ de José Izquierdo (c’est sous ce nom qu’il se présentait à Sainte-Anne, mais on n’a jamais su si c’était véritablement le sien ou un surnom), elle représentait plusieurs milliers de pages et une “adresse” litanique ininterrompue tout le long de son séjour à l’hôpital.

Ici ce sont les tout premiers feuillets tapés par Mina.

M. Ey...