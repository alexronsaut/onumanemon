---
title: "Parcours du Parc n°8"
date: 2008-02-29 22:22
fichiers: 
  - Parcours_du_Parc_8.jpg
sous_titre: "Les Adolescents. Joyelle & Hill" 
date_document: "1978" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - photographie
---

Format 13 x 18 cms. Cette photographie faisait partie de l‘exposition au “Quartier” en 2005