---
title: "Pemmy Noël"
date: 2012-11-26 19:41
fichiers: 
  - Pemmy_Conte_Epinal.pdf
sous_titre: "Contes, Nouvelles et Récits de Nycéphore" 
date_document: "1976" 
tags:
  - document
  - OGR
  - texte
---

Il ne restait à la petite Pemmy, cachée au fond de la pièce des claies de jonc et de paille où s’égouttaient et sèchaient les énormes meules de fromages, qu’à énumérer tout le jour des listes face auxquelles elle se trouvait la nuit en rêvant, écrites sur un tableau noir comme la fortune de Chienfou :  


Arrosoir Chandeliers  
Limbes Flache  
Poire Débridement  
Horloge Escargots de Chine  
Orangée Confetti  
Asperges Confiture  
Rive Araignée  
Encolure Cachectique  
Larix Sorcière  
Bleue Ouais  
Verruqueux Morasse  
Boîte Péjoratif  
Dé Radis  
Turquoise Outrage  
Quatre Nus  
Prélart Pécari  
Unicorne Corps  
Entonnoir Encre  
Cobra Suint…  
_etc._

 _lire la suite…_