---
title: "Groupe de la Folie-Méricourt à Arras"
date: 2008-06-07 18:54
sous_titre: "Les Orphelins Colporteurs. Saison de la Terre" 
date_document: "1989" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_**Théâtre d’Arras**_  
_(Toute la Bande de la Folie-Méricourt, Monique en tête, avec Ariane, Frédéric/que, Rio, etc. fuit vers le nord, parallèlement à Orphée, qu’elles entr’aperçoivent de temps à autre, une ou deux fois, de loin, harassé de neige alors qu’elles-mêmes coupent régulièrement à travers champs, franchissant les clôtures et les barbelés. Elles se rendent à la Concentration du Nord répartie dans les grandes villes autour des Trous Noirs Miniers : Loos, Liévin, Herzelle, Douai, Arras… et jusqu’à la mer du Nord.  
Des discussions cruciales se tiennent, animées par “les plus rapides” à travers toutes ces villes : dans les cafés, les maisons, toutes halles et toutes salles disponibles. À chaque maison qu’on franchit, chaque porte qu’on ouvre, une bouffée de débats et de cons bus surgit ; cela porte sur les champs les plus divers, depuis les discussions sur le cinéma, animées par Dupin et Hœyliss, jusqu’aux plus grandes finesses prosodiques de la poésie. Les groupes ne sont pas préformés, chacun circule d’un lieu à l’autre, s’arrête comme cela lui convient et participe ou non aux débats.  
À Arras, une pièce se joue où des personnages qui vont du Moyen-Âge à la Révolution interviennent pêle-mêle.)_  
  
_**Arras**_  
_(Magloire vient d’être dénoncé par l’ouvrière en linge Rimbaud)_  
**Magloire :** « Par mon âme, en automne, on en tuera trois cents !  
**Morgue :** — Certes, Madame, ce sera un grand festin, que les Ours se gavant pour l’Hiver, si bons danseurs dans 95% des forêts.