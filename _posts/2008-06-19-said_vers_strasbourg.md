---
title: "Saïd vers Strasbourg"
date: 2008-06-19 21:46
fichiers: 
  - Said_vers_Strasbourg.pdf
sous_titre: "Les Escholiers Primaires. Extensions de la Ligne de Nicolaï. Automne" 
date_document: "Tours 1984" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_**Saïd. Vers Strasbourg !**_  
**Mañana :** “Alors que “j’en décousais” avec Macha, Saïd mettait la   
dernière main à son système concernant la classification des esquives, et voulait le confronter à l’expérience d’Habersetzer, qu’il devait rencontrer le week-end même à Strasbourg, avant d’en faire un matériau chorégraphique qui puisse servir soit d’intermède (comme au temps du   
Roi-Soleil), soit en l’intégrant au travail dramatique et en particulier à la   
chorégraphie des Andalous.”  
Pendant qu’il travaillait, Claudia l’observait de tout près (les dents, les cheveux), en réservant sur son visage des zones de prélèvement pour son univers, fait de petits morceaux, d’intenses instants, pris ici ou là, et jointoyés, comme d’autres se font une robe de patchwork. Sa Carte du Tendre allait de Pantin au Chemin Vert où elle était née.  
Voici la classification des esquives à laquelle était parvenu Saïd :  
– côté/déplacement latéral du buste  
– déplacement/retrait du buste/rotation  
– absorption par le hara  
– retrait arrière du buste  
– déplacement/rotation/retrait 1/4 de cercle du pied du côté du coup  
– rotations du buste  
– rotations de la tête :  
 . sur son axe  
 . en effectuant 1/2 cercle d’un côté ou de l’autre  
En ce qui concerne les contre-attaques, il me semblait que sa typologie était presque exhaustive, en tout cas bien plus complète que celle de R. Habersetzer.