---
title: "Campagne/Compagne"
date: 2008-05-08 23:05
fichiers: 
  - Campagne-Compagne.pdf
sous_titre: "Les Orphelins Colporteurs. Quintette de Campagne" 
date_document: "1976" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_**Luc. Campagne/Compagne**_  
C’est dans le Château de Terraube, chez Lydou, alors que je venais assister au mariage de Aube avant de me joindre à leur troupe pour Cádiz, que j’ai trouvé “son” journal à côté de “l’Avis aux Campagnes” lancé par Rbsprr.  
Ce “Girondin” Amateur des Jardins et Amoureux des Contrées avait recherché pendant des années son amie intime disparue. Un rêve lui fit voir que la géographie de l’aimée s’était épanouie dans l’Univers, et qu’il suffisait qu’il retrouve là tel bosquet, ici tel flanc de coteau, pour que de ces repérages et de cette reconstitution mentale, elle renaisse ! Ainsi il traversa le Monde.  
À un moment donné, il rencontra Orphée, dans le Gers, lui-même à la recherche d’Eurydice dans les rallyes que l’on sait, qui lui fit part de la mission errante des “Enguirlandés” ; il se sentit, à raison ou non, relever de cette immense migration infinie, et il continua ainsi, selon on ne sait quelles pérégrinations exactes, cette recherche à travers plusieurs pays, rencontrant parfois les membres de ceux qui portaient des tournoiements de lumières.  
  
Il serait plus juste de dire qu’au lieu de lire j’assistai à son journal, car dans cette pièce à tapisserie de ton fruitif, en le lisant j’eus la sensation immédiate de rejoindre l’un de ceux que je suis. J’eus l’impression de découvrir un journal que j’aurais écrit. Que son auteur qui était resté là, celui que je fus et qu’il incarna, hanta ce château sans me nier pour autant. Je le retrouve aujourd’hui dans cette pièce curieusement fermée depuis le jour de ma naissance, par le hasard d’un enfant, ici mort dans un accident, un oncle de Lydou, tombé dans la mare aux pieds des remparts, l’hiver, et noyé sans qu’on l’entende.