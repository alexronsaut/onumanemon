---
title: "Now Snow !"
date: 2007-05-06 10:04
fichiers: 
  - 131.pdf
sous_titre: "Livre Poétique de Nycéphore. 1968-1984. Futur Antérieur. Poème n°7" 
date_document: "31 Décembre 1968." 
tags:
  - document
  - OGR
  - texte
---

_Ce poème figure dans l'ouvrage “Quartiers de ON !”_ 

_Isabelle Revay_ **4. Now Snow**  
  
Adieu !  
Façon dont l’omoplate bouge ;  
Au-dessous ce désordre invraisemblable de sens :  
Rouilles, buissons, vignes rouges  
Sur le bassin du paysage cliché.  
  
Adieu !  
En retournant la tête vers  
La nostalgie hivernale des cinco  
En fixant la nécessité de refaire  
Toute la langue et tout l’Univers en même temps (Chinois)  
  
Adieu !  
Anna Livia  
«Vers la terre où coulent à flots le lait et le miel»,  
Vers le mythe avant sa déception,  
Vers les jambes avant leur faille,  
Dans leur mouvement !  
  
Adieu !  
De la terrasse du château Neuschwanstein  
Vers le paradis de la terre noire en bande  
Et les landes plus ou moins grises  
Et l’incertitude des rochers  
(Abrupts ou autres) ;  
  
Vers la vue non déçue, sans lange,  
Vers le paysage sans mot :  
Génial, donc _innommé_.