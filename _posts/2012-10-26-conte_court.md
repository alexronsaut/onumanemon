---
title: "Conte Court"
date: 2012-10-26 20:04
fichiers: 
  - Conte_Court.pdf
sous_titre: "Contes, Nouvelles et Récits de Nycéphore" 
date_document: "1976" 
tags:
  - document
  - OGR
  - texte
---

Le Paralytique était stuporeux sur le bord : on boira toute l’eau de la piscine à son horrible avènement ; on va partir en Allemagne voir l’homme à l’oreille coupée ; la tache de lumière choit bêtement dans un bosquet dont les arbustes ont des reflets roses.  
Une chose représente autre chose : soleil ou lampe ou bien la figure d’une femme de serf, sucrée et blanche, à peine molle, poupine, terriblement sensuelle ! Puis c’est le méandre des causes. Cantate, prairie, anémones sauvages en tapis, crescendos tragiques de cordes, puis vents.  
Ma mère me dit toujours de faire attention aux fleurs de l’esprit. Sur le plâtre, sur les parquets creux et verts de moisissures : des animaux, des peintures… fond moral, peinture allégorique jolie et reposante abîmée nuit et jour. Trait formant un triangle gagné d’un cancer fou… estimation de perte progressive éclairée plus ou moins faiblement.  


  
_lire la suite…_