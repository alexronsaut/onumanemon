---
title: "Je viens vous dire bonjour"
date: 2010-06-02 17:53
fichiers: 
  - SIgi.pdf
sous_titre: "Amères Loques, Livre de nouvelles de Nicolaï" 
date_document: "1976" 
tags:
  - document
  - OGR
  - texte
  - edition
---

Texte publié dans le recueil _Ogr_ par Tristram, en 1999.