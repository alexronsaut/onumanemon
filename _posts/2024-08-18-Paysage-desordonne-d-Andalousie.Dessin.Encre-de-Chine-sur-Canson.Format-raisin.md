---
title: Paysage désordonné d'Andalousie
sous_titre: ""
date: 2024-08-18 14:46
date_document: "Après 1984"
tags:
    - document
    - dessin et gravure
    - Cosmologie Onuma Nemon
fichiers:
    - "/Paysage-desordonne-d-Andalousie.Dessin.Encre-de-Chine-sur-Canson.Format-raisin.jpg"
---

Dessin. Encre de Chine sur Canson. Format raisin

Don à la Bibliothèque Kandinsky de Beaubourg.
