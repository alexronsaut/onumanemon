---
date: 2019-12-30 1:14
title: La fille de Murasaki
sous_titre: Joël Roussiez
date_document: "2011"
fichiers:
- "/La fille de Murasaki.pdf"
tags:
- texte
- DAO
- document

---
**La fille de Murasaki** (Dai Ni No Sami)  
La fille de Murasaki se souciait des personnes souffrantes et, de son écritoire, songeait à leur douleur. Un homme de deuxième rang vint lui rendre visite : qui tisse dans vos yeux ce brouillard de tristesse ?

_(lire la suite…)_