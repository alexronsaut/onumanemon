---
title: "Mort d’Eurydice"
date: 2008-03-27 21:53
fichiers: 
  - Ligne_du_Chaos_Layout_1.pdf
sous_titre: "Les Grands Ancêtres. Ligne de Mac Carthy. Terre" 
date_document: "1992" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_**Gers. Mort d’Eurydice**_  
Eurydice est disparue dans le Gers en même temps que sa voiture, sans qu’il y ait eu d’accident. C’était la compagne d’école de Lydou et de Aube. Elles ont connu et fréquenté les mêmes endroits ; toutes deux essaient à présent dans une concentration féroce, grâce à la machine de Georges le Fou, de retrouver sa mémoire grâce à leur mémoire commune possible, communale et primaire, transversale parfois.  
Si elles y parviennent, elles atteindront également au lieu où elle se trouve actuellement

ﾠ

ﾠ