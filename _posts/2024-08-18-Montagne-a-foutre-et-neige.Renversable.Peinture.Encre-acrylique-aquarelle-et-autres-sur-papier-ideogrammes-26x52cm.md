---
title: Montagne à foutre et neige. Renversable
sous_titre: ""
date: 2024-08-18 14:46
date_document: "Après 1984"
tags:
    - document
    - peinture
    - Cosmologie Onuma Nemon
fichiers:
    - "/Montagne-a-foutre-et-neige.Renversable.Peinture.Encre-acrylique-aquarelle-et-autres-sur-papier-ideogrammes-26x52cm.jpg"
---

Peinture. Encre, acrylique, aquarelle et autres sur papier idéogrammes 26x52cm

Don à la Bibliothèque Kandinsky de Beaubourg.
