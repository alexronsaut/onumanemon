---
date: 2020-03-02 12:21
title: Du Poisson-Lune à l'Onyx
sous_titre: Mathias Perez
date_document: Après 2000
fichiers:
  - du-poisson-lune-a-l-autre-onyx
tags:
- texte
- billet

---

DU POISSON-LUNE A L’ONYX

Vous avez attiré mon intention sur de grosses conneries racontées par des universitaires nées au moins vingt ans après les évènements sur la vie culturelle à Bordeaux dans les années soixante. Or comme le disait Saint-Simon, les Mémoires permettent du moins qu’on ne perpétue pas des versions officielles mais néanmoins fausses de l’histoire.

La chance que nous avons tout de même c’est qu’il y a parité en matière d’ânesses et de bourrins, tant à l’université qu’en radiophonie et dans le journalisme des rats crevés.

Je voudrais préciser ceci à propos du café-théâtre _Poisson-Lune_ devenu _Onyx_, qui était situé au 94 rue Camille Sauvageau à Bordeaux, quartier Sainte-Croix.

Je connais très bien son histoire pour la bonne raison que je suis le cousin du propriétaire du bar _l’Ibéria_, Robert Triguero qui a ouvert cette salle dans un entrepôt attenant à son café dans son prolongement rue Saint-Benoit.

Je suis charpentier de marine et par extension menuisier. À l’époque je travaillais sur les quais et j’habitais au 9 rue du Port   (à deux pas de là), en même temps que toute la famille Perez répartie dans tout l’immeuble vétuste, humide, et parfaitement malsain : un vrai taudis enchanté sur cinq étages, totalement détruit par la suite pour devenir cette parodie bourgeoise du _Théâtre du Port de la Lune_.

Robert Triguero, mon cousin, tenait le café de _l’Ibéria_ où venaient régulièrement les étudiants des beaux-Arts. Ces derniers avaient trois cafés de prédilection : d’abord _Chez Janine_, en face de l’Abbatiale Sainte-Croix et à l’angle de la rue du Portail, ensuite _l’Ibéria_, dix mètres plus loin, et enfin au _Longchamps_, cours de la Marne, qui accueillait également les Navalais.

C’est donc tout, logiquement aux étudiants des Beaux-Arts que Robert Triguero a parlé de son projet de cabaret et lieu d’exposition, et c’est Daniel Busto qui a mis en forme avec Jean-Louis Froment le premier projet de soirées de lectures dans le cadre d’un _cabaret de poésie_.

Jean-Louis Froment à l’époque arrivait de Vevey en même temps que Delay et qu’un dénommé Pastor, si je me souviens bien. Il n’avait pas encore épousé Josy, la propriétaire de la Galerie Du Fleuve (que tenait à l’époque la vénérable Madame Henriette Bounin) ; il était loin de son traitement mensuel de 100 000 francs au _CAPC_ et vivait plutôt modestement de son métier d’étalagiste. J’ai encore des photos de ses vitrines, notamment une très belle réalisée aux Nouvelles Galeries, rue Sainte-Catherine, avec de très grands pains ronds. Il était alors assez proche d’une autre décoratrice en volume haute en couleurs qui s’appelait Suzanne Palassy, et que certains étudiants surnommaient gentiment “Suzanne Perroquet” en raison de ses vêtements vivement bariolés.

Ce n’est que bien plus tard, en 1969, grâce à Lardin et Chaban qu’il a obtenu la gestion d’un atelier aux Beaux-Arts de Bordeaux, malgré son diplôme suisse, comme Delay.

\*

L’aménagement du lieu comme _Poisson-Lune_ a commencé en janvier 1967, et c’est moi qui m’y suis attaqué. Il ne peut y avoir d’erreur de date, car j’ai conservé tous les cahiers de chantier ainsi que les photographies des différents stades de travaux.

Les étudiants des Beaux-Arts ont énormément aidé aux travaux, notamment Pierre Barès pour le gros œuvre, Jean-Luc Selleret (cousin de Jean-Louis Froment), Daniel Busto et Françoise Labat (qui ont réalisé le plafond dont j’ai construit l’armature, en peignant des mains négatives au pistolet et des empreintes de mains et de pieds).

La première soirée au _Poisson-Lune_a eu lieu le 20 avril 1967. La carte d’invitation que Robert avait fait imprimer était ridicule (en lettres d’or torsadées comme pour une boîte de nuit), et Froment décida d’en refaire une autre aussitôt.

\*

Guy Suire n’est intervenu que dans un deuxième temps, en même temps que Guy Baloup et Thérèse Liotard et toute une équipe de comédiens ; Suire s’occupait de théâtre et travaillait surtout à la radio, comme Busto, et c’est alors que le lieu a pris l’appellation de café-théâtre. Le premier à être créé en province.

À ce moment la plupart des participants du premier noyau (Nicolas Remcsack, Pierre Barès, Jean-Luc Selleret, Nadine Meyran, et au premier chef Jean-Louis Froment), sont partis, car ils ne supportaient pas Suire.

Et c’est le jeudi 19 octobre 1967 que le passage s’est fait du _Poisson-Lune_ à _l’Onyx_. Le titre avait été choisi en commun par Busto et Baloup en référence à des bouts-rimés de Queneau parodiant Mallarmé.

La première exposition a eu lieu en février 1968. Une émission radio de Bordeaux-Aquitaine a été consacrée à _l’Onyx_ le 23 février 1968 et une télé le 10 mars 1968 en même temps qu’à _Sigma_. On retrouvera tout cela dans les _Sud-Ouest_ de l’époque avec plusieurs articles de Jean-Gérard Maingot (le Dandy de Cheverus !) et Pierre Paret (l’Ancêtre).

Parmi les poètes il y avait Nadine Meyran (qui a ensuite travaillé au _CAPC_), Anne-Marie Perier, Nicolas Remcsack, Paul Chose… J’en oublie. Et pour les chanteurs Jean-Claude Coquempot, Bernard Balavoine (frère de Daniel), surnommé “la chèvre” par Remcsak, François Huchet, Franck Ferrand et bien d’autres.

Les pièces représentées choisies par Suire allaient de Guy Foissy à Tardieu, Obaldia ou Arrabal et à des pièces de membres du groupe. Les collaborations avec la radio permettaient de faire venir des personnes plus connues comme Colette Magny. Gripari, par exemple s’est déplacé en personne pour assister à la création de sa pièce _La Divine Farce_.

Dans les expositions organisées, on a pu voir des travaux de Francis Limérat, Pierre Barès, Alain Vallet, Jean-Luc Selleret, Christian Delafond, Michel Jouhanneau, Jean-Marie Poumeyrol, Chantal Delafond, Alain Lestié…

On rencontrait là en 1967 et 68 en majorité les étudiants du CREPS (parmi lesquels des théoriciens politiques tels que André Decroix, le grand ami de Lapassade et de Lourau, Huberdeau Dumas, clown et théoricien de la boxe), ou Patrick Lacoste, le rugbyman psychiatre, à l’époque Grand Prix de poésie-jeunesse décerné par Jean-Pierre Rosnay. J’y ai vu Jean-Noël Cuin jouer de la scie musicale avec Emmanuel Lillet, son compagnon d’alors.

Différents happenings eurent lieu au Théâtre Barbey voisin (grâce à l’aide de Charles Imbert et Raymon Paquet), toujours en 67 et en 68, organisés par Busto, René Strubel, Loïc Picard ou Jean-Bernard Désobeau, dont un fameux _Sans paroles ni musique_, le 7 avril 1967 (enregistré par l’ORTF). Ces happenings réunissaient plusieurs comédiens parmi lesquels Jean-Pierre Nercam, Françoise Cabrié, Annie Roussel ou Thérèse Liotard (_L’une chante, l’autre pas_), initiatives singulières sans aucun rapport avec la programmation de l’Onyx.

Ensuite, après les évènements de 1968, la sœur de Robert Triguero et son comparse hébété se sont opposés à la poursuite du projet jugé peu rentable pour se remettre à la bibine honorable, et Robert a dû abandonner le lieu en faillite que tous les étudiants des Beaux-Arts ont alors déserté devant le couple maudit. La dernière fois que j’ai vu Robert c’est en 1970 à Paris chez sa mère dans un état lamentable.

Au-delà je n’ai rien su, sinon que la programmation ultérieure et touristico-intestinale n’avait plus rien à voir avec le projet de 1967. En Avril 1970 _l’Onyx_ se trouvait _Chez Jimmy_, un endroit peu recommandable où Patrick Lacoste a dû faire office plusieurs fois de “videur”, ce qui lui réussissait parfaitement, avant qu’il ne devienne lacanien sur le Parc Bordelais. En 1979 _l’Onyx_ était établi dans le quartier Saint-Pierre où il est resté jusqu’à la fin.

Je pense que vous pourriez publier les premiers articles de_Sud-Ouest_ que je vous envoie par courrier, notamment ceux de Jean-Gérard Maingot, passionné par la création du lieu et qui assista à toutes les “premières”.

Il faut noter tout de même qu’en mai 1968 un des membres fondateurs du lieu a été expulsé par le collectif pour avoir entre autres frappé un flic avec une statuette en bois de Don Quichotte (figure qui pourtant convenait bien au quartier !), et pour le danger qu’il était supposé représenter par rapport aux institutions.

On ne pourra prétendre l’inverse à propos de ce tribunal ridicule, car je travaillais sur un décor ce jour-là dans la pièce où il s’est tenu, et j’ai parfaitement entendu les jugements des uns et des autres.

Dans ce “tribunal populaire” figuraient tout de même de prétendus anarchistes (la chose est drôle !). L’un d’entre eux, dont le nom n’importe pas plus que Laxatif ou Furoncle, s’est soudainement revêtu d’une peau de mouton en se découvrant occitaniste pur et dur depuis l’Antiquité, alors qu’il venait juste d’apprendre l’occitan à Périgueux avec la méthode Assimil !

Le seul à avoir alors violemment protesté contre cette procédure, ce fut Jean-Louis Froment, bien qu’il n’ait plus fait partie du groupe depuis longtemps.

_Mathias Perez_