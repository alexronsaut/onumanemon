---
date: 2021-08-31 2:17
title: Carte du Monopotapa (à Aurélie D.)
sous_titre: ''
date_document: Après 1984
fichiers:
- "/carte-du-monopotapa-verso.jpg"
- "/carte-du-monopotapa-recto.jpg"
tags:
- document
- sculpture
- Cosmologie Onuma Nemon

---
Châtaignier tronçonné. Crayon, encre noire, encre de Chine. 25 x 30cm.