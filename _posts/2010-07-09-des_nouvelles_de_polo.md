---
title: "Des Nouvelles de Pol’O"
date: 2010-07-09 13:26
fichiers: 
  - P.O.L_Courrier._12.98.jpg
  - Monde.Tristram-RLG.9.8.96.jpg
  - OriginalArticleTristram.opt.pdf
sous_titre: "Le Bêtisier" 
date_document: "7 Décembre 1998" 
tags:
  - document
  - HSOR
  - texte
---

Peu avant ce courrier, Sylvie Martigny, Onuma Nemon et Jean-Hubert Gailliot avaient publié dans _Le Monde_ un article critique à propos de la NRLG (que l'on trouverai ici ainsi que le texte original non “réduit”), pour en dénoncer le formalisme sous des enjoliveurs prestigieux.  
  


Il faut préciser que P. O. L. avait été averti de l’article par Gailliot qui avait largement raboté les aspects les plus violents du texte original. Et que par sa tendance à avoir toujours le cul entre deux chaises (et les appendices qui pendent ?) Le Monde avait cru bon de contrebalancer cet article critique par une défense un peu mièvre du si bien nommé Kéchichian. Toutefois P. O. L. renvoya aussitôt les volumes des deux auteurs (pour O. N. il s'agissait de _Tuberculose du Roman_ et pour Gailliot sans doute de _La Vie Magnétique_ ?) qui avaient été acceptés. Au téléphone P. O. L. précisa qu’il ne saurait accepter les textes d’écrivains qui critiquaient d’autres auteurs de la maison.  


O. N. lui fit remarquer que cela soulignait l’aspect d’_écurie_ (pire que l’école et à l’opposé de tout mouvement) de ses protégés, pour craindre la moindre contradiction et un texte plutôt tendre, par rapport à des distributions de baignes à la Cendrars, lui véritable homme de mouvement, mais P. O. L. maintint son refus.  


_Etc._  
Marc Bohor.