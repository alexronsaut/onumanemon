---
title: "Six Poèmes de Décembre 1968. A. Béthanie"
date: 2007-06-14 12:41
fichiers: 
  - 184.pdf
sous_titre: "Livre Poétique de Nicolaï. 1968-1984. Pressent. Poème n°6A" 
date_document: "Décembre 1968" 
tags:
  - document
  - OGR
  - texte
---

_**A. Béthanie**_  
  
J’ai 22 ans ; j’suis interné  
Depuis deux ans. Pas de problêmes. Sont  
Salement les mots qui partent d’ici  
N’empêchés. Une fois fixés ils sont  
Plus durs que béton : c’est embêtant !  
_Elle a menti_ ?