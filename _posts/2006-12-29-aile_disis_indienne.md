---
title: "Aile d'Isis Indienne"
date: 2006-12-29 16:30
fichiers: 
  - 33.jpg
sous_titre: "Aquarelle, feuille d’or, collage métal peint." 
date_document: "Entre 1984 et 2000" 
tags:
  - document
  - OR
  - peinture
---

Cette aquarelle a fait partie de la présentation de la Cosmologie au “Quartier” de Quimper en 2006.