---
title: "Toujours des couloirs"
date: 2007-10-09 14:18
fichiers: 
  - 281.mp3
sous_titre: "Les Grands Ancêtres. Ligne de Vivien de Nérac" 
date_document: "1989" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - son
---

Pages sonores inédites. Disque 2. Piste 11.  
Réalisation de Philippe Prévot dans les studios de LIMCA à Auch.