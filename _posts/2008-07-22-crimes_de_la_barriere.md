---
title: "Crimes de la Barrière"
date: 2008-07-22 23:15
fichiers: 
  - Nicolai.Terre_.Voyous_dOrnano.pdf
sous_titre: "Les Escholiers Primaires. Extensions de la Ligne de Nicolaï. Terre" 
date_document: "1984" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

Ensuite Camille et Basta (désormais son mari) s’attardent à Bordeaux avant de se rendre au bord de la mer, vers Andernos ou Le Porge, les plages des pauvres, ou bien Arcachon, Le Cap Ferret, preuve d’un luxe inaccoutumé. En s’arrêtant à Bordeaux, ils vont voir Aube, à Bruges, chez le grand-père “aux doigts gros comme des saucisses de Toulouse” et les cousins Artaud, un peu tarés ; tandis que la palette des après-midi mal vernis s’avance, ils doivent subir les baisers mouillés ; ils se voudraient prestes, attentifs, mais ils sont disparates. Puis comme il n’y a pas de place chez elle, pour dormir tranquille, Aube leur a trouvé la maison vide d’une amie, pour une nuit, rue Vercingétorix, vers la Barrière d’Ornano, pas loin de “chez Sambo”.  
C’est le jour de ses vingt ans, et Camille se repose sur le bord du lit, _après_, rue de Bègles, paumes en pronation serrant le bois des montants, et projetant la tête humérale en avant et vers le haut, le cubital antérieur et le premier radial épanouis de part et d’autre ; et, grâce au reflet, sur le rebord du muret humide au-delà de la terrasse où la pluie cesse à peine sur le fond des branches désastrées de platanes, les restes d’ocre-sienne et clair, la tête penchée, son dos rond et puissant de sirène (comme elle a coutume de s’asseoir sur le bord du bassin), ou de pianiste (tout à l’heure sur sur son tabouret), dominant, arche puissante, si bien lié à la fragilité de l’ensemble, aussi surprenant que le soudain profil d’Isis éclatant tout au long des salles du pavillon égyptien du Louvre, impossible à imaginer à partir de la vue des hanches de face, fessiers puissants de gymnaste sortant d’un plan inattendu sur l’ébauche, peut-être la danseuse de Degas aussi.