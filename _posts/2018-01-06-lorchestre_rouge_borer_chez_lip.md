---
title: "L'Orchestre Rouge & Borer chez Lip"
date: 2018-01-06 15:19
fichiers: 
  - lorchestre_rouge_24.9.73.pdf
  - borer_chez_lip_3.10.1973.pdf
tags:
  - document
  - DAO
---

J'ai retrouvé au milieu de mes archives professionnelles ces deux coupures de presse de Libération qui dataient du début octobre 1973 et que j'avais collées ensemble, précisément au retour d'un long séjour à Varsovie.

Quelques années plus tard j'ai eu la chance de connaître Alain Borer comme enseignant.

_Stanislaw Brzezinski_

_Langennerie à Chanceaux-sur-Choisille_