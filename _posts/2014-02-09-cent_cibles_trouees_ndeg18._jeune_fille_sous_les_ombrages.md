---
title: "Cent Cibles Trouées : N°18. Jeune fille sous les Ombrages"
date: 2014-02-09 22:08
fichiers: 
  - 18.Jeune_Fille_sous_les_Ombrages.jpg
sous_titre: "Acrylique blanc sur Cible 17 x 17cm pour pistolet 10m" 
date_document: "Après 1984" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - peinture
---

À G. C.