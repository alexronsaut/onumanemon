---
title: "Le Réseau Osiris 44 à Dijon"
date: 2008-05-28 20:20
fichiers: 
  - Reseau_Osiris_a_Dijon.pdf
sous_titre: "Les Gras. Ligne de Henri. Terre" 
date_document: "1986" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

Cela étant, supposons qu’un associé de Champlain qui n’ait connaissance ni du témoignage de Skorzeny depuis le pic du Gran Sasso, ni de la plus belle carte de neige paillettée d’argent et d’or de Henri, décroche son appareil. Après une double présélection (_espace_, puis _temps_), il se trouve relié à un sélecteur de zone situé dans la zone des neiges 22, qui sera plus tard installée par un de ses descendants (le terrible Henri, destiné malheureusement à mourir dans une humidité des neiges qu’il a toujours détestée, en Côte-d’Or, à la recherche du secret d’Aloysius et de Bruges). Les chercheurs discriminateurs de cette zone sont représentés sous la même forme que ceux de la zone 44, c’est-à-dire par des chercheurs dont le champ d’exploration possède deux parties, _a’_ et _b’_. La partie _a’_ donne accès à des lignes auxiliaires qui sortent de la zone après avoir traversé un circuit de discrimination temporelle, et aboutissent chacune à un sélecteur de zone géographique situé dans le centre nodal 33, alors que la partie _b’_ donne accès à d’autres sélecteurs de plusieurs milliers de plateaux à la fois géographiques et géologiques. On ne peut pas parler pour autant de “mémoire”.