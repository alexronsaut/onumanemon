---
title: "Intermède CH"
date: 2009-10-13 20:40
fichiers: 
  - Intermede_CH.pdf
sous_titre: "Tuberculose du Roman" 
date_document: "1972" 
tags:
  - document
  - OGR
  - texte
---

_Ce texte a été repris dans la partie Schola de l’Ourcq des États du Monde (Ligne des Escholiers Primaires. Ligne de Didier. Saison de la Terre. )_  
  
« On ne peut pas avancer comme ça sans progrès, dit John.  
— Co-co-comment ça demande CH. ?  
— Cent trente six moins cinquante-sept, soixante dix-neuf. (_Elle avait déjà pillé deux banques_). Comme un enfant de sept ans. Ça va pour les curés, pas pour nous ; c’est _le jugement_, qui compte.  
— On pouvait pas partir d’ailleurs que du milieu du pré, dit L., puisqu’on y était ; ni aboutir autre part qu’ici. Pour se repérer jusqu’au bord du lac, ensuite, ça, c’est une autre histoire !…  
— Co-co-comment ça i s’appelait, le gars ?  
— Manvantara, un truc comme ça. Leurs familles, c’est d’un compliqué !  
— Si vous m’aviez laissé tirer un peu du lait de cette vache, au moins ! »  
\[………………………\]