---
title: "Portrait de Tsukada, Nambu et quelques autres"
date: 2007-01-19 16:43
fichiers: 
  - 44.pdf
sous_titre: "Article de la revue DAO" 
tags:
  - document
  - Sabaki
  - texte
---

Maître Tsukada était vraiment l’inverse du gymnaste bio et refoulé à la Rouet. Dans les stages organisés par Maître Nambu à Calagogo ou à Martha’s Vineyard, il déambulait toujours cigarette aux lèvres en karategi court qui laissait voir des tatouages bleus et en raclant les zoori-getas, portant des lunettes fumées, les cheveux en brosse et un visage mitraillé de petite vérole. On avait l’impression qu’il était dans une aura de graffiti et qu’il sortait du quartier Yakuza.  
Il était maigre mais avec une musculature d’écorché, très bien dessinée, pure objectivité avant le style de frappe, comme Truong, et il avait un kime foudroyant.  
C’est du reste curieux comme à partir des années 75 à peu près la débilité de la musculation de force a repris le dessus dans les dojos français pour faire oublier leur faillite primordiale dans les arts de combat orientaux. On a voulu réintroduire les catégories de la boxe anglaise et le poids comme condition de pensée efficace.