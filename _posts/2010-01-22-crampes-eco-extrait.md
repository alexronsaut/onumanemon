---
title: "Crampes. Éco (extrait)"
date: 2010-01-22 16:46
fichiers: 
  - crampes-eco.pdf
sous_titre: "La Tribu des descendants d’Ossip, le Tzigane." 
date_document: "1975-1984"
tags:
  - document
  - OGR
  - texte
---

“L’ordre règne à Santiago.”  
Òn y voyait le profil d’une femme maigre aux traits émaciés vêtue kaki avec un bandeau dans les cheveux portant une mitraillette également kaki moulée contre elle.  
« C’est pour éviter les cheveux dans les yeux au moment de tirer, le bandeau ?  
— Quel modèle !  
— Quelle couverture !  
— C’est fait pour brouiller la vue.  
— Moi j’ai connu des modèles nus qui hurlaient lorsqu’on rentrait dans leur loge après la pose _alors qu’ils étaient en train de se rhabiller_ ! dit Basta.  
— Le mi-nu c’est toujours plus cochon, dit la dame en bleu ; le nu c’est un déguisement parfois ; ce sont les douze coups de mi-nu !  
— Ohhhh ! »  
  
etc.