---
title: "Colomb : Embarcadère"
date: 2008-02-22 17:05
fichiers: 
  - Ligne_Don_Qui.Colomb.Embarcadere.pdf
sous_titre: "Les Grands Ancêtres. Ligne de Don Qui" 
date_document: "1978" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

**Le Futur :** « À peine descendu sur le quai pour quelques heures de nuit, puis en trop-plein de chair sur la place ; seul le boulanger pêtrissant, qui m’a donné l’heure sous une pluie plus ou moins épaisse selon les endroits, était levé avant l’aube. J’allai voir sur les Champs le balaiement du phare (comme ailleurs à Queyries, à Nantes, à Dunkerque…), lequel accueille le vaisseau Argô, devenu Le Lyncée, qu’Orphée doit quitter pour La Carabela La Niña, première chose magique reçue, en me lavant, par la fenêtre maritime ; alors qu’une fois à terre, ce fut le rougeoiement interne à l’Église reclose.  
Près de là, le mitronm’a donné l’heure ; c’était le seul à œuvrer. Nous sommes à débarquer dans la saison qu’on sait du fameux foin, du fumier, du foitrail, le f de la raison, la moisson incluse dans la fenaison, comme Pollock trouva (et Monet avant lui) l’équivalent de la musication en peinture.