---
title: "La soupe pré-biotique du Net"
date: 2008-05-27 14:55
fichiers: 
  - Le_Net_la_Soupe_.pdf
sous_titre: "Sphère Critique" 
date_document: "2008" 
tags:
  - document
  - HSOR
  - texte
  - edition
---

Texte paru dans la revue [Fusées n°14](http://poezibao.typepad.com/poezibao/2008/10/poezibao-a-reu.html#fu14)

_**1. Idée**_ Le chercheur autrefois souvent trouvait ses idées parmi les ombres du plafond, dans la rêverie ; et elles surgissaient de collisions incongrues ; aujourd’hui il est fixé sur un écran. Ce n’est pas du tout la même chose. Voilà ce que me dit un ami mathématicien.  
Je pensais du coup à Lagrange, ce mathématicien à qui la musique servait de clôture, d’abri à l’inspiration : passée les trois premières mesures il partait dans son univers sans plus rien pour troubler ses élucubrations ni gêner l’agencement de ses hypothèses.  
L’idée se forme par tournoiements dans le vide, adhérences, accrétions successives ; elle se balade, va dans le hasard des roulis, mais si tout de suite le tissu social la retient, il bloque cette errance ; elle n’a plus la chance de ce hasard des roulis et l’idée disparaît avant même de prendre corps. Sans doute ce que Denis Roche appelait “signifiant baladeur”.  
Aujourd’hui il y a ce fait démocratique indiscutable qu’une découverte peut être offerte immédiatement à une foule d’autres chercheurs dans le monde grâce au Net, mais en même temps elle est livrée pieds et poings liés, sans le temps d’un rebond, et sans que le chercheur ait eu le temps d’amasser en lui assez d’inactuel.  
L’aspect chronique du compte-rendu empêche le bondissement de la crise. Il faut garder la chance d’une surprise de la pensée, de la perte d’une lettre, d’une découverte, d’un surgissement.  
  
Kafka affronté à cela aurait abandonné immédiatement certaines des pierres anguleuses que roule son journal. Par exemple, quand il parle de “La fille au visage plat, dont la robe grossière ne commence à se déplacer que tout en bas, dans l’ourlet. Quelques-unes sont habillées ici comme les marionnettes qu’on vend pour les théâtres d’enfants à la foire de Noël, c’est-à-dire que leur robes sont faites de ruches et d’or collés et cousus à points lâches, de sorte qu’on peut les découdre d’un coup et qu’elles se disloquent entre vos doigts. La patronne, avec sa chevelure d’un blond mat fortement tirée sur un bourrelet certainement dégoûtant, avec son nez descendant en pente raide suivant une direction qui se trouve dans un rapport géométrique quelconque avec ses seins tombants et son ventre tendu, se plaint de maux de tête provoqués par le fait que c’est aujourd’hui samedi, qu’il y a bien du tapage et que ça n’en vaut pas la peine.”
