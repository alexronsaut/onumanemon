---
date: 2019-12-23 9:35
title: Cible rouge n°8
sous_titre: Cible imprécisée
date_document: Après 1984
fichiers:
- "/Cible rouge.8.Cible imprécisée.jpg"
tags:
- dessin et gravure
- Cosmologie Onuma Nemon
- document

---
Pistolet 10m & dessin à la plume sur anciens cartons de cibles format 14 x 14cm en 5/10èmes