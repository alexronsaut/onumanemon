---
title: "Le 12 Septembre"
date: 2011-09-12 21:18
tags:
  - billet
---

À revoir les attentats du 11 septembre à l'occasion de leur commémoration : toujours la même _sidération_, malgré le fait d'avoir vu et revu ces images des milliers de fois et malgré l’épaisseur des médias, la connerie des speakers, etc. : le mille-feuilles aurait-il gardé la crème de l’émotion immédiate du crime à cause de la symbolique millénariste et du jamais-subi dans une Amérique intouchable ?  
C’est inaugural, comme Christophe Colomb et comme l_’Hombre_ sans Homme d’Hiroshima ou comme l’émotion ressentie au surgissement vif d’une saison qu’on célèbre (l’automne, en particulier), dans un lieu où elles sont particulièrement “tranchées”. Sagesse de l’émotion.  
En réalité ça ne vient pas de ça mais de l’incarnation de l’horreur, d’être renvoyé par le film au regard des passants, à leur visage horrifié, d’entendre le “Oh ! My God !”, de voir la course éperdue et tâtonnante du filmeur… Puis grâce à l’efficacité du calcul médiatique de Benladen (les 17 minutes entre l’attaque de la première et de la deuxième tour ayant permis de bénéficier de l’arrivée d’équipes professionnelles), ces réactions sont d’autant plus sensibles et rentrent d’autant plus dans le champ de la compréhension. On reçoit _en direct_ le décalage entre la vision et la compréhension, comme lorsqu’on assiste à un accident dont on est partie prenante.  
Autre chose remarquable : la beauté du couple Obama. Ça se distingue, chez les gouvernants. Ils font aussi fort que les Kennedy, comme on dirait dans _Ciné-Monde_ (ou dans _Le Figaro_, ou _Le Nouvel Observateur_ ou _Libé_, puisque c’est désormais la même idéologie).  
Sûrement que _les Tours_ sont devenues la scène inaugurale pour certains comme le film d’Abraham Zapruder le fut pour De Lillo (qui du reste a écrit également _L’Homme qui tombe_.)  
  
Sans chercher à s’interroger sur les _111_ jours qu’il reste avant la fin de l’année, la Onzième arcane du tarot, c'est _La Force_ : l’âme qui dompte un lion, dirait l’ami Vivien Isnard, au moins aussi numérologue et astrologue que peintre.  
Le 12 c’est le renversement du _Pendu_, et c’est aussi _L’Homme qui tombe_. Et pour le 13, c’est-à-dire demain et _L’ombre du Mat_, on se réservera la lecture de _Vendredi 13_ de Goodis, même si ça par malchance ça tombe un mardi, en attendant des divertissements nucléaires pour faire mieux que l’exploit de trois mille morts grâce à une fourchette.  
  
O. N.