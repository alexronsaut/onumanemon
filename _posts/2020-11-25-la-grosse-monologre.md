---
date: 2020-11-25 18:39
title: "La Grosse. Monologre"
sous_titre: "(extrait des États du Monde)"
date_document: "avant 1984"
fichiers: 
  - "/La Grosse.Monologre.pdf"
tags:
  - texte
  - Cosmologie Onuma Nemon

---

Ceci est un extrait du Monologre de La Grosse dans les _États du Monde_ ; à la fois Voix et Figure essentielle de la Cosmologie. Il y a deux Grosses en réalité : la Super-Grosse, Fernande, dite Magdalena puis Hermana, la dernière de ses sœurs, qui est aussi Héra, Tante Pim, Moby Dick et sa réincarnation en La Estrella chez Cabrera Infante, etc… C’est à la mort de Fernande qu’Hermana prend sa place, grossit encore un coup, et reprend son numéro de cirque.
Son Monologre, long d’à peu près trois cents pages traverse toute la Cosmologie, réparti en plusieurs quartiers.

À chaque tranche, le ton change, les images, les références (passant par plusieurs guerres, entre autres), et la dernière tranche finit par un émiettement, un ressassement plus court (derniers sillons du disque, dernières lignes de la spirale obsessionnelle) de la plupart des noms de la Tribu Zeusteiner, feuilletés avec des photographies. L’extrait qui suit est le deuxième morceau du Monologre qui fait dans son ensemble surtout référence à deux enfants morts à deux générations d’intervalle : Lulu, fille d’Hermana, et Didier, son petit-fils (bien que les générations ne veuillent pas dire grand’chose là-dedans), ainsi qu’à tous les enfants morts du Quartier Saint-Michel. Les autres morts évoqués sont Fernande et son premier amour Prosper, l’homme au “suicide polygraphique” (rasoir + poison + noyade + révolver), puis les Ancêtres : Baptiste, le carrieur de pierres au mouchoir fin et à la face blème, Pierre de Nérac, etc.

O. N.

*

Camps de Concentration 1939-45  
Mort de Didier 1949. Mort de l’Abuela 1950  
“C’était l’Hiver, je m’en souviens, tout s’est précipité : Didier le pauvre petit venait de mourir, Marie avait la tremblôte et je t’avais amené chez Michaud, l’oculiste. Schelley m’a dit : “Vous êtes allées chercher le plus con sur la place de Bordeaux ; tout juste bon à soigner les nègres !” Il voulait te faire des piqûres de lait autour de l’œil ! “C’est un con ; il a traité que des canaques parce qu’ils osaient pas se plaindre, dans la brousse !” Il m’envoie chez Nicolas, à l’Abbé-de-l’Épée, près des Sourds & Muets. En plein Hiver ; ça neigeait à grosses bourres, on patinait sur la Devèze ; même les gosses laissaient des traces.

(lire la suite…)
