---
title: "Trois Petits Tours et puis s’en vont…"
date: 2011-05-14 21:36
fichiers: 
  - Turn_Turn_Turn.pdf
  - Chutier_Vue_densembleNet.jpg
  - Detail_des_Bandes_du_Chutier.Format_Net.jpg
  - Discman_Chutier_Stereo.Format_Net.jpg
  - Petite_Vitrine_Magique.Format_Net.jpg
  - Stereoscopie__Portrait_Ovale.Format_Net.jpg
sous_titre: "Souvenir de la Petite Maison" 
date_document: "13 Mai 2011" 
tags:
  - document
  - DAO
  - texte
---

**Turn turn turn** c’est une chanson de The Byrds, groupe américain des années soixante, c’est un air qui tournait dans ma tête, c’est le titre de cette exposition. T**urn turn turn** c’est les paroles de l’Ecclésiaste. **Turn turn turn** ce pourrait être le titre de _l’homme qui tombe_, une image phonographique, une gravure rock’n’roll, Icare et le onze septembre avec ces slhouettes qui chutent le long des buildings en flammes. **Turn turn turn** c’est _review_, un présentoir à images, un piège à regard conçu plus pour agacer l’oeil que pour magnifier la troisième dimension, c’est le tour d’un monde en chute libre. **Turn turn turn** c’est _Chutier_, un croisement entre pellicule cinématographique et ruban tue-mouches, l’idée d’un film possible, un hommage à Gil Wolman, une pensée à Jean Luc Godard « qu’est ce que j’peux faire, j’sais pas quoi faire ». **Turn turn turn** c’est _Aux étoiles_, _Le portrait ovale_, un retour sur mes premières images,trace contre trace avec la photographie, des images amoureuses. **Turn turn turn** c’est _dessins d’atelier_ (_ma vie ouvrière_), tombeau/jardinière aux dessins tracés pendant vingt-cinq ans de vie d’usine, Ne travaillez jamais écrivait sur un mur Guy Debord, et pourtant l’artiste ne parle que de travail, travaille tout le temps, c’est des moments détournés au travail quand la vie est ailleurs, juste masquée par le bruit des machines. **Turn turn turn** c’est tout ce temps passé à reproduire, tracer, graver, résister en somme à ce « désespoir de l’art et son essai désespéré pour créer l’impérissable avec des choses périssables , avec des mots, des sons, des pierres, des couleurs afin que l’espace mis en forme dure au delà des ages » ( J.L.G. Histoire(s) du cinéma). **Turn turn turn** c’est la fin de l’Artothèque, trois jours d’exposition, ma petite révolution de mai.  
Vincent Compagny 2011.  
 _lire la suite…_