---
title: "Dentiste & Bacon"
date: 2009-01-29 16:53
fichiers: 
  - Nany._Dentiste__Bacon.pdf
sous_titre: "Ligne des Adolescents. Aube et Nany. Été. L’Académie" 
date_document: "1969" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

_**Nany.**_  
(Le Dentiste me convoque dans l’ancien saloon où il travaille, et au lieu des réparations prévues avant mon récital de gospels autour des Apôtres chez La Grosse, qui a une grande terre, un peu plus bas, alors que je me préparais même à lui en entonner un du bon négrier John Newton, il m’impose un énorme appareil (paraît-il “nécessaire à mon registre”) en citrine transparente et résine polyesther, mais d’une construction extrêmement lourde et comportant des sortes de roues ou de “cales” aussi grosses que des demi-pommes, qui me forcent les joues plus encore que ne le fera le futur Barrault dans le rôle d’Opale, de Renoir, et les blessent.