---
title: "Osiris à Dijon"
date: 2008-05-24 19:31
fichiers: 
  - Osiris_a_Dijon.pdf
sous_titre: "Les Gras. Ligne de Henri. Terre" 
date_document: "1986" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

Texte inédit  
  
_**Osiris, écharde des dieux rend visite à Henri à Dijon**_  
Quand Osiris voyage, c’est avec Isis, (qui n’est pas la Sabine qu’on croit, c’en est une autre, qui se nomme parfois Linda Lovelos !). Il voyage également en compagnie d’Orphée. L’arrêt sous le pont, toute cette poésie-là, ça c’est quand Osiris se rend chez Henri. Il a pour but de créer un ballet avec Isis et de mettre au point un réseau télégraphique efficace avec les Enguirlandés et les Conjurés Clignotants de la Tour Eiffel.  
Ça commence toujours à la fin par la difficulté d’ingestion du café, et la démesure des entrelacs vertigineux de métal dans les hauteurs prodigieuses de la Nuit au-dessus, jusqu’à l’excès d’acide nitrique du steak venu des cuisines du Tartare ingurgité à Midi qui produit des anthrax…  
Un rien d’Hiver suffisant à dissoudre et confondre toute son énergie depuis le petit café bleuâtre au-delà de Lyon vers la Côte-d’Or, jusqu’ à la terrasse du cimetière aux abords de la ville : épines du désert, chardon, croissants et tour ; puis _sa voix_ (l’esquive est un don des dieux, il se souvient aussitôt des étoiles d’argent et dorées chues de la petite carte envoyée par Henri), pour ne plus laisser que la forme horrible.