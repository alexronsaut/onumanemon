---
title: "Noir et Blanc"
date: 2008-12-17 21:12
fichiers: 
  - Noir_et_blanc.pdf
date_document: "2004" 
tags:
  - document
  - texte
---

_Cette nouvelle est de Claire Viallat. Elle fait partie d’un volume en cours d’élaboration._  


  
“La journée s’annonçait claire”. Or, il se trouve que L’Ombre est partout, à la fois savante et tactile ; elle fait partie du sujet et de l’objet autant qu’elle s’en détache. Hombre = c’est l’Homme en espagnol. On dit souvent d’une femme qu’elle est l’ombre de son père ou de son mari : “La fille de…”, “La femme de …” Mais si l’on sait que l’ombre c’est l’âme, ça voudrait dire que c’est ce qui leur échappe de meilleur. Comment dès lors ne préfèrerait-on pas lâcher la proie pour l’ombre ? Le réel du grand Autre chez Lacan, en somme.  
Claire Viallat reprend ici une énigme ancestrale.  
Comment suivre la ligne de crête de la sagesse entre ombre et lumière, et ne pas basculer au Pays des Morts (il n’y a pas d’ombre au Paradis), engloutie dans ce double anonyme du sujet, comment ne pas disparaître dans l’autre innommable dont les oreilles de loup pointent dans tout autoportrait (“l’autre-au-portrait”), trou de suspens vibratoire de la discontinuité dans le temps, latence prête à bondir sur l’apparence manifeste.  
Ou bien, dans le plus pur démon de Midi à l’ombre courte, comment emprunter un passage cristallin vers l’au-delà ou l’Amour danse grâce au cadran solaire d’Arsène Lupin dans le Triangle d’Or ?  
Ombre élastique, ombres des personnages à une autre heure que celles des arbres, dans Marienbad, hors-champ total des ombres d’Hiroshima qui sont les seules à rester alors que les corps (qui les ont portées ?) ont disparu après la lumière aveuglante.  
Femme sans ombre de Richard Strauss (1864-1949), ombre de Peter Schlemihls qui s’échange dans une triangulation avec âme et bourse.  
Puis topologie, topologie : la science de nouer des ectoplasmes ?  
  
Onuma Nemon. Octobre 2008  
  
 _**NOIR ET BLANC**_  
  
Si au lieu d’une figure vous mettez l’ombre seulement d’un personnage, c’est un point de départ original, dont vous avez calculé l’étrangeté.  
Gauguin à Emile Bernard, 1888-1891, Pierre Cailler, Genève, 1954.  
  
Tous excellent à donner un contrepoint de chair, de vie, au fantôme qui occupe le centre du récit, et qui transforme la vision du monde autour de lui.  
Critique du film « L’Adversaire » de Nicole Garcia par Aurélien Férenczi, Télérama n°2746.  
  
L’ « ombre représente la somme des domaines du réel que l’homme ne veut pas voir ni reconnaître en lui-même et qui lui sont, de ce fait, non connus donc inconscients. L’ombre représente le plus grand danger pour l’être humain car il ignore son existence, il ne la connaît pas. C’est l’ombre qui fait que nos désirs et nos aspirations ainsi que le résultat de nos efforts se manifestent finalement dans le sens contraire de ce que nous attendions. Les manifestations de l’ombre sont projetées par l’homme sur le monde extérieur où elles prennent la forme du « mal ». Cette projection lui évite de voir que la source de ce mal est en lui, ce qui l’effraierait trop. Tout ce que l’homme ne veut pas, ne supporte pas, n’aime pas incarne son ombre, elle est la somme de tout ses refus.  
Thorwald Dethlefsen, Rüdiger Dahlke : Un chemin vers la santé, ed° Randin-Aigne, 1990, Suisse.