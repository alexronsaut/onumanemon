---
title: "Parcours du Parc n°7"
date: 2008-02-29 21:57
fichiers: 
  - Parcours_Parc_7.jpg
sous_titre: "Les Adolescents. Joyelle & Hill" 
date_document: "1978" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - photographie
---

Format 13 x 18 cms. Cette photographie faisait partie de l’exposition au “Quartier” en 2005