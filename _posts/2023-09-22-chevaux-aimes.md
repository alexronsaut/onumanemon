---
title: "Chevaux aimés (Bardes du Khorassan, Ostad Elahi)"
sous_titre: "Joël Roussiez"
date: "2023-09-22 16:16"
date_document: ""
fichiers:
    - /chevaux-aimes.pdf
tags:
    - document
    - texte
    - DAO
---

Comme une âme qui s’en va un petit air de flûte dans l’assemblée avait éteint les paroles et quelques uns marchant dehors baissaient la tête sous leurs turbans. Mais les chevaux piaffaient dehors, une tension dans le troupeau irritait et de même dans l’assemblée circulaient des jurons. Le cercueil resta dans l’église, et quand le musicien chanta «il est parti, ils est parti! Que les lointains l’accueillent» on entendit des pleurs et quelques gémissements. .…

(lire la suite…)
