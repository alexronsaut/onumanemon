---
title: Vide-Grenier
date: 2023-12-19 8:17
sous_titre: Typhaine Garnier, dessins d’Onuma Nemon
date_document: 2014
tags:
    - document
    - DAO
    - texte
fichiers:
    - "/vide-grenier_extrait.pdf"
---

Grande braderie des fétiches, liquidation des souvenirs les plus _chers_. En vrac défilent, retapés en objets de langue, motifs de l’enfance, choses du cœur et déboires du corps, dans une traversée éberluée de l’affolant fourbi de la vie, vaguement classé ici en chapitres (« Fragile », « Tout-venant », « Ne pas ouvrir », « Fins de série »).  
Les dix « Vracs » d’Onuma Nemon (encre de Chine sur Arches, 2018 et 2023) n’illustrent pas, ils sont plutôt la rêverie du texte, en une série de paysages mentaux dont les lignes précises et les détails minutieux ne prennent cependant forme en aucun objet identifiable.  
Les choses surgissent et disparaissent dans un même mouvement joyeux. _Liquidation_ dit flux, dévalée de prose rapide. Entre distanciation grinçante et sensualité éberluée, l’écriture bazarde vite, sans s’apitoyer. Les souvenirs sont affublés d’un costume bouffon, l’intime emballé dans la rhétorique kitsch des petites annonces. Et bien sûr, l’emballage craque, farci d’éléments exogènes (amorces de récits, bouts de dialogues, couplets de chansons, réminiscences diverses...). Le modèle explose en même temps que les objets évoqués : bon débarras !

[https://lurlure.net/vide-grenier](https://lurlure.net/vide-grenier)

Éditions Lurlure
Emmanuel Caroux
7 rue des Courts Carreaux
14000 Caen (France)
