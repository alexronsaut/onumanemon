---
title: "Prosper & Orphée au Pays des Morts"
date: 2008-04-17 18:37
fichiers: 
  - Prosper_et_Orphee_au_Pays_des_Morts.pdf
sous_titre: "Les Gras. Automne" 
date_document: "1984" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_**Chambre de l’Hôtel où loge Orphée, au Pays des Morts**_  
On ne peut vivre avec les Morts, mais on ne peut vivre non plus sans eux.  
“Cette chambre est dans un carrefour électrique et joyeux ; elle a de multiples entrées qui ne se supperposent pas.  
De multiples accès par le rêve.  
Il n’y a pas de continuité logique dans la façon de l’atteindre, mais cependant la réunion de tous ces accès lui confère une entrée multiple, une infinité de correspondances entre les différentes torsions de plans.  
Il y a entre autres une véritable “réception” située flanc droit d’un immeuble, au deux ou troisième étage. Ensemble très propre et très aéré ; meubles cirés et napperons.  
La patronne est opulente ; beaucoup de draps ajourés de dentelles, jusque dans la tenue des femmes de chambre.