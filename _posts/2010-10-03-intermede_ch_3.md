---
title: "Intermède CH 3"
date: 2010-10-03 19:35
fichiers: 
  - Intermede_CH_3.pdf
sous_titre: "Tuberculose du Roman" 
date_document: "1972" 
tags:
  - document
  - OGR
  - texte
---

_Ce texte a été repris dans la partie Schola de l’Ourcq des États du Monde (Ligne des Escholiers Primaires. Ligne de Didier. Saison de la Terre. )_  
  
« Toutes deux sont aimantées par la clairière vers ce presbytère qui date d’avant la Révolution : CH. avec son hoquet sur le chaos de ses verbigérations, et L. Après un long piétinement autour du puits comblé de barreaux de chaises pris d’un désordre fourchu, cela nous semble un entassement au moins aussi insubmersible dans l’esprit amiral, une impossibilité esthétique de liens, de passages, d’adoucissements, que le vrac de notre arrivée dans cette situation. Notre rencontre à trois est aussi brute que cet hétéroclite-là, pensées de petits bois griffus et impraticables ; la parole qui aurait pu nous lier a fait défaut, et le hoquet monstrueux de CH. nous interdit toute tentative. Après le passage de la ligne de crête calcinée, nous nous sommes retrouvés au hasard des souffles des rues, puis au fond du couloir de la Vallée, dans les manies qui nous emportent, moi abruptement au bord de ces toutes deux viandes, qui ne sont pas sœurs. »  
  
Le temps d’extase de la cantatrice, l’élancement des reins dans la reprise du morceau, la visite inattendue et bavarde, le vol de la claque dans les dorsaux de l’haltérophile et sa conséquence ici à plat, voilà qui est agréable à qui sait parler par métaphores.  
Mais ce n’est pas dans cette sorte de _roman absolu_ que nous abonderons, _hélas_ !