---
title: "Ceci n’est pas une Pipe !"
date: 2012-07-17 22:00
tags:
  - billet
---

Est-ce _Public Image Limited_ et Lydon qui m’ont appris quelque chose de nouveau sur Michel Foucault ? Non.  


Un Lindon Chasse l’Autre  


J’ai été heureux une seule année à l’école, et récemment d’apprendre grâce à Lindon que Michel Foucault avait attrapé le sida dont il était mort en taillant une pipe sans protection à un étudiant aux États-Unis. Plus encore que Samir Nasri quand Raymond Domenech a perdu son travail. C’est vrai que la face du monde en eut été changée s’il s’était fait enculer sur un tabouret en lisant _Ce qu’aimer veut dire_, de Lindon (ouvrage bourré des anecdotes autour des écrivains tellement délicieux qui fréquentaient les éditions de pâpâ), ou telle autre variante de posture.  
_Il est ravi nous sommes ravis vous êtes ravis ils sont ravis, Passé composé j'ai été ravi tu as été ravi il a été ravi nous avons été ravis vous avez été ravis…_ (Est-ce du Quint-Âne ? Non, ce serait plutôt : “J’ai été contente.” et “J’étais contente.” C’est du pur Net.)  
On apprend aussi que Foucault n’était pas un second père. Ouf !  
  
Cela est aussi passionnant que les souvenirs d’enfance dudit Lindon ou jadis le texte _Enculade comme si vous y étiez_, paru aux mêmes éditions de Minuit, de Denis Jampen, bien connu ici à Nantes (Burroughs revu francité mièvre, urine de banlieue et zone de lisière), ou que les détails de la diététique de Robert Matzneff, obsédé par le tri sélectif des moins de seize ans.  
C’est la plus belle vision qu'on puisse avoir de l’œuvre de Foucault, qui resterait sans cela absolument incomprise, et on ne peut plus aprocher le génie de _Naissance de la Clinique_ sans cette clé. De ce fait l’ouvrage _Ceci n’est pas une pipe !_ de 1973 (l’année du texte de Jampen), s’en trouve illuminé _a posteriori_ !  
C’est quelque chose d’essentiel et pas du tout d’ordurier, ni de proche de ce qu’aurait pu écrire Minute ou dire Jean-Marie Le Pen. Non, C’est la Pine ! C’est vraiment, ressenti, profondément. C’est du Connan vrai breton. Humain, pédagogique, descriptif, narratif, incisif, physiologique, aussi bon qu’Amélie Nothomb.  
J’espère qu’avec l’accélération et l’amplification des moyens numériques et leur possiblité du reportage en direct, nous pourrons assister à l’agonie de Mathieu Lindon : s’il se faisait tout à coup empaler de force, il aurait le réflexe de nous retransmettre ça en direct en branchant son portable ; il nous donnerait immédiatement ses sentiments jusqu’à la dernière seconde, que l’on connaisse à vif son intériorité grâce à cette morsure par l’ourlet. Quel enseignement, quel prestige ! Et la littérature en sortira grandie, encore une fois.  
_J. F. Inlande. Nantes._