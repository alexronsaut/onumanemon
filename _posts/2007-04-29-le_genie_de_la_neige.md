---
title: "Le Génie de la Neige"
date: 2007-04-29 21:44
fichiers: 
  - 127.pdf
sous_titre: "Livre Poétique de Nicolaï 1968-1984. Poème n°8" 
date_document: "1969" 
tags:
  - document
  - OGR
  - texte
---

Les poèmes des Livres Poétiques sont indiqués ici par leur numéro, chaque poème correspondant au poème qui porte le même numéro chez le frère “en face”.  
Isabelle Revay.