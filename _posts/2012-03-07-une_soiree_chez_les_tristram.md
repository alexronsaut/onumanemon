---
title: "Une soirée chez les Tristram"
date: 2012-03-07 18:01
fichiers: 
  - une_soiree_chez_les_tristram.pdf
sous_titre: "Entretien avec Brigida Gazapón" 
date_document: "15 Mars 2006" 
tags:
  - document
  - HSOR
  - texte
---

Voilà donc longtemps que la rédaction de cet entretien était en attente de mise sur le Net, dans l’espoir de retrouver la version définitive et intégrale. Faute de mieux nous sommes partis de la bande enregistrée, qui comporte une partie inaudible et la fin effacée. L’original appartient à Brigida Gazapón qui a réalisé l’entretien et qui à l’époque s’occupait d’une petite revue en Argentine. Depuis elle est partie en Australie retrouver Lena et Miss Ross. Et plus aucune nouvelle. Si vous la croisez en “crawl-walking” dans la brousse en compagnie des kangarous chargés de répandre la bonne nouvelle de sa revue logée dans leur poche ventrale, parmi les dingos et les lapins excités, faites-lui signe de notre part, pour qu’on sache un jour de quel orage il s’agissait et quelle était la conclusion.  
_NDLR_