---
title: "Les rubans et la Croix"
date: 2011-08-01 12:41
fichiers: 
  - Les_Rubans_et_la_Croix.pdf
sous_titre: "Texte de Joël Roussiez" 
date_document: "2011" 
tags:
  - document
  - DAO
  - texte
---

Une barque se dandine sur le lac tranquille, de petites vagues cognent sa coque solide qui glisse contre le quai de bois et les roseaux. Son havre est étroit mais agréable, la berge est douce, couverte d'herbe verte en pente légère jusqu'au seuil d'une jolie chaumière de pierres. Des bouquets de roses surgissent dans le feuillage épais et sombre des rosiers rustiques; on entend le chant d'une flûte picolo et dans l'âtre brûle un bon feu; si la journée est belle, resplendissante même, le matin reste froid car nous sommes au printemps. Le printemps au bord du lac perd très lentement la fraîcheur de l'hiver mais l'automne résiste et garde longtemps les chaleurs de l'été. « Quand l'automne sera là, je ne serai plus là », une jeune fille le chantonne, sa sœur joue de la flûte; allongée sur le lit, elle accompagne les paroles joyeuses. Une soupe légère mijote, la mère repasse une ceinture de dentelle, ensuite il faudra faire le chemisier, « ma fille se marie avec sa chemise de lin ! », et puis « pom-pom-pom ! » rentre le tonton, « j'apporte le jasmin et les fleurs d'oranger », « pose-les ici » et voilà qu'il s'assoit. Oui, il prendra bien un bol de café, ça fait une tirée du château jusqu'ici... La flûte joue doucement avec un peu de précipitation, c'est la mélodie des amants qui dit: si tu me quittes alors je meurs et si tu restes, je vais mourir sur l'heure...  
  
_lire la suite…_