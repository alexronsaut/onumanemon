---
title: "Aux temps de la fleur et des épines"
date: 2011-04-06 19:45
fichiers: 
  - auxtempsdesfleursetdesepines.pdf
sous_titre: "Texte de Joël Roussiez" 
date_document: "2011" 
tags:
  - document
  - DAO
  - texte
---

Croquant des oeufs comme le bon sauvage, à pleines dents et se réjouissant, il faisait bon en ces temps se lever tôt, accompagnant le jour venant mais encore pris par la nuit, au chaud et se restaurant en jouissant du parfum des choses et de son corps réveillé. C'était au temps de la fleur et quasi au printemps; les bêtes avaient le poil brillant et les oiseaux une voracité fantastique. On fournissait en graine et en fourrage pour les dernières fois, les granges étaient maintenant presque vides ; on s'occupait de les balayer et d'y entreprendre quelques réparations.  
  
_lire la suite…_