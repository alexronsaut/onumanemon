---
title: "Portrait d’Eurydice"
date: 2006-12-30 17:53
fichiers: 
  - 38.jpg
sous_titre: "Encre de Chine" 
date_document: "1982" 
tags:
  - document
  - OGR
  - dessin et gravure
---

Ce dessin a fait partie de la présentation de la Cosmologie au "Quartier” de Quimper en 2006.