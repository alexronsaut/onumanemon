---
title: "Astrobade"
date: 2008-07-26 09:36
fichiers: 
  - Gastrobas.pdf
sous_titre: "Les Escholiers Primaires. Ligne de Nicolaï. Été. Lycée" 
date_document: "1979" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

 _**Astrobade**_  
Gastrobas adorait voler le pain à la cantine du Club des Coqs Rouges ; il le volait par tronçons de miches, en prenant le bout dur. Le Club était toujours resté dans l’ancien bâtiment-entrepôt d’un crépi doré qui abritait jadis le tissu en énormes coupons d’un commerçant juif aujourd’hui ruiné, à l’angle de Sainte-Eulalie.