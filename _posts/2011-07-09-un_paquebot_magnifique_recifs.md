---
title: Un Paquebot Magnifique & Récifs
date: 2011-07-09 14:45
fichiers:
- "/roussiez-joel-la-rumeur-libre-editions.pdf"
- "/roussiez-un-paquebot-magnifique.doc"
- Roussiez.Un_Paquebot_Magnifique.pdf
date_document: 14 juin 2011
tags:
- document
- HSOR
- texte
sous_titre: ''
---

Avec Roussiez on part dans l’aventure de la langue, un vrai roman d’aventures et l’aventure du roman, dans le picaresque de _Don Quichotte_ et de _Jacques le Fataliste_ : ce qui doit advenir advient. On est dans _Le Chancellor_ et son présent absolu, _Un Capitaine de Quinze Ans_, et surtout dans la fin de la première partie de _Vingt-Mille-Lieues sous les Mers_, les explorations abyssales et la visite du cimetière marin.  
On peut lire _Un Paquebot Magnifique_ comme on lisait autrefois le récit du voyage du _Rhin_ de Victor Hugo, vaste déroulement d’où surgissent les récifs des burgs mystérieux à travers des paysages crépusculaires, mille richesses historiques et des géographies à explorer, des contes qui se déploient en pop-up comme le Diable de _Pécopin_, ce qui correspond chez Roussiez à _L’Histoire de Majnûn_ ou _À propos de l’oiseau lapidormeur_.  
Chez Roussiez aussi mille savoirs de l’époque sont brassés, et l’infini écrase de temps à autre le sillage de l’aimable errance comme chez Victor Hugo la plaine de Soissons s’ouvre sur César, Clovis et Napoléon parmi des ombres qui passent ; à la faveur d’une inscription sur une pierre Jules César se transforme en Jésus-Christ.  
Dans les deux ouvrages, moins d’exotisme que de dépaysement temporel, et le passage d’un itinéraire didactique à un véritable enjeu politique, je veux dire _un enjeu guerrier_.  
J’ai la chance d’habiter dans un village dont le Saint arbore à la fois plume et épée, et suis bien heureux de constater que l’incision de Roussiez n’a rien à voir avec cette manie des ouvrages depuis quelques années dont le pseudo-héros est un écrivain ou dont le secret est un livre (éventuellement possédé par un diable aimable et mondain, un diable “Saint-Louis en l’Île”).

_lire la suite…_

_(La Rumeur Libre fait partie de ces éditeur qui ne confondent pas la pensée du corps et la physiologie et préférent l’épos Roussiezien qui comporte un Z. à la socialité bêtasse. Décidément encore une petite maison sympathique de pauvres.)_

Et également : [Éthique de la Description](https://onuma-nemon.net/posts/ethique-de-la-description.html) et [Troupeaux et Voyage](https://onuma-nemon.net/posts/troupeaux-et-voyage.html )