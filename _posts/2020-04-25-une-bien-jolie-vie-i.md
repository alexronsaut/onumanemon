---
date: 2020-04-25 11:58
title: Une bien jolie vie. I
sous_titre: Texte de Joël Roussiez
date_document: 2O19
fichiers:
  - "/une-bien-jolie-vie-I.pdf"
tags:
  - texte
  - DAO
  - document

---

**Une bien jolie vie I**

Quatre trèfles dans la balance pour faire bon poids dans la pesée des radis que tu as achetés ; le marchand t’a aimée et veut ton bonheur ; c’est lui qui a choisi pour toi que faire de ta vie...

_(lire la suite…)_
