---
title: "Buenos-Ayres"
date: 2011-05-22 12:25
fichiers: 
  - Buenos-Ayres.pdf
sous_titre: "Ligne de l’Oncle Aveugle" 
date_document: "17 février 1982" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

Comme le réel jaillit sonnant et trébuchant d’une théorie qui est bonne, à présent l’Oncle Émilio est aveugle, sa grande œuvre ratée, malgré la drogue. Il ne peut demander à qui que ce soit d’autre de fouiller ni de chosir parmi les tonnes de manuscrits. C’eut été un travail impossible, même avec une quinzaine de collaborateurs.  
Il aurait dû répartir les doses les plus fortes juste avant l’inscription ; la plupart du temps il s’était trouvé plutôt abattu qu’illuminé.  
À présent il venait de s’allonger sur le divan de la terrasse, et le banc froid de poissons de l’air qui survenait aurait dû - c’est certainement cela - s’accomoder de la bombe précédente et tiède.  
Avril serait bientôt là, bonne saison sans métaphore. Allers-retours de la terrasse au Parc, soupçonner les auras bourdonnantes des dernières lampes du kiosque, attentif aux lambeaux de musique. C’était l’heure où la marâtre devenait pommier devant la mare, avec cette bonté, cet éclat de la disparition propre aux choses les plus aventureuses.  
“C’est à présent l’informel, se dit l’Oncle, ces siècles passés sous la peau de l’hiver et qui luisent. Z ! Riez ! Je fournis le champagne et toute la joie sans discontinuer, sans psychologie.”  
Les piaillements n’avaient de cesse et les vibrements des mouches, ces corsetières au-dessous de l’ensommeillement… Faisant s’envoler son cerveau comme un ballon dans le jour gris de la Cordilière.  
Il y avait toujours eu ce brouillage de la vue par le blanc d’œuf du monologue intérieur, mais aujourd’hui il avait beau tendre assidûment les doigts dans cette percée de l’air chaud à la recherche des joues rougies des filles comme des pommes chauffées au four, il ne lui restait même pas le cernement figural de l’objet au fond des méandres de la matière grise : son souvenir s’était également dissous tandis qu’il entendait chanter des travailleurs qui revenaient :  
“Gars du Nord,  
Pies et Porcs,  
Poupées de Nuremberg,  
Église !”  
 _lire la suite…_