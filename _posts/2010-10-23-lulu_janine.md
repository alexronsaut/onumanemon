---
title: "Lulu & Janine"
date: 2010-10-23 19:01
fichiers: 
  - LuluJanine.jpg
sous_titre: "Les Maigres Tendres" 
date_document: "1946" 
tags:
  - document
  - HSOR
  - photographie
---

Lulu est ici à droite, quelques mois avant sa mort, avec sa copine Janine à gauche. On se reportera à l'historique des Tribus, pour les détails. 

_NDLR_