---
title: "Usines et Défaillances"
date: 2008-12-26 19:31
fichiers: 
  - Usines__Defaillance._Said._.pdf
sous_titre: "Les Escholiers Primaires. Ligne de Nicolaï. Été." 
date_document: "1978" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

Enfant, j’ai débarqué à l’île Seguin. Ensuite, devenu karatéka, lorsque j’ai de nouveau travaillé en usine, c’était uniquement pour notre communauté et pour préparer l’installation de mon “Camp du Gers” avec Maître O, en attendant de pouvoir rejoindre les autres en Afrique et aux Amériques. J’ai alors énormément volé, saboté et détruit de matériel ; cela faisait partie de nos consignes. Je glandais au lieu de travailler, provoquant les abrutis, les frappant entre les rangées de pièces, semant les objets n’importe où, brisant les plus précieux. D’autres du groupe sont venus travailler avec moi occasionnellement : Le Gitan, Pipo, Walter, Nany El Niño, et même une fois Osiris, mais le jour où il est venu, il neigeait et l’Usine a fermé, ça tombait mal !