---
title: "Rimbaud & Daudet"
date: 2010-08-01 15:08
fichiers: 
  - Rimb.Daudet.pdf
sous_titre: "Hommage à Cabaner !" 
date_document: "1989 et 1992" 
tags:
  - document
  - HSOR
  - texte
---

\[……………\]  
“**Nicolaï :** “Nous étions en attendant Nicolas en train d’écouter la causerie de Sévèrimus à propos du Poor Arthur et de ses emprunts volontaires à Daudet, dans un café, à l’angle de La Samaritaine et du Pont-Neuf adorés, dans ces temps où l’on redoute la douleur de l’orage sur les bois œuvrés (ceux-là mêmes où le premier imbécile gueule dans la forêt, alors qu’une immense vapeur s’élève et arrose la surface des Temps : “Alors, Éole, gros sac !” et puis aussitôt ensuite avachi dans le fondement de sa voiture: “Tiens ! pour le vent ! Tiens ! pour le soleil !”) autant qu’on déverse ensuite le petit jour du rabbin de l’Ancien Testament caché derrière la porte des chiottes !  
(_Le “Café de l’Univers” et devenu l’Univers Rimbaud. Chaque phrase de lui est prise sur un méridien, dont la possibilité infinie des sources jaillit. Barque devant le Moulin, rivalité du Collège et de l’Institut, mouche latine bombinante et sacrée. Café plein d’Écho. Saint-Sépulcre. On y boit du moka d’Éthiopie ; noir comme le Diable et velouté comme un Ange ; on ignore les mélanges crémeux comme la robe des capucins._)  
Nycéphore : “Maintenir l’énigme contenue, concentrée, terrible, du dehors-dedans, voilà le grand mérite de La Samaritaine ! Du mélange de l’éclairage artificiel et de la lumière naturelle, du néon coulant ses sirops sous la pluie, de la mousson catastrophique des stigmates illuminés, que sont ces traits d’éclair à deux ou trois fractures successives.  
Prendre et induire la foudre, la canaliser, capter cette puissance de l’éclair, de l’angoisse du _petit jour_ adolescent, l’incertitude du cauchemar et de la veille ; réussir dans le travail colossal d’une formidable forme, un creuset géant, d’un moule divin, à réunir tout ça !  
Et à présent, le jour baisse atrocement dans toute la ville où tombe un brouillard noir, une pluie de suie fine ; en dix minutes, la nuit est là, charbonneuse des débuts de l’Industrie, _jamais profondément dite_, sinon par Rimbaud. Puis vient le déluge fracassant équinoxial, incoercible !  
Un Chanteur (la table à côté) : «— Je vais vers la Lessive ; Elle a pas de rien jusqu’aux genoux !»  
\*  
«— Vous vouliez qu’on sorte d’ici, dit Sévèrimus _l’Homme-Pie_ à Nicolas à présent installé à sa table du “Café de l’Univers”, face au Pont des Mariages Secrets du Square du Vert Galant ! Regardez l’horrible crachin froid : on se croirait tout à coup en Hiver !  
— L’Univers a tourné !  
— D’ici qu’on traverse pour que vous achetiez vos pigments et du noir vignette, cela vous fera comme un trou gelé sur le crâne, et vous serez ensuite bon pour une horrible migraine jusqu’au soir, ou, comme dit Lawrence, “un craquèlement de tout l’être sous la tension des vagues douloureuses roulant de la colonne au cerveau jusqu’au moment où l’aube blanchit les fentes du hangar.”  
Il en profita pour me déballer sa théorie des correspondances intertextuelles (il avait étudié chez Kristeva, à Jussieu : vent froid et angines phonématiques !) et des emprunts éventuels de Rimbaud chez Daudet, dans _Le Petit Chose_, pour les _Intimités d’un Séminariste_.