---
title: "Hypostase"
date: 2009-08-17 09:51
fichiers: 
  - Hypostase.pdf
sous_titre: "Livre Poétique de Nycéphore. 1964-1984. Futur Antérieur. Poème n°15" 
date_document: "Décembre 1964" 
tags:
  - document
  - OGR
  - texte
---

_**15. Hypostase**_  
  
Dans le cercueil d’Hypostasie  
Des chérubins brillent de hargnes ;  
Lambeau, je bruis de l’Allemagne  
Et dépasse des hérésies.  
  
D’où portes-tu cette oriflamme  
Issue d’un mort qui croirait digne  
(_On guette au coude, on sort les lames !_)  
D’laisser l’Empire au fils d’un cygne ?  
  
Nous livrons ici la deuxième page d’un long poème de l’Auteur, “Hypostase”, auquel fait pendant “Lueur” de Nicolaï. 

_I. Revay_