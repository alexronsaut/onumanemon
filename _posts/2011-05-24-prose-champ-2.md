---
title: Pr’Ose !
date: 2011-05-24 13:51
sous_titre: Version Définitive
date_document: 1969. Publié 24 Mai 2011.
tags:
    - document
    - HSOR
    - texte
fichiers:
    - "/prose-champ-1-et-2.pdf"
    - "/critique-PrOse-CCP25.2013.couv.pdf"
    - "/critique-PrOse-CCP25.Texte.pdf"
    - "/dissonances-pr-ose.pdf"
---

Pr’Ose ! A été écrit essentiellement en 1969 dans un rêve proche de _La Légende des Siècles_, remixé par l’influence de la radio où je travaillais alors, et par l’influence de Cendrars, Neruda, de la Beat Generation et de quelques autres. On y trouve une grande partie des Voix de la Cosmologie, qui se succèdent en fonction du pressent (l’urgence du temps qui passe et qui presse), mais aussi à dire l’éternité des Saisons.  
Ces Voix prennent en écharpe l’Histoire des Peuples et des Arts ce qui permet littéralement de les déporter, d’ouvrir l’anecdote en la brisant, d’élargir au plus vaste le propos. Ce sont aussi des _hypomnemata_.  
Par exemple Don Qui débouche sur le siècle d’or espagnol, Ritam dans l’Inde, etc. Parfois au contraire ces Voix embrayent par une ligne brisée sur un monde géographique ou historique qui leur correspond de façon moins évidente.  
Malgré son titre, la scansion de ces lambeaux est poétique (ce que signalent les capitales dans les versets).  
Pr’Ose ! fait partie du continent HSOR, qui dans la logique alchimique de la Cosmologie n’est pas du plomb, mais du zinc, le zinc des comptoirs et des anecdotes, celui de la gravure et celui des toits de Paris que contemple Jean à son arrivée dans les années soixante.  
On trouvera ici les deux Champs agrémentés de quelques inserts plus récents au moment de leur reprise.  
Ce Champ-là c’est le champ agricole, celui de la vue et de la peinture en Chine et surtout le champ énergétique qui fut le propre de la Cosmologie tout le temps de son écriture : un accroissement par tous les endroits à la fois.

L’ouvrage est disponible dans sa version intégrale et définitive aux Éditions de l’URDLA, à Villeurbanne.
