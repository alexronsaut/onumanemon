---
title: "Jules-Arthur"
date: 2015-04-07 22:18
fichiers: 
  - jules-.arthur.pdf
sous_titre: "Tribu des Gras" 
date_document: "1984 et Après" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

_**Je ne sais rien de Jules-Arthur**_

Je ne sais rien de Jules-Arthur de la Crapaudine ; je l’ai toujours rencontré en coup de vent. J’ai cette photo dans le désert avec les casques, dans un groupe, et c’est à peu près tout.

Il tenait ce surnom du supplice subi plusieurs fois, les membres attachés derrière le dos, et pendu au soleil.

Sa mère Rosa n’était pas pauvre, mais il plaignait souvent une pauvre tante : Sabine l’amie de Jo, sur l’Ourcq. Il en parlait à mi-voix, tête baissée.

“Sabine avait attendu Jo tout le temps sur le fossé, près de la voiture, ne sachant l’ouvrir, plus de deux heures en plein soleil. Quand je suis arrivé elle m’a demandé en toute hâte un morceau de pain, au bord du malaise.

Il pleuvait.

Elle était trempée.

Elle n’avait pas mangé depuis cinq heures du matin.



_(lire la suite…)_