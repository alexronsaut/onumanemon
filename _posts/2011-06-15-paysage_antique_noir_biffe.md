---
title: "Paysage Antique Noir Biffé"
date: 2011-06-15 22:13
fichiers: 
  - Paysage_Antique_Noir.Biffe_.jpg
sous_titre: "Encre de Chine sur Bristol. Format raisin" 
date_document: "1984" 
tags:
  - document
  - HSOR
  - dessin et gravure
---

Toute une série de paysages biffés datent de cette année-là, dont une suite prémonitoire de montagnes noires et de corbeaux. 

_NDLR_