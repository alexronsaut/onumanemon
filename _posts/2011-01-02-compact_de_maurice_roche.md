---
title: "Compact de Maurice Roche"
date: 2011-01-02 21:30
fichiers: 
  - Compact.Couleurs.pdf
sous_titre: "Édition en couleurs de 1997" 
date_document: "25 Janvier 1995" 
tags:
  - document
  - DAO
  - texte
---

Il s'agit ici de la première maquette des 85 premières pages composées par la maison d’édition _La Petite École_, qui a co-produit et co-édité avec _Tristram_ la première version historique de Compact en couleurs tel que Maurice Roche avait souhaité pouvoir le réaliser depuis 1966. La conception graphique était de Anne Drucy.