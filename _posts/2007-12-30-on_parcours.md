---
title: "ON : Parcours"
date: 2007-12-30 18:55
fichiers: 
  - Parcours_Bio.pdf
date_document: "Décembre 2007" 
tags:
  - document
  - DAO
  - texte
---

Sphère Biographique. La Fin.  
O. N. est né en **1948**. Origines : Cuba, Andalousie, Tziganes.  
**1954** : En même temps que l’écriture, la Cosmologie se met en place, de façon secrète.  
À partir de 1966 On fait des études de photographie, marqueterie, reliure, et se prépare à devenir bibliothécaire avant de se diriger vers les arts plastiques.  
De 1966 à 1968 On est producteur d’émissions radiophoniques et crée le premier café-théâtre de province où On expérimente ses premiers essais dramatiques (sous l’influence notamment de Jean Vauthier, qu’On rencontre alors), et où ont lieu des représentations d’Arrabal, Gripari, Obaldia, et des montages poétiques autour de la Beat Generation, le Dadaïsme, Cendrars, etc. On est assistant-décorateur d’Andréou sur le Don Quichotte de Paisiello et participe à Sigma (Bordeaux), mais également à des travaux dans le cadre du tout nouveau Service de la Recherche situé alors Centre Pierre Bourdan. En dehors de ça happenings. Travaux de décoration théâtrale à l’étranger. La Cosmologie se cristallise sous forme d’un délire mystique et se constitue en Cinq Continents, mais reste une élaboration secrète.