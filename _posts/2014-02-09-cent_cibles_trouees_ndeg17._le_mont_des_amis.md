---
title: "Cent Cibles Trouées : N°17. Le Mont des Amis"
date: 2014-02-09 21:53
fichiers: 
  - 17.Le_Mont_des_AmisJ.J..jpg
sous_titre: "Acrylique blanc sur Cible 17 x 17cm pour pistolet 10m" 
date_document: "Après 1984" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - peinture
---

À J. J.