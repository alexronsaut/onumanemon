---
title: "L’oie qui découvrit un écrivain"
date: 2008-03-07 23:05
fichiers: 
  - Loie_qui_decouvrit_la_poesie.pdf
sous_titre: "Pirate Farfali, tome II, partie 2, chapitre 1, page 7 - Joël Roussiez"
tags:
  - document
  - DAO
  - texte
---

Publication : [Contre-Feux](https://web.archive.org/web/20081216013335/http://www.lekti-ecriture.com/contrefeux/) (archives)

