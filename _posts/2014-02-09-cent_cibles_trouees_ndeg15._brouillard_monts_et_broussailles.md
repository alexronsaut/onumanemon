---
title: "Cent Cibles Trouées : N°15. Brouillard, Monts et Broussailles"
date: 2014-02-09 21:47
fichiers: 
  - 15.Brouillard_Monts_et_BroussaillesSylvie.jpg
sous_titre: "Acrylique blanc sur Cible 17 x 17cm pour pistolet 10m" 
date_document: "Après 1984" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - peinture
---

À Sylvie