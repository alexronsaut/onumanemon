---
title: "Décompte de la Faim"
date: 2013-07-04 23:16
fichiers: 
  - Decompte_de_la_Faim.pdf
sous_titre: "Contes, récits, nouvelles" 
date_document: "Avant 1980" 
tags:
  - document
  - OGR
  - texte
---

Dans le jardin du château de Nuada, fumée s’élevant du sol et intelligence du bois (dans laquelle excédait Art), à l’abord d’un tas de vieilles tuiles et de rames brisées couvertes d’insectes morts, An Scamall s’est avancé, portant dans son cartable en peau de loup un pain fourré aux figues et des galettes de maïs parmi des carrés de porc cuit à la bière avec des noisettes. Et Sciathán Spota avec lui.  
_lire la suite…_  
  
_Ce texte figure dans le recueil de Contes de Nycéphore, du Continent OGR. Il est tout particulièrement crypté selon la logique buissonnante de la mythologie celtique.  
Les deux personnages Nuage de Son et Tache d’Aile, dont le nom est transcrit ici en gaëlique sont dans les États du Monde les enfants de René Mac Carthy, le petit-fils d’un des Quatre Grands Ancêtres (ou Chevaliers de l’Apocalypse), à savoir Auguste Mac Carthy.  
On les trouve également dans une des pièces du Théâtre Lycéen._  
_NDLR_