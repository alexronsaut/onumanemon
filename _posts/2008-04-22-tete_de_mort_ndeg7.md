---
title: "Tête de Mort n°7"
date: 2008-04-22 21:57
fichiers: 
  - MortChine7.jpg
sous_titre: "Ligne du Chaos. 3." 
date_document: "1982" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - dessin et gravure
  - edition
---

Encre de Chine sur Arches 20cm x 25 cm. Figure dans “États du Monde”, version définitive.

Publication : [publie.net](http://www.publie.net/tnc/spip.php?article85)