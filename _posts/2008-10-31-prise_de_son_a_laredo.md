---
title: "Prise de Son à Laredo"
date: 2008-10-31 20:41
fichiers: 
  - PriseDeSon_Laredo.AubeNany.pdf
sous_titre: "Les Adolescents. Aube & Nany. Été" 
date_document: "1967" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

_Laredo. Samedi 5 août 7h1/2_.  


Cher passant du Styx,  
Je voulais t’écrire allongée sur le sable de “notre” coin ; promesse que je ne tiendrai pas puisque je suis sur mon lit : beaucoup trop de vent dehors !  
Je n’ai pas osé tourner la tête pour te voir partir ; ainsi tout à l’heure tu m’accompagneras sans le savoir dans le vieux village aux rues de cendres grises. Je voudrais tant te donner la main et caresser tes cheveux !  
Après t’avoir quitté j’ai couru acheter des timbres et faire provision d’enveloppes violettes (pour toi) ; mais il n’y a nulle part possibilité de trouver des bandes magnétiques vierges, comme tu me l’as demandé ; le mieux sera que tu en récupères encore à la Radio. Je suis d’autant plus désolée que nous n’ayons pu enregistrer le vent sur la plage, ton départ, notre dernière promenade en ville… que Jacqueline était bien là, avec son magnéto, à nous attendre au “Las Vegas” depuis une demi-heure où nous n’avions pas su la voir.  
Nous avons rencontré aussi Loco, l’ancien videur à la “Rana Loca”, l’été, pendant ses vacances. C’est un gars du C. R. E. P. S., tu sais, un Anarchiste, un copain de Jésus et de toute sa bande : Minet, Gérard, Bernard… Ces temps-ci, il adore faire des blagues au téléphone ; il nous a dit qu’il pouvait t’aider pour infiltrer des lignes dans des immeubles, avec un magnéto. En partant il nous a donné une carte de sa boîte.