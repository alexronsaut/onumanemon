---
date: 2021-10-10 17:17
title: "Ztaphysagria de Nicolaï (extrait)"
sous_titre: "Mettray n°14. Été 2021"
date_document: "1969-1982"
fichiers:
- "/mettray-2021-extrait-de-ztaphysagria.jpg"
- "/mettray-2021-recto.jpg"
- "/mettray-2021-verso.jpg"
tags:
- texte
- document
- edition

---

Publication: [Mettray n°14](https://mettray.com/revue/mettray-s02-n14)