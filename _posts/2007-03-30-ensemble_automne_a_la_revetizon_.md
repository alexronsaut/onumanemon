---
title: "Ensemble Automne à la Revetizon !"
date: 2007-03-30 09:57
fichiers: 
  - 100.pdf
sous_titre: "Livre Poétique de Nycéphore. 1968-1984. Poème n°4" 
date_document: "Octobre 1968" 
tags:
  - document
  - OGR
  - texte
---

Livre Poétique de Nycéphore  
  
Les poèmes des Livres Poétiques (1964-1968 et 1968-1984) sont indiqués ici par leur numéro dans chaque volume, chaque poème correspondant au poème qui porte le même numéro chez le frère “en face”.  
Isabelle Revay.