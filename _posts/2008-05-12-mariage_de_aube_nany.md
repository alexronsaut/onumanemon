---
title: "Mariage de Aube & Nany"
date: 2008-05-12 18:24
fichiers: 
  - Aube__Nany._Mariage.pdf
sous_titre: "Les Adolescents. Ligne Aube & Nany. Été" 
date_document: "1992" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition 
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

On voit ici (mais qui sait lire aujourd’hui ?) la multiplicité des écritures invitées (comme les invités au mariage) sur des registres tous différents, portées par tout un tas de personnages mythologiques traditionnels ou bien qui font partie des Tribus de la Cosmologie et qu’on retrouve ailleurs : Monique, de la Bande de la Folie-Méricourt (autour de laquelle entre autres se centre le recueil de Nouvelles de 1972 intitulé “Tuberculose du Roman”) ; Lydou, la compagne de Jean Sales le cinéaste, amie d’enfance de Aube qui habite le chateau de Terraube ; Maître Ho, un Chinois avec des disciples japonais qui organise des stages dans le Gers dont un des plus fameux participants est Saïd Hadjl… tout un bariolage d’étangetés qui constitue les cortèges. Et, parmi les allusions obscènes de rigueur dans ce genre de cérémonie : “…le vent fait claquer la chemise (“Remballe ça ; on ne s’en servira pas aujourd’hui” - raccourci - .)” pour un raccourci de détumescence que n’importe qui trimballe parmi les éméchés que toute fête excite, on trouve l’une des amorces des États du Monde en tant que tels : sensations, visions fugaces que personne ne saurait porter, comme on se demandait à l’invention de “La Paluche” de Beauviala : “quel corps peut porter cet   
œil ?”  
On se demande surtout qui sera capable de produire la théorie de cette énormité où curieusement on trouve moins de citations d’auteurs disparus, malgré la quantité des références et des allusions, que de courants du présent, courants qui sont dans l’océan des auteurs vivants, toutes nations confondues, les remous opérationnels d’une écriture en train de se faire et que l’auteur nomme pressent, à la fois ce qui urge et ce qui s’inscrit aujourd’hui, mais que masquent bien sûr les huiles de la surface.  
**Lucinda Véron-Féret**  
  
_**Mariage de Aube & Nany**_  
(_Lydou est là, l’amie de Aube, venue pour son mariage_.)  
**Lydou** : “Bien-être global au lever : jonquilles, coucous… se penchent !  
Puis faim d’après-midi : brumes nombreuses au bas des arbres,  
Toujours dansants dans les prairies.  
Éden latéral : aucune explication ;  
Foulard de soie de la surprise,  
Selon si Déméter ramène Perséphone :  
Pluie, ou faiblesse, ou changement de tenue !  
Plus jamais le facteur Antonin Triptolème,  
Dans la lumière tiède du petit matin  
(Entre le Printemps & l’Été), avec toutes ses sacoches de blé :  
“_Antonin, canne de nain_ !”, chantaient les gosses.”  
  
Nous survenons au Château de Terraube par la route trop blanche, presque peinte, comme certaines façades sur la route de Saint-Puy, sauf l’Ecole. Les six mille amis approchent du village pour le mariage d’Aube et de Daniel, quittant les tentes, les cavernes, les buissons, les rochers, les tours et les citernes. Il y a ceux qui viennent d’Auch, de Fleurance et Condom, les élèves de maître Ho venus de Tokyo et ceux de Cádiz, d’Ampuero, de Laredo, de Santoña.  
**Suzuki** : « Allez ! On marche, on mange. »  
De grands chiens et des petits chiens les accompagnent, des petits chiens perdus et de grands chiens sauvés dans les premiers coucous d’entre les touffes et par les cîmes.  
(**Aube** : _“Je me suis levée ce matin à l’aube, et suis sortie me caresser à l’eau des calices de pêchers. Visage de bonheur du bébé à travers son verre, en_   
_compagnie de tante et moi, quand nous avons petit-déjeuné, au Moulin. Puis je suis partie accompagner les chasseurs de la métairie et rentrée seule à pieds dans l’air frais où flottent les glycines._ _Une violente envie de peindre m’a saisie mais je ne voulais pas manquer ton arrivée; et puis ça ne convenait pas pour le jour du mariage.”_)  
**Hasegawa** : « Tu y es allé, là-dessous ? »  
**Suzuki** : « Oui. Les corps étaient tout pleins d’eux-mêmes, les bras gonflés dans les chemises, membres charnus et ronds. Longtemps, je suis resté au bas du mur de la construction surchauffée (“il avait le feu au plafond !”) jamais poursuivie, au pied des piscines, somnolent, hirsute et près des ivrognes au sac ouvert, humide. La roue de la loterie du village tournait dans un grésillement d’attente. »  
**Yukio** : « Mon souffle devient lourd. Je n’atteindrai pas le Château. Pardonnez-moi de n’avoir pas rempli mes devoirs de civilité. Je vais en souriant à la rencontre de la mort. »  
(Cri terrible de la lance entendu par Hakuin, et de Breton surpris dans les chiottes). Il récite cette “chanson d’Aube” avant de mourir :  


“3 octobre 33 hommes 33 maîtres.  
Baguette qui fait vibrer le gong.  
Ho !  
Épée précieuse du roi-diamant,  
Lion au poil d’or tapi sur le sol,  
Perche à explorer munie d’herbes  
À son extrémité qui fait ombre.  
Ho !  
Shikan !  
Ta !  
Za !”