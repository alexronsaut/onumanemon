---
date: 2020-11-19 18:34
title: Aux malheurs éternels
sous_titre: Joël Roussiez
date_document: "2014"
fichiers: 
  - "/aux-malheurs-eternels.pdf"
tags:
  - texte
  - DAO
  - document

---

_Aux malheurs éternels_ a été publié dans Temps divers ou le jardin varié des jours, chez Héros Limite, Genève 2014.