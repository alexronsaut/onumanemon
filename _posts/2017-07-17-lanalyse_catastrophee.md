---
title: "L'Analyse Catastrophée"
date: 2017-07-17 18:25
tags:
  - billet
---

C’est en 1975. Cette femme à la figure ravagée, H. C., me parle de son travail actuel avec Michael Londsdale et de son analyse : elle représente pour moi _l’analyse catastrophée_, l’analyse qui fournit pratiquement le révolver du suicide, et qui représente à peu près toute l’œuvre de Marguerite Duras (laquelle a remplacé un canon par un autre).  
Ce type d’analyse (éminemment sadique), butant sur une immense catastrophe sans espoir, s’est répandu comme de la poudre noire dans le milieu intellectuel des années 70, qui se sentait coupable de tout, depuis le culbutage de la bonne jusqu’à l’inceste avec la mère dans la salle de bains ou le fait d’avoir taillé des pipes à Mao. Et pourtant Leclaire avait déjà pris son heureux virage Zen, Lacan s’en sortait très bien en matière de liquide (mieux que Duras), et avec les impôts ; Kristeva avait collé de nouvelles paillettes romanesques autour d’une théorie défraîchie ; même si toutes ces têtes qui branlaient dans le manche avaient été envoyées paître depuis longtemps avec les moutons de la logique par la grande vague joyeuse du Deleuzisme triomphant.  
Mais voilà : il lui fallait aussi des pauvres à l’analyse, du misérabilisme, des esclaves somatiques qui ne s’imaginent pas qu’ils allaient prendre à leur tour le fauteuil, des gens qui fassent des ménages pour tout savoir de leur désespoir avant de se jeter en Seine (en connaissance de cause !). C’est ça _l’ob-Scène des Pauvres_ !  
C’est ça qui fait la différence : un pauvre qui qui n’a pas exactement réalisé à quel point il est une merde (et ceci sans échappatoire : ni Colonies, ni Castelvin, ni changement de Classe), ne peut fournir un suicide cohérent. Claude Guillon fournit son _Suicide, mode d’emploi_ également en hiver 1975.  
  
Nous organisons alors (avec des ex d’une de ces nombreuses revues qui commencent par T. uniquement par suivisme), une exposition franco-russe (comme le dessert). Mais on pense plutôt à un _désert_ : après Staline et ses fans, plus rien que du flanc. Ou du flanco-prusse, couleur lourde comme l’eau du même _non_. _Pietro Bodhisva_