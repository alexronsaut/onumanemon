---
title: "Mont Graffiti à Tissu Noir"
date: 2010-01-02 17:52
fichiers: 
  - Mont-GraffitiA_TissuNoir.14.CD8_.jpg
sous_titre: "Dessin crayon graphite, mine de plomb et collage tissu" 
date_document: "1983" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - dessin et gravure
---

Il existait trois variantes de ce dessin, aujourd’hui perdues.  
_I. Revay_