---
title: La lettre à Tesson
date: 2018-11-03 00:00:00 +0000
sous_titre: Marges des États du Monde
date_document: 1971
fichiers:
- "/lettre-à-tesson-14-18.pdf"
tags:
- texte
- Cosmologie Onuma Nemon
- document

---
LA LETTRE À TESSON

(14-18)

« Voilà ce qu’Yvonne m’écrivait, au milieu de mes pérégrinations, ce 20 Août 15 : “En tout cas, soldat Tesson, tout ce que je vous conseille (au-delà de nos débats faussement mystiques), c’est “l’exercice au plus tôt !”

_(lire la suite…)_