---
title: "De L’Homme Qui Rit à Studebacker"
date: 2008-01-12 22:17
fichiers: 
  - Studebacker.pdf
sous_titre: "Carte n°5. Annexe 1" 
date_document: "1997" 
tags:
  - document
  - LOGRES
  - texte
---

Ce texte explicite en partie la tension au travail dans LOGRES.  
  
Logres, pays des Ogres et territoire d’Arthur chanté par Calogrenant et défendu par Lancelot, (première ébauche de la Cosmologie actuelle), ainsi que OGR (qui fut le premier Continent émergé issu de la division de LOGRES), présentent tous les avantages de la contrainte dans un autre corps. C’est comme “L’homme qui rit” : un corps qui rit d’un rire imposé de l’extérieur alors que l’intérieur n’est qu’un conglomérat de morceaux tragiques. Là encore, toute l’importance du Moyen-Age et du désordre Celte, l’incapacité à faire cuirasse et encore plus armée.  
Il y a un élément, une caractéristique, qui se conserve d’un continent à l’autre, de OGR à OR, du moins, et qui disparaît sans doute dans O, c’est la fulgurance, le trait foudroyant qui dans son zigzag procède à des associations apparemment saugrenues.  
Ce trait de caractère bien sûr détermine toute l’œuvre graphique.