---
date: 2021-05-09 18:00:00 +0200
title: Denis Roche n'est pas mort et Maurice est toujours là !
sous_titre: 
date_document: "2021"
fichiers: []
tags:
- texte
- billet

---
Denis Roche n’est pas mort, même si la plupart du temps il vit dans une pièce plongée
dans une semi-obscurité ; il ne s’agit pas vraiment d’une salle à manger, mais d’une simple
pièce carrée entourée de dressoirs avec de grands compotiers où murissent des fruits.

Cette semi-obscurité me fait penser au mythe entretenu à propos de James Dean dans
plusieurs biographies de l’acteur, et dont Denis m’avait parlé : ses fans prétendaient qu’il était
toujours vivant après son accident automobile mais qu’il avait à peu près perdu la raison et
qu’on l’avait dissimulé dans la maison d’un petit village où on pouvait l’apercevoir avec des
jumelles à travers les lames des volets, assis et tremblant de tous ses membres.

Rien de la sorte avec Denis dans cette pièce de _recueillement_ où il reste assis souvent à
méditer. D’autres fois il dresse sa haute stature dans l’ombre, avec sa chevelure bouclée et il me
raconte des anecdotes toujours drôles : il n’a rien perdu de son humour. Par exemple il prend
un cure-dent et fait semblant de perforer des saucissons qui pendraient du plafond pour les
goûter, comme dans cette séquence qu’il aime tant de _La Stratégie de l’araignée_. Quand il me
parlait de ce film autrefois il me disait qu’à ce moment-là toute la salle de cinéma “sentait le
saucisson mûr”.

C’est Françoise Peyrot qui veille toujours aussi amoureusement sur lui ; de là cette
sorte de certitude bienheureuse qu’il a acquise. Peut-être plus calme qu’auparavant. Il ne fume
plus ces Craven rouge dont Bernard Plossu aussi a gardé le souvenir des matins dans la petite
pièce sous les toits rue Jacob. (On a perdu les parfums des tabacs : l’Amsterdamer, le Gris et le
Caporal Supérieur des ancêtres, le Virginia des étudiants ou le Semois corsé des Ardennes
d’Arthur. Plaisir de fumer un Partagas dans un fumoir, à Bruges.)

Il a marqué d’un signe de tête son assentiment pour ce que nous étions en train de
faire ; enfin, quand je dis _nous_, il s’agit plutôt de Didier. Il trouve un vrai mérite à la
singularité de son entreprise.

Assis sur une belle chaise cannée, il m’a montré une liasse de textes inédits.

« Tu les reconnais, m’a-t-il dit ?  
— Oui. »

En rêve on reconnaît _infailliblement_ un matériau énigmatique qui serait pourtant
impossible à définir.

À présent il poursuit l’écriture de ce recueil ; il a tout le temps qu’il faut pour mettre au
point cet embranchement oublié, car c’est une autre orientation préalable à la déprédation qui
l’a conduit au _Mécrit_, qui date, me semble-t-il dans le rêve, de _Miss Élanize_. Jusque-là il
travaillait avec une sorte de culte apparent de la paresse, qui n’était que la préparation de
l’emportement, la fulgurance soudaine. À présent qu’il dispose de tout son temps, forcément
l’écriture est d’un autre ordre.

Il fait toujours l’éloge de Bernard Plossu comme il le faisait dans son bureau dans les
années 70 en me parlant de ce type extraordinaire dont j’ignorais absolument tout alors et qui
réussissait à vivre de sa double passion : le voyage et la photographie. Et il a toujours un très
fin goût culinaire.

L’appartement où il se trouve communique avec celui où demeure Maurice Roche,
tellement bien que j’ai commis une erreur une fois et que j’ai fait habiter Maurice rue
Henri Barbusse alors qu’il demeure dans un tout autre quartier. Leurs immeubles font partie
des 500 logements construits par Haussmann qui communiquent secrètement entre eux. Jules
Romains a parlé du mystère de ces passages secrets entre les appartements parisiens, et plus
tard Aragon a repris cela.

En vérité Maurice n’habite pas un appartement mais une chambre de bonne au dernier
étage d’un immeuble ; ces combles appartiennent à une collectionneuse d’amants dont
Maurice fait partie ; il nous avait parlé de cela lorsque nous projetions d’écrire ses mémoires ;
ils sont tous rangés à l’étage dans une chambre semblable avec un numéro et elle va les cueillir
à tour de rôle ; Maurice a le numéro 13 : pas de bol !

O. N.