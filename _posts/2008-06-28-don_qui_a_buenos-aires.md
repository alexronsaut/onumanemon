---
title: "Don Qui à Buenos-Aires"
date: 2008-06-28 13:38
fichiers: 
  - DonQui.B.Aires_.pdf
sous_titre: "Les Grands Ancêtres. Ligne de Don Qui. Été" 
date_document: "1989" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

 _**Nouvelles du Tío de Buenos Aires**_  
Buenos Aires s’est appelée dans l’Antiquité _Buenos Eros_, car elle était la capitale d’un Eros primordial antérieur à toute division des sexes, Chaos et Genèse.  
Domingo, frère d’Eliseo, réfugié à Buenos Aires (_on vous dira l’Histoire de la Ville_), est un Fou du Cinéma. C’est le mari exilé de la Tía, restée habiter rue du Port parce que c’est de là qu’il est parti et elle attend qu’il revienne par-là. Il était parti pour faire fortune ; il a fait fortune, il est resté. Il veut saisir la Vie entre ses mains. C’est un collectionneur, et en particulier des Inventions-Ancêtres du Cinéma : zootrope, praxinoscope, etc. Il n’a pas eu d’enfant et vit la plupart du temps dans l’Obscurité, surtout depuis qu’il est devenu aveugle, même s’il distingue un fond gris-doré, “là votre main, et même un peu de votre visage, de près…”  
C’est l’Oncle des Dimanches Après-Midi. Il s’est dit qu’en se gardant, lui l’apôtre du Mouvement Généralisé, des influences de la Tribu, il conserverait la nécessité impérieuse de la Recherche des Dimanches Après-Midi, cette exigence dont participe également la Radiophonie d’une façon non négligeable. Non pas l’exigence dans son énoncé, mais dans sa matière, dans sa sensualité (idée qui imprègnera le projet “Aube-Matière” de Daniel) : parfums, extase, vibrations, type d’oppression, inclinaisons de la lumière, coïncidences d’Ouranos et de la Terre déployés.