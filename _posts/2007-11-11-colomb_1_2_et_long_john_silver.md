---
title: "Colomb 1 & 2 et Long John Silver"
date: 2007-11-11 11:21
fichiers: 
  - 312.pdf
sous_titre: "Les Grands Ancêtres. Ligne de Don Qui. Les Aïeux de Don Qui" 
date_document: "1991" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_**Colomb suivi de Long John Silver**_   
Du boulevard les grises balayures s’étaient enfin enlevées de nos yeux, malgré nos regrets de cet amour tangentiel des croupes, dont la rotondité ne sert qu’à mieux compresser notre organe, en nous en étant déjà de nous-même défait, sans même y avoir pénétré.  
Le signe du soleil fondant après les derniers bois était aussi celui où le vent redoublerait vers les reins (heureusement garnis de flanelle), en prenant pour ricochet la glaciation des ondes vipérines. J’étais enfin libre de faire jouir mon corps et mon âme (…………) des mêmes nervures de coque, défait du port où l’immonde silhouette d’un barbotis flasque disparaissait avec le bruit des chaînes qu’on mouille   
Mais à peine passé le port, où le vent tombait, le soleil masqué jusque là rebondit au-delà de la fin de l’après-midi ! comme si nous changions d’univers ; la fatigue, la grande fatigue qui est la mienne, ajoute une grande saveur de viande meurtrie et d’os brisés à tout.