---
date: 2019-12-23 9:40
title: Cible rouge n°6
sous_titre: Profil droit du Professeur
date_document: Après 1984
fichiers:
- "/Cible rouge.6.Profil droit du Professeur.jpg"
tags:
- dessin et gravure
- Cosmologie Onuma Nemon
- document

---
Pistolet 10m & dessin à la plume sur anciens cartons de cibles format 14 x 14cm en 5/10èmes