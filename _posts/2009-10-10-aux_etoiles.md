---
title: "Aux Étoiles"
date: 2009-10-10 14:05
fichiers: 
  - Aux_Etoiles.jpg
sous_titre: "Vincent Compagny, document 1" 
date_document: "1986" 
tags:
  - document
  - DAO
---

Vincent Compagny travaille dans la communication le jour et s’acharne à la défaire le reste du temps, tâche qui est la définition même de la poésie.  
Ainsi dans les usines on a vu des magasiniers détruire des accessoires de luxe qu’ils étaient obligés de trimballer du soir au matin et qui faisaient leur exploitation.  
Donc Vincent Compagny soit attente aux objets dans sa façon de les disposer de façon parodique, pléthorique (dépradation de leur immédiateté utilitaire, fonction retard du signe), soit rature le médium photographique en l'attaquant, en y portant des embus de sens, en défaisant la transparence, en forçant par la gravure un présent qui bouscule le passé argentique.  
Les apparences sont contre lui. 

_Isabelle Revay_