---
title: "Aube à Tokyo"
date: 2017-07-17 19:08
fichiers: 
  - aube_a_tokyo.pdf
sous_titre: "Tribu des Adolescents. Aube & Nany. Automne" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

11 000 mètres d’altitude. 900km/heure. Passage le long du cercle polaire, soleil de minuit, nuit pincée entre la lumière du couchant et celle du levant. Intense humidité stagnante. Nuit. Shinjuku.  
Aube écrivait à Monique (qui aurait dû venir) que son expo à Tokyo le 26 août faisait partie de Femmes et Histoire. Mr Yamagishi lui offrait le luxe de montrer des morceaux de peau et de chair dans une galerie. “Les femmes ont créé 52 % de toutes les formes de pensée humaine ; que les hommes assument au moins les 48 % qui leur restent !” Elle avait envoyé un télégramme à l’ambassade de Russie Bd Lannes pour le soutien des femmes russes en lutte comme elle en enverrait quatre ans plus tard encore pour éviter le séjour en camp à Nathalia Lazareva. À Shigel elle avait dit : “You are not living in my body.” et “J’ai le droit de briser l’ordre des chapitres.”

_(lire la suite…)_