---
title: "L’Idiot à Béthanie"
date: 2008-07-13 16:15
fichiers: 
  - Le_Singulier.pdf
sous_titre: "Les Escholiers Primaires. Le Singulier. Automne" 
date_document: "1973" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

La présence obsessionnelle de ma pensée à des tâches quelconques était un signe de déséquilibre, ou bien était-ce au contraire le fait de les exécuter machinalement, tenant compte du fait que nous cherchons le déganguement du primaire par les ruptures d’Univers.  
Je devins immobile.