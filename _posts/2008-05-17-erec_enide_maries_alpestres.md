---
title: "Érec & Énide Mariés Alpestres"
date: 2008-05-17 18:04
fichiers: 
  - Erec__Enide.Orphelins.Automne.pdf
sous_titre: "Les Orphelins Colporteurs. Ligne de l'Hospice. Automne" 
date_document: "1991" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

L’invocation de Léopardi n’est pas pour rien dans cette _cristallisation_ des Voix qui représente (avant le Continent O final) l’aboutissement de la Cosmologie en sa forme poétique, emportant à la fois les “États” désubjectivés, la dimension épique et une narrativité qui procède par sauts, en pointillés.  
On distinguera cette Vérité, ici dite “Voix de basse, grave, noire, en même temps qu’un essoufflement de marche sur la Neige” de la Verrité, _résistance du défaut_ du verre à la vue, présente en de multiples endroits de “Quartiers de ON !” et dont on donnera ici même une condensation.  
**Lucinda Véron-Féret**  
  
  
_**Chalet des Mariés Alpestres (Érec et Énide)**_  
_**Chez Clotilde. Printemps**_  
**Clotilde :** « Prenons galoches à taches rayées. Bouchons avec une   
rissole. Un batteur soulève la batteuse avec ses reins. »  
**Énide :** « Le requiem du requin blanc,  
l’ours dans son blanc suaire,  
albatros, miracle de plumes,  
vastes prairies liquides de Krill…  
Prends la garde près des orages  
(Seules, les Nuées envisagent) ;  
Voici les mythes d’autrefois,  
Inexplicables de parfum,  
Inouis, pullulants, sauvages.»  
  
Désormais, à cette hauteur, toute saison a disparu, dans cet endroit qui lave l’œil de la poussière des âges filtrant le soleil à travers les yeux des morts pour en obscurcir l’éclat et qui, par une perception sporadique de détails isolés, brise les shèmes imposés faisant écran à l’émerveillement.  
La présence des Mariés Alpestres, devant le chalet de Clotilde, ressemble à un transport au cerveau, une trépanation, la vrille de la mèche à l’aplomb de l’oreille dans un angle de 15° vers l’occiput. Leur retard à surgir est semblable à celui de la compréhension des ravages de l’amour. _On ne peut plus y surseoir_, voilà ce qu’ils disent, et cependant, à travers leur esprit, courent encore transversalement des allongements répétés, des lâchetés, des faiblesses.  
“Nous répugnons à l’albinos, avec sa blancheur diffuse, à la tristesse de la ville de Lima, qui porte le deuil blanc de ses tremblements de terre, au surnaturel marmoréen de ce mort-là, aux processions du dimanche blanc de Pentecôte et à la grandeur sauvage des “Blancs Chaperons” rapportée par Froissart d’entre ses feuilletés et les résonnances des condensateurs par où sa voix passe, mais, plus encore qu’aux manteaux de neige sur l’épaule des fantômes, à cette étendue, à cette ubiquité dans le temps, mythique, à cette majesté, cette pureté, cette effroyable divinité de silence, peuplée de sens, remplie de l’absence des couleurs et de la fusion de toutes les races qu’est la Neige !”  
  
**Érec :** « Laissez-moi toucher la lumière blanche  
Et voir, sur la même ligne :  
Le mal, moindre bien ; le bien, moindre mal.  
Tenus entre les deux mains.  
  
Dieu est un climat :  
Par exemple la première chute de neige,  
Un été indien,  
Ou le bruit du vent contre la porte\*. »  
(_\*Beatles : “Christmas”_)