---
title: "Orphée en Rallye"
date: 2008-04-06 10:21
fichiers: 
  - Orphee_Rallye.pdf
sous_titre: "Les Grands Ancêtres. Mac Carthy. Saison de la Terre" 
date_document: "1992" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_**Orphée. Rallye de Mort**_  
Un pote à moi retrouvé à Terraube et qui était commissaire sur cette course, m’avait demandé si je pouvais servir de copilote à un gars dont le sien venait de subir un très grave accident. Je n’avais pas tout de suite reconnu le paternel de Marie-Violante, (une ennemie d’enfance de Nicolaï perdue de vue depuis longtemps), malgré son pif écrasé et ses oreilles en choux-fleur, et pour cette fois-là, je ne pourrais éviter de grimper dans sa Datsun, “voiture sauvage” dont à l’époque il se servait encore en rallye.  
Pour éviter de tourner deux pages à la fois et de se planter, le copilote précédent avait repris le vieux truc du rouleau de papier-cul bien solide comme “road-book”, sur lequel il avait redessiné tous les virages lors du premier parcours fait d’abord très lentement avec un repérage précis, y compris sur le kilométrage. Ce genre de notation de la route en sténo, avec quelle vitesse pour aborder et pour sortir, les diagonales éventuelles, me paraissait un furieux secret sur ce qui va arriver, magie d’un paysage surgissant d’un rouleau. On sait ce qui nous attend. C’est une architecture d’avant Monteverdi, d’avant l’irruption de l’émotion et son crash baroque de mille ruptures. Alors que même la voie de retour d’un parcours connu, par exemple, comme me l’a déjà fait remarquer Ulittle Nemo, est généralement impossible à prévoir à partir des seules remembrances de l’aller : c’est véritablement un paysage au-delà du miroir.  
Avec ça on était attentif aux cahots, bien mieux que sur un carnet à spirales, on anticipait toute trajectoire sans risquer d’abîmer la voiture, et on pouvait réussir à arriver dans les meilleurs temps.  
Pas besoin du nombre de pages ni de reporter les premières notes de la page suivante en bas à droite comme sur un carnet : seulement l’intitulé de la course et la distance à parcourir, figurant en haut. Un copain à moi ainsi s’est écrasé, parce qu’un musicien grec, copilote avait tourné par erreur deux pages à la fois, donnant un virage très rapide à gauche pour un très lent à droite.  
Quand je conduis, je me dis souvent : “Je me suis soumis à elle pour l’Eldorado, parole de miel et lune de même, mais c’est La Mort qui me passionne, essaim vibrant des fureurs noirâtres. Nicolaï lui-même a connu trois femmes, mais surtout une Fée, en dehors, la Fée Noire des moutonnements de la Ruhr, et c’est bien grâce à elle, sur la prairie aux Ombres Noires, qu’il a pu entrevoir la demeure de La Belle Au Bois Dormant près de Hrad Smrti, la seule Fiancée capable de devenir La Future, mais que sa folie sexuelle l’a empêché à jamais de rencontrer. C’est La Mort que je tiens devant les yeux lorsque je conduis.”