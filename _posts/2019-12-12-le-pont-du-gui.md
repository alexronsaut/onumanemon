---
date: 2019-12-12 5:01
title: Le pont du Gui
sous_titre: Autour de la Cosmologie O.N.
date_document: ""
fichiers:
  - "/le-pont-du-gui-1994.jpg"
tags:
  - photographie
  - DAO
  - document

---

Rendons hommage à ceux qui, selon le projet de la Cosmologie, peuvent _réellement_ transformer les œuvres et les rendre meilleures à savoir d’abord Didier Morin qui a rendu ce site possible avec Alexandre Ronsault, et Bernard Plossu, rencontré grâce à lui, qui nous relie vraiment au début du double enchantement de la marche et de l’écriture avec la Beat Generation.

Bernard Plossu a eu cette infinie générosité de se déplacer pour photographier quelques-uns des territoires minuscules du Quartier Saint-Michel de la Cosmologie, dont La Flèche (reproduite sur la couverture des _États du Monde_), et la rue dite Sauvage que l’on trouve en particulier dans _Roman_, écrit en 1968. On trouvera ici un choix de ces photographies.

_NDLR_
