---
title: "Académie : Nature Morte au Pot"
date: 2008-04-17 18:12
fichiers: 
  - NatureMorteAcademie.jpg
sous_titre: "Aquarelle & Encre de Chine sur Arches. 30cm x 40cm" 
date_document: "1967" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - peinture
---

Cette œuvre figurait dans l’exposition au “Quartier” en 2005