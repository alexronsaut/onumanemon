---
title: "Ainsi qu'il en est"
date: 2011-04-06 19:41
fichiers: 
  - ainsiquilenest.pdf
sous_titre: "Texte de Joël Roussiez" 
date_document: "2011" 
tags:
  - document
  - DAO
  - texte
---

La vie allait comme s'il sonnait des cloches, tristes mais d'une grande beauté et, sur ce fond, s'improvisait une mélodie complexe qui se simplifiait par ce qu'on aurait pu appeler « ce procédé ». On marchait dans des territoires boueux, en plaine hongroise si l'on veut, à la fin de l'hiver droit devant où le ciel changeant ne cessait de nous tirer.  
  


_lire la suite…_