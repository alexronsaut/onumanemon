---
title: Ange du Pays des Morts
sous_titre: ""
date: 2024-08-18 14:46
date_document: "Après 1984"
tags:
    - document
    - dessin et gravure
    - Cosmologie Onuma Nemon
fichiers:
    - "/Ange-du-Pays-des-Morts.Dessin.Encre-de-Chine.Format-Grand-Aigle.jpg"
---

Dessin. Encre de Chine. Format Grand Aigle

Don à la Bibliothèque Kandinsky de Beaubourg.
