---
title: "Colomb : Le Pénitencier de Santoña"
date: 2008-01-17 10:43
fichiers: 
  - Penitencier_de_Santona.pdf
sous_titre: "Ligne de Don Qui Domingo" 
date_document: "1991" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_**Pénitencier de Santoña**_  
Onan est déjà mort depuis longtemps et toute la racaille des gardiens phalangistes sur les plages ne pense qu’à mourir ; ils crient “Vive la Mort !” même en ronflant, tandis que la plupart de ceux qui veulent à tout prix s’embarquer ne pensent qu’à vivre, dussent-ils en périr. On _voit_ déjà le _cher Gérard_ dans son chalet de garde-forestier avec sa radio à ondes courtes surveillant les incendies soudains.  
Les feux au mois d’août, à quatre heures, après un très gros orage ; cette cloche soudaine d’hiver et la veste mise sur les épaules (on oublie toujours le numéro de _cette valse_ de Chopin, dont les bribes par la croisée). On hésite, entre le tchocolalt bouillant de pures fèves broyées et le vin chaud.  
  
Ils sont plusieurs dans le Pénitencier à préparer ça : s’enfuir la nuit, par la plage, en barque (plusieurs sont marins) longer la côte jusqu’au Sud, à Palos de Moguer (où l’un d’entre eux connaît un moine) : ils trouveront bien moyen de s’embarquer pour quelque part, un autre monde !

\*

« Qui jouit dans mes bras ?  
J’ignore.  
Résédas, jasmins, flots de roses ! »  
(Il implorait en pleurant la  
Flaveur de Sainte Anne, une nuit.)  
  
Coquillages des dents, perdus  
Dans un gouffre de soleil ;  
À jamais nous vous cherchons,  
Andalouses,  
Comme parmi tous les orangers  
Le citron !