---
title: "Le Théâtre de Rétif"
date: 2008-10-19 17:11
fichiers: 
  - Retif.Esplanade.pdf
sous_titre: "Esplanade des Girondins. 1989" 
date_document: "1989" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

« Qui mourrai-je de quoi ? D’une inattention portée au temps, bonheur attendu de rien, attenant à rien, et dont rien ne peut rendre compte ? D’une simple vasodilatation de l’âme… »  
Il était dix-sept heures quarante, de l’autre côté de la Révolution, bord droit, au mois de Mars, et la surface presque chaude de l’herbe de l’Esplanade recevait de grandes tracées d’ombres en rayures _sans véhicule_.  
Sur le bord même de la Révolution, à gauche, sur les terrasses du quai de Calonne interdit aux piétons la nuit, il y avait des accidents en contrebas, à l’endroit du parapet du Château-Trompette en cours de démolition, des brûlots divers (la bête prise, les bleus sans liesse), et le remblais en surplomb etait empli d’un attroupement de citoyens, arrêtés au-dessus de la situation.  
Les sans-culottes et les officiers municipaux chassaient les voyeurs venus là d’un double geste énervé des mains, du genre : “Tirez-vous, sales rapaces, agioteurs du malheur !”  
Nicolas voyait ça par son fiacre, mais surtout par la fenêtre de droite vers l’Esplanade (qui n’était encore qu’une esquisse au milieu des ruines, un trapèze immense irrégulier et tordu), il observait ces _ombres esseulées_, sans origine, et se retourna plusieurs fois sur la gauche, de plus en plus vivement, dans le but d’en surprendre les émetteurs fugaces.  
Mais il ne vit rien, rien qui soit susceptible de les projeter.