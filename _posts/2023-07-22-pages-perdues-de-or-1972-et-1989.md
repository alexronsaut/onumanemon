---
title: "Pages perdues de OR 1972 et 1989"
date: "2023-07-22 14:36"
date_document: "1972 et 1989"
fichiers:
    - /le-seau-dans-le-vide.pdf
    - /vis-a-vis.pdf
tags:
    - document
    - texte
    - OR
---

Ces pages perdues et non intégrées dans OR avec leur mise en colonnes, renvoient ici
volontairement au recueil en cours de Typhaine Garnier dit Refaire, textes lisibles ici dans
DAO : [Refaire. Poèmes de Typhaine Garnier](/posts/refaire-poemes-typhaine-garnier.html)
