---
title: "Tout doit disparaître !"
date: 2007-10-09 13:24
fichiers:
    - tout-doit-disparaitre.pdf
date_document: "2007"
tags:
    - document
    - DAO
    - texte
---

Voilà ce qui était écrit sur le calicot, l’immense banderolle ballante dans le grand hall de marbre blanc aux vitres brisées de “Isla de Os”, cette sorte de domaine fabuleux, banderolle déchirée par endroits, salie, et que le vent passant malmenait par bouffées, au milieu des restes fossiles et des entassements de boîtes d’archives parmi d’autres colonnes de papiers en partie effondrées, des cadres soucieux ou brisés contenant des peintures, une grande diversité de vieux meubles de rangement en mauvais état, une incroyable collection d’œuvres d’art de tous les pays, et tout au fond une grande caisse en bois de la longueur d’un homme allongé et de la hauteur d’un garçon de douze ans, fermée partout sauf sur sa face avant, avec une séparation horizontale comme pour y dormir, si elle n’eut été encombrée d’une quantité de cartons à dessins, de plaques de bois et de métal gravées, de calligraphies enroulées et de toutes sortes de feuilles de papier en vrac.

_(lire la suite…)_
