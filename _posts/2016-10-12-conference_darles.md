---
title: "Conférence d'Arles"
date: 2016-10-12 13:40
fichiers: 
  - christian_gix.pdf
sous_titre: "Christian Gix" 
date_document: "2014" 
tags:
  - document
  - texte
---

Le Cinéaste Christian Gix (originaire de Metz, et qui vit en Patagonie), a tenu cette conférence à Arles à propos du photographe Marc Giloux, et en particulier à propos de son ouvrage _Œuvre ouverte et Polygraphie_, paru au Québec.

Cette conférence sera reprise dans l'ensemble des manifestations des fameuses _Rencontres de Cambrai_.