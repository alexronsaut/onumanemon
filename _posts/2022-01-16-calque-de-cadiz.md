---
title: "Calque de Càdiz (Hommage à Françoise Nuñez)"
date: "2022-01-16 16:25"
sous_titre: "Dessin aux crayons graphite et couleurs sur Canson et calque 24x32 cms"
date_document: "Càdiz 1969"
fichiers:
  - /calque-de-cadiz-1.jpg
  - /calque-de-cadiz-2.jpg
tags:
  - document
  - dessin
  - OGR
---

.