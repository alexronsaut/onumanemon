---
layout: post
title: L'Émoi 68
date: 2018-06-08 00:00:00 +0000
sous_titre: La Science du Jour
date_document: Avant 1984
fichiers:
- "/L'Émoi 68-1"
- "/L'Émoi 68.pdf"
tags:
- OGR (épanchements)
- OGR

---
_Que faisiez-vous en Mai 1968 ?_

À cette question inquisitrice, un ami suisse répondait “Je courais le Marathon.”ce qui avait la double vertu d’être vrai et très utile à ce moment-là.

Un autre grand philsophe hégélien et belge (grand pour moi entre mille autres choses par sa pratique croisée de la musculation et du tennis et cette phrase : “La cuisine belge, c’est la qualité française alliée à la quantité allemande.”), apprenait à ce moment-là à sauter à travers les vitres des cafés avec un casque de moto pour éviter une répression redoutable (et les éclats !).

Voilà deux hommes pragmatiques.

Dans ce que O. N. appelle ici “l’Académie", École d’Art à Bordeaux il y a eu beaucoup de liens avec la Fac de Lettres, le Campus et la Fac de Sciences, mais plus encore avec les amis du C.R.E.P.S. coureurs et boxeurs. Des liens également avec ces deux capitales que sont Toulouse et Paris.

Il y a eu aussi des bousculades majuscules et pas mal d’errances dans le bon sens.

L’engagement est devenu tout de même très vite _balnéaire_ dès juillet (malgré l’Université d’Été au Campus), et pour la rédaction du grand projet pédagogique futur il n’y avait plus que _trois_ personnes à l’Académie, et _quatre_ dans le bureau de Chaban pour le “rendez-vous ultimatum” d’évacuation du chef de guerre le samedi 16 juin.

Il y aura certainement eu à partir d’aujourd’hui des héros grandioses des barricades d’hier (dont d’aucuns devaient être  barricadés chez eux), ou le professeur Fromage (qui ne cesse de couler en peinture !), mais le plus drôle est certainement de s’apercevoir combien on était _ailleurs_ et parfois même dans une inactualité absolue.

_Pierre G. Sivocq_

_(lire la suite…)_