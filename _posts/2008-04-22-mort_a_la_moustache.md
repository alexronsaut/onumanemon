---
title: "Mort à la moustache"
date: 2008-04-22 22:06
fichiers: 
  - MortMoustache.jpg
sous_titre: "Ligne du Chaos. 3." 
date_document: "1982" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - dessin et gravure
  - edition
---

Encre de Chine sur Arches 15cm x 18cm. Figure dans “États du Monde”, version définitive.

Publication : [publie.net](http://www.publie.net/tnc/spip.php?article85)