---
date: 2019-09-13 7:37
title: Dimanche
sous_titre: Os de Poésie
date_document: "1965"
fichiers:
- "/Dimanche, non attribué.pdf"
tags:
- texte
- OGR
- document

---
DIMANCHE

C’est dimanche, les coups de gris,

La lithurgie, huîtres et le coke.

Voici la douche wagnérienne,

Babas au rhum et Chantilly,

Et le crabe en métal doré.

Grand-mère est vraiment furieuse :

Les avions nous survolent, noirs ;

La neige aussi s’est assombrie

Sous les étoiles aux voix de chats ;

Il a neigé tout un ennui.

etc.

(_lire la suite_…)

Dimanche, poème non attribué à un frère ni l’autre. Nini. Ni Nycéphore, ni Nicolaï. Fait partie de tout ce vrac, ce pisé, adobe, bauge ou torchis, ce remplissage entre les cloisons.

À 17 ans le vrac Aragonien, probable… qui lui même (“plafonds/profonds”), n’est pas loin des scansions Hugoliennes.