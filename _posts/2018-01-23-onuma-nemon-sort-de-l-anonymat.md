---
title: "Onuma Nemon sort de l'anonymat"
date: 2018-01-23 11:30
fichiers: []
sous_titre: ""
date_document: "23 janvier 2018"
tags:
  - extension
---

On trouvera ici l'entretien *in extenso* réalisé par Pierre Benetti et Tiphaine Samoyault pour la revue *En Attendant Nadeau*, dans les locaux et avec le soutien de *Mediapart*.

<iframe src="https://www.youtube.com/embed/CDDvIBQWB3U" frameborder="0" width="688" height="387"></iframe>