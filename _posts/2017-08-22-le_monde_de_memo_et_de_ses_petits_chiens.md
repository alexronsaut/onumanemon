---
title: "Le monde de Memo et de ses petits chiens"
date: 2017-08-22 19:50
fichiers: 
  - le_monde_de_memo.pdf
sous_titre: "Tribu des Adolescents. Automne" 
date_document: "1982" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

Il faut que je vous dise à propos de Memo et des Quatre Petits Chiens Brefs de la Mort (Dic, Duc, Fac, Fer), que ces derniers devenaient parfois (comme les Grands Ancêtres), les Quatre Chevaliers de l’Apocalypse, ou les paroles de Dieu aux Impératifs irréguliers, autant que le Christ réparti en Quatre Animaux.  
Quand l’un de ses quatre chiens se promène, Memo n’est jamais loin ! Il les surveille, mais travailleur du Royaume des Ombres, il se montre peu aux vivants n’étant généralement là que pour éviter la venue d’embranchements catastrophiques prévus à l’échelle de l’univers, simplement dans le but d’avertir tel ou tel des impasses où il risque de s’engager et lui permettre ainsi de choisir un autre “montage”, de changer d’aiguillage et de destinée, sauf dans le cas où la mise en gare de triage d’un wagon doit précisément permettre d’éviter un conflit mondial.  
“La littéralité blanche doit toujours être défaite par les accidents et par les engouffrements historiques comme les coulures sur une peinture de Bacon”, dit Memo.  
  
_(lire la suite…)_