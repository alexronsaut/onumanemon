---
layout: post
title: 'Alain Vallet : Encres'
date: 2018-06-03 00:00:00 +0000
sous_titre: Janvier 2018
date_document: ''
fichiers:
- "/Encre 8.jpg"
- "/Encre 7.jpg"
- "/Encre 6.jpg"
- "/Encre 5.jpg"
- "/Encre 4.jpg"
- "/Encre 3.jpg"
- "/Encre 2.jpg"
- "/Encre 1.jpg"
tags:
- peinture
- calligraphie
- DAO
- document

---
**Défricher-Déchiffrer**

Ces encres ont la particularité d’être des palimpsestes de nus anciens. En effet il y a eu d’abord des nus académiques, réalisés dans un atelier près de la maison de Victor Hugo. Ces nus avaient la particularité d’être dégagés par l’entour : au lieu de la construction académique habituelle à partir du modèle comme centre, le peintre les avait laissé deviner dans une sorte de _bénéfice de la lumière_. Il a ensuite repeint par dessus (travaillant aussi sur des répulsions huile-encres), dégageant cette fois-ci des blancs comme des lumières dans un taillis, des trous dans un mur ou des déchirures dans un drap. Au-delà de Pyrame et Thisbé, je ne peux m’empêcher de penser ici aux notations des _Choses Vues_ de Victor Hugo lorsqu’il dessinait un nu aperçu par la fente d’un mur, écrites en espagnol et en abrégé de telle sorte que n’importe qui ne puisse les déchiffrer.

_O. N._