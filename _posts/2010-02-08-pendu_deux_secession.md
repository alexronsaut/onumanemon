---
title: "Pendu Deux (Sécession)"
date: 2010-02-08 19:01
fichiers: 
  - Pendu_Deux_Secession.jpg
sous_titre: "Encre de Chine sur Arches format raisin" 
date_document: "1984" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - dessin et gravure
---

Photographie : Didier Morin.