---
title: "Double Trou des Morts"
date: 2007-03-23 09:55
fichiers: 
  - DoubleTrouDeLaMort.jpg
sous_titre: "Encre de Chine. Format Raisin." 
date_document: "Après 1984" 
tags:
  - document
  - OR
  - dessin et gravure
---

Figure dans “Quartiers de ON !”. Chant XI.