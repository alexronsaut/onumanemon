---
title: "Zinaïda : Enfin merci…"
date: 2007-10-15 12:00
fichiers: 
  - 279.mp3
sous_titre: "Les Adolescents" 
date_document: "1989" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - son
---

Pages sonores inédites. Disque 2. Piste 5.  
Réalisation Philippe Prévot dans les studios de LIMCA à Auch.