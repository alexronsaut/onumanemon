---
title: "Carte Géante de l’Automne"
date: 2009-05-26 23:20
fichiers: 
  - Carte_dAutomne.jpg
sous_titre: "Encre de Chine, Pastels gras" 
date_document: "2005" 
tags:
  - document
  - OR
  - dessin et gravure
  - extension
---

Carte de 3m x 3m ayant figuré dans l’exposition du Quartier en 2006