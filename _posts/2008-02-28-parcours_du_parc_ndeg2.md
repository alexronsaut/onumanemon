---
title: "Parcours du Parc n°2"
date: 2008-02-28 21:59
fichiers: 
  - Parcours_Parc_2.jpg
sous_titre: "Les Adolescents. Joyelle & Hill" 
date_document: "1978" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - photographie
---

Format 13 x 18 cms. Cette photographie faisait partie de l’exposition au “Quartier” en 2005. Des agrandissements avaient été effectués pour voir _qui était sous les arbres ce jour-là_.