---
title: "Equitable Building"
date: 2008-05-13 13:50
fichiers: 
  - Equitable_Building.pdf
sous_titre: "Les Escholiers Primaires. Ligne de Nycéphore. Automne" 
date_document: "1991" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

Texte inédit  
  
_**La Déliaison**_

Roland Bergotte est assis au café en bas de l’Equitable Building à New York en compagnie d’un vieil ami à lui, André, qu’on voit souvent par ici, sommeiller sur les pailles, aux terrasses, ou rêvassant sur les quais de l’Hudson. Ils tiennent une sorte de conférence improvisée en compagnie de Nycéphore, qui ne parle pas beaucoup, et de Nathalie, assise en retrait, qui doit exécuter ce soir une danse d’un sommet à l’autre du building, exercice préparatoire à son numéro périlleux au sommet de l’antenne radio de l’Empire State Building. Tout un cortège de curieux, de journalistes et d’admirateurs se presse devant l’immeuble, répandu sur la place et depuis les rues avoisinantes, descendu en masse des voitures et tramways.  
Mais taisons-nous et écoutons plutôt.  
André : « Vous serez sans doute d’accord sur ceci que le texte littéraire et le texte du rêve ne se rapprochent que sur un point : celui d’être tous les deux présentés à travers l’élaboration secondaire. »  
Roland : « Oui ; ce qui réclame de distinguer plus avant dans le texte la poésie qui est une danse au-dessus des mots dressés dans leur verticalité sensique et dans l’oubli de la chaîne métonymique (c’est du moins ce que j’ai trouvé à propos de “ces mots-objets sans liaison” dans _“Les Absolus”_), ce qui fait du Chinois la langue poétique par excellence, écriture et sans doute _pensée déjà imprimante_ avant l’invention de l’imprimerie, parce que constituée d’une évidence de _“blocs”_ ; au contraire de l’écriture alphabétique occidentale pour laquelle est indispensable le “saut qualitatif” de cette _dé-liaison_ de la chaîne, faute de pouvoir passer aux caractères mobiles de Gutenberg. C’est également a contrario la “ficelle de Gutenberg”, ficelle de l’élaboration secondaire qui fait tenir ensemble les caractères, empêche que la page ne tombe “en pâte”, et résiste aux pressions primaires. La poésie trouve son acuité de cette tension extrème entre les deux poussées. Eros & Thanatos à l’œuvre. »  
Il boit un peu de son lait-fraise, puis il reprend :  
« La poésie, bien que traversant par son chanfrein toute l’histoire de la langue, présente donc le grand intérêt des discontinuités, du “délié” d’un geste dans l’espace, d’attitudes qui “ne font pas” histoire, ni préhistoire, ni lien ni sauce.  
Et pour revenir à ce projet de _La danseuse au sommet de l’Empire_, que Nathalie Pelleport ici présente va bientôt réaliser (dont la performance de ce soir sera déjà un grand préalable), et que notre ami Nycéphore Naskonchass poursuit depuis Now Snow (et sans doute bien avant !), il est proche du _raccourci sauvage_ de la sculpture de Degas, cette plasticité féroce, terriblement incarnée et redoutablement articulée, au désir entre autres, mais ni viande ni pure idéalité ridicule (du type Ecusette de Noireuil pour cette bourrique d’André Breton) ; plutôt formule de _Rêvité_ (la Vérité qu’on retourne), d’un ensemble de mouvements et de leur possibilité contingente d’enthousiasme dans leurs articulations réciproques.