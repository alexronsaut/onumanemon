---
title: "Joël Roussiez"
date: 2014-07-30 23:39
fichiers: 
  - roussiez.dico_.pdf
sous_titre: "Pour le Dico" 
date_document: "2014" 
tags:
  - document
  - HSOR
  - texte
---

Texte destiné au Dictionnaire de l’URDLA : on verra si ça passe !

_NDLR_

L’exigence morale de Roussiez consiste à se dépecer du monde de la vulgarité instruite, refusant le récit grossier comme l’action quelconque. Il cherche l’améthyste du gave, l’animalité des parois et les lieux de torsions et d’enroulements fantastiques des corps de l’enfance. Car l’aventure Roussiez ce n’est pas celle du signifiant, mais celle du roman où le monde s’ouvre au fur et à mesure de l’énoncé comme le corps devant le médecin.

  
(lire la suite…)