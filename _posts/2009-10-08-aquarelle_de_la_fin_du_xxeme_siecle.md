---
title: "Aquarelle de la fin du XXème Siècle"
date: 2009-10-08 18:07
fichiers: 
  - AquarelleBonnier_1985.jpg
sous_titre: "Alexandre Bonnier" 
date_document: "1985" 
tags:
  - document
  - DAO
  - peinture
---

Alexandre Bonnier, homme des grandes fêtes des Beaux-Arts de Lille, contemplateur de la chute de la manne céleste à Mâcons en mangeant des cuisses de grenouille, créateur avec Giacomoni du Centre de l’Environnement et surtout des Trois Départements des Écoles d’Art après 1968 bien avant que n’arrivent au galop les rabatteurs du milieu, culturistes généraux ou autres champions de la gonflette, puis que ne s’y engouffre le pire bataillon des distributeurs d’u.v. en rondelles, couilles d'anges, huberts-chiâssepot-incestueux, drac-queens et connards barbants ; Alexandre l’homme de la Mort en Rose Majeur. 

O. N.