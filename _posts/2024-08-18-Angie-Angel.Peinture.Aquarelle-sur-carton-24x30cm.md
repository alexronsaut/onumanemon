---
title: Angie & Angel
sous_titre: ""
date: 2024-08-18 14:46
date_document: "Après 1984"
tags:
    - document
    - peinture
    - Cosmologie Onuma Nemon
fichiers:
    - "/Angie-Angel.Peinture.Aquarelle-sur-carton-24x30cm.jpg"
---

Peinture. Aquarelle sur carton. 24x30cm

Don à la Bibliothèque Kandinsky de Beaubourg.
