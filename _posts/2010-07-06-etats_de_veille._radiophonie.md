---
title: "États de veille. Radiophonie"
date: 2010-07-06 21:52
fichiers: 
  - Etats_de_veille.pdf
sous_titre: "Livre de Nycéphore" 
date_document: "1969" 
tags:
  - document
  - OGR
  - texte
---

Texte destiné à la radio dont la seconde partie a été perdue (on a rajouté le commentaire de cette perte, pris ailleurs, à la fin). 

_Marc Bohor_   
  
**états de veille 1969  
(paysages aperçus sous les paupières)**   
_**I.**_  
Désormais on prendra des bains de sable.  
Tout d’abord des animaux familiers ; puis le paysage devient féérique sous des pluies d’étincelles, douces manières d’amusement, traversées de tombes aimables, pâleurs roses, chutes liquides, coussins fadasses. Plus au fond : crépitements rouges, apparitions d’or et de teintes pailles, fulgurations oranges, motifs crémeux, mosaïques de carreaux bientôt en décomposition ; puis ces ensembles sont bientôt soufflés par les courants d’air frais de la soirée malgré les dispositions équilibrées balancées de part et d’autre d’axes médians, eux-même rapidement délités, filant, et le tout est repris sous la lancée statique d’arcs-en-ciel, ensuite diffusés sous forme de voix, d’ondulations de sang, de poussière bleue, quantité invraisemblable de formes parmi lesquelles jaillissent des bulles d’or avec des sons stridents.  
  
À présent ce sont des champignons avec des sillons comme enduits de pommade, des serpentins colorés qui traversent et balaient l’espace de l’œil à travers des tissements de brume.  
  
_lire la suite…_