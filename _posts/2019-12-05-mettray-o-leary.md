---
date: 2019-12-05 9:45
title: Mettray/O'Leary
sous_titre: Intervention de Didier Morin à Beaubourg
date_document: 22 Septembre 2017
fichiers:
  - "/InfoMettray. O'Leary.22 sept vf-1.jpg"
tags:
  - texte
  - DAO
  - document

---
Hommage rendu à un cinéaste extrèmement important pour le mouvement de Mai 68, dans le même désordre de flambée enthousiaste que Bernard Plossu et la Beat Génération.

NDLR