---
title: "Cent Cibles Trouées : N°13. Deux Vietnamiens déchargeant un bâteau"
date: 2014-02-09 21:27
fichiers: 
  - 13._Deux_Viet._dechargeant_un_bateau.B.Hermann.jpg
sous_titre: "Acrylique blanc sur Cible 17 x 17cm pour pistolet 10m" 
date_document: "Après 1984" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - peinture
---

À Brigitte Hermann