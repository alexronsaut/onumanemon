---
title: "Nicolaï et Macha. Hôtel Saint-François"
date: 2008-10-04 22:09
fichiers: 
  - Theatre_Lyceen.pdf
sous_titre: "Les Adolescents. Été" 
date_document: "1975" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

« Alors Flahaut lui dit : “Hortense, calmez-vous ! Paluchez-vous d’un seul doigt.” » Macha lisait ça à Don Jujus, les cuisses écartées sur le lit, Macha qui venait de Mouch, en Turquie ; elle était douce au toucher. Lui était plutôt dru, et orienté vers l’Orient. Rosa, à Caen, lui en avait parlé, ancienne copine à elle du Conservatoire.  
Elle le masturbait sur elle, elle avait deux petites roses (rouge, blanche) prises dans son bracelet de la main gauche ; les secousses ne les firent jamais tomber. L’œil unique de la caméra portative de Jujus allait et venait. Et rang’ ! Et rang’ ! Elle était là, dans l’hôtel Saint-François, pour travailler avec le Groupe du Théatre du “Styx” du Lycée, juste en face, rue du Mirail, qui à présent avait son bus près de l’Académie, et pour préparer le départ à Cádiz. Lui venait du groupe des Anartistes qui habitaient une sorte d’entrepôt dans la zone des marais de Sainte-Croix ; il avait rencontré Macha avec moi à l’Académie ; ils se plurent momentanément. Ensuite, dès qu’elle aurait les mains libres, elle devait écrire un papier sur la prochaine fermeture de La Roquette, d’ici quatre à cinq ans. Elle se souvenait de ses copines prisonnières mineures, quand Laurence y était, qui regrettait son chien, son seul ami. Quand l’une d’elles recevait une lettre d’amour elle le lisait à toutes les autres et chacune faisait son commentaire ; ça durait parfois des heures autour d’une mince feuille griffonnée.  
Dans la chambre à côté un autre comédien de ses amis, surnommé “Aubusson”, comme les compagnons, de son vrai nom André Névrose, et affublé d’un énorme tarin, sans déroger là où un marquis n’aurait pu vendre du drap, puisait tout le feu d’une pièce où il était question d’un texte inachevé volé dans un coffre à soie et d’un autre composé de ses résidus.