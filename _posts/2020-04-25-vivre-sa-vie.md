---
date: 2020-04-25 12:06
title: Vivre sa vie
sous_titre: Texte de Joël Roussiez
date_document: "2018"
fichiers:
  - "/vivre-sa-vie.pdf"
tags:
  - texte
  - DAO
  - document

---

**Vivre sa vie** (JL Godard, C. Hoebecrau)  
Encombrée par un corps qui ne me convient pas, j’aime mon visage et la forme de mes reins. Quand je marche dans la rue, les regards s’arrêtent sur ma croupe et la honte me saisit d’être ainsi construite que j’attire la concupiscence. Mon humeur n’est pas capricieuse sans raison, je suis à l’intérieur d’un corps qui ne me convient pas, un peu trop gras ici et trop d’os par là. Je ressemble aux femmes anciennes. J’ai des formes, dit-on pour me complimenter.

_(lire la suite…)_
