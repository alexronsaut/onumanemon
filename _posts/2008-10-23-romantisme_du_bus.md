---
title: "Romantisme du Bus"
date: 2008-10-23 15:55
fichiers: 
  - Romantisme_du_Bus.pdf
sous_titre: "Les Adolescents. Été. Le Lycée" 
date_document: "1989" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

_**Texte inédit**_  
  
Le trajet vers le Lycée c’était d’abord le trajet en bus pour nous tous, Nicolas parmi nous, et l’Idiot au crâne difforme de la rue Courteault spécialiste de Chateaubriant autant que de Lamartine qui parlait de ces habitudes au bas de la Morgue d’entreposer sur un bateau surmonté d’un pavillon noir par couches les cadavres des assassinés entre des couvertures de paille. Et on traversait toute la ville dans ce foutu bus puant sous les averses, tant il pleut à Bordeaux, avec partout des indondations à cause de ce temps pourri, de ce ciel de là-bas qui crève comme un phlegmon. On y faisait des concours à qui donnerait le plus de métaphores excrémentielles sur sa mère, incarnée comme une matoose, une merdouse. C’était à qui la détaillerait le mieux, filandreuse, émiettée, puant les œufs pourris ou l’acide chlorydrique comme des épinards hâchés, etc. On imaginait comme papoose Dieu son mari, vieux personnage incapable et barbu, grincheux et chieur en diable lui aussi à cause des intempéries qu’il dispersait sur Bordeaux, sans doute recouvert de la farine algérienne, uniquement préoccupé de précipitations aqueuses sur la ville, car Bordeaux est une Indochine, une cuvette malséante et putride.  
On avait aussi des chansons pour les quelques grues qu’on tracassait dans le trajet, vendeuses des Noga et autres :  
“Pouffiasse du lendemain grise,  
Matinée de crise…” ainsi de suite.  
Puis il y avait ce voyageur qui riait avec nous, obsédé par les contiguités fécales dont il nous parlait : manger un camembert bien puant assis sur le trône tout en débourrant ; il faisait souvent le trajet en notre compagnie, en imper mastic quelconque, un journal plié à la main.