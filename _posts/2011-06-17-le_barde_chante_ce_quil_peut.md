---
title: "Le Barde Chante ce qu’il peut"
date: 2011-06-17 22:54
fichiers: 
  - Le_Barde_Chante_ce_quil_peut.pdf
sous_titre: "Texte de Joël Roussiez" 
date_document: "2011" 
tags:
  - document
  - DAO
  - texte
---

**Le barde chante ce qu'il peut**

Nous placions nos dieux dans les angles de nos pièces afin qu'ils ne dérangent pas et veillent ainsi discrètement sur nos vies. On plaçait haut leurs figures afin qu'elles dominent la pièce entière, cacher nos gestes à nos dieux n'avait guère d'importance mais on souhaitait pouvoir se retourner sur eux et les apercevoir à tout instant. Nous n'avions pas de rituel par lesquels nos vies auraient pris les contours d'une routine douce et prévisible, nous n'attendions aucun retour du temps, ainsi nous fallait-il une présence constante et cependant discrète... Lors de nos voyages, on emportait leurs figures dans des boîtes dont une des parois manquait afin qu'on puisse les voir plus facilement; certaines boîtes avaient un petit système qui permettait à la figure de tourner et de présenter ainsi tantôt sa face, tantôt son profil de manière analogue à celle qui paraissait à nos yeux dans les angles de nos pièces... Nos maisons n'étaient pas grandes mais on y ménageait de grandes ouvertures car il fallait que la nature y entrât le plus possible, sans cela ce qu'on craignait le plus, c'était l'enfermement.  
  
_lire la suite…_