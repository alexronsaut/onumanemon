---
title: "Réponse à Menstyle"
date: 2010-07-29 23:52
fichiers: 
  - Menstyle.pdf
sous_titre: "Biographie et non Autobiographie" 
date_document: "2009" 
tags:
  - document
  - HSOR
  - texte
---

**Biographie et non Autobiographie**  
L’autobiographie, c’est la façon d’éprouver dans un corps la vérité du monde. Ce n’est certes pas l’anecdote, ni les aventures du personnage de la carte d’identité, totalement fictif. _ON n’a jamais existé ailleurs que sous un nom d’emprunt._ Tous les papiers étaient faux, sans qu’il y ait même d’origine à la question. J’en _pars_, c’est une lancée, pas un aboutissement. S’il y a une individualité qui se constitue, c’est par l’écriture elle-même. C’était déjà le projet de Montaigne. L’auteur est produit par _l’écriture de la Vie_ : non par son point de départ, mais par son point d’arrivée, son _résultat_. Il n’y a personne au départ. Il y a Onuma Nemon à l’arrivée.  
  
_lire la suite…_