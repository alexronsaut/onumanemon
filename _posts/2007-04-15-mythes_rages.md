---
title: "Mythes & Rages"
date: 2007-04-15 19:34
fichiers: 
  - MythesRages.pdf
sous_titre: "Livre Poétique de Nycéphore 1968-1984. Poème n°2" 
date_document: "Août 1968" 
tags:
  - document
  - OGR
  - texte
---

**2. Mythes & Rages**  
  
_**A. Z**_  
  
La Noire Épouse de Mars ;  
(Rapide différenciation sous les pêchers et les cerisiers en fleurs.)  
  
La porte démolie, la couleur brune ;  
Le studio divin défait & sale.  
  
« Huan Ta Pu Na O ! »  
Xuoti et Tlaloc.  
  
Vite, le Matin des Orgies  
De la Colêre des Auteurs, sinon…  
  
Tous leurs thrésors, aussi mignons que Périmèle,  
Doivent être tirés de Naos vers l’Autel  
Et immolés !  
  
_lire la suite…_