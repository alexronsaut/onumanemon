---
title: "Brouillons des Croisés"
date: 2012-01-25 23:00
fichiers: 
  - Rose-Croix.pdf
sous_titre: "Vive la Rose-Croix !" 
tags:
  - document
  - HSOR
  - texte
---

 Pas plus que le précédent, ce texte assimilable aux _Croisés_ n’a été repris ni dans les **États du Monde** ni dans _**Histoire Deux**_, du Continent **OGR**. On y retrouve entre autres l'épouvantable Emicho. 

_Isabelle Revay_ 

 

 

Qu’importe l’ordre des départs : Chaos, Cosmos, toute la merde ! D’abord des Faces pour des Visages ; (Haïssons pas les paillettes, tous feux éteints Avant l’Apocalypse, Ni les babillages de 18 mois, Attachants gazouillis d’emprunts.), Des Nombres pour créer des Peuples. Visages d’hommes libres sur des corps de femmes soumises Et vice-versa. _Déex li volt_.  
Pierre l’Ermite et sa lettre déclancha tout (“Si Constantinople tombe, Tours aura été inutile. Les Turcs, peuple débile de tous temps…”) Puis Pierre retourna dégoûté à Constantinople.  
Ceux-là criaient “Deus lo vult”, Inondés de sanquette, Affublés de fausses culottes, Singes ornés des maux des autres. C’est la tournée du Pape : Tours, Bordeaux, Toulouse, Montpellier, Nîmes (La durée d’un accouchement.), Marchands ceints de peaux d’Artistes qui traversaient l’Apulie Pour aller lui gratter l’oreille, Eux moins que rien : l’ombre déportée d’autres ; Jaugez le peu dont il reste ! Prostituées payées du Saint-Père, Famine, lèpre, fièvres, peste et batailles… Pour ces sauterelles religieuses. “Rien, moins que rien : pourtant la Vie. La pierre est fraîche, la main tiède.” chantent-ils.  
D’abord Al-Akim 1010, mauvais chiffre ; (Même Vivien n’y pourrait rien, Même Jean-Pierre.) La Rage qui touche, Blancheur qui pique au lieu du rouge qui tache ; Qui frappe vite et net, tue sur place, élimine frisettes autour du trou ; _Le tsuki de la pensée en acte_, Coup de poing redoublé sans appel de Bruce Lee & Kanazawa, Foudroiement épileptique de Dostoïevski, Zébrure visuelle de Virginia, Crise atomique du crâne au soir, 



_Lire la suite…_