---
title: "Fabien et le Phalanstère."
date: 2011-07-08 16:41
fichiers: 
  - Fabien.Phalanstere.pdf
sous_titre: "Ligne des Enfants. Orphelins Colporteurs. Saison de l’Automne" 
date_document: "1986" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

Comment sentir les taudis de Lewisham ? Le glissement de l’eau, de l’eau sans fin ! Fabien éprouve ça. Tout est le fleuve, toute vie est le fleuve, toute œuvre est un fleuve de bois flottant. Et elle qui ne vivra plus sans lui, plus jamais sans lui : comment s’approcher de cette douleur !  
Fabien se souvient en contrepoint de l’émotion cardiaque au printemps, près du château, chez Stanislas, du matin frais de lumière épanouie où il est allé au marché avec sa femme et sa petite fille pour acheter un gateau avec elles, du lit de fortune et de la chemise de nuit de lin brut de sa compagne joyeuse en comparaison de cette douleur lointaine, inaccessible de Lewisham, comme il voit un lien entre “Voyage au centre de la Terre” et “Au-dessous du volcan”. Pour lui les voyelles ne furent jamais que des ornements. Il suffit d’un carquois de flêches d’acier, d’un cable aux torsions d’acier et d’un trombone d’airin brisant l’air avec deux ou trois notes aigües. Il préfère les 200 mots de la déclaration d’indépendance américaine au 60 000 mots nécessaires à établir le détail du commerce des œufs de cane.  
  
_lire la suite…_