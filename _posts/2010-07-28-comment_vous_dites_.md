---
title: "Comment Vous Dites ?"
date: 2010-07-28 16:29
fichiers: 
  - Comment_Vous_Dites_.pdf
sous_titre: "Projet Radio-Crâne Nocturne" 
date_document: "2003" 
tags:
  - document
  - HSOR
  - texte
---

— Le projet radiophonique O N ! est une des _Extensions_ de la Cosmologie Onuma Nemon, liée à ses aspects polygraphiques et polyphoniques, au même titre que la monstration de quelques travaux plastiques originaux, la réalisation d’une ou deux “Machines Conjuratoires”, sculptures ou dispositifs de grande taille, ou les lectures publiques sans auteur faisant intervenir voix et images vidéo et cinématographiques.  
— Pour cela il est essentiel qu’il puisse être réalisé au _moment de la parution de l’ouvrage_. Car il permet, comme le C. D. par une sorte “d’écoute optique” (Stephen Heath) une “méthode de lecture” de la structure éclatée du texte.  
— Les “Nuits” seront un travail spécifique pour la Radio. Les extraits choisis sont inédits, et ne font pas partie du volume publié par les Éditions Verticales. Ils consistent dans un _Monologre de La Grosse_, elle-même sautant d’un registre à l’autre, changeant sans cesse de ton (on peut éventuellement utiliser plusieurs voix), mais permettant du moins, dans cette “unité” de rassembler et de “tenir ensemble” (comme la ficelle de Gutemberg autour du pavé de composition qui empèche le texte de “retomber en pâte”) des éclats issus de diverses émissions des années 50-60.  
 _lire la suite…_