---
title: "Charlotte au Sang"
date: 2008-07-23 09:21
fichiers: 
  - Charlotte_au_Sang.pdf
sous_titre: "Les Gras. Printemps. L’esplanade des Girondins" 
date_document: "1978" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

Sous-bois, déséquilibre, cassures, pour Charlotte Corday. Toujours de cordée, jamais de Montagne. À Glatigny, chez l’auteur des “Glaneuses”, où elle avait sa chambre, et passant de chez lui chez Roncerailles, dans ce trou de chaumière loin de l’Ami du Peuple qui donna naissance à cet autre poète aussi faux que Larronde.  
Elle a vécu du manoir au château ; on s’appelait d’un lieu à l’autre.  
Sa Trinité est plus de métal blanc que de dorure, multiplicité prise dans le rythme ternaire de la course Augustinienne. Elle est plus proche des coupes farouches de son grand-oncle Corneille que des serpents sinueux de Racine.  
Elle craint ce “trop” qui vient en elle, cet afflux de sensations, d’harmonie, de bien-être, cette _surabondance_ dangereuse (qui va basculer), cet excès d’odeur (comme un fauve migraineux), de réminiscences qui la soufflètent, etc.  
Toute sa vie elle a gardé sa cartographie de faiblesse rainurée de bois tendre, visible en glacis sous son teint diaphane comme à la fenêtre étroite donnant sur une cour obscure elle calquait de petits dessins qu’elle appliquait sur la vitre ; par là encore, comme avec les crayons de couleur de Glatigny, elle était proche d’Arthur. Par ces muscles involontaires elle faisait circuler à vif les hontes et rages de l’époque rougissant de riens pour les autres. Elle avait des visions en coin d’œil aussi, à la Frankie Adams, au printemps, sur le sol poudreux des chemins ou faux vives lancées sur le sol chez elle : pas de parquet, du carrelage ; pas de plafond, des solives ; une vaste cheminée.