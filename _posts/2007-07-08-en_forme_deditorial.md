---
title: "En forme d'Éditorial"
date: 2007-07-08 11:55
fichiers:
    - le-dit-d-hier-et-de-deux-mains.pdf
sous_titre: "Le Dit d'Hier et des Deux Mains"
date_document: "Aujourd'hui"
tags:
    - document
    - DAO
    - texte
---

_Le Dit d’Hier et des Deux Mains_

Les deux prêtres discutent dans la pièce et je ne suis pas là. C’est cinq ans avant que j’apparaisse dans ce monde ; il est question de la Vallée des Ombres, de la Chine, du musée des Horreurs avec cordes et hâches qui ont servi à tuer, et de cette image d’un chien qu’on a chassé mangeant un étron trop glaireux sur les traces du Christ et faisant la grimace ; il y a des fleurs qu’on voit dans le jardin par la croisée et je n’existe pas.

_(lire la suite…)_
