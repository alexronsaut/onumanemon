---
title: Carte Chinoise
sous_titre: ""
date: 2024-08-18 14:46
date_document: "Après 1984"
tags:
    - document
    - calligraphie
    - Cosmologie Onuma Nemon
fichiers:
    - "/Carte-Chinoise.Calligraphie.Encre-de-Chine.Papier-Nepal-et-Arches.Format-raisin.jpg"
---

Calligraphie. Encre de Chine. Papier Népal et Arches. Format raisin

Don à la Bibliothèque Kandinsky de Beaubourg.
