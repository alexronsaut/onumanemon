---
title: Chinois à SteCroix
sous_titre: ""
date: 2024-08-18 14:46
date_document: "Après 1984"
tags:
    - document
    - dessin et gravure
    - Cosmologie Onuma Nemon
fichiers:
    - "/Chinois-a-SteCroix.Dessin.Fusain-Crayon-format-raisin.jpg"
---

Dessin. Fusain & Crayon, format raisin

Don à la Bibliothèque Kandinsky de Beaubourg.
