---
title: Carte du Désert
sous_titre: ""
date: 2024-08-18 14:46
date_document: "Après 1984"
tags:
    - document
    - peinture
    - Cosmologie Onuma Nemon
fichiers:
    - "/Carte-du-Desert.Peinture.Aquarelle-sur-papier-Nepal.30x50cm.jpg"
---

Peinture. Aquarelle sur papier Népal. 30x50cm

Don à la Bibliothèque Kandinsky de Beaubourg.
