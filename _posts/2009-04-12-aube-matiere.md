---
title: "Aube-Matière"
date: 2009-04-12 12:42
fichiers:
    - aube-matiere.mp4
date_document: "1967"
tags:
    - document
    - Cosmologie Onuma Nemon
    - video
---

Court Métrage 16mm Noir et Blanc Muet de Nany Machin.

**Réalisation Roland Collas.**
