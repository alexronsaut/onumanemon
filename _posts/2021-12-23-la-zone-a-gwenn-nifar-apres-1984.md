---
title: "La Zone (à Gwenn Nifar) après 1984"
date: "2021-12-23 12:13"
sous_titre: "Performance"
date_document: "2012"
fichiers:
- "/la-zone-a-gwenn-nifar.jpg"
tags:
  - document
  - sculpture
  - Cosmologie Onuma Nemon
---

Châtaignier : tronçonneuse, pinceau encre noire, crayons graphite. Format 6 cms x 46 cms x 46 cms.
