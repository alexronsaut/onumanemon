---
title: "Célébrages. Poèmes de Typhaine Garnier"
date: "2023-04-28 21:41"
date_document: "Février 2023"
fichiers:
    - /celebrages.pdf
tags:
    - document
    - texte
    - DAO
---

Au fil des mois, la nature ressort ses appâts, et chaque fois on s’y laisse prendre. Avant-printemps : premiers chatons aux branches nues, primevères, etc. Surprise, ravissement, on est ému comme si on avait oublié ces « tours » pourtant vus autant de fois qu’on a d’années ou presque. Il y a de quoi enrager. Les « Célébrages » sont les chromos d’un calendrier perpétuel qui tente de fixer cela une bonne fois pour toute, et qu’on n’en parle plus. Heureusement, c’est raté.

Arno Schmidt : « Le plus fiable, ce sont encore les beautés de la nature. Ensuite les livres, puis un rôti braisé choucroute. Tout le reste est versatile et vain. »

On se reportera ici même aux _Absolus_ de Onuma Nemon, petites et très anciennes proses dont le projet était proche de cet ouvrage-ci, tout récent.
