---
title: "Cent Cibles Trouées : N°16. La Pierre Sacrée"
date: 2014-02-09 21:50
fichiers: 
  - 16.La_Pierre_SacreeS.PLanche.jpg
sous_titre: "Acrylique blanc sur Cible 17 x 17cm pour pistolet 10m" 
date_document: "Après 1984" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - peinture
---

À Sylvie