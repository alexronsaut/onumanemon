---
title: "Bouillons du Dernier Cri"
date: 2007-11-08 22:24
fichiers: 
  - 310.pdf
sous_titre: "Livre Poétique de Nycéphore 1968-1984. Futur Antérieur. Poème n°15" 
date_document: "Fin 1969" 
tags:
  - document
  - OGR
  - texte
---

**15. Bouillons du dernier cri**  
  
La supposition est mince :  
On voit clair !  
À travers la barbe puante de poisson  
Du moujik voleur de chevaux ;  
Zinaïda la première.  
  
Mille falots des exilés qui passent  
Sur la grand’route de Sibérie,  
Chaînes aux mains et aux pieds :  
Promenade aux flambeaux des spectres.  
Sur les côtés : cavaliers sabre au clair, révolver au poing.