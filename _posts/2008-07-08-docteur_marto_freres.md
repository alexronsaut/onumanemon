---
title: "Docteur Marto & Frères"
date: 2008-07-08 20:44
fichiers: 
  - Docteur_Marto__Freres.pdf
sous_titre: "Les Grands Ancêtres. Ligne de Don Qui. Été. Buenos-Aires" 
date_document: "1984" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

Ici le morceau rythmique de la fin est représentatif de la façon dont la langue devient le portrait filmique lui-même de la disparue, mitraillage kaléidoscopique.

_I. Revay_

Ça y est : par deux trous dans le cercueil ils descendent le corps de la mère directement dans le congélateur. Le mari et son frère de 14 ans enfant de chœur sont arrivés par surprise la nuit à l’Hôpital ; ils ont atteint le corps dans la chambre froide, lui ont administré des piqûres d’antigel par voie intramusculaire, l’ont enlevé : une ambulance les attendait. Puis, dès la maison de la Plaza Constitución, ils ont mis l’épouse dans le cercueil avec de la neige carbonique et ensuite ils l’enveloppent dans du papier cellophane pour la mettre dans le congélateur. Ils font partie de la secte Orphiste.  
Elle est passée des communs à la crypte, la grotte, la crupta, la crotte qui a été recreusée dans sa voûte pour pouvoir ouvrir le couvercle.  
Le Docteur garde sa femme cryogénisée avec lui, parce qu’elle n’a pas voulu d’enfant, comme un espoir lointain, et, enfermé dans cette crypte, il passe son temps à se projeter sans cesse d’anciens films de sa vie en 9,5 mm noirs et blancs, au-dessus du bourdonnement du corps mort. Monica était la sœur de Monique, de la Folie-Méricourt, musicienne accomplie comme elle. Il est né en 48. En 84, il était persuadé que le monde allait se retourner, il pensait qu’on allait rentrer dans _le Siècle d’Or et l’Ère d’Orphée_, et pour rentrer dans ce Siècle d’Or, il lui fallait absolument cryogéniser son épouse et tous les êtres chers de sa famille.  
Ensuite toutes les personnes mortes de la Tribu de Prosper et des Frères Naskonchass lui ont été fournies par les Docteurs Civière, Decaisse et Dufourgon sis place de La Monnaie, les plus mauvais de Bordeaux. Comme ils s’occupaient beaucoup de ceux-là, tous en sont morts assez rapidement. Quant au Docteur Pantoja, il se chargeait de l’export des corps.  
À présent, au centre de la crypte, le gros congélateur est celui de Roméo et Juliette, puis vient celui des amants Colomb et Souley, de Bordeaux, et ensuite toute la liste des cadavres du Docteur ; ça fait un bruit d’Enfer !