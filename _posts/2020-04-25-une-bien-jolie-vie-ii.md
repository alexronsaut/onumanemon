---
date: 2020-04-25 12:04
title: Une bien jolie vie. II
sous_titre: Texte de Joël Roussiez
date_document: "2018"
fichiers:
  - "/une-bien-jolie-vie-II.pdf"
tags:
  - texte
  - DAO
  - document

---

**Une bien jolie vie II**

Au soir de sa vie en la ville de Vannes, un marchand sans histoire raconta qu’il avait senti ton bonheur tandis qu’il mangeait des radis à la table d’une auberge...

_(lire la suite…)_
