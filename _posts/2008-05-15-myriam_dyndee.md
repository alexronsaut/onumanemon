---
title: "Myriam Dyndee"
date: 2008-05-15 22:11
fichiers: 
  - Mario_Dyndee.Terre_.pdf
sous_titre: "Les Ophelins Colporteurs. Ligne de l'Hospice. Terre" 
date_document: "1988" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

Ce texte fait partie de “Quartiers de ON !” paru en 2004 aux éditions Verticales

Avant d’arriver au chalet des Alpes, Mario était bloqué par la chute de neige sur l’autoroute, avec Myriam Dyndee qui partait aux Indes. Elle voulait de là-bas prier pour eux, envoyer de la pensée positive au Mouvement, et “se rincer avant cela dans la blancheur” !  
Il lui dit (_il se voyait la sauter à toute force sur le siège arrière dans des ondulations et des pressions de son tube comme un arum précieux et contourné, s’imaginait qu’elle le suce tête de ballon fou avec des retraits formidables,   
l’aspiration terrible du vide, les mille ressources de la muqueuse et les dix mille détours de la langue, jusqu’à la fracassante gelée, qu’elle le branle avec une brassée de fleurs exotiques, les mouvements du bassin et des hanches des poignets, et les crépitements cristallins des pas de danse des doigts sur le terrain de la concentration pelvienne, pour le moins !_)