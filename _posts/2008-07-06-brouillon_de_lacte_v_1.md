---
title: "Brouillon de l’Acte V (1)"
date: 2008-07-06 17:35
fichiers: 
  - LeDauphin.Esquisse_de_lActe_V1.jpg
sous_titre: "Le Dauphin" 
date_document: "1964" 
tags:
  - document
  - LOGRES
  - LOGRES (épanchements)
  - texte
---

L’archivage lui-même de ce drame en vers n’ayant pas encore été fait, nous ne sommes pas en mesure de dire s’il s’agit du plan définitif.  
_Isabelle Revay_