---
title: "L’Exaltation, le Magnéto"
date: 2010-07-28 22:05
fichiers: 
  - LExaltation_La_Chasse.pdf
sous_titre: "Lettre à Dominique Poncet" 
date_document: "2005" 
tags:
  - document
  - HSOR
  - texte
---

(_**Lettre à Dominique Poncet. 2005.**_)  
  
Cher Dominique,  
À propos de _l’exaltation_ et de _la chasse_ (les deux sont liées !), pour poursuivre ce que je disais à Veinstein, j’utilise surtout la course, le sac ou la corde sur des temps d’une heure et plus. Didier du reste a réalisé, en 75 ou par là un des premiers enregistrements au sac de frappe, d’une telle durée, en sous-sol de béton, avec un son très métallique.  
Lorsque je cours j’emporte souvent avec moi une liasse de feuillets écrits, et c’est seulement vers la fin, quand le cœur est prêt à exploser (midi, été : moment et saison du cœur), que je relis cela littéralement sur les hauts-plateaux ou à travers les pentes diagonales, entre l’horreur des Nuits et la Neige du jour. Les morceaux qui ne sont pas assez tendus tombent d’eux-mêmes. Puis je reprends l’ensemble la plupart du temps au magnétophone de poche, cette fois-ci en marchant. Quand je ne cours pas (“katas, sac, corde” suivant les saisons), je travaille aussitôt “dans la foulée” de l’entraînement : le corps “entraîne” l’écriture. Mais la nouvelle diction doit être reprise à son tour par l’écrit, car la voix et l’exaltation ne sont pas une preuve d’efficacité, de même que le bonheur d’écrire ne prouve rien de la qualité de l’écriture, comme disait à peu près Gracq. En tout cas, il y a une _cadence_, une prosodie explicite.  
  
_lire la suite…_