---
title: "La Nappe"
date: 2017-05-01 22:00
fichiers: 
  - nappe.pdf
sous_titre: "Histoire Deux. Livre de Nycéphore" 
date_document: "1976-1982" 
tags:
  - document
  - OGR
  - texte
---

**La Nappe** La Nappe, voilà comment j’imagine le début du monde, avant tout nom. Mais ce connard de traquenard n’en a rien à faire, ni cette vielle pute de machine à écrire Adler, réincarnation de Chancel comme Duras l’est de Sagan, celle qu’on appelait _la Thénardier_ avec son Jules à la mie de pain, poète à trous multiples : j’ai jamais vu Cosette, mais je connais Valjean. Ou du moins si, je comprends ce nom, c’est parce qu’ils n’arrêtaient pas de _faire causette_. Une niaise causette sans effet aucun.  
« Docteur ! Docteur ! Je m’excuse : vous voulez bien m’acheter des mensuelles ? C’est là que j’écris mon journal. »  
  
La Nappe, mais prise dans un mouvement… pas du tout le lac de la Tranquilité. Ça non, alors. Ça serait une tout autre hypothèse. La nuit, on réfléchit.

(_lire la suite…_)