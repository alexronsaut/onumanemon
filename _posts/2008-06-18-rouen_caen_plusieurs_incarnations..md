---
title: "Rouen & Cæn : plusieurs incarnations."
date: 2008-06-18 09:19
fichiers: 
  - Toyrangeau.Rouen_Caen.pdf
sous_titre: "Les Anartistes. Toyrangeau. Saison de la Terre" 
date_document: "1982" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_(Usines de la Toussaint)_  
À Rouen passe au petit jour le cortège d’un jeune homme de 25 ans qui va être fusillé, plein d’émotion et de malfaisance. Il a la sensibilité infinie de vérité de celui qui a rédigé le procès de Jeanne d’Arc, ici-même.  
Il a été amené de Fresnes dans la nuit. Long couloir de la cathédrale d’acier :  
« Au revoir Béraud ! Adieu Combel ! »  
Voiture Amilcar noire jusqu’au poteau dressé au bas d’une butte de gazon, surplombant le fleuve.  
Il refuse le bandeau sur les yeux. Il crie à ceux qui le mettent en joue:  
« Courage ! » Et il est tombé.  
Anoxies du bonheur, erreurs de la colère, envolées…  

\*

Toyrangeau : “Je devais me lever très tôt à Rouen, parti de Tours hier avec Nini Ruth pour retrouver entre autres Nicolaï et Charette à l’Atelier des Décors (il montait un spectacle là-bas, comme souvent, et ailleurs ; il y connaissait tout le monde ; on devait y choisir des éléments de décors pour Cádiz entre autres) et je m’éveillai en sursaut (il faisait crûment froid), croyant que la petite pendule sur la table de nuit m’avait oublié. Il n’en était rien et son battement cardiaque me parut assourdissant, s’associant à une douleur fulgurante du deltoïde qui provenait d’un effort fait en rêve, de transport de caisses très lourdes, que je venais d’abandonner précipitamment. J’enfilai aussitôt un tricot de laine angora et replongeai.