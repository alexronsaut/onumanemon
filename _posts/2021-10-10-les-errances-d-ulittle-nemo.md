---
date: 2021-10-10 17:00
title: "Les errances d'Ulittle Nemo"
sous_titre: "Os de Poésie de Nycéphore"
date_document: "Après 1984"
fichiers:
- "/les-errances-d-ulittle-nemo.pdf"
tags:
- texte
- OGR
- document

---

Enfin une saison ! Ça faisait longtemps ! Saluons-la ! Où va Ulysse il y fait froid : lucarne ouverte de l’atelier, soudain très sombre ! La pluie très forte, comme sur le plafond, sur les désespoirs de poète et le muguet fâné. Il est sur nos genoux, le gros chat, le griboux, à articuler ses pluriels.  
Rue Charlot, Tour du Temple : les jeunes filles du matin, font tournoyer le dé de cristal triomphant du printemps dans la violence du zapateado.  
Je me souviens, rage étrangère à la France, d’un pays de tartes welshes, confitures, avec une langue rauque qui vocifère ; je n’ai jamais voyagé qu’une seule fois dans des cafés enclos par de minuscules ruelles, et vu grâce à cela des splendeurs baroques.

C’est magnifique : on attend la neige ou la guerre, l’attaque des Aigles, les oiseaux noirs passant comme des obus ; dans la guitoune à gaufres face au manège, dans un moment d’émotion pure, Eva laisse une rose devant la porte, portique de lueurs fameuses à travers les larmes ou la pluie…  
Heureusement immérité argent éblouissant et souvenances du futur : boules de neiges dans l’azur et de noirs klaxons dans les rues ; glacis de grâce où les œillets de neige vont,

_(lire la suite…)_

**

Ce poème en versets détaille beaucoup d’aspects de Bordeaux. Nous le dédierons en particulier aux deux amis Didier Morin et Bernard Plossu (qui a aimablement revisité et photographié les quartiers des États du Monde). Tous deux grands voyageurs comme Ulittle Nemo.  Curieusement j’y ai retrouvé des souvenirs d’Arras en 1986 proches des images de Plossu et cette litanie incantatoire aussi qui est une sorte d’absolu pour lui :“On continue !”  
Nous le dédierons aussi à Alain Vallet, d’une Tribu gitane.  
O. N.
