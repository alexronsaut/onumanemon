---
date: 2019-12-12 5:23
title: J'ai tout arrangé
sous_titre: 'Joël Roussiez'
date_document: "2011"
fichiers:
- "/J’ai tout arrangé .pdf"
tags:
- texte
- DAO
- document

---
**J’ai tout arrangé :** (Shakespeare, R. Musil)  
J’ai tout arrangé pour ma disparition, mes livres, mes papiers sont à la disposition de mes proches. Je ne lègue pas mon meilleur lit puisqu’aujourd’hui ces choses n’ont plus de valeur. Je ne lègue donc rien que des écrits d’auteurs et quelques uns de moi.

(lire la suite…)