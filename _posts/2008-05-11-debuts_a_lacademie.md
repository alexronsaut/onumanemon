---
title: "Débuts à l’Académie"
date: 2008-05-11 18:13
fichiers: 
  - Debuts_Academie.pdf
sous_titre: "Ligne des Adolescents. Aube & Nany" 
date_document: "1974" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

Ce texte est inédit

En arrivant à l’Académie, Nany fut enthousiasmé par le ballet des étudiants transportant des panets avec des maquettes de déco-volume dessus ; tout ce va-et-vient effervescent correspondait à la préparation de l’exposition qui aurait lieu en fin d’année ; ces maquettes représentaient rarement de grands ensembles architecturaux ; simplement des aménagements de pavillons ou de villas, mais il n’avait jamais vu un tel agencement de matériaux “propres” sans trace de colle, sauf sur ses maquettes d’enfant d’avions et de bateaux : vitres de rhodoïd, bristol blanc des parois, menuiseries de balsa et de peuplier… l’œil glissait parfaitement là-dessus, et par ces villas à ciel ouvert on pouvait apercevoir jusqu’au moindre aménagement des salons, chambres, cuisines et salles de bains : petit poufs de carton recouverts de tissu, lavabos et faïences issus des chambres de poupées, achetés chez Verdeun, rideaux aux fenêtres. Parfois, projet de luxe, on apercevait une piscine. Il était ébahi par ce luxe en Gulliver admirant des demeures miniatures, presqu’à chercher leurs habitants, ébahi par la tenue printanière des étudiants, les chemises de coton bleues, l’époustouflant lilas mauve fleuri, la verdeur d’amande des tilleuls, celle plus soutenue des marronniers avec les cloches blanches pour des Pâques perpétuelles, le vernis des pavés de l’entrée et le crissement du gravier sableux de la cour arrière aux rares cailloux grisés, et par l’énorme fraîcheur recelée par l’énorme bâtisse de pierre de l’ancien Couvent Bénédictin des frères défricheurs.  
Ces matériaux, ces tenues, cette mise à dispositon de tous les ateliers, bénéfiques, son ombre rapide et fraîche la première fois qu’il fut dans la galerie du premier étage, projetée sur les arbres en contrebas (ressurgis avec la même intensité quand il en parle), il ne ferait plus tard qu’à peine les entrevoir, prenant des œillères au fur à mesure pour focaliser sur les actions, mais il aurait perdu cette vision périphérique, cette exaltation ronde du monde et de l’œil, comme l’éblouissement des rayons sur le pont en venant, puis sur une montre au poignet d’une fille venant en face, et cette voiture aux enjoliveurs clinquants qui curieusement avait gardé ses phares pour rouler en plein soleil (feu droit plus puissant que le gauche) en toute incongruité avec la verdure partout exultante et la félicité poudreuse plus loin vers les coteaux.  
Toute cette magie de la pratique se perdrait avec _la restriction du champ_, car il ne trouverait aucun plaisir à faire des maquettes aussi bien en déco plane qu’en déco volume.