---
title: "Musil Définitif"
date: 2008-03-07 22:37
fichiers: 
  - Musil_definitif.pdf
date_document: "Septembre 2004" 
tags:
  - document
  - DAO
  - texte
---

Cet article de Joël Roussiez est paru dans l'Atelier Du roman N°39

**Formellement :**  
Je ne traiterai pas de la fameuse ironie de R. Musil qui a déjà été souvent analysée et que l’on peut voir s’annoncer dès le début du livre par la référence à la météorologie énoncée de manière scientifique mais disqualifiée aussitôt par des personnifications et par un récapitulatif amusant « autrement dit, si l’on ne craint pas de recourir à une formule démodée mais parfaitement judicieuse : c’était une belle journée d’août 1913 . » Ce début bien connu en reprend de similaires ; chez Scarron par exemple : « le soleil avait achevé plus de la moitié de sa course et son char, ayant attrapé le penchant du monde, roulait plus vite qu’il ne voulait. Si ses chevaux eussent voulu profiter e la pente du chemin, ils eussent achevé ce qui restait du jour en moins d’un quart d’heure…. Pour parler plus humainement et plus intelligemment, il était entre cinq et six quand une charrette entra dans les halles du Mans. » ou chez Sénèque (d’après Paul Veynes) : « Déjà le dieu du soleil avait raccourci son trajet, déjà le sommeil voyait augmenter son horaire et la Lune…En d’autres termes c’était déjà octobre… ». J’avancerai néanmoins par ces exemples que, chez R. Musil, ce procédé est moins ironique qu’il n’y paraît, c’est à dire qu’il est moins essentiellement critique. Car, s’il semble être d’abord un moyen d’amuser et de s’amuser par une distanciation, il est peut-être aussi l’expression d’une joie à découvrir dans la formule surannée une justesse tout aussi grande que dans la description météorologique qui précède. Il s’agirait alors, davantage qu’une ironie, du plaisir de voir s’accomplir par la langue et la pensée une sorte d’unité. On pourra remarquer ainsi qu’il sert la description de la ville qui suit, laquelle ne présente aucune distanciation mais au contraire amplifie la présence émotionnelle de la ville par l’emploi de nombreux adjectifs impertinents, de métaphores et d’un phrasé mélodieux caractéristique du lyrisme. C’est ainsi que derrière distanciation et humour, ce roman affirme un lyrisme qui trouvera son exaltation thématique dans l’amour fraternel \* et l’Autre Etat.

Publication : [Contre-Feux](https://web.archive.org/web/20081216013335/http://www.lekti-ecriture.com/contrefeux/) (archives)