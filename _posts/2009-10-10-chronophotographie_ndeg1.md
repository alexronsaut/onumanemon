---
title: "Chronophotographie n°1"
date: 2009-10-10 15:12
fichiers: 
  - AuteurRoger.Chronophoto1.jpg
sous_titre: "Christian Roger" 
date_document: "1984" 
tags:
  - document
  - DAO
  - photographie
---

On se souvient des photos de Christian Roger réalisées pour l’ouvrage consacré à Gaudier-Brzeska par les éditions Tristram, mais on connait moins bien ses photographies au temps de pose excessif qui donnent à ses personnages des allures de fantômes dans un univers familier à la fois nocturne et cristallin. 

Ici il s'agit de deux chronophotographies réalisées pour le numéro O du bulletin DAO. 

_Isabelle Revay_