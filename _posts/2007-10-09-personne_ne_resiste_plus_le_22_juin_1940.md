---
title: "Personne ne résiste plus le 22 juin 1940"
date: 2007-10-09 13:57
fichiers: 
  - 295.mp3
sous_titre: "Les Gras en Guerre" 
date_document: "1989" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - son
---

Pages sonores inédites. Disque 2. Piste 31.  
Réalisation de Philippe Prévot dans les studios de LIMCA à Auch.