---
title: "Studebacker et Les Archaïsmes"
date: 2008-01-13 11:36
fichiers: 
  - Logre__OGR.Carte_5.Annexes_1_et_2.pdf
sous_titre: "Carte n°5. Annexe 1 & 2" 
date_document: "1997" 
tags:
  - document
  - OGR
  - texte
---

Ce texte est explicatif des tensions et des archaïsmes à l’œuvre dans le continent OGR de la Cosmologie et de la façon dont ces archaïsmes ont subsisté à la façon d'un _enkystement_.  
  
  
Les archaïsmes de forme (alexandrins ou octosyllabes dans la première partie du “Tome Poétique”, construction romanesque traditionnelle de “Roman” et de “Phényx, Styx, X”), ne doivent pas être traités aujourd’hui à la façon du mensonge post-moderne. Il n’y a jamais eu, il n’y aura jamais de dépassement des nouvelles formes par les anciennes.  
Du reste les formes dont il est question ici, ont été abandonnées par l’Auteur, comme les vieilles peaux des mues successives.  
La particularité tient seulement à ceci que l’abandon a pris sans doute plus de temps que chez d’autres, et surtout que ces vieilles enveloppes ont continué parfois à insister en même temps que de plus “modernes” avaient vu le jour (par exemple certains poèmes de 1964 ou 1965 sont traités en vers libres, tandis que d’autres reprennent des formes traditionnelles ; plus rarement, mais cela a lieu aussi, les deux sortes de formes coexistent dans le même texte).  
Autre particularité : elles ont subsisté alors que “l’environnement” de l’Auteur (radiophonie, contacts, spectacles, évènements) s’exprimait tout à fait autrement, comme un enkystement (à la façon du B.K. de la Tuberculose).  
Cet enkystement avait sûrement à voir avec la présence du frère inclu en soi, persistant et s_**a**_ignant parfois à la façon d’une maladie dans le temps, d’une affection du Temps. Mais il ne s’y réduit pas.