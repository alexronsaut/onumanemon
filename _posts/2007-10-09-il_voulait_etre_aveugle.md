---
title: "Il voulait être aveuglé"
date: 2007-10-09 13:56
fichiers: 
  - 292.mp3
sous_titre: "Les Orphelins Colporteurs. Jean à Paris" 
date_document: "1989" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - son
---

Pages sonores inédites. Disque 2. Piste 27.  
Réalisation Philippe Prévot dans les studios de LIMCA à Auch.