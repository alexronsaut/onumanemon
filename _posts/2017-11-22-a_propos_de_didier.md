---
title: "À propos de Didier"
date: 2017-11-22 10:37
fichiers: 
  - autour_de_didier.planches.pdf
sous_titre: "Lignes des Escholiers Primaires. Trio des Enfants Malades." 
date_document: "1980" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

_(_ À propos de Didier _fait partie du Tome III (non publié) des_ États du Monde _, consacré aux Enfants, en particulier aux_ Lignes des Escholiers Primaires _, et encore à l’intérieur de celles-ci du_ Trio des Enfants Malades _. Le texte lui-même, par contre, a été publié par une revue étrangère._  
_Didier, c’est le frêre disparu de Nycéphore et Nicolaï._  
_Mais pour le coup le texte ici est d’abord un hommage à “deux vrais frères”, à savoir Didier Morin et Bernard Plossu, qui ont considérablement aidé à la publication de la Cosmologie._  
_Didier Morin avec tout le tournoiement du Vortex de Mettray._  
_Bernard Plossu qui a eu le cœur de faire tout un reportage dans le quartier de ces Enfants : Saint-Michel de Bordeaux._  
_“Des mecs réglo” aurait dit Burroughs.)_  
  
Le quai brille à présent avec sa densité de néons en premier plan, tel qu’on le voit depuis les toits de la rue Carpenteyre. C’est _Lui_ d’abord qu’on voit en arrivant. À peine sevré du lait maternel, pour ainsi dire le lendemain, Didier se leva en souriant _avec des dents noires_ ! Il en avait peu, mais ça suffisait : il avait mordu à la Mort dans la nuit et montrait à présent le vaisseau qui mène au pays d’Orphée, amarré sur le quai Sainte-Croix. Cavité, ventre, crématoire, locomotive ; tout à la fois. Cette teinte d’encre qui gagnait tout avait pénétré au cœur même de la porcelaine ; on eut beau lui laver la bouche tant et plus, rien n’en partit ni ne déteignit. Dès lors, il tomba malade, gardant toujours ce sourire atroce de Saint jusqu’à la fin, ce sourire insupportable !  
L’ombre des dents se reportait partout, et il se mit seulement à hurler en mourant ; un très long cri silencieux, bouche démesurément ouverte : on n’entendait aucun son, mais au fond de sa gorge, au lieu de la luette on voyait le champignon atomique ! On avait oublié de le porter en riant dans toute l’enceinte de la maison et surtout à travers le Jardin Noir, de répandre sur lui l’eau lustrale ; il ne restait que la lettre Z, ballante, accrochée à un clou en bas de l’escalier, sur la porte vers l’Atelier. José était débordé.  
  
_(lire la suite…)_