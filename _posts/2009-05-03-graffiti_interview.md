---
title: "Graffiti (Interview)"
date: 2009-05-03 20:44
fichiers: 
  - Graffiti_Interview.pdf
sous_titre: "La Bande à Jésus. Été. 4" 
date_document: "1973" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

**Extrait de la version définitive des _États du Monde_ (en cours de réduction).**  
  
« Marette : C’est les deux premières notes ça ; c’est le _la_ et c’est “pouec pouec”. Hi ! Hi ! Hi !  
Françoise : Elle est con, Marette ! C’était la poésie des cafés de Poitiers à ceux de Dijon. _Les alcolytes anonymes_ comme dit Jérôme. Avant y’avait eu les rencontres avec Raoul dans le Brabant, mais son bouquin a été publié grâce aux “provos” et pas grâce à Queneau qui l’avait soutenu. De ton côté, toi Nany, tu rencontrais Engel après “Dans la jungle des villes”. Après avoir été séparés, on a été _surexposés_.  
Riri : Les “Provo” sont devenus les “Kabouters”, après l’échec de leur mouvement. Moi j’ai rencontrés à Amsterdam Roel Van Duyn. Ses références c’était Marx, Kropotkine, Paul Goodman et Dada. Comme je prépare un bouquin sur la dissolution des avant-gardes, ça m’a intéressé. Ils traînaient peu avec les situationnistes qu’ils trouvaient arrogants et méprisants, stupides, sauf Constant, l’architecte, vite exclu du groupe.  
Françoise : Y’a l’Orange-free-state. Ils occupent des centaines de maisons abandonnées et les restaurent. On ne les expulse pas.   
Anne : Puis y’a les free-clinics.  
Françoise : Moi j’ai vu surtout les fermes biologiques à la campagne, leurs magasins coopératifs et gratuits de produits naturels qui servent de lieux d’agitation et leurs services d’aide aux vieux.  
Riri : Les Kabouters organisent le Provotariat d’avant-garde avec leur journal _Panique_. De la main gauche on installe l’utopie dans le vieux monde, comme un champignon qui va proliférer ; de la droite on attise le feu et on attaque l’ennemi. Ils sont en lien avec les Diggers à San-Francisco…  
Anne : Ringolevio, c’est un bouquin que Jésus adore.  
Riri : Puis avec It et Oz en Angleterre.  
Françoise : À Londres c’est l’été dernier, non, qu’il y a eu le procès d’Oz ?