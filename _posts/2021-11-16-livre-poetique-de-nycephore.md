---
date: 2021-11-16 16:00
title: Livre Poétique de Nycéphore
sous_titre: ""
date_document: 1964-1984
fichiers:
    - "/livre-poetique-de-nycephore.pdf"
tags:
    - texte
    - document
    - OGR
---

Le Livre Poétique de Nycéphore est un recueil de 240 pages qui s’étend sur vingt ans (1964-1984). Il a pour pendant un volume semblable de Nicolaï. Cela ne résume pas toute la partie poétique de l’œuvre qui comprend également des Os de Poésie (de 1984 à 2000), des poèmes en prose, poèmes sonores, etc. Ceci vaut également pour Nicolaï.

On a laissé l’intégralité du volume sans aucun choix, même si il y avait des faiblesses, à cause des correspondances en vis-à-vis entre les deux frères de chacun des poèmes. Il y a beaucoup de “remplissage versifié” : en 1964 l’auteur avait 15 ans, et il était curieusement à la fois dans quelque chose de très actuel proche de Cendrars ou la Beat Generation dans son travail radiophonique et tout une série de poèmes qui en dérivaient ; et de complètement archaïque dans d’autres poèmes ou dans l’écriture de Roman,  proche de son espace de réclusion. Les deux on co-existé longtemps.

S’il y avait un choix de l’auteur ce serait là où la versification laisse la place à l’aspect hypnotique et incantatoire. On a développé ailleurs cette problématique autour de figures qui semblent traverser les siècles et qui ne tiennent pas à la prosodie.
