---
title: "Des radis de Bègles à la Rosière de Pessac"
date: 2010-07-27 12:05
fichiers: 
  - freud.Hs_.jpg
sous_titre: "Freud. Introduction à la psychanalyse" 
date_document: "1916. Traduction française S. Jankélévitch. 1922" 
tags:
  - document
  - DAO
  - texte
---

**Et de Cendrars, dans _L'Homme Foudroyé_ :**  
(à propos des surréalistes)  
“Je n'aimais pas ces jeunes gens que je traitais d’affreux fils de famille à l’esprit bourgeois, donc arrivistes jusque dans leurs plus folles manifestations.”  
(à propos de Charles-Albert Cingria et par extension de Gide)  
“Ah ! Ces pédérastes (1), le pauvre et génial raté !”  
(1) “Pour la définition de ce terme voir les pages 671 et 672 du _Journal_ d’André Gide (Bibliothèque de la Pléïade. N.R.F. Paris 1941). Oh ! Chochote, que de mensonges, de complaisances, de clichés, d’hypocrisies, de crises de nerfs, de vantardises, de poses, de vanités, de larmes de crocodile, d’esthétisme, d’art, de morale dans ce journal intéressé tenu par un hystérique qui écrit devant son miroir : « _Chaque pensée prend un air de souci dans ma cervelle ; je deviens cette chose laide : un homme affairé_. » (page 195).  
Je sors ahuri de cette lecture de 1332 pages comme si j’avais relevé les inscriptions de 1332 pissotières de Paris que sont les chapelles littéraires. André Gide : le maquereau des grands hommes. Il lui faut tout le Panthéon : Goethe, Shakespeare, Dostoïevsky, Stendhal l’Égotiste et l’exemple du _Journal_ des Goncourt pour le mettre en train ; mais quand il y est, il enfilerait le piano, et vous le place. Quel maniaque !”  
_Geneviève Vivian_