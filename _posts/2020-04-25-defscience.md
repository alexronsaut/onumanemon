---
date: 2020-04-25 12:10
title: Defscience
sous_titre: Texte d'Alain Montesse
date_document: 1er Janvier 2015
fichiers:
  - "/defscience-v1.pdf"
tags:
  - texte
  - DAO
  - document

---

On trouvera ici un texte emblématique d'Alain Montesse, poète sonore, cinéaste, mathématicien et performer… qui a fait partie de la belle équipe du Poisson-Lune à Bordeaux dans les années 1966-1968

_NDLR_
