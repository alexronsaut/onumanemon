---
title: "Terre de Grogne"
date: 2010-03-23 12:01
fichiers: 
  - Terre_de_Grogne.pdf
sous_titre: "Livre Poétique de Nycéphore 1964-1984 Poème n°35" 
date_document: "Décembre 1965" 
tags:
  - document
  - OGR
  - texte
---

**35. Terre de Grogne** Les hommes dans les forêts tirent  
Sur leurs caoutchoucs indécents ;  
C’est bon comme le somme est bon !  
  
Dans le glossaire de leurs veines  
Levant une aile ensanglantée  
Entre leurs deux très gros poumons.  
  
Seules les races se fondront  
Dans l’herbe en fin de matinée  
Plus chaude et fourrée de soleil.  
  
À l’instant, le seul givre n’est  
Que cette tension souveraine  
Nous embarassant de buées !  
  
Quand s’allument toutes les plaines,  
Grappes noires de sensations  
En bas du chemin gras de boue,  
  
C’est une lueur dynamite,  
Un humanisme paysan  
Pris d’une confusion humide.  
  
Leur soupe de poireaux s’avale  
Avec des cailloux sans couleurs ;  
Inaltérables forces, bris. **\***  
 _etc._  
  
  
Ce texte, jadis édité dans un version ultérieure plus longue (596 vers) par Tristram & DAO, dans la première partie du Livre Poétique qui faisait partie des “livraisons périodiques”, retrouve ici sa première version de 393 vers.  
Il s’agit d’_enfermer Dieu dans un cristal_, une bouteille. La litanie produit une nappe de signifiance (plus qu’une musique) sans aucun sens nominal.  
De la connotation pure sans aucune dénotation ?  
Il y a des actions, même des héros, mais la question n’est pas là. C’est un bain incantatoire.  
D’autres poèmes ont ainsi, à la façon japonaise, enclos quelques parcelles d’or dans une laque obscure.  
Le but était pour certains lieux magiques détruits depuis (le Phœnyx d’Arlac, des passages secrets du Peugue et de la Devèze, le Grand entrepôt de Lescure…), de les sceller hermétiquement, quitte à les rendre définitivement incompréhensibles pour ceux qui ne les auraient pas connus (et qui de toute façons vont disparaître aussi), inaccessibles à jamais et lancés dans le vide pour un tournoiement infini jusqu’à ce qu’une machine à remonter le Temps surgisse.