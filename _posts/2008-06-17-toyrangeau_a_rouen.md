---
title: "Toyrangeau à Rouen"
date: 2008-06-17 11:55
fichiers: 
  - Toyrangeau.Rouen_.Leonard.Pipes_.pdf
sous_titre: "Les Anartistes. Saison de la Terre" 
date_document: "1982" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_**Léonard et des pipes**_  
“Voici une voiture américaine défoncée, blanche, sale ; puis tout un vieil autobus de défoncés à leur tour, les poches pleines de doses, avec dans les pognes des frontignans de Graves, qui traverse la ville arrivant droit du mont Tessin.  
Darnes soudaines du cauchemar ! Plus de julot vedette, ni de sexe à la fleur d’anis. Dès qu’elle boit trop, elle perd son bikini, Nini. “Donne-nous un an, et je partirons ! Sinon je m’avangerai !” Multiples pluriels, chez elle. On a raclé ce seul argument par quoi elle aurait pu être émue, Nini Ruth :  
« C‘est par ici, qu’il est mort ! »  
Nini nous parle du grand mort et du petit mort, de sa conception dans le Vent par la bouche ouverte, alors qu’elle survolait, là-haut, le Château, grâce au rhume et à la codéïne. J’adore quand on parle de Léonard, avec Nini.  
_“Je porterai toujours ce sourire de visage envisagé pendant le voillage, et sans jamais me dévêtir ! L’excessive force de la lumière est à ce prix. Tout est comme une neige silencieuse désormais.”_  
« Il aurait lu ça dans Milan, tu vois ! Un milan, un vautour... Le   
vautour, c’est la Mort, c’est la Mère. Les souvenirs d’enfance sont   
débauchés par elle.  
— Quand j’étais couché dans Marie, près du couvent Sainte-Monique, la première nuit, j’ai pensé : “Voilà que j’épouse la mer Maorte ! Sacré coup au cœur !” » dit Nicolaï.