---
title: "Chemin de Maître-Jean"
date: 2008-07-22 21:59
fichiers: 
  - LIdiot_et_autres_fous.pdf
sous_titre: "Les Escholiers Primaires. Le Singulier. Automne" 
date_document: "1989" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

Rue Maître-Jean : La Clinique. Elle devait recevoir la visite d’un ami. Sonnette, arrivée sur gravillons ; Marguerite est bien là. À moins que ça soit Madeleine…