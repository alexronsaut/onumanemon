---
title: "Martin le Monteur"
date: 2009-05-03 20:53
fichiers: 
  - Martin_le_Monteur.pdf
sous_titre: "Aube & Nany. Terre. 5" 
date_document: "1973" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

**Extrait de la version définitive des _États du Monde_ (en cours de réduction)**  
  
Martin, le monteur a 29 ans depuis deux mois exactement, ce 11 novembre 1973. Il sent le Chili pris dans un piège autocratique comme celui d’Arturo Hui. Jusqu’à son anniversaire il était immortel ; plus maintenant. Il se souvient de la phrase qu’elle lui a dit à propos d’Allende en bas de la rue Washington après lui avoir acheté un gâteau : “Vous n’êtes plus un collégien !”  
Il a attendu longtemps que Nany le rejoigne tout en faisant des dérushages et des débuts de mixage ; il devait lui amener des bruits de littoral andalou enregistrés voilà quatre ou cinq ans qu’il voulait filtrer, écouter à plusieurs vitesses, essayer de décrypter. Ne le voyant pas venir, il va partir. Nany se sera probablement trompé de studio.  
Martin aime les instruments rarissimes, vielles ou autres, reconstitués et construits d’après des tableaux, des fresques ou des sculptures.  
Toute la journée il a été nappé d’informations qui ne l’accrochent pas (car il considère que chacun a son son, selon le plus dénominateur non commun), à part les bandes enregistrées de Wafa du 7 octobre dernier faisant état d’attaques d’appareils sionistes dans la région de Ain Atta au Sud-Liban et de la riposte des commandos palestiniens ayant détruit la station radio en langue arabe installée à Jérusalem. Puis d’un bombardement sioniste sur le mont Hermon. À par ça “Le vent, Léon, le feu, les bruits du petit matin dans la rue du Cardinal-Lemoine, le culte d’un blanc vaudou, un safari…” Le plan de la table de mixage sembla s’incliner jusqu’à tomber, prise dans une durée de pestilescence masochiste affreuse, surchargée des confidences dramatiques venues des quatre coins du monde. Rien qui saute à l’oreille, qui d’un coup fasse jaillir le mort hors du trop-plein des veillées mortuaires, ces innombrables veillées que constituent la plupart des dramatiques radiophoniques comme cette mauvaise parodie de Guitry où ce dernier figurait comme personnage en majordome pétomane dans une pièce anale où l’on ne faisait que manger. Ou encore comme celle de cette nuit dont le narrateur assistait à la mort d’une mère Marie ou d’une tante Lulu lors d’une première séance de cinéma ; et il hésitait à assister à la deuxième séance pour revoir ça alors qu’il avait rendez-vous avec sa plus jeune fille, déjà atrocement malheureux d’avoir assisté voilà quelque temps à une opération inutile et sans nécessité aucune sur le corps de son autre fille plus agée ; c’était totalement catastrophique et il pleurait énormément. Toujours est-il qu’il revenait à un moment, et c’était pour la messe funèbre tenue à l’église, totalement effondré. Puis il sortait dans une ville de fantaisie, de fréquence fantasmagorique, féérique, de connivence avec des filles qu’on retrouve en douce des autres… méandres, méandres, commerce, immense traîneau dans la neige en relation avec des femmes connues.