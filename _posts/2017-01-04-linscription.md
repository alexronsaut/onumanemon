---
title: "L'Inscription"
date: 2017-01-04 18:34
fichiers: 
  - linscription.pdf
sous_titre: "Ce qui compte n'est jamais là" 
date_document: "2014" 
tags:
  - document
  - HSOR
  - texte
  - edition
---

Paru dans la revue [Mettray n° 7](https://mettray.com/revue/mettray-s02-n07), en Septembre 2014. Sans doute le commentaire le plus précis de l'horizon de la Cosmologie.

_I. Revay_