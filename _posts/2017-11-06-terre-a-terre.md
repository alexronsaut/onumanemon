---
title: "Terre-à-terre"
date: 2017-11-06 12:00
fichiers: 
  - g.venier._terre-a-terre.couv_.dao_.pdf
  - g.venier._terre-a-terre.p1.pdf
  - g.venier._terre-a-terre.p_2.pdf
  - g.venier._terre-a-terre.p3.pdf
sous_titre: "Ouvrage poétique de Gilles Venier" 
date_document: "Éditions Encres Vives 2017" 
tags:
  - document
  - DAO
  - texte
---

_On ne saurait trop recommander de lire (d'entendre), ce lyrique Chant du Monde, qui dans la fraîcheur magnifique de son inventaire fait penser à toute l'époque Linnéenne de Paul-Armand Gette, ce redoutable explorateur des lisières et zones de bordures, friches et autres…_

_NDLR_