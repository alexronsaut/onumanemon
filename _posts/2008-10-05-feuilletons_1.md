---
title: "Feuilletons ! 1"
date: 2008-10-05 18:47
fichiers: 
  - Feuilletons__1.pdf
sous_titre: "(fragment)" 
date_document: "14 juillet 1968" 
tags:
  - document
  - HSOR
  - texte
---

_Ce texte fait partie d’une suite dite “Feuilletons !” de 1968 et 1969, ensemble non destiné à la publication, dont nous donnons un fragment._  
  
La tour sombre du sapin, au bout de l’allée.  
Nathalie parlait de très loin,  
Unie à d’autres difficiles signes  
Inclinés, se déversant en limaille.  
  
Propriétés des retours  
De rameaux bruissants  
Sous les ombres du vent.