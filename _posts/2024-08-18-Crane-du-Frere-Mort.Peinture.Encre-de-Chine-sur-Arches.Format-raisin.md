---
title: Crâne du Frère Mort
sous_titre: ""
date: 2024-08-18 14:46
date_document: "Après 1984"
tags:
    - document
    - peinture
    - Cosmologie Onuma Nemon
fichiers:
    - "/Crane-du-Frere-Mort.Peinture.Encre-de-Chine-sur-Arches.Format-raisin.jpg"
---

Peinture. Encre de Chine sur Arches. Format raisin

Don à la Bibliothèque Kandinsky de Beaubourg.
