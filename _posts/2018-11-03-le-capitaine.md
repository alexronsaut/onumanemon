---
title: Le Capitaine
date: 2018-11-03 00:00:00 +0000
sous_titre: Marges des États du Monde
date_document: Après 1984
fichiers:
- "/le-capitaine.pdf"
tags:
- texte
- Cosmologie Onuma Nemon
- document

---
LE CAPITAINE 

On l’appelle aussi “Capitaine”, comme ça, lorsqu’il trouve  de beaux décors au plus loin, avant des voisinages sinon indéterminés mais du moins innommés.

_(lire la suite…)_