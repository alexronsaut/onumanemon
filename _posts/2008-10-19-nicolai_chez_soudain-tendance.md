---
title: "Nicolaï chez Soudain-Tendance"
date: 2008-10-19 18:40
fichiers: 
  - 2._Nicolai.SoudainArlac.pdf
sous_titre: "Les Escholiers Primaires. Ligne de Nicolaï. Été" 
date_document: "1978" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

J’avais rêvé, en entrant dans le monde de la photographie, de devenir reporter-criminel, mais pour rien au monde je ne voulais _finir_ en tireur commun avec une chaîne cernant les pieds cadenassée autour de la colonne du Durst, comme tous ceux qui travaillaient là-dedans et qui n’avaient même pas le droit d’aller pisser : on leur apportait un pot en plastique (il n’y avait pas de _tireuses_ !), ni même, version luxe, à faire des diapos de fonds de papillons et de fleurs le week-end comme en commettaient le couple Soudain, tellement parfaits, tellement anglo-saxons (or pâle) et tellement cons, avec la sangle de leurs deux Rollei passant en travers de leurs deux Lacoste.  
« I doit être tourné vers la Noël, un tel film, un tel crâne de caméra, et y’a des individus glauciers qui mijotent dans le scénario ! »  
La caméra est tombée. On va pas la laisser tourner par terre tout l’été. Il ne faut être en retard ni sur le rêve ni sur l’instruction d’une découverte philosophique due à une substance non numérale. On ne peut non plus laisser passer ça sans bouger du pif. Aussi la présence du petit con à mobylette derrière Arlette et moi, sur l’Intendance, face au porche du premier Studio Soudain, m’énerve.  
« Qu’est-ce qu’il y a ?  
— C’est parce que vous me fixez, je bouge pas. »  
J’avais la chance d’être mineur. Je l’ai fixé sans hésiter d’un coup de lame et davantage, un mois plus tard. Qu’on annonce la bonne nouvelle ! Ensuite, on pourrait filmer les inondations. Il y a eu à peine un entrefilet, personne n’a cherché.