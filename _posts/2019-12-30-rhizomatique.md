---
date: 2019-12-30 1:02
title: Rhizomatique
sous_titre: Stéphane Breton
date_document: "2017"
fichiers:
- "/Stéphane Breton.pdf"
tags:
- texte
- DAO
- document

---
**Avant-propos**

On couche des mots pour saisir une idée, une forme, une expérience, un sentiment, une impression.

On tente ainsi de découvrir un essentiel. On apprivoise alors des réalités, des mondes, des imaginaires.On parcourt de nouvelles terres.

Ce travail d’écriture, de semis, de tamisage, est le résultat de rencontres, d’échanges réels ou rêvés avec des passagers de la vie – passés ou présents. Sans eux, rien ne serait réellement possible.

Les lignes de cet ouvrage sont le passage à une brièveté qui m’est chère car elle est une correspondance sans destinataire connu. Ne pas s’appesantir, juste s’appuyer pour rebondir, aller en-deçà et au-delà. Un même mouvement. Une brièveté faite pour effleurer un inachevé, tisser des liens vers des horizons indéterminés.Une reformulation permanente pour saisir ce qui est, ce qui advient.

Comme un rhizome, ces lignes s’entrelacent, se ramifient dans tous les sens. Elles se transforment l’une l’autre et peuvent parfois se traverser. Elles ne sont ni linéaires ni hiérarchisées. Elles s’efforcent de se connecter de manière hétérogène et de se perturber de façon imprévisible. Elles sont rhizomatiques.

J’espère que vous aurez le temps de passer sur ces mots comme un marin sur les mers sans rien espérer ni vouloir. Peut-être même dans un état de confusion, de veille troublée, de conscience narcotisée, il pourrait se passer quelque chose que vous seul estimerez alors singulier. Un intérêt pourrait se dévoiler. Une intention se produire. Une question se poser. Un rien exister.

Paris, novembre 2017

_(lire la suite…)_