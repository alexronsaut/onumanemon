---
title: Chat de Chine
sous_titre: ""
date: 2024-08-18 14:46
date_document: "Après 1984"
tags:
    - document
    - calligraphie
    - Cosmologie Onuma Nemon
fichiers:
    - "/ChatDeChine.Calligraphie.Encre-de-Chine-sur-Arches.Format-raisin.jpg"
---

Calligraphie. Encre de Chine sur Arches. Format raisin

Don à la Bibliothèque Kandinsky de Beaubourg.
