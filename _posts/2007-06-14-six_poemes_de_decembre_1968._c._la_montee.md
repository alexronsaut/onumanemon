---
title: "Six Poèmes de Décembre 1968. C. La Montée"
date: 2007-06-14 13:02
fichiers: 
  - 186.pdf
sous_titre: "Livre Poétique de Nicolaï. 1968-1984. Pressent. Poème n°6C" 
date_document: "Décembre 1968" 
tags:
  - document
  - OGR
  - texte
---

**_C. La Montée_**  
  
Elle a cheveux de baillon tendre  
Ramenés par ma main dont l’odeur…  
Bruns, roux, forts, doux !  
  
Œuvrée contre moi quand ses lèvres  
De cruauté sur le morfil,  
Or visible par la fenêtre  
Où les yeux de l’Été pénètrent.  
  
Parmi les fleurs elle est terrible !  
Chaleur déversée des fumiers,  
Nuque mordue au fond des greniers.  
  
Nacre et ventre,  
Étoiles de lait,  
Cuisses ! Serpents, lianes,  
O sa toilette, Osyris,  
T’as vu ? !