---
title: "Après Roman : OR"
date: 2018-10-03 00:00:00 +0000
sous_titre: ""
date_document: 26 Août 1991
fichiers:
    - "/apres-roman-or.pdf"
tags:
    - texte
    - OR
    - document
---

Arrivé à ce point du récit dans l’Écriture (lequel ?), fortin fort peu prenable, il s’agissait de fabriquer de l’Or avec le matériau brut de tous les jours et à partir de la bibliothèque constituée, et ceci _pour un nouveau réalisme_ (Faulkner, Rimbaud, Claudel même ; on était très loin de Zola) ; prendre la cythare d’Orphée dans une transe Quechua pour _chanter le monde_, au lieu de le décrire, _transformer ce qui est privé de connaissance_.

_(lire la suite…)_
