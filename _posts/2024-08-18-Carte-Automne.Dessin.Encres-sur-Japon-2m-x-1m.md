---
title: Carte Automne
sous_titre: ""
date: 2024-08-18 14:46
date_document: "Après 1984"
tags:
    - document
    - dessin et gravure
    - Cosmologie Onuma Nemon
fichiers:
    - "/Carte-Automne.Dessin.Encres-sur-Japon-2m-x-1m.jpg"
---

Dessin. Encres sur Japon 2m x 1m

Don à la Bibliothèque Kandinsky de Beaubourg.
