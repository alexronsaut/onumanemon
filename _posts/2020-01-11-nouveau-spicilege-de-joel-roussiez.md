---
date: 2020-01-11 10:37
title: Nouveau Spicilège de Joël Roussiez
sous_titre: Ensemble de textes inédits
date_document: "2020"
fichiers:
- "/Y être sans y être.pdf"
- "/trois cavaliers.pdf"
- "/Douce demeure.pdf"
- "/Comme des pinceaux légers.pdf"
- "/Ce qui vient et s’en va.pdf"
- "/Au cheval qui boit.pdf"
- "/Ah.pdf"
tags:
- texte
- DAO
- document

---
On se reportera impérativement au site de [la Rumeur Libre](https://www.larumeurlibre.fr/auteurs/joel_roussiez), l'éditeur de Joël Roussiez pour trouver les derniers volumes parus, et en particulier l'ensemble de récits _Au Verger des Anciens_.

_Une bien jolie vie I et II_ sont extraits de _Livres de courtes proses III_ et _Vivre sa Vie_ de _Livre de courtes proses II_. Ce sont des recueils inédits…

NDLR