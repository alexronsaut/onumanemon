---
date: 2019-12-30 1:05
title: A propos de Rhizomatique, de Stéphane Breton
sous_titre: ''
date_document: Après 1984
fichiers:
- "/à propos de Rhizomatique.pdf"
tags:
- texte
- DAO
- document

---
à propos de _Rhizomatique_ de Stéphane Breton

Il y a toujours un grand bonheur aphoristique : celui de partir avant de développer, ou de cristalliser et de fuir. De fouetter plutôt que d’enserrer en risquant contraindre.

Même si Stéphane Breton (dont je ne sais _rien_, pas même son âge), m’en veut de le réduire de cette manière, c’est avec cette sorte d’emportement de non-philosophe que je l’ai lu, les branches brisées derrières soi, les feuilles ouvertes, désas- tre ou apocalypse, mais l’univers encore heureusement frais de ses blessures comme on est à vingt ans.

_(lire la suite…)_