---
title: "La Descente au Jardin"
date: 2007-11-05 10:02
fichiers: 
  - 307.pdf
sous_titre: "Livre Poétique de Nycéphore 1968-1984. Poème n°11" 
date_document: "Pâques 1969" 
tags:
  - document
  - OGR
  - texte
---

**11. La Descente au Jardin**  
  
Allons ! Totalité frugale,  
Premier jour !  
Toutes brillantes des ondées du rêve,  
Offertes dans l’urne du matin,  
Devant,  
S’envolent, claquent,  
Les tourterelles !  
  
On ne revient jamais aux mêmes endroits du jardin.  
(Toujours j’y replonge !)  
Tête envolée sous le figuier  
De dahlias, couleurs et désordres…  
  
(Là-bas  
Quatre Pavillons  
Quinze mois,  
Dernier somme ;  
  
Masse de boue devers l’École ;  
Carte des tertres de hasard.)  
  
Le tilleul, puis  
La touffe d’arums, puis  
Le ciel d’eau intangible et nue,  
Plus noire que sapinière.