---
title: "La Chine en Déconstruction et Robespierre & Saint-Just"
date: 2012-03-20 23:07
fichiers: 
  - ChineEnDeconstruction.jpg
sous_titre: "Exposition URDLA Printemps 2012" 
date_document: "Après 1984" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - dessin et gravure
---

Le premier travail de dessin est réalisé sur le lange ayant servi à l’impression des gravures jusqu’en 1984.  
Le second, de sculpture, comprend une toile noire en trapèze irrégulier reposant, arête contre arête sur la plaque d'acier taillée en guillotine de la presse qui a servi à imprimer les gravures jusqu’en 1984.  
Entre les deux une série de bois gravés de crânes et un masque du Pays des Morts.  
_NDLR_