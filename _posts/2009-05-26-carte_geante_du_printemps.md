---
title: "Carte Géante du Printemps"
date: 2009-05-26 22:58
fichiers: 
  - Carte_du_Printemps.jpg
sous_titre: "Encre de Chine, Crayons de couleur, Aquarelle" 
date_document: "2005" 
tags:
  - document
  - OR
  - dessin et gravure
  - extension
---

Cette carte de 3m x 3m faisait partie de l’exposition au _Quartier_ en 2006