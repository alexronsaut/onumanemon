---
title: "Amélie Derlon"
date: 2010-08-01 10:46
fichiers: 
  - Amelie_Derlon.pdf
date_document: "Octobre 2009" 
tags:
  - document
  - DAO
  - texte
---

Il y a une nonchalance dans la vidéo, celle qui sans doute exaspère lorsqu’elle devient maniérisme, mais il y a surtout comme le tapis du temps qu’elle retire sous le pas des acteurs, comme pour passer de l’existence à l’essence, comme pour creuser ce qui d’habitude ne jouit que dans son emportement.

Le cinéma c’est l’art du mouvement, l’émotion première du saisissement de la vie en fuite, avec les fabuleux débuts de la chronophotographie, la vie même en train de naître sur la pellicule grâce à ce spasme, cette saccade de la griffe, diastole et systole, obturation et lumière. Au contraire, dans son lisse non fractionné, la vidéo creuse le temps, l’évide, pouvant tomber tout aussi bien dans la nuit et dans la vacuité. L’acteur est sur un tapis roulant : il court sur place.

_lire la suite…_

Commentaire de plusieurs œuvres d’une jeune artiste de Marseille, entre autres vidéaste, qui a travaillé avec Didier Morin. 

_NDLR_

Publication : [Amélie Derlon](http://www.enba-lyon.fr/postdiplome/0910/?id=derlon)