---
title: "Les Ennemis de Juan"
date: 2014-07-27 19:04
fichiers: 
  - Les_Ennemis_de_Juan.pdf
sous_titre: "La Tribu des Maigres Tendres. Trio Juan, Manolín, Norberto" 
date_document: "1975" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

Ce siècle avait vingt et un ans ; Juan réunit dès qu’il sut marcher les auteurs de ses jours dans un cauchemar commun : des fantoches en conjuration voués à sa perte. 

Enfant, lorsqu’il était seul avec son père ou sa mère, il s’exprimait librement, mais dès que tous les deux étaient conjoints, c’était la méfiance, le doute, la contraction intérieure qui devenait une tétanie mentale. C’était une coalition, il ne pouvait plus rien dire de personnel ; il lançait des généralités comme un clou chasse l’autre. Parfois l’alerte disparaissait, il se sentait pleinement rassuré pour un temps très bref. L’absence de contradiction le fortifiait dans l’idée qu’il n’avait rien d’anormal. Sa mère, quand il partait en pension, lui confectionnait un plat qui devait lui faire plusieurs repas, et elle lui donnait des boîtes de conserve, d’huile, de sucre, de pommes de terre, etc. Et elle lui laissait de l’argent liquide pour s’acheter des magazines et des sucreries. C’est lui qui avait réclamé cet éloignement de la pension dont il n’y avait nul besoin, son école se trouvant dans le Quartier. 

Dès sa jeunesse Juan avait appréhendé la cause de sa misère comme dûe à un complot de ses “ennemis de lisière”, comme il disait. Il en souffrit par paliers avant de devenir complètement inconsidéré. Un jour il aperçut un vêtement oublié sur un banc de la petite allée qui menait au jardin des Abattoirs tandis que deux buses traversaient son ciel ; il en conclut on ne sait quel pressentiment féroce, ainsi que de la vue de la villa abandonnée cimentée de moellons artificiels blanchâtres, à quelque distance de là, avec un cèdre grandiose au-devant ; il en retira la certitude d’une sorte de scansion impersonnelle comme des humeurs du monde, hors les mots. 

L’Abbé Depardieu de Saint-Michel qui exerçait en même temps que le Père Bonnet l’avait attiré à lui, mais c’était celui qui s’amusait beaucoup à faire tournoyer les filles et dont on voyait le caleçon tandis qu’il tournait. Juan eut beaucoup de mal à s’en défaire et il connut alors des bouffées délirantes : les tentures de la sacristie en forme d’oriflammes rouges le poursuivaient partout ; il voyait se lever le bras armé de Saint Michel qui allait projeter sa lance au travers de son corps ; le sang du tissu rejoignait le sang qui sortait en bouillonnant de ses naseaux infectés, car il souffait de sinusites aiguës. 

  


_(lire la sute…)_