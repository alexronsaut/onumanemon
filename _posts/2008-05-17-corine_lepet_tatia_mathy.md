---
title: "Corine Lepet & Tatia Mathy"
date: 2008-05-17 16:54
fichiers: 
  - Corine_Lepet__Tatia_Mathy.pdf
sous_titre: "Les Orphelins Colporteurs. Ligne de l'Hospice. Terre" 
date_document: "1991" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

Texte inédit

**Mario** : “C’est à ce moment-là que je reçois le télégramme ne   
comportant qu’un diagramme qui m’appelle auprès d’une jeune fille, Corine Lepet, résidant dans une station d’hiver près d’Innsbruck toute l’année. Le diagramme dessine l’onde de choc d’un projectile fusiforme.  
Madre de Dios ! Quel flux ! S’agit-il d’une assignation (dès la page 9999 !) dans cette commune située à 1040 mètres d’altitude, ou bien du cri du fauconnier pour déloger le gibier d’un buisson, sinon de la coupure qui fait si bien communiquer deux dépressions ensemble ? Plutôt du “trobar clus”.  
À travers l’écarté des feuilles : le flaquement des ailes et le cri des grues, malgré la grève.