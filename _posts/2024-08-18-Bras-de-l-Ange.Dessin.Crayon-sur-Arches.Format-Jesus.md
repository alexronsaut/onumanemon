---
title: Bras de l'Ange
sous_titre: ""
date: 2024-08-18 14:46
date_document: "Après 1984"
tags:
    - document
    - dessin et gravure
    - Cosmologie Onuma Nemon
fichiers:
    - "/Bras-de-l-Ange.Dessin.Crayon-sur-Arches.Format-Jesus.jpg"
---

Dessin. Crayon sur Arches. Format Jésus

Don à la Bibliothèque Kandinsky de Beaubourg.
