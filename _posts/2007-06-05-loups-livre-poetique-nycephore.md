---
title: "Loups"
date: 2007-06-05 15:19
fichiers: 
  - 133.pdf
sous_titre: "Livre Poétique de Nycéphore 1968-1984. Poème n°9" 
date_document: "Janvier 1969" 
tags:
  - document
  - OGR
  - texte
---

Ce poème a paru en 1969 dans “Mangane”, la revue des Voyous de Saint-Michel et fut diffusé également en radio.  
Isabelle Revay