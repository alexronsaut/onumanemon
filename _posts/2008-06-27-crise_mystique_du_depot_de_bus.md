---
title: "Crise Mystique du Dépôt de Bus"
date: 2008-06-27 10:05
fichiers: 
  - Nicolas.Commandements.pdf
sous_titre: "Les Adolescents. Ligne de Nicolas. Été" 
date_document: "1966 & 1971" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_**Crise Mystique du Dépôt des Bus**_  
Celui qui vient du Dépôt des Autobus passant sous des platanes et des tilleuls sur les Boulevards vers le cimetière, mais de l’autre côté sous les pins parasols de l’allée des Pins (il ne s’en souvient guère !) a vu “Irène soignant saint Sébastien” mais moins intéressant que les Commandements qui lui furent dictés.  
C’est Nicolas Zemacks, toujours soigneux, pli et revers sur un pantalon de laine gris, fine silhouette issue de l’Est, brume et toux incessante de fumeur invétéré, suivi par une quantité d’insectes et d’animaux de toutes sortes, notamment les blattes.  
Il dit : “Dieu, j’attends des jours le fade artifice des rosaces défleuries, des Rois de Gloire qu’on défenestre, et le Carnaval éprouvé, pour noter sur un terrain d’égalité les tombes aux teintes détestées jadis enfant.  
Par une romance mécanique sotte aux pieds nus du joli village, j’appelle à boire le sang noir des victimes qui circule dans le rocher, les ombres confondues des Jeunes Filles dans la cire et des Anges demeurés.  
Orphelin, j’ai grandi en perpétuant cette féérie !”  
Il va.  
Ce paquet de chair étroite comme un gnome rouge, choit.  
Belles noctambules, arômes indiens, piments désunis.  
Champ où le seul jeu est le croquet avec des maillets d’os et des boules de peau.  
Chaque cri lance un écho vers une cible invisible.