---
title: Labyrinthe 4
sous_titre: ""
date: 2024-08-18 14:46
date_document: "Après 1984"
tags:
    - document
    - 
    - Cosmologie Onuma Nemon
fichiers:
    - "/Labyrinthe4.Dessin.Encre-de-Chine-sur-Arches.Format-raisin.jpg"
---

Dessin. Encre de Chine sur Arches. Format raisin

Don à la Bibliothèque Kandinsky de Beaubourg.
