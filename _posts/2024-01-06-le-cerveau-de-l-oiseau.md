---
title: Le Cerveau de l’Oiseau
date: 2024-01-06 8:48
sous_titre: à Typhaine
date_document: 2023
tags:
    - document
    - Cosmologie Onuma Nemon
    - dessin et gravure
fichiers:
    - "/le-cerveau-de-l-oiseau.jpg"
---

Encre et taches diverses sur papier de Chine pour idéogrammes. Format 52 x 25 cm
