---
title: "Mémoires du Caporal Paul Tesson"
date: 2010-01-17 19:24
fichiers: 
  - Memoires_du_Caporal_Tesson.pdf
sous_titre: "Livre Poétique de Nycéphore 1964-1984 Poème n°32" 
date_document: "1939" 
tags:
  - document
  - OGR
  - texte
---

_Cette suite de vers écrits au dos de cartes postales répond dans le Livre Poétique de Nicolaï au poème de 23 vers_ À Garde ! **32. Mémoires du Caporal Paul Tesson**  
  
Toujours dans les sentiers les rares ambroisies,  
L’obligation du front aux _coulères_ soudaines ;  
“Mon Colonel, voici vous offrir l’hérésie  
D’un réserviste plein de poudres et de haines !”  
  
_etc._