---
title: "D'après “Discipline” de Throbbing Gristle"
date: 2011-09-05 00:00
sous_titre: "Vidéo originale d’Amélie Derlon Cordina" 
date_document: "2010" 
tags:
  - document
  - DAO
  - video
---

15’30’’  
  
Notes :

En automne, dans un paysage sylvestre isolé, un cycliste enveloppé par cette nature, la  
traverse, la gravie et redescend, on le suit jusqu'à le perdre.

C’est le partial et imparfait portrait d’un homme-à-vélo dont on ne sait pas où il va, ni d’où  
il vient, comme continuellement entre un point A et un point B. Il pourrait être un natif de  
Tlön ("Ficciones" de Borges), sans futur ni passé, juste continûment dans un présent. Nous  
le prenons en cours de route, le suivrons, puis dans la descente nous le laisserons et lui  
nous sèmera.

Ce film naît de THROBBING GRISTLE, de leur morceau « Discipline »\* filmé au Kezar  
pavillon à San Francisco, en 1981. Genesis Breyer P’Orridge envisagé comme un «demi-  
sage» : sa dévotion pour le « I want discipline » qu’il use. Mon film est une libre  
interprétation de ce moment, faisant de cette transe une tentative de sport à haut niveau.  
Pour un cyclisme aux acharnés héros : Jewey Jacobs ("Le Surmâle" d’Alfred Jarry), le  
Chronos (Maurice Roche) ou les échappées de « Sunday hell » (Jorgen Leth).

Amelie Derlon Cordina

ﾠ

Pour ceux que ce travail intéresse ou passionne, la première projection de _Mange tes morts_ de Amélie Derlon Cordina (film vidéo le plus récent, et postérieur à _Discipline_), aura lieu dans le cadre de l’exposition collective :

**To Hug a Snake**  
Exposition en résonance avec la 11ème Biennale de Lyon.  
du 10 septembre au 15 octobre 2011   
Vernissage vendredi 9 septembre à partir de 18h30   
Subsistances, 8 bis quai St Vincent, 69001 LYON.