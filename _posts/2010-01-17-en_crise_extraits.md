---
title: "En Crise (extraits)"
date: 2010-01-17 14:33
fichiers: 
  - En_Crise.pdf
sous_titre: "Livre Poétique de Nycéphore 1964-1984 Poème n°29" 
date_document: "23 Mars 1965" 
tags:
  - document
  - OGR
  - texte
---

_À ce poème de 108 vers de Nycéphore répond dans le Livre Poétique de Nicolaï_ Catéchèse. 

I. Revay  
  
**29. En Crise**  
  
\[................................................\]  


 **\***  


  
Les fastes de l’égorgement  


D’une truie blanche dans la cour ;  
Sous nos yeux son débordement  
De vanne atroce sans secours.  
  
L’enfer dans une faille vide,  
Le hurlement populacier ;  
Dans une carène d’acier  
L’horreur toute neuve sans ride.  
  
Les cadavres en rangs serrés  
Que l’on projette en bas des pentes ;  
Et puis tout seul ce vieux sacré :  
Astapovo : fièvre quarante !  
  
_etc._