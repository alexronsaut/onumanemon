---
title: "Cent Cibles : Anges d’Orphée"
date: 2012-02-05 13:15
fichiers: 
  - Anges_dOrphee.jpg
sous_titre: "Acrylique blanc sur Cible 21 x 21cm pour carabine 50m" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - peinture
---

Ces cibles pourraient correspondre aux calendriers des postes pour la nouvelle année. Il y manque la liste des Saints. 

_O. N._