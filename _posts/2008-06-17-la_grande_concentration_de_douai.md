---
title: "La Grande Concentration de Douai"
date: 2008-06-17 10:46
fichiers: 
  - Concentration_de_Douai.pdf
sous_titre: "Folie-Méricourt vers le Nord. Terre" 
date_document: "1991" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
  - edition
---

**Ce texte figure dans _Quartiers de ON !_ paru en 2004 [aux éditions Verticales](http://www.editions-verticales.com/fiche_ouvrage.php?id=116&rubrique=3), augmenté de ses étoilements plastiques, inserts et éléments sonores.**

_Dans cette énorme Concentration de Douai, on trouve toute sorte de pédants, dont Sévèrimus, ce rat de bibliothèque délirant sur l’intertextualité de Rimbaud, qui parle ailleurs des correspondances entre les “Intimités d’un Séminariste” et “Le Petit Chose”.  
Chaque porte qu’on ouvre donne sur un conférencier, un causeur, un débat…_  

Se tient entre autres, dans une des salles, le “Congrès international de sciences Onomastiques”.  
Toute une Tribu puante de singes, de vieux josués d’incantations se pressent dans l’assemblée, à dialecter. Sévèrimus, s’excitant, relance de plus belle ses éternels arguments sur l’intertextualité. Et cette fois sur les emprunts de Rimbaud à Vigny, repris par deux choreutes. Ce n’était pas souvent qu’un rat de bibliothèque de son acabit trouvait à être entendu.  
« Des Peaux-Rouges criards les avaient pris pour cibles, Les ayant cloués nus aux poteaux de couleurs. Rimbaud.  
— Aux harpons indiens ils portent pour épaves Leurs habits déchirés sur leurs corps refroidis. De Vigny.  
— Parfois, martyr lassé des pôles et des zones, Rimbaud.  
— À celui qui soutient les pôles et balance L’équateur hérissé des longs méridiens. De Vigny.  
— Plus fortes que l’alcool, plus vastes que nos lyres... Rimbaud.  
— Plus rare que la perle et que le diamant ; De Vigny.  
— J’ai heurté, savez-vous d’incroyables Florides... Rimbaud.  
— Un soir enfin, les vents qui soufflent des Florides L’entraînent vers la France et ses bords pluvieux. Un pêcheur accroupi sous des rochers arides Tire dans ses filets le flacon précieux.(....) Quel est cet élixir noir et mystérieux. De Vigny.  
— Si je désire une eau d’Europe, c’est la flache Noire et froide où vers le crépuscule embaumé Un enfant accroupi plein de tristesse, lâche Un bâteau frêle comme un papillon de mai. Rimbaud.  
— Qu’il est sans gouvernail, et partant sans ressource, De Vigny.  
— Me lava, dispersant gouvernail et grappin. Rimbaud.