---
title: "Cent Cibles Trouées : N°14. Tirs au-dessus du Pont"
date: 2014-02-09 21:30
fichiers: 
  - 14.Tirs_au-dessus_du_LacbonEric.jpg
sous_titre: "Acrylique blanc sur Cible 17 x 17cm pour pistolet 10m" 
date_document: "Après 1984" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - peinture
---

À Daniel Michiels