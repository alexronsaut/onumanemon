---
title: "Carte de Mort"
date: 2008-05-09 09:02
fichiers: 
  - CarteMortURDLA.jpg
sous_titre: "Ligne du Chaos" 
date_document: "1982" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - dessin et gravure
  - edition
---

Cette gravure sur bois qui figure dans la version définitive des “États du Monde” des éditions publie.net est éditée et vendue par l’URDLA

Publication : [Carte de Mort. Éditions de l'URDLA. 2007](http://www.urdla.com/estampes/estampe.htm)