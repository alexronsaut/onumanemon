---
title: "Nunchaku Lumineux"
date: 2007-03-16 18:27
fichiers: 
  - 93.jpg
sous_titre: "Photo de Christian Roger. 13cm x 18cm" 
date_document: "1982" 
tags:
  - document
  - DAO
  - photographie
---

Déjà publié dans le premier bulletin DAO