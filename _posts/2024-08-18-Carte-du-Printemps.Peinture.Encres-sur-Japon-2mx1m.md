---
title: Carte du Printemps
sous_titre: ""
date: 2024-08-18 14:46
date_document: "Après 1984"
tags:
    - document
    - peinture
    - Cosmologie Onuma Nemon
fichiers:
    - "/Carte-du-Printemps.Peinture.Encres-sur-Japon-2mx1m.jpg"
---

Peinture. Encres sur Japon 2mx1m

Don à la Bibliothèque Kandinsky de Beaubourg.
