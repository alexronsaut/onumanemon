---
title: "La centaine"
date: 2007-10-15 13:36
fichiers: 
  - 270.mp3
sous_titre: "Les Grands Ancêtres. Ligne de Don Qui. Amères Loques" 
date_document: "1989" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - son
---

Pages sonores inédites. Disque 1. Piste 11.  
Réalisation Philippe Prévot dans les studios de LIMCA à Auch.