---
title: "Carte Géante de l’Été"
date: 2009-05-26 23:08
fichiers: 
  - Carte_de_lEte.jpg
sous_titre: "Crayons de couleur, Pastels Gras, Encre de Chine, Encre brune" 
date_document: "2005" 
tags:
  - document
  - OR
  - dessin et gravure
  - extension
---

Carte de 3m x 3m ayant figuré dans l’exposition au _Quartier_ en 2006