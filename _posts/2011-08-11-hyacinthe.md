---
title: "Hyacinthe"
date: 2011-08-11 11:51
tags:
  - billet
---

Hyacinthe a l’habitude de copier, presque malgré moi, mes lettres et celles qu’on m’adresse, parce qu’il prétend avoir remarqué que j’étais souvent attaqué par des personnes qui m’avaient écrit des admirations sans fin ou qui s’étaient adressées à moi pour des demandes de service. Quand cela arrive, il fouille dans des liasses à lui seul connues, et, comparant l’article injurieux avec l’épître louangeuse, il me dit   
« Voyez-vous, monsieur, que j’ai bien fait ! »  
  
_François-René de Chateaubriand. Mémoires d’Outre-Tombe III. Note du 11 août 1836 à propos de lettre à Madame Récamier du 18 mai 1831_