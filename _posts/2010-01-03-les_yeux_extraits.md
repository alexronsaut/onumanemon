---
title: "Les Yeux (extraits)"
date: 2010-01-03 17:12
fichiers: 
  - Les_Yeux.pdf
sous_titre: "Livre Poétique de Nycéphore 1964-1984 Poème n°30 (extrait)" 
date_document: "1965" 
tags:
  - document
  - OGR
  - texte
---

_Ce poème de 91 vers fait pendant à_ Prière Acceptable _dans le Livre Poétique de Nicolaï._ I. Revay _**Noël**_  
Arrière les Lucifériens,  
Saprémie de poisons putrides !  
Dans les sentes de cuir humide  
On se désoriente d’un rien.  
  
On vient de choisir le sapin  
Bien loin des parcs à escargots  
Chez Gootfried, le Tristan des Goths,  
En Alsace, avec des grappins  
  
_etc._