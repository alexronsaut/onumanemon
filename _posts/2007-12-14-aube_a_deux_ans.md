---
title: "Aube à Deux Ans"
date: 2007-12-14 16:28
fichiers: 
  - Aube_a_Deux_Ans.pdf
sous_titre: "Les Adolescents. Ligne d’Aube. Hiver" 
date_document: "1978" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - texte
---

_“Il va réveiller la fille !”_ elle entend. Elle se souvient de sa mère disant cela une nuit où Jean-Paul s’était mis à pleurer.  
Mais non elle ne dort pas. Elle a deux ans à peine et elle entend qu’on parle d’elle et elle l’entend lui, qui pleure.  
C’est son premier souvenir, clair, net, précis. Ils dorment tous les quatre dans la même chambre.  
Aube est dans un lit de fer forgé peint en blanc, habillé d’un tissu bleu sur lequel elle regarde, lorsque le jour se lève ou pendant les siestes, les petits motifs rouges et jaunes cernés de filets noirs.  
Elle y voit des balles, des ballons. Il y a des chiens aussi, quelques oiseaux, des poissons.