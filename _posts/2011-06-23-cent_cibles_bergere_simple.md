---
title: "Cent Cibles : Bergère Simple"
date: 2011-06-23 13:32
fichiers: 
  - Bergere2.jpg
sous_titre: "Acrylique blanc sur Cible 21 x 21cm pour carabine 50m" 
tags:
  - document
  - Cosmologie Onuma Nemon
  - peinture
---

Cette série de Cent Cibles, semblable (selon l’auteur) à une suite de “Calendriers des Postes”, s'apparente à d’autres travaux de gravure au fusil des années 80 et à des gravures sur arbre vivant (émergence des figures de divinités à travers l’écorce). 

_NDLR   
  
Celle-ci est à Claire Viallat_