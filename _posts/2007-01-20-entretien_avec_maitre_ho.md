---
title: "Entretien avec Maître Ho"
date: 2007-01-20 11:45
fichiers: 
  - 45.pdf
sous_titre: "Documents de la Cellule Sabaki" 
tags:
  - document
  - Sabaki
  - texte
---

— Quand est-ce que vous avez connu Onuma Nemon ?  
— Je l’ai connu lors d’une compétition au stade de la Porte Pouchet, à Paris, dans les années 70. Très bon ! Mais il n’a pour ainsi dire pas fait de compétition, sinon universitaire, amicale. On aura su apprécier assez tôt, voir ses qualités inventives.  
— Du reste, le karaté à cette époque-là était essentiellement universitaire ?  
— Oui, c’était un moment de grande utopie. Je crois même que certains venaient pour apprendre le cri qui tue ou acquérir les méthodes infaillibles de Nat Pinkerton. On fréquentait les dojos comme les bibliothèques. C’était en tout cas une époque de recherche, d’expérimentation dans les arts martiaux tout à fait extraordinaire. Il y avait beaucoup d’échanges d’école à école. Oui, oui, on essayait beaucoup de choses qui paraîtraient peu orthodoxes aujourd’hui !  
— De quelle école était-il ?  
— Je crois savoir quelques petites choses sur sa biographie, et en particulier qu’il a abordé le karaté avec le shotokan comme à peu près tout le monde à ce moment-là, au tout début de sa découverte en France, avec Me Kase, rue de la Montagne Ste Genevière, en 65 ou 66. Nous n’étions pas dans le courant international, alors ! Puis vers 67, 68 peut-être, avec le Burdigala Club de la police à Bordeaux, ensuite avec Me Murakami et le shukokaï.  
De là il a rencontré à Paris au Club Corvisart Truong et Dang, deux élèves de Me Nambu, créateur tout récent du sankukaï. Et c’est une école qui lui correspondait parfaitement physiquement : standard-stance, les esquives taï-sabaki, tenshin jodan-uke, tenshin gedan-baraï, tout en cercles, ce qui compte, très beau !  
Ensuite il n’a pas suivi le Nambudo, trop éloigné du contact pour lui, que j’aimais pour d’autres raisons. Mais peu importe.  
Entretemps il a travaillé avec Tokitsu, Kamahora, Tsukada… J’insiste encore, mais ce sont ceux qui cherchaient dans des directions nouvelles en France.