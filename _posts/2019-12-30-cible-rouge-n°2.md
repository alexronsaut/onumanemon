---
date: 2019-12-30 12:56
title: Cible rouge n°2
sous_titre: Plis de dos
date_document: Après 1984
fichiers:
- "/Cible rouge.2.Plis de dos-2.jpg"
tags:
- dessin et gravure
- Cosmologie Onuma Nemon
- document

---
Pistolet 10m & dessin à la plume sur anciens cartons de cibles format 14 x 14cm en 5/10èmes