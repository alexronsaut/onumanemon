---
title: "Ah ! Défaillance, Ezra !"
date: 2007-10-15 13:09
fichiers: 
  - 276.mp3
sous_titre: "Écharde du Paradis" 
date_document: "1989" 
tags:
  - document
  - OR
  - son
---

Pages sonores inédites. Disque 1. Piste 26.  
Réalisation Philippe Prévot dans les studios de LIMCA à Auch.