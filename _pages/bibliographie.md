---
layout: default
date: 2021-11-16T13:13:13.736Z
title: Bibliographie
---
# Bibliographie

## À propos du fondateur du groupe DAO

Dans [Art Press 483 - 484](/files/art-press-483.pdf), décembre 2020 - janvier 2021

## Une bonne façon d'en sortir !

[Onuma Nemon sort de l’anonymat - En attendant Nadeau](/files/onuma_nemon_sort_de_lanonymat_-_en_attendant_nadeau.pdf)

## Lithoral, 30 avril 2017

[Télécharger une copie de l'article](/files/lithoral-etats-du-monde-onuma-nemon.pdf)

## Les Inrockuptibles du 19 au 25 Octobre 2016. Yann Perreau

* [Couverture](/files/inrocks_oct.16_couv.reduite_pour_le_site.jpg)
* [Page 80](/files/inrocks_oct.16.page_.1.pour_le_site.jpg)
* [Page 81](/files/inrocks_oct.16._page2.reduite_pour_le_site.jpg)

## La Cause Littéraire. 23 Janvier 2017. Alain Marc

* [Sommaire](/files/la_cause_litteraire.23.1.2017.sommaire.pdf)
* [Article](/files/la_cause_litteraire.etats_du_monde23.1.2017.pdf)

## Exposition de gravure à la Bibliothèque Nationale. Automne 2016

* [Couverture](/files/expo_bn_automne_2016.couv_.reduite_pour_site.jpg)
* [Article](/files/expo_bn_automne_2016.article.reduit_pour_site.jpg)

## Art Press 441. Février 2017

À propos du premier volume de États du Monde.

* [Couverture](/files/art_press_2.2017.couv_.reduite_pour_site.jpg)
* [Article](/files/art_press_2.2017.article.reduit.pdf)

## Un grand entretien et article de Jean-Emmanuel Denave à propos de l’actualité de l'exposition de la Cosmologie à l’URDLA de Villeurbanne.

Isabelle Revay.

* [Consulter en ligne](http://www.petit-bulletin.fr/lyon/expositions-article-42263.html)
* [Télécharger](/files/Journal_Lyon.pdf)

## Les Mardis Littéraires, 7 décembre 2004

[Les Mardis Littéraires](http://fr.wikipedia.org/wiki/L'Atelier_litt%C3%A9raire). Réalisée par Pascale Casanova,  avec Dominique Poncet, écrivain, fondateur et responsable de la revue La Main de Singe, Claude Riehl, traducteur et spécialiste de Arno Schmidt, Alain Nicolas du journal L'Humanité.

La belle émission de Pascale Casanova qui fut interdite en 2010.

<audio controls src="/files/mardis-litteraires-041207.mp3"></audio>

## Lettre de Roland Barthes de 1972 à propos des premiers éléments de la Cosmologie

Ce qui fut alors transmis à [Roland Barthes](http://fr.wikipedia.org/wiki/Roland_Barthes) (bien qu’extrait du coffre de Cuba), n'avait rien à voir avec la Cosmologie comme ensemble qui ne devait voir le jour qu’en 1984. C’était un ouvrage de plus d’un millier de pages réalisé à une cinquantaine d’exemplaires en linotypie et relié de  façon artisanale par O. N. deux ans plus tôt à Bordeaux dans un atelier d’imprimerie et de reliure où il travaillait alors.\
Son titre était Aperçu d’un Voyage au Pays des Morts, et le personnage essentiel, outre Ulittle Nemo, était Prosper, de la Tribu des Gras. La traversée était émaillée de poèmes, digressions, récifs de voyage, etc.\
Ce qui fut transmis en 1984 à quelques amis était un entassement d’un ou deux milliers de feuilles en vrac dans un carton destiné aux blocs de papier-machine.\
Il y a fort à parier que la version finale ressemblera à ça.\
NDLR

* [Recto](/files/Barthes_20.6.72_Recto.jpg)
* [Verso](/files/Barthes_20.6.72_Verso.jpg)

## Lettre de Jouannais. Art Press. 1995

* [Jean-Yves Jouannais](http://www.editions-verticales.com/auteurs_fiche.php?rubrique=4&id=42)
* [Télécharger le document](/files/Linfame.pdf)

## Des écrivains et des traducteurs aidés en 2008.

[Télécharger le document](/files/AidesEcrivainsARALD.2008.pdf)

## Revue Prétexte n°21-22. 1999. Le seul entretien détaillé sur l'ensemble de la Cosmologie.

[Télécharger l'entretien au format PDF](/files/revue-pretexte-21-22.pdf)

## Cahier Littéraire de Libération du 21 Janvier 1999. Entretien avec Mathieu Lindon

[Télécharger le document](/files/Libe_21.1.1999.pdf)

## Article de Régis Nivelle publié sur onlit.be

[Télécharger le document](/files/regis-nivelle-lithoral-etats-du-monde-onuma-nemon.pdf)

## Une chronique de l’enfance transmuée en chant poétique saisissant

Par Emily Barnett, à propos de *Roman*, dans Les Inrockuptibles du mardi 7 au 13 avril 2009

[Télécharger le document](/files/InrocksAvr09.pdf)

## Onuma Nemon. Vaisseaux de la Métaphore. Propos recueillis par Olivier Lamm. Chronic’Art. Avril 2009.

[Télécharger le document](/files/chronicart-avril-2009.pdf)

## Art Press n°307. Décembre 2004. Article de Yan Ciret.

* [Couverture](/files/ArtPressCiret.jpg)
* [Article](/files/ArtPress307YanCiret.pdf)

## Livre & Lire, mensuel du livre en Rhône-Alpes n°240 - mars 2009

Article de Yann Nicol.

[Télécharger le document](/files/LivreLire_Mars2009.pdf)

## Une chorégraphie d’Onuma Nemon : Roman, par Joël Roussiez

[Télécharger le document](/files/roussiez-sur-lekti-ecriture.pdf)

## À propos de *Roman*, *Danse avec l'Ombre*, par Richard Blin, dans le Matricule des Anges. Mars 2009.

[Télécharger le document](/files/richard-blin-matricule-des-anges-a-propos-de-roman.jpg)

## Menstyle.fr À propos de *Roman* et de la Cosmologie en général

[Télécharger le document](/files/entretien-avec-menstyle-fr.pdf)

## Aperçus de l'Exposition du Quartier. De janvier à mars 2006.

[Télécharger le document](/files/quartier-quimper.pdf)

## Revue Mettray de Didier Morin n°s 1 et 7

[Télécharger le document](/files/mettray-7.pdf)

## Lire. Novembre 2004. Article de Baptiste Liger.

[Télécharger le document](/files/lire-novembre-2004.pdf)

## Entretien téléphonique avec Marc Blanchet. Le Matricule des Anges. Mai-Juillet 1999.

[Télécharger le document](/files/matricule-des-anges-26.pdf)

## Annonce de ON ! Editions Verticales.

[Télécharger le document](/files/verticales-roman.pdf)

## L'Humanité du 4 novembre 2004. Article d'Alain Nicolas.

[Télécharger le document](/files/lhumanité-20041104.pdf)

## Note de lecture de Michèle Grangaud.

[Consulter en ligne](http://www.sitaudis.com/Parutions/on-de-onuma-nemon.php) ou [Télécharger le document](/files/sitaudis-2004.png)

## Chronique de Bartlebooth. 14 Février 2005.

[Consulter en ligne](http://bartlebooth.over-blog.com/archive-02-14-2005.html)

## Bibilographie du CIPM

[Consulter en ligne](http://www.cipmarseille.com/auteur_fiche.php?id=1392)

## Chronic’art. Article de Bastien Roques.

[Consulter en ligne](http://www.chronicart.com/livres/chronique.php?id=3682)

## CIPM. 1er avril 2005. Présentation de la Cosmologie par Dominique Poncet. DVD “Isla de Os” de Didier Morin.

[Consulter en ligne](http://www.evene.fr/culture/agenda/presentation-de-l-oeuvre-d-onuma-nemon-avec-didier-morin-et-domi.php)

## Note de Tex Willer

[Consulter en ligne](http://www.peres-fondateurs.com/forum/viewtopic.php?pid=109213)

## La Main de Singe n°4. 2005.

* [Couverture](/files/MainDeSinge4.Couv_.net_.jpg)
* [Page 22](/files/MainDeSinge4.p22.net_.jpg)
* [Télécharger le document](/files/MainSinge_4.pdf)

## La république Mondiale des Lettres. 2006.

[Consulter en ligne](http://www.larepubliquemondialedeslettres.com/index.php?page=bios/nemon)

## La Polygraphe, de Henri Poncet. Printemps 2004.

[Consulter en ligne](http://www.plumart.com/vf1300/html/body_3313poncet.html) ou [Télécharger le document](/files/henry-poncet-les-auteurs-de-comp-act-1999.pdf)

## Note de lecture de Pierric Maelstaf. 1999.

Télécharger le document : [partie 1](/files/ca-et-la-1999-0.pdf), [partie 2](/files/ca-et-la-1999-1.pdf), [partie 3](/files/ca-et-la-1999-2.pdf), [partie 4](/files/ca-et-la-1999-3.pdf)

## Inculte n°3. Mossberg avec Eric ARLIX.

[Consulter en ligne](http://www.imho.fr/livre.php?lid=10)

## Revue Inculte n° 2.

[Consulter en ligne](http://www.imho.fr/livre.php?lid=9)

## La semaine de France-Culture du 26 décembre 2004 au 1er janvier 2005

[Consulter en ligne](http://palomar.hostultra.com/monk/programmes/prog2004/culture20041226.html)

## Arrnaud Bongrand.Roman.La Revue Littéraire.Janvier 2009

[Télécharger le document](/files/la-revue-litteraire-1.09.pdf)

## Architectonic, à propos de OGR et du Flat Iron.2006

[Télécharger le document](/files/ogr-a-propos-du-flat-iron.pdf)

## Baptiste Liger.Ogr.Journal du Centre.11 Fév.1999

[Télécharger le document](/files/baptiste-liger-journal.pdf)

## Christiane Poulin.Sud-Ouest.Feuilletons C.O.N.Tristram 1995.

[Télécharger le document](/files/christiane-poulin-dans-la-langue-de-personne.pdf)

## Chronicart.Bastien Roques.OGR. 14 Fév.1999

[Télécharger le document](/files/bastien-roques.pdf)

## Noé Peyne.Présentation Ogr.French Book News.Londres.9.1999

Télécharger le document : [partie 1](/files/noe-peyne-1.pdf), [partie 2](/files/noe-peyne-2.pdf), [partie 3](/files/noe-peyne-3.pdf), [partie 4](/files/noe-peyne-4.pdf)

## Site Gilles Arnaud Sphère. Critique Ogr et ON ! Septembre 2008

[Télécharger le document](/files/premier-focus-sur-onuma-nemon-les-rives-de-la-cosmologie-transformante.pdf)

## David Marsac. Pr'Ose ! Revue Dissonances 23.Hiver 2012

Télécharger le document : [partie 1](/files/dissonances-2012.pdf) et [partie 2](/files/dissonances-2012-couv.pdf)

## Jean-Pascal Dubost. Pr'Ose !. CCP n°25.2013

Télécharger le document : [partie 1](/files/jean-pascal-dubost-pr-ose.pdf) et [partie 2](/files/jean-pascal-dubost-pr-ose.couv.pdf)

## Josyane Savigneau.OGR.Il voulait qu'on l'appelle Personne.Le Monde.26.3.199

Télécharger le document : [partie 1](/files/savigneau-1.pdf) et [partie 2](/files/savigneau-2.pdf)

## D.J.OGR.Premier Roman.Nouvel Obs.Avril Avril 1999

Télécharger le document : [partie 1](/files/nouvel-obs-avil-99-1.pdf) et [partie 2](/files/nouvel-obs-avil-99-2.pdf)

## Onuma Nemon crève la cimaise - L'Alamblog

[Télécharger le document](/files/onuma-nemon-creve-la-cimaise-lalamblog.pdf)

## Max Schoendorff.Quartiers de ON ! Le Requiem du requin blanc.Ça Presse n°23.Déc 2004

[Télécharger le document](/files/max-schoendorff-quartiers-de-on-le-requiem-du-requin-blanc-ca-presse-n-23-dec-2004.pdf)
