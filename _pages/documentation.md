---
title: Documentation
---

<p style="color: red; font-weight: bold;">
  ⚠️ Cette documentation est pour le moment obsolète. ⚠️
</p>

# Entrée des artistes

- Rendez-vous sur [Forestry](https://app.forestry.io/)
- Cliquer sur **onumanemon.github.io**

# Créer un Document ou un Billet

- À droite, cliquer sur **Posts**
- En haut, cliquer sur **Add new**
- Dans la boîte qui apparaît, entrer le **Titre** et cliquer sur **Create**
- Ensuite, entrer chaque info nécessaire sur la gauche, et le **commentaire** sur la droite
- Enregistrer le brouillon : en haut, le bouton vert **Save draft**
- Publier : en haut, sous Draft, cliquer sur **OFF** puis le bouton vert **Save**
- Supprimer : en haut, à côté du bouton Save draft, bouton **"..."** puis **Delete**

# Sauts de ligne

Au sein du commentaire :

- Touche `Entrée` pour un nouveau paragraphe (grand saut de ligne)
- Touches `Shift` + `Entrée` (ou `Majuscule` + `Entrée`) pour un saut de ligne au sein d'un paragraphe

# Tags

Au lieu de l'ancien système qui séparait Document et Billet, Ensemble et Media, on a ici un champ unique où l'on sélectionne ce qui est souhaité.

Par exemple : _document_, _LOGRES_ et _photographie_.

Les tags _edition_ et _extension_ permettent de publier dans les pages idoines.

# Fichiers

- Cliquer sur la croix
- Au sein de la liste des medias, cliquer sur la croix pour sélectionner le fichier à insérer.

Le nom d'un fichier doit contenir uniquement **des lettres** de _a_ à _z_ en minuscule, **des chiffres** et **des tirets** - ou \_. Tout autre caractère risque de compliquer inutilement les choses.

Exemples :

- OK : "un_document_de_novembre-1976.pdf"
- Pas OK : "Un document de Février & Mars 2005.pdf"

# Modifier une Page

Les **Pages** sont utilisées pour les avant-propos, la présentation générale, la bibliographie, les extensions, les liens...

- À droite, cliquer sur **Pages**
- À gauche, cliquer sur **Pages** (...)
- Cliquer sur la Page souhaitée
- ...

# Consulter le site

Après publication, compter un peu moins de 5 minutes pour que le site se mette à jour.

Si le site ne se met pas à jour : `cmd` + `e` puis `cmd` + `r`.