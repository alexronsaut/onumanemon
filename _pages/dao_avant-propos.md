---
title: "DAO : Avant-propos"
date: 2007-12-06 12:02
datepublication: 6 Décembre 2007
tags:
---

Le Collectif DAO est né en 1982 en souvenir d’Orwell et pour saluer le Siècle d’OR à venir en 1984.  
 Ses premières interventions ont eu lieu à Rouen (à propos de l’Aube de l’Industrie), à Rennes, à Caen, à Nantes, parfois en lien avec la cellule Sabaki d’Arts Martiaux mise en place à peu près dans le même temps.  
 L’anonymat et les travaux collectifs faisaient partie du projet s’inscrivant entre autres dans un Volume Ouvert des Actions Transitives, des Destroy Positive Works, etc.  
  
 DAO a publié entre autres des textes de Jean Schatz, alors président de l’école Européenne d’Acupuncture, de Maître Ho, de J.-C. Christo, de Ritam la petite dame Indienne, etc. Ceci en cent exemplaires, dits des “Sangs Gitans”. Ainsi que huit bulletins publiés en plus grand nombre.  
 Désormais DAO accueille ici des invités qui n’ont rien à voir avec la Cosmologie mais qui font un travail tout à fait singulier en dehors de toute institution et de la reconnaissance qui y est généralement attachée et "qui préfèrent la vérité à la beauté", dans le but de créer un petit musée et une bibliothèque imaginaires, ainsi que des _Idiolectes_, propos critiques de lecteurs .  
_Marc Bohor_