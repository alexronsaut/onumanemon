---
title: "OGR : Avant-propos"
date: 2007-12-06 12:36
datepublication: 6 Décembre 2007
tags:
---

**OGR. 1964-1966. À propos des Livres Poétiques de Nycéphore & Nicolaï.**  
J’appartenais totalement au son. Il y a des motifs, des lieux : la Devèze par exemple, l’un des deux ruisseaux avec le Peugue qui disparaissent sous Bordeaux. C’est donc très précisèment la peinture d’un endroit de la banlieue de la ville près de Saint-Augustin. Et en même temps la résonnance sonore fourmille d’holophrases ; _elle s’honore de ça_.  
Je voudrais aujourd’hui faire partager cette ferveur, cette fureur chatoyante des états mentaux, dont il est pourtant probable qu’il en reste peu dans le texte lui-même, tellement cela était proche du chaos (au double sens).  
_Anciens Sons_ qui répond chez Nicolaï au poème _Crise_ chez Nycéphore, évoque les émissions radiophoniques, est au contraire une évocation “intellectuelle” de la magie de la radio ; comme quoi deux poèmes écrit à peu près en même temps (main droite/main gauche) n’affrontent pas du tout le même univers.  
Les poèmes 9 et 10 de Nycéphore (_Ivresse_ et _Une Tasse_) avec également en face _O les épiciers bucoliques_ et _À la province, le fournil_ sont à la limite de cette “crise”. Ils sont en partie descriptifs et en partie inscriptifs. Il y a apparemment une anecdote, et des attroupements possibles ; bien sûr cela s’effondre par endroits, tombe dans la fournaise, mais on n’est pas totalement dans la forge, on n’a pas disparu dans le son. _O les épiciers bucoliques_, c’est du descriptif. Par contre “et rare avant les tiens des biens” c’est de la forge. “La nourrice est en vain dans l’état des torpeurs”, c’est du semi-descriptif, tandis que “tant que la neige fond contre le vieux clocher qu’on est allé y quérir quelque cartilage”, c’est de la forge, etc.