---
title: "Papillons"
date: 2011-06-05 10:22
datepublication: 5 Juin 2011
tags:
---

_En observant la longue ombre oblique courir sur les bords du lac, ce matin à la fraîche, propulsée par sa seule énergie, indépendante et indifférente à ma personne, en avance, décalée, autonome, c’est encore l’évidence de ce sujet anonyme, indifférent, étranger surtout. On a photographié toute une série d’ombres très allongées ainsi, autour du sac de frappe. Pour le coup, le narcissime d’un ombre, ce serait vraiment lamentable.  
Le blog ce sont des petits papillons lumineux s’échappant de la penderie : pas de nécessité d’y inscrire chaque jour son carnet anecdotique et encore moins d’y déverser ses humeurs. À peine une fois on inscrit un matin merveilleux dans sa banalité dicible, mais on préfère (fasciné qu’on a toujours été par des lignes transversales dans le Temps et les longs couloirs des vers à travers la bibliothèque, par les retours du même le long des années différentes, comme on a écrit pendant des années une série de poèmes à des moments précis des saisons), retrouver dans les paperasses des amis ou des auteurs qui le sont aussi, dans leur courrier, les témoignages de_ tel jour qui se trouve être le jour présent_, une façon de les interpeller : “Que fais-tu donc là-bas aujourd’hui, camarade ?”  
20 Mai 2011  
  
On peut se rendre compte que ce blog n’a pas servi à grand’Chose, preuve supplémentaire de_ l’inactualité _du propos, mais les amis du site qui désormais ont en charge toutes les archives de la Cosmologie feront probablement en sorte d’en profiter pour laisser également la place à ceux qui ont accompagné ce travail un temps.  
22 Octobre 2011  
O. N._