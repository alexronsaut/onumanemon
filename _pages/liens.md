---
layout: default
title: Liens
---

# Liens

- À propos de 4 Taxis, partenaire de DAO et de Quartiers de ON !
  - [Revue May](http://www.mayrevue.com/questions-sur-4-taxis/)
  - [4 Taxis le magazine de la cambrousse internationale](http://www.florenceloewy.com/archives/expo/2010/4%20Taxis/4_Taxis.htm)
- [À propos de Publie.Net](https://www.publie.net/2019/06/16/carnet-de-bord-2019-semaine-24/)
- [À propos du Flat Iron](http://myarchitechtonic.wordpress.com/?s=Onuma+Nemon). [Télécharger le document](/files/ogr-a-propos-du-flat-iron.pdf) Toutefois il faut préciser que sur la trajet du Peugue à Bordeaux, en remontant vers Saint-Augustin et l’École Combes, il y a un immeuble de pierre et de brique tout à fait singulier qui n’est là que pour faire se réunir en pointe les deux rues parallèles Antoine Dupuch et Berruer, réplique minimale et parodique du Flat Iron, mais sur trois étages ! Et que c’est grâce à deux allusions américaines : celle du Flat Iron miniature et celle de la Maison Carrée, construite sur le modèle de la Maison Blanche par Peixotto, le banquier de la Révolution, et située un peu plus haut à Arlac (devenue Le Phœnyx), que s’est déployée dans la Cosmologie Onuma Nemon toute la carte des États-Unis à partir d’une diagonale Arlac-Saint-Augustin. NDLR.
- [Démonstration de l’Ami Bernard](http://aikido-toulouse.com/page-31.html)
- [Musée de l'OHM & Autres](http://www.youtube.com/watch?v=90g5LDxF9lw&feature=share&list=UUG9zVGWz0ywSBDjeB3RL_Rw)
- [Joris Dijkmeijer](http://www.jorisdijkmeijer.fr/)
- [Pascale Hémery](http://www.pascale-hemery.com) : Potentiel Hydrogène
- [Didier Morin](http://didiermorin.com), véritable roman d’Aventures
- [Voyance et Sen-no-sen de Bernard Plossu](http://www.madeinphoto.fr/)
- [Bernard Plossu](http://documentsdartistes.org/artistes/plossu/page1.html), Horrible Travailleur
- [Lamarche-Vadel toujours vivant ?](http://www.imec-archives.com/fonds_archives_fiche.php?i=BLV), voir aussi [cet entretien](/files/Lamarche-Vadel.pdf)
- [Οδυσσέας Ελύτης](http://fr.wikipedia.org/wiki/Odyss%C3%A9as_El%C3%BDtis), Odysséas Elytis, aussi important que Pound.
- [Blog anartiste et sphère ouverte](http://www.gilles-arnaud-sphere.com/?page_id=2815)
- [Christophe Bouchet](http://memorybethaniendamm.blogspot.com/), l'artiste du Mur de Berlin de 1984 à 1989, et le spécialiste du salmis de corbeau. Voir aussi [L'histoire du mur de Berlin par Thierry Noir](http://www.galerie-noir.de/ArchivesFrench/wall.html) et [À l'ombre du Mur](http://www.memorial-caen.fr/10EVENT/ombre_tex5.htm)
- [Mini Gallery Marc Giloux](http://www.marcgiloux.com/)
- [Siona Brotman](http://brotmansiona.free.fr/Siona_Brotman/Bienvenue_sur_le_site_de_Siona_Brotman.html), membre de la Bande des 89.
- [Les lendemains de Frank Denon](http://www.frankdenon.com/), digne descendant de l’écrivain et graveur Napoléonien
- [Michel Jouhanneau](http://www.saint-emilion-museepoterie.fr/sources/net2jouhan.wmv), céramiste.
- [Alain Montesse](http://alain.montesse.voila.net/index.html), le polygraphe
- Le site du très précieux climat et très rare [Roussiez](http://pagesperso-orange.fr/roussiez/index.htm)
- [Vidéo de Frank Denon](http://www.francefineart.com/rubrique_carteblanche/pages01/03pound_seq-1.html). La maison du Sourd
- [Tristan Tzarathustra](http://tristantzarathustra.blogspot.com/2009/04/pays-voir-blanc.html)
- Fissurations anonymes de [Céline Martinet](http://www.flickr.com/photos/martinetceline/)
- [ETC-ISTE](http://etc-iste.blogspot.com/2009/04/faire-pisser-le-chien.html)
- Publications Électroniques avec [François Bon](http://librairie.immateriel.fr/fr/livre/9782814500587/pr-ose-chant-1)
- [François Bon : Le Tiers Livre](http://www.tierslivre.net/spip/spip.php?article1070)
- [La Main de Singe](http://lamaindesinge.blogspot.com/)
- [URDLA](http://www.urdla.com/) : Centre International de l’Estampe et du Livre
- [Random Voodoo](http://www.randomrainfalls.com/archive.html)
- [Du Beau Monde !](http://www.lekti-ecriture.com/blogs/dubeaumonde/index.php?Onuma-nemon)
- [Revue Mettray](http://www.mettray.com)
