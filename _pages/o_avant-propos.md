---
title: "O : Avant-propos"
date: 2025-02-02 16:42
datepublication: 2 Février 2025
tags:
---

O est du côté de la soustraction et de la disparition, du trou de réel.  
O procède à une abrasion forcenée sans effets ni reliefs, éloge de la faiblesse destiné
à ces Tribus de pauvres d’où l’ON vient, vers un lointain énigmatique de musique,
de pure contemplation et d’amour fou. Ce sont des choses qui ne passent ni par
l’œil, ni par l’oreille, _de simples choses qui sont_. C’est une animalité politique.  
Rien de spectaculaire, la couleur ayant disparu avec la surface presque néantisée ; le
bois du train flottant précédent est poncé jusqu’à “la perce”, ainsi que son sens. On
y retrouve par à-coups des intuitions primordiales, pré-linguistiques, qui sont le
point de départ et le moteur ultime de cette œuvre.

O, c’est le Grenier magique hanté de son récepteur radiophonique à galènes et à
ondes courtes des voix de l’Aimée et de l’Autre Monde.  
O, le Cerveau et sa fabuleuse chimie.  
O procède, non pas d’un “retour”, mais d’une abrasion forcené des procédures,
selon une intuition implicite difficile à définir, flottements électriques à la surface
des sens.  
O c’est la clôture d’un conte : l’Auteur créé à partir de l’Œuvre, surgi comme Ogre
dévorateur, devient son propre Ôteur et disparaît en elle (dans sa propre gueule)
comme le fauve saute à travers le cercle de papier du cirque, ou le rat que l’enfant
cherche dans les reliefs du sou de bronze troué et dont on finit par lui dire qu’il a
disparu par le trou de la pièce, Valjean fondu dans la pièce de monnaie de l’Enfant,
avec laquelle il joue !  
Ni héros ni anti-héros : Zéro. Lieu vide et bulle suspensive. O nu, cerceau de feu
par où s’engouffrèrent Voix et Visions, voilà ce qu’ON est.
