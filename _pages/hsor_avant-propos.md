---
title: "HSOR : Avant-propos"
date: 2007-12-06 12:37
datepublication: 6 Décembre 2007
tags:
---

Dans l’alchimie complexe de la Cosmologie, HSOR est un continent différent de celui de la trilogie OGR-OR-O. Ce n’est pas la différence entre l’or et le plomb, mais avec le zinc : le zinc des toits, celui du comptoir. HSOR au début se nommait ainsi à cause de son intrication avec l’HiStOiRe du temps et ses anecdotes, mais s’est modifié au fur à mesure pour englober également une sorte de journal et des _récifs de voyage_ : ce qui affleure ou qu'on heurte au fil de la randonnée ; c’est le lieu des brouillons, des essais, des esquisses, de différentes tentatives.  
En conséquence seulement une petite partie en est publiable. Journal et correspondances, anecdotiques, seront détruits.  
On trouvera ici les rares entretiens de l’auteur, quelques textes théoriques, les différentes
_Cartes_ dressées du territoire de la Cosmologie au fur et à mesure de ses transformations ainsi
que quelques inédits, et des “chutes” écartées de la trilogie _OGR-OR-O_, et de la version définitive des _États du Monde_.

Les textes de ce Continent qui permettent le mieux d’appréhender le projet dans son ensemble, sont : _L’Inscription_, l’entretien _La Pièce est enfumée_, en référence à Lermontov, ainsi que, pour la partie technique, _L’Appendice et Cartes_.
