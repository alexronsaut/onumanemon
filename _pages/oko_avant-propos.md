---
title: "OKO : Avant-propos"
date: 2024-10-26 14:31
datepublication: 10 Octobre 2024
tags:
---

**OKO**, c’est la Matière Noire qui affecte l’univers. Ici figurent les œuvres détruites.
