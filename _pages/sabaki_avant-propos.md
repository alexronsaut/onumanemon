---
title: "Sabaki : Avant-propos"
date: 2007-12-06 12:26
datepublication: 6 Décembre 2007
tags:
---

La Cellule Sabaki d’Arts Martiaux organisée essentiellement autour de Yukio Narita, Tenshin Nakahara, Maître Okada et Walter Hertz et œuvrant essentiellement dans les disciplines du Iai-Do, du Kendo, du Karaté, et de l’Acupuncture a organisé des séminaires de travail, des stages et des démonstrations de Katas, de coupe et de combats à arme vive d’abord au Château du Mas d’Auvignon dans le Gers puis dans différents endroits des Alpes et des Cévennes, à Rouen et en Haute-Savoie. Elle a su également rendre hommage à Maître Nambu et au Sankukaï  
Elle est destinée en partie à des hauts gradés et des membres de la Cellule, mais pas exclusivement.  
Elle a publié plus d’une centaine de bulletins à usage interne, à raison de deux livraisons chaque année depuis 1980, coordonnant différents niveaux de recherche sur les correspondances entre les points d’atémi et les points d’acupuncture, entre bunkais, katas et idéogrammes, jusqu’à l’établissement de cartes et de traités.  
Mais elle a travaillé également sur des domaines divers comme la représentation des Arts Martiaux au cinéma par le groupe Wu-Tang Clan, l’héritage de Mishima, des exemples et photos de coupe des Mille bambous dans la Montagne, des Souvenirs de “kata ta pathè” selon les ages, ou les emplacements précis des 5647 morts de Kobe du 17 janvier 1995 dessinés par le satellite.  
  
Nous communiquerons ici quelques-uns des documents qui peuvent présenter un intérêt pour un large public.