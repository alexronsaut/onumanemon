---
layout: default
title: Avant-propos
---

La _Cosmologie Onuma Nemon_ s’est mise en place très tôt de façon inconsciente
dans l’enfance, par un travail _à deux mains_ de deux frères dont l’un disparu. Le vivant écrivait à la
place du mort autant que pour lui-même. Main droite, frère mort. Œuvre plus ou moins
délirante enfouie de 1964 à 1984 à la limite du _fétiche_ et connue seulement d’une dizaine de
personnes, comprenant l’amorce d’une première constitution dite des _Cinq Continents_ liée au
début du travail sonore (en particulier radiophonique), cinématographique et plastique.
La Cosmologie a été mise à jour seulement en 1984, circulant d’abord auprès de rares amis sous la
forme d’un énorme _Tas de Feuilles_ logé dans un de ces cartons qui contiennent les liasses de
papier-machine.

Aujourd’hui, ces cinq continents sont devenus six, résumés en une _formule_ : _LOGRES-OGR-OR-
O-HSOR-OKO_.

_LOGRES_ (pays des Ogres chanté par Calogrenant), est le continent de l’adolescence, cannibalisme
et apprentissage technique.

Après la dévoration de trois lettres, _OGR_ s’est vraiment constitué en travaillant des formes
classiques et distinctes de récits, poèmes, nouvelles, dessins, etc. où _la division en deux_ fut bientôt
remplacée par un _tournoiement à trois_, le frère vivant se redivisant en deux (Nycéphore &
Nicolaï) aux côtés du disparu (Didier). C’est le Continent le plus volumineux de l’ensemble. Le
Volume OGR paru chez Tristram en 1999 en est un tout petit extrait ; la majeure partie figure
désormais sur le site en attendant que l’ensemble y soit disponible.

_OR_ a surgi en 1984, division du monde en _Cinq Saisons_ à la Chinoise (annonce du siècle d’or et
renversement des chiffres de la naissance). Apparaissent alors des _essaims à plusieurs Voix_ ; il ne
s’agit plus de récits complets mais d’amorces et de germes qui cessent avant même de “prendre”,
dans un souci d’emporter ensemble romanesque, épique et poétique. Chacune de ces Voix
démultipliant à chaque fois les registres utilisés, notamment le domaine graphique. Les œuvres
plastiques viennent alors former des _Étoilements idéogrammatiques_ dans le texte et s’articulent avec
des _Extensions en volume hors du livre_ (arts martiaux, photographie, machines magiques, cinéma,
son...).  
L’exposition du QUARTIER à Quimper a repris cette disposition.

_O_, dont la masse est la moins importante, vise à un fonctionnement neuronal dans l’écriture et à
la disparition de l’auteur en même temps que de tous les effets, images et autres, écriture abrasive
ou néante. _O_, a signé en 2000 la clôture de la Cosmologie, et depuis on n’a fait que la _dégraisser_.

Proliférant en même temps, on a _HSOR_ et _OKO_.  
_HSOR_ accueille un tressage d’histoires singulières avec l’HiStOiRe du temps : journal, récifs de
voyage, ainsi que les _Cartes_ des territoires, la théorie, etc. Dans son avant-propos on trouvera
l’indication des textes qui permettent le mieux d’appréhender l’ensemble de l’œuvre.
Le seul continent résiduel est _OKO_, qui ne contient que la matière noire des œuvres détruites.

L’étape ultime de mise en place, qui a pour nom _États du Monde_, et dont _ON !_ (renommé
_Quartiers de ON !_), n’est que la première partie, immense train de bois flottants, opère une
traversée de tous les continents de la Cosmologie.

\*

La _Cosmologie O.N._ peut être abordée aussi bien par _Quartiers_ ou par _Saisons_ que par _Lignes_
ou par _Chants_. Ces quatre dispositions ne s’excluent pas et la Cosmologie ne s’y réduit pas non
plus. Il n’y a pas de lecture totalisante de la _Cosmologie O.N._ C’est une technique _d’agrégats_
toujours en expansion, en transformation, et à la limite de la désarticulation.

— _Quartiers de ON !_ paru chez Verticales est construit par _Chants_.
— L’exposition au QUARTIER de Quimper de la partie _OR_ était présentée en _Saisons_.
— Ce qui en est paru dans quelques revues était réparti la plupart du temps en _Quartiers_.
— _États du Monde_ paru à Mettray, est agencé par _Figures_ et _Saisons_.

\*

Pour ce qui est de l’édition et de la lecture, pas de rencontres transcendantes, rien que des hasards
contingents. La fondation de Tristram en 86 était un moment historique heureux, puis il y a eu la
rencontre du colosse Wallet et de ses associés.  
Et la très ancienne amitié avec Didier Morin a fait qu’il s’est engagé (plus que tout autre !) pour la
défense à la fois plastique et scripturale de cette singularité. En ajoutant à cela le site sur lequel
vous êtes, entièrement construit et agencé bénévolement par Alexandre Ronsaut pour Mettray.
Le site est sans doute (comme un cd-rom) conceptuellement le lieu idéal du déploiement de cette
œuvre : on y circule comme dans un cerveau. Toutefois, qu’on ne s’y trompe pas : depuis 2004 de
sa fondation, il n’y a jamais eu le moindre signe du moindre lecteur. _La Cosmologie ne sert à rien_,
comme le loup ou l’odeur des feuilles mortes brûlées en automne.

On ne saurait toutefois oublier les soutiens précieux, au premier chef celui des plus proches de la
Tribu qui ont permis de façon essentielle que cela ait lieu, en _subissant_ les impératifs
dévorateurs de cet Ogre chronophage pour le temps qui aurait dû leur être consacré.
Ensuite quelques lecteurs exceptionnels des ouvrages eux-mêmes, comme Joël Roussiez ou
Typhaine Garnier, capables de soutenir ce rêve, d’en suivre tous les méandres et d’en faire
ressortir tous les plans d’incidence de la lumière.  
Il faudrait, avec le nom de tous ceux-là et de quelques autres, composer une _formule magique_,
parallèle à celle de la Cosmologie. En effet, la Cosmologie née dans une _impasse_ d’une _malédiction_
proférée, a tournoyé en vain pour s’en défaire. La malédiction, comme chez le grand Will, a été
défaite, retournée (merci aux bonnes fées !), mais _l’impasse est toujours là_, et la version non publiée
des _États Définitifs du Monde_, correspond parfaitement à l’état apocalyptique du monde actuel.

Aujourd’hui la fin rejoint le commencement : en effet, le _Tas de Feuilles_ sous une forme plus
élégante de coffret (ou de cercueil d’enfant), contenant pour l’instant plus de 4000 pages
imprimées et disposées dans les boîtes des différents Continents, ainsi que toute une série de
travaux plastiques qui en sont les _Étoilements_, existe à la Bibliothèque Kandinsky de Beaubourg,
grâce à l’intervention de Mica Gherghescu et le soutien de Diane Toubert, donation qui reste
ouverte pour permettre l’ajout de parties qui n’ont pu encore être mises en pages, et qui réalise le
vœu initial d’offrir l’œuvre à ceux qui peuvent la rendre meilleure. Cela aurait été absolument
impossible sans l’engagement _colossal_ de Typhaine Garnier pour permettre cette mise en forme.
L’ensemble des gravures de 1970-1971 ainsi que quelques autres et dessins préparatoires, sont
consultables dans le _Cabinet des Estampes_ de la B. N. Richelieu.

Les inédits que nous n’aurons plus le temps de mettre en forme seront détruits en même temps
que toute la documentation générale, les documents originaux déjà exposés et l’ensemble du
journal et des correspondances ; cela fait partie de l’éthique du projet. L’important est que ç’ait
été _inscrit_. Un temps il y a eu cette utopie d’offrir tous les inédits à quelqu’un qui les transforme
et les utilise sous son propre nom : c’était une belle façon _de devenir enfin quelqu’un d’autre_, mais
ça n’a pas eu lieu. Le plus drôle c’est que si ça avait lieu malgré tout, personne n’en saurait rien,
pas même Onuma Nemon !  
Le seul regret c’est de n’avoir pu offrir une lecture intégrale sonore ou mieux encore
_radiophonique_, de la Cosmologie, pour rejoindre les enchantements du début. Cela, aucun autre
ne peut le faire.

> «&nbsp;On admire Charles Quint, cette sorte d’Arlequin vêtu des états du monde,
> moins pour Bruges et tout ce qu’il a conquis, que pour le _desengañado_, cette
> extraordinaire jouissance d’abdiquer alors qu’il était à la tête du plus grand
> royaume d’Occident, passant le reste de sa vie à se frotter une émeraude sur
> les tempes pour chasser l’auréole d’or de la migraine, ou le long des après-midi
> à contempler sa collection d’œuvres d’art.&nbsp;»
>
> _Prosper. États Définitifs. Chez Rodolphe._

[Parcourir l'inventaire](/inventaire)
